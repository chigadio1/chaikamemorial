using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public enum Status_Canvas_Anim_ID
{
    START = 81001,
    END
}

public class StatusCanvas : BaseGameCanvas
{
    /// <summary>
    /// スマホのマスク
    /// </summary>
    RectMask2D rect_mask_2d;

    /// <summary>
    /// チャットデータ
    /// </summary>
    List<ChatData> chat_data_array = new List<ChatData>();

    /// <summary>
    /// ステータステキスト
    /// </summary>
    StatusUIData[] status_text_array = new StatusUIData[(int)Status_ID.MAX];

    /// <summary>
    /// アニメーター
    /// </summary>
    Animator animator;

    bool change_flag = false;
    bool inversion_flag = false;

    
    public override void BeginInit()
    {
        change_flag = false;
        inversion_flag = false;
        rect_mask_2d = gameObject.GetComponentInChildren<RectMask2D>();
        animator = gameObject.gameObject.GetComponent<Animator>();
        var child_text = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        foreach(var child in child_text)
        {
            if(child.gameObject.name == "StatusNumText01")
            {
                status_text_array[0] = new StatusUIData();
                status_text_array[0].BeginInit(child, Status_ID.STUDY);
            }
            else if (child.gameObject.name == "StatusNumText02")
            {
                status_text_array[1] = new StatusUIData();
                status_text_array[1].BeginInit(child, Status_ID.EXERCISE);
            }
            else if (child.gameObject.name == "StatusNumText03")
            {
                status_text_array[2] = new StatusUIData();
                status_text_array[2].BeginInit(child, Status_ID.CHARM);
            }
        }



    }

    public  void Update()
    {
        UpdateExec();
    }

    public void UpdateExec()
    {

        foreach(var data in status_text_array)
        {
            data.Update();
        }

        if (IsPlayAnim() == false) return;
;
    }

    public void AddChatData()
    {


        var data  = new ChatData();
        data.BeginInstance(gameUiCore.Instance?.GetGameObjectUI(gameUiCore.Default_UI_Object_ID.TalkFrame),rect_mask_2d.gameObject);
        chat_data_array.Add(data);
        AddPositionChat(150.0f);
    }

    public void AddResultData()
    {

        int select_add_index = chat_data_array.Count - 1;
        chat_data_array[select_add_index].BeginResultInstance(gameUiCore.Instance?.GetGameObjectUI(gameUiCore.Default_UI_Object_ID.ResultIcon),rect_mask_2d.gameObject);
        AddPositionResult(200.0f);

    }

    public void AddPositionChat(float add_position)
    {
        foreach(var chat in chat_data_array.AsEnumerable().Reverse())
        {
            chat.AddTalkPositionY(add_position);
        }
    }

    public void AddPositionResult(float add_position)
    {
        foreach (var chat in chat_data_array.AsEnumerable().Reverse())
        {
            chat.AddResultPositionY(add_position);
        }
    }

    public void Release()
    {
        foreach(var data in chat_data_array)
        {
            data.Release();
        }
        chat_data_array = new List<ChatData>();
        foreach(var data in status_text_array)
        {
            data.Release();
        }
        status_text_array = new StatusUIData[(int)Status_ID.MAX];
    }

    public void SetTextTalk(string value_text)
    {
        if (chat_data_array.Count <= 0) return;

        int index = chat_data_array.Count - 1;

        chat_data_array[index].SetTextTalk(value_text);
    }

    public void SetResultSptite(Sprite sprite)
    {
        if (chat_data_array.Count <= 0) return;

        int index = chat_data_array.Count - 1;

        chat_data_array[index].SetResultSptite(sprite);
    }

    public void SetStatusNum(int index, int value_num)
    {
        status_text_array[index].SetStatusNum(value_num);
    }

    public void SetStatusNumInit(int index,int value_num)
    {
        status_text_array[index].SetStatusNumInit(value_num);
    }

    public bool GetIsNumFix(Status_ID index)
    {
        return status_text_array[(int)index].IsOldFix();
    }




    public void PlayAnim(Status_Canvas_Anim_ID id, bool inversion = false)
    {
        int id_num = (int)id;
        if (id_num < (int)Status_Canvas_Anim_ID.START || id_num >= (int)Status_Canvas_Anim_ID.END) return;
        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", -1.0f);
        else animator.SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            animator.SetFloat("Speed", 1.0f);
        }
        animator.Play($"{id_num:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {
        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag)
        {
            if(gameObject.activeInHierarchy == false)
            {
                return true;
            }
            return stateInfo.normalizedTime >= 1.0f;
        }
        else return stateInfo.normalizedTime <= 0.0f;
    }
}

public class ChatData
{
    public Vector2 position = new Vector2(90,-350);
    public RectTransform talk_rect_transform;
    public GameObject talk_obj;
    public TextMeshProUGUI name_text;
    public TextMeshProUGUI talk_text;
    public Image result_stamp_sprite;

    public void SetTextTalk(string value_text)
    {
        if (talk_text == null) return;
        talk_text.text = value_text;
    }

    public void SetResultSptite(Sprite sprite)
    {
        if(result_stamp_sprite == null) return;
        result_stamp_sprite.sprite = sprite;
    }

    public void AddTalkPositionY(float add_y)
    {
        position.y += add_y;
        talk_rect_transform.anchoredPosition = position;
        if(result_stamp_sprite != null)result_stamp_sprite.rectTransform.anchoredPosition = new Vector2(195.0f, result_stamp_sprite.rectTransform.anchoredPosition.y + add_y);
    }
    public void AddResultPositionY(float add_y)
    {
        position.y += add_y;
        talk_rect_transform.anchoredPosition = position;
        result_stamp_sprite.rectTransform.anchoredPosition = new Vector2(195.0f, result_stamp_sprite.rectTransform.anchoredPosition.y + add_y);
    }

    public void BeginInstance(GameObject text_obj_instance,GameObject parent)
    {
        if (text_obj_instance == null) return;
        talk_obj = GameObject.Instantiate(text_obj_instance);


        talk_rect_transform = talk_obj.GetComponent<RectTransform>();

        talk_rect_transform.SetParent(parent.transform);
        talk_rect_transform.anchoredPosition = position;
        talk_rect_transform.localScale = Vector3.one;
        var childs = talk_obj.GetComponentsInChildren<TextMeshProUGUI>();
        foreach(var child in childs)
        {
            if(child.gameObject.name == "NameText")
            {
                name_text = child;
            }
            else if (child.gameObject.name == "Text")
            {
                talk_text = child;
            }
        }
    }

    public void BeginResultInstance(GameObject result_obj_instance, GameObject parent)
    {
        if(result_obj_instance == null) return;

        var result_obj = GameObject.Instantiate(result_obj_instance);


        result_stamp_sprite = result_obj.GetComponent<Image>();

        result_stamp_sprite.rectTransform.SetParent(parent.transform);
        result_stamp_sprite.rectTransform.anchoredPosition = new Vector2(195.0f, position.y - 130);
        result_stamp_sprite.rectTransform.localScale = Vector3.one;
    }

    public void Release()
    {
        if(talk_obj != null)
        {
            GameObject.Destroy(talk_obj.gameObject);
        }
        if(result_stamp_sprite != null)
        {
            GameObject.Destroy(result_stamp_sprite.gameObject);
        }
        talk_obj = null;
        result_stamp_sprite = null;
        talk_rect_transform = null;
        name_text = null;
        talk_text = null;
    }
}

public class StatusUIData
{
    public int old_num = 0;
    public int new_num = 0;
    public TextMeshProUGUI num_text;

    public void BeginInit(TextMeshProUGUI text_mesh,Status_ID status_id)
    {
        num_text = text_mesh;
        old_num = new_num = PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetStatus(status_id);
    }

    public void Update()
    {
        if (num_text == null) return;
        if (old_num != new_num)
        {
            if (new_num > old_num)
            {
                old_num += 1;
            }
            else old_num -= 1;
            old_num = Mathf.Clamp(old_num, 0, 99999);

        }

        num_text.text = $"{old_num:00000}";
    }

    public bool IsOldFix()
    {
        old_num = Mathf.Clamp(old_num, 0, new_num);
        return old_num == new_num;
    }

    public void SetStatusNum(int value_num)
    {
        old_num = new_num;
        new_num = value_num;
    }

    public void SetStatusNumInit(int value_num)
    {
        old_num = new_num = value_num;
    }

    public void Release()
    {
        if(num_text != null)num_text.text = "00000";
        num_text = null;
        old_num = new_num = 0;
    }
}

