using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎ゲームキャンバス
/// </summary>
public class BaseGameCanvas : MonoBehaviour
{
    public virtual void BeginInit()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
