using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleBsckGroundCanvas : BaseGameCanvas
{
    /// <summary>
    /// �w�i�摜
    /// </summary>
    private Image back_back_groun_sprite;

    /// <summary>
    /// �w�i�}�e���A��
    /// </summary>
    private Material back_ground_material;



    public void Start()
    {

    }

    public void Update()
    {

    }

    public override void BeginInit()
    {
        back_back_groun_sprite = GetComponentInChildren<Image>();
        if (back_back_groun_sprite == null) return;
        back_ground_material = back_back_groun_sprite.material;
    }

    public void SetChange(float add_value)
    {
        if (back_ground_material == null) return;
        back_ground_material.SetFloat("_alpha", add_value);
    }

    public void AddChange(float add_value)
    {
        if (back_ground_material == null) return;
        float local =  back_ground_material.GetFloat("_alpha");
        back_ground_material.SetFloat("_alpha", local + add_value);
    }

    public bool IsEndChange()
    {
        if (back_ground_material == null) return true;
        float local = back_ground_material.GetFloat("_alpha");

        return local <= 0.0f || local >= 0.5f;

    }

    public void OnDestroy()
    {
        SetChange(0.0f);
    }
}
