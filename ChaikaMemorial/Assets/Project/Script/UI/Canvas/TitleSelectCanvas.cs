using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleSelectCanvas : BaseGameCanvas
{
    /// <summary>
    /// �I���̘g�摜
    /// </summary>
    private Image select_sprite;

    public override void BeginInit()
    {
        select_sprite = GetComponentInChildren<Image>();
    }

    public void SetWidthScale(float value)
    {
        if (select_sprite == null) return;
        var scale = select_sprite.rectTransform.localScale;
        scale.x = value;
        select_sprite.rectTransform.localScale = scale;
    }

    public void SetPositionY(float value)
    {
        if (select_sprite == null) return;
        var position = select_sprite.rectTransform.anchoredPosition;
        position.y = value;
        select_sprite.rectTransform.anchoredPosition = position;
    }
}
