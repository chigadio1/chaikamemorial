using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public enum Play_Select_Anim_ID
{
    NONE = -1,
    ID_8501 = 8501,
    ID_8502,
    MAX
}

public class PlayerSelectCanvas : BaseGameCanvas
{
    Animator remaind_animator;

    /// <summary>
    /// �e�L�X�g
    /// </summary>
    TextMeshProUGUI remaind_text = null;
    public override void BeginInit()
    {
        remaind_animator =  gameObject.GetComponentInChildren<Animator>();
        remaind_text = remaind_animator.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        remaind_text.text = "";
    }

    public void SetRemaindText(string name)
    {
        if (remaind_text == null) return;
        remaind_text.text = name;
    }

    public void Update()
    {
        if(gameObject.activeInHierarchy == false)
        {
            remaind_animator.StopPlayback();
        }
        else
        {
            if(IsPlayEndAnim())
            {
                if(remaind_animator.GetCurrentAnimatorStateInfo(0).IsName("8501"))
                {
                    PlayAnim(Play_Select_Anim_ID.ID_8502);
                }
            }
        }
    }

    public void PlayAnim(Play_Select_Anim_ID id)
    {
        if (id <= Play_Select_Anim_ID.NONE || id >= Play_Select_Anim_ID.MAX) return;
        if (remaind_animator == null) return;

        int id_num = (int)id;
        remaind_animator.Play($"{id_num:0000}");
    }

    public bool IsPlayEndAnim()
    {
        if (remaind_animator == null) return true;

        AnimatorStateInfo stateInfo = remaind_animator.GetCurrentAnimatorStateInfo(0);

        return stateInfo.normalizedTime >= 1.0f;
    }
}
