using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Title_Select_Type_ID
{
    NONE = -1,
    YES,
    NO,
    MAX
}

public enum Title_Select_Message_ID
{
    NONE = -1,
    NOTE,
    ERROR,
    MAX
}

public class TitleSelectSubCanvas : BaseGameCanvas
{
    /// <summary>
    /// YES,NOの画像配列
    /// </summary>
    private Image[] select_type_sprite_array = new Image[(int)Title_Select_Type_ID.MAX];

    private Image[] select_message_sprite_array = new Image[(int)Title_Select_Message_ID.MAX];

    private TextMeshProUGUI[] select_message_text = new TextMeshProUGUI[(int)Title_Select_Message_ID.MAX];

    private readonly string[] select_message = new string[(int)Title_Select_Message_ID.MAX]
    {
        "セーブデータを初期化します\\nよろしいですか？",
        "セーヴデータが存在しません"
    };

    public override void BeginInit()
    {
        var childs = GetComponentsInChildren<Image>();

        foreach(var child in childs)
        {
            if(child.gameObject.name == "YesImage")
            {
                select_type_sprite_array[0] = child;
            }
            else if (child.gameObject.name == "NoImage")
            {
                select_type_sprite_array[1] = child;
            }
            else if (child.gameObject.name == "NoteImage")
            {
                select_message_sprite_array[0] = child;
                select_message_text[0] = child.GetComponentInChildren<TextMeshProUGUI>();
            }
            else if (child.gameObject.name == "ErrorImage")
            {
                select_message_sprite_array[1] = child;
                select_message_text[1] = child.GetComponentInChildren<TextMeshProUGUI>();
            }
        }
        SetSelectTypeAlpha(Title_Select_Type_ID.YES, 0.0f);
        SetSelectTypeAlpha(Title_Select_Type_ID.NO, 0.0f);
        SetSelectTypeColor(Title_Select_Type_ID.YES, 0.5f);
        SetSelectTypeColor(Title_Select_Type_ID.NO, 0.5f);
        SetSelectMessageAlpha(Title_Select_Message_ID.NOTE, 0.0f);
        SetSelectMessageAlpha(Title_Select_Message_ID.ERROR, 0.0f);
        OffSelectMessage(Title_Select_Message_ID.NOTE);
        OffSelectMessage(Title_Select_Message_ID.ERROR);
    }

    public void SetSelectTypeAlpha(Title_Select_Type_ID id,float value)
    {
        if (id <= Title_Select_Type_ID.NONE || id >= Title_Select_Type_ID.MAX) return;
        if (select_type_sprite_array[(int)id] == null) return;
        var color = select_type_sprite_array[((int)id)].color;
        color.a = value;
        select_type_sprite_array[(int)id].color = color;
    }

    public void SetSelectMessageAlpha(Title_Select_Message_ID id, float value)
    {
        if (id <= Title_Select_Message_ID.NONE || id >= Title_Select_Message_ID.MAX) return;
        if (select_message_sprite_array[(int)id] == null) return;
        if (select_message_text[(int)id] == null) return;
        var color = select_message_sprite_array[((int)id)].color;
        color.a = value;
        select_message_sprite_array[(int)id].color = color;

        color = select_message_text[((int)id)].color;
        color.a = value;
        select_message_text[(int)id].color = color;
    }

    public void OnSelectMessage(Title_Select_Message_ID id)
    {
        if (id <= Title_Select_Message_ID.NONE || id >= Title_Select_Message_ID.MAX) return;
        if (select_message_text[(int)id] == null) return;

        select_message_text[(int)id].gameObject.SetActive(true);
    }

    public void OffSelectMessage(Title_Select_Message_ID id)
    {
        if (id <= Title_Select_Message_ID.NONE || id >= Title_Select_Message_ID.MAX) return;
        if (select_message_text[(int)id] == null) return;

        select_message_text[(int)id].gameObject.SetActive(false);
    }

    public void SetSelectTypeColor(Title_Select_Type_ID id, float value)
    {
        if (id <= Title_Select_Type_ID.NONE || id >= Title_Select_Type_ID.MAX) return;
        if (select_type_sprite_array[(int)id] == null) return;
        var color = select_type_sprite_array[((int)id)].color;
        color.r = color.b = color.g = value;
        select_type_sprite_array[(int)id].color = color;
    }

}
