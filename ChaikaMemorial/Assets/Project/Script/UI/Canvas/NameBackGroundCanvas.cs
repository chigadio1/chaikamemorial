using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.UI;

public class NameBackGroundCanvas : BaseGameCanvas
{
    private List<NameImageBackGround> image_list = new List<NameImageBackGround>();

    // Start is called before the first frame update
    void Start()
    {
        BeginInit();
    }

    // Update is called once per frame
    void Update()
    {
        foreach(var image in image_list)
        {
            image.Update();
        }
    }

    public override void BeginInit()
    {
        var childs = gameObject.GetComponentsInChildren<Image>();
        foreach (var child in childs)
        {
            if(child.gameObject.name == "BackGroundPanel")
            {
                continue;
            }

            NameImageBackGround add = new NameImageBackGround();
            add.BeginInit(child);
            image_list.Add(add);
        }
    }

    public void OnDestroy()
    {
        image_list.Clear();
    }
}


public class NameImageBackGround
{
    public RectTransform rect;
    public float move_end_time;
    public float calc_time;
    public Vector2 start_pos ;
    public Vector2 move_start_pos;
    public Vector2 end_pos;
    public float length;
    public Vector2 vec;

    public void BeginInit(Image image)
    {
        rect = image.rectTransform;
        start_pos = rect.anchoredPosition;

        move_end_time = 0.0f;
        calc_time = 0.0f;
        move_start_pos = end_pos = Vector2.zero;
        length = 0.0f;
        vec = Vector2.zero;
    }

    public void Update()
    {
        if (rect == null) return;
        if(calc_time >= move_end_time)
        {
            move_end_time = Random.Range(3.0f, 8.0f);
            calc_time = 0.0f;
            length = Random.Range(100.0f, 150.0f);
            vec.x = Random.Range(-100.0f, 100.0f);
            vec.y = Random.Range(-100.0f, 100.0f);
            vec = vec.normalized;
            end_pos = start_pos + vec * length;
            move_start_pos = rect.anchoredPosition;
        }

        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, move_end_time);
        float lerp = calc_time / move_end_time;
        
        rect.anchoredPosition = Vector2.Lerp(move_start_pos,end_pos,lerp);


    }
}