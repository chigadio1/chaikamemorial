using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Setting_System_ID
{
    NONE = -1,
    CharacterFovo,
    NickNameChoice,
    Choice,
    MAX
}

public class SettingCanvas : BaseGameCanvas
{

    readonly string[] system_addressable_name = new string[(int)Setting_System_ID.MAX]
    {
        "Assets/Project/Assets/Object/UI/CharacterFavo.prefab",
        "Assets/Project/Assets/Object/UI/NickNameChoice.prefab",
        "Assets/Project/Assets/Object/UI/ChoiceObj.prefab",
    };
    /// <summary>
    /// キャラネームテキスト
    /// </summary>
    TextMeshProUGUI name_text = null;

    /// <summary>
    /// テキスト
    /// </summary>
    TextMeshProUGUI talk_text = null;

    /// <summary>
    /// セレクトデータ
    /// </summary>
    ChoicesStorageData choices_storage_data = null;

    /// <summary>
    /// カットフェード
    /// </summary>
    Image cut_fade_image = null;

    /// <summary>
    /// スチル
    /// </summary>
    Image still_image = null;

    /// <summary>
    /// システム系オブジェクト
    /// </summary>
    GameObject systemObject = null;

    /// <summary>
    /// System系のアドレサブル
    /// </summary>
    AddressableData<GameObject> addressable_data = null;

    /// <summary>
    /// キャラクターデータ
    /// </summary>
    CharacterFavoSystemNovelObject character_favo;
    public CharacterFavoSystemNovelObject CharacterFavoSystemNovelObject { get { return character_favo; } }

    /// <summary>
    /// ニックネームチョイス
    /// </summary>
    NickNameChoiceObject nick_name_choice;
    public NickNameChoiceObject NickNameChoiceObject { get { return nick_name_choice;} }

    /// <summary>
    /// 選択肢
    /// </summary>
    ChoiceObject choice_object;
    public ChoiceObject ChoiceObject { get { return choice_object; } }

    public override void BeginInit()
    {
        choices_storage_data = new ChoicesStorageData();
        var chileds = gameObject.GetComponentsInChildren<RectTransform>();
        foreach(var ch in chileds)
        {
            if(ch.name == "CharaName")
            {
                name_text = ch.GetComponent<TextMeshProUGUI>();
            }
            else if(ch.name == "TalkText")
            {
                talk_text = ch.GetComponent<TextMeshProUGUI>();
            }
            else if (ch.name == "Choices")
            {
                choices_storage_data.Load(ch.gameObject);
            }
            else if(ch.name == "CutFade")
            {
                cut_fade_image=ch.GetComponent<Image>();
            }
            else if (ch.name == "Still")
            {
                still_image = ch.GetComponent<Image>();
            }
            else if(ch.name == "SystemObject")
            {
                systemObject = ch.gameObject;
            }
        }
        ShowHideSelect(0, false);
        ShowHideSelect(1, false);
        ShowHideSelect(2, false);
    }

    public void ResetAll()
    {
        if (name_text != null) name_text.text = "";
        if (talk_text != null) talk_text.text = "";
        if(cut_fade_image != null)
        {
            cut_fade_image.sprite = null;
            var color = cut_fade_image.color;
            color.a = 0.0f;
            cut_fade_image.color = color;
        }
        if (still_image != null)
        {
            still_image.sprite = null;
            var color = still_image.color;
            color.a = 0.0f;
            still_image.color = color;
        }
    }

    public void ShowTalkText(string value_text)
    {
        if (talk_text == null) return;
        
        talk_text.text = value_text;
    }

    public void ShowNameText(string value_text)
    {
        if (name_text == null) return;
        name_text.text = value_text;
    }

    public void ShowHideSelect(int index,bool value_flag)
    {
        if (index >= 3) return;
        if (index < 0) return;
        if (3 > choices_storage_data.choices_array.Length) return;

        choices_storage_data.choices_array[index].sprite.gameObject.SetActive(value_flag);
    }

    public void SetTextSelect(int index,string value)
    {
        if (index >= 3) return;
        if (index < 0) return;
        if (3 > choices_storage_data.choices_array.Length) return;

        choices_storage_data.choices_array[index].text_text.text = value;
    }

    public void SetSpriteColor(int index,Color color)
    {
        if (index >= 3) return;
        if (index < 0) return;
        if (3 > choices_storage_data.choices_array.Length) return;

        choices_storage_data.choices_array[index].sprite.color = color;
    }

    public void SetCutFadeImage(Sprite value_image)
    {
        if (value_image == null) return;
        if(cut_fade_image == null) return;
        cut_fade_image.sprite = value_image;
    }

    public void SetCutFadeAlpha(float alpha)
    {
        if (cut_fade_image == null) return;
        alpha = Mathf.Clamp(alpha,0.0f,1.0f);

        Color color = cut_fade_image.color;
        color.a = alpha;
        cut_fade_image.color = color;
    }
    public void SetStillImage(Sprite value_image)
    {
        if (value_image == null) return;
        if (still_image == null) return;
        still_image.sprite = value_image;
    }

    public void SetStillPosition(Vector2 position)
    {
        if (still_image == null) return;
        still_image.rectTransform.anchoredPosition = position;
    }
    public void SetStillScale(Vector2 scale)
    {
        if (still_image == null) return;
        still_image.rectTransform.localScale = scale;
    }
    public void SetStillAngle(float angle)
    {
        if (still_image == null) return;
        still_image.rectTransform.localEulerAngles = new Vector3(0.0f, 0.0f, angle);
    }

    public void SetStillAlpha(float alpha)
    {
        if (still_image == null) return;
        alpha = Mathf.Clamp(alpha, 0.0f, 1.0f);

        Color color = still_image.color;
        color.a = alpha;
        still_image.color = color;
    }

    public RectTransform GetStillRectTransform()
    {
        if(still_image == null) return null;
        return still_image.rectTransform;
    }

    public void SetChild(GameObject child)
    {
        child.transform.SetParent(systemObject.transform);
        var rect = child.GetComponent<RectTransform>();
        if(rect != null)
        {
            rect.localScale = Vector3.one;
            rect.anchoredPosition = Vector3.zero;
        }
    }

    public void DestroyChild()
    {
        if (systemObject == null) return;
        if (systemObject.transform.childCount == 0) return;

        GameObject.Destroy(systemObject.transform.GetChild(0).gameObject);
    }

    public void ValueSystemDataInstance(Setting_System_ID id)
    {
        if(id == Setting_System_ID.CharacterFovo)
        {
            var obj = GameObject.Instantiate(addressable_data.GetAddressableData());
            SetChild(obj);
            character_favo = obj.GetComponent<CharacterFavoSystemNovelObject>();
            if (character_favo != null)
            {
                character_favo.BaseInit();
            }
        }
        if(id == Setting_System_ID.NickNameChoice)
        {
            var obj = GameObject.Instantiate(addressable_data.GetAddressableData());
            SetChild(obj);
            nick_name_choice = obj.GetComponent<NickNameChoiceObject>();
            if (nick_name_choice != null)
            {
                nick_name_choice.BaseInit();
            }
        }
        if(id == Setting_System_ID.Choice)
        {
            var obj = GameObject.Instantiate(addressable_data.GetAddressableData());
            SetChild(obj);
            choice_object = obj.GetComponent<ChoiceObject>();
            if (choice_object != null)
            {
                choice_object.BaseInit();
            }
        }
    }

    public void SystemLoad(Setting_System_ID id)
    {
        if (addressable_data == null) addressable_data = new AddressableData<GameObject>();
        addressable_data.LoadStart(system_addressable_name[(int)id]);
    }

    public bool IsSystemLoad()
    {
        if (addressable_data == null) return true;
        return addressable_data.GetFlagSetUpLoading();
    }

    public void Release()
    {
        if (addressable_data == null) return;
        addressable_data.OnAutoRelease();
        addressable_data = null;
        if (character_favo != null)
        {
            character_favo.Release();
            character_favo = null;
        }
        if (nick_name_choice != null)
        {
            nick_name_choice.Release();
            nick_name_choice = null;
        }
        if (choice_object != null)
        {
            choice_object.Release();
            choice_object = null;
        }
        DestroyChild();

    }

    public void OnDisable()
    {
        if (addressable_data == null) return;
        addressable_data.OnAutoRelease();
        addressable_data = null;
        if (character_favo != null)
        {
            character_favo.Release();
            character_favo = null;
        }
        if (nick_name_choice != null)
        {
            nick_name_choice.Release();
            nick_name_choice = null;
        }
        if(choice_object != null)
        {
            choice_object.Release();
            choice_object = null;
        }
    }
}

/// <summary>
/// セレクトデータ
/// </summary>
public class ChoicesData
{
    /// <summary>
    /// 画像
    /// </summary>
    public Image sprite = null;
    /// <summary>
    /// テキスト
    /// </summary>
    public TextMeshProUGUI text_text = null;

    public void SetSpriteColor(Color color)
    {
        if (sprite == null) return;
        sprite.color = color;
    }

    public void SetTest(string value_text)
    {
        if(text_text == null) return;
        text_text.text = value_text;
    }
}

/// <summary>
/// セレクトデータをまとめたクラス
/// </summary>
public class ChoicesStorageData
{
    public const int select_num = 3;
    /// <summary>
    /// セレクトデータ配列
    /// </summary>
    public ChoicesData[] choices_array = new ChoicesData[select_num];

    public void Load(GameObject parent)
    {
        var chiles_sptite = parent.GetComponentsInChildren<Image>();
        choices_array = new ChoicesData[select_num];
        int index = 0;
        for(int count = 0; count < chiles_sptite.Length;++count)
        {
            var text_mesh = chiles_sptite[count].GetComponentInChildren<TextMeshProUGUI>();
            if(text_mesh == null)
            {
                continue;
            }
            choices_array[index] = new ChoicesData();
            choices_array[index].sprite = chiles_sptite[count];
            choices_array[index].text_text = text_mesh;

            choices_array[index].text_text.text = $"{index:0000}";
            ++index;

        }
    }
}
