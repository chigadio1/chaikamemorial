using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrentDataCanvas : BaseGameCanvas
{
    /// <summary>
    /// 現在の日付
    /// </summary>
    TextMeshProUGUI year_month_day_text;

    /// <summary>
    /// 祝日や学校行事
    /// </summary>
    TextMeshProUGUI event_text;
    public override void BeginInit()
    {
        var texts = gameObject.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var text in texts)
        {
            if(text.gameObject.name == "CurrentText")
            {
                year_month_day_text = text;
            }
            else if(text.gameObject.name == "EventText")
            {
                event_text = text;
            }
        }

        ///現在の日付を取得
        if(year_month_day_text != null)
        {
            var year = PlayerCore.Instance.EventCalenderCoreData.GetNouTurnYearMonthID(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);
            var day = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);
            string text = "";
            text = $"{(int)year.Item1 + EventYearData.start_year:0000}年{(int)year.Item2 + 1:00}月{day.GetDay:00}日 ({DayWeekConvert.GetDayWeekJPID(day.GetDayWeekID)})";
            year_month_day_text.text = text;
            if(day.GetListDayDetails.Count > 0 && event_text != null)
            {
                string text2 = "(!) ";
                foreach (var data in day.GetListDayDetails)
                {
                    if (data.day_holiday_id > Day_Holiday_ID.NONE && data.day_holiday_id < Day_Holiday_ID.MAX)
                    {
                        text2 += $"{EventDayConvert.GetDayHolidayComment(data.day_holiday_id)} ";
                    }
                    if(data.day_school_event_id > Day_School_Event_ID.NONE && data.day_school_event_id < Day_School_Event_ID.MAX)
                    {
                        text2 += $"{EventDayConvert.GetSchoolEventComment(data.day_school_event_id)}";
                    }
                }
                event_text.text = text2;
            }
        }
    }

    public void Awake()
    {
        BeginInit();
    }
}
