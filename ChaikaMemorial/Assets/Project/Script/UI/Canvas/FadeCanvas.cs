using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeCanvas : BaseGameCanvas
{
    /// <summary>
    /// フェードイメージ
    /// </summary>
    private Image fade_image;
    public override void BeginInit()
    {
        var childs = gameObject.GetComponentsInChildren<Image>();
        foreach(var child in childs)
        {
            if(child.gameObject.name == "FadeImage")
            {
                fade_image = child;
            }
        }
    }

    public void SetFadeAlpha(float value)
    {
        if (fade_image == null) return;
        value = Mathf.Clamp(value, 0.0f, 1.0f);
        var color = fade_image.color;
        color.a = value;
        fade_image.color = color;
    }

    public float GetFadeAlpha()
    {
        if(fade_image == null) return 1.0f;

        return fade_image.color.a;
    }
}
