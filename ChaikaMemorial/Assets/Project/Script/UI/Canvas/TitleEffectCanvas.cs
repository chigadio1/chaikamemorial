using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleEffectCanvas : BaseGameCanvas
{
    public enum Title_Effect_Anim_ID
    {
        NONE = -1,
        ID_8401 = 8401,
        ID_8402,
        ID_8403,
        ID_8404,
        ID_8405,
        MAX
    }
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(IsPlayEndAnim())
        {
            Title_Effect_Anim_ID random = (Title_Effect_Anim_ID)Random.Range((int)Title_Effect_Anim_ID.ID_8401, (int)Title_Effect_Anim_ID.MAX);
            PlayAnim(random);
        }
    }

    public override void BeginInit()
    {
        animator = GetComponent<Animator>();

        Title_Effect_Anim_ID random = (Title_Effect_Anim_ID)Random.Range((int)Title_Effect_Anim_ID.ID_8401, (int)Title_Effect_Anim_ID.MAX);
        PlayAnim(random);
    }

    public void PlayAnim(Title_Effect_Anim_ID id)
    {
        if (id <= Title_Effect_Anim_ID.NONE || id >= Title_Effect_Anim_ID.MAX) return;
        if (animator == null) return;

        int id_num = (int)id;
        animator.Play($"{id_num:0000}");
    }

    public bool IsPlayEndAnim()
    {
        if (animator == null) return true;

        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        return stateInfo.normalizedTime >= 1.0f;
    }
}
