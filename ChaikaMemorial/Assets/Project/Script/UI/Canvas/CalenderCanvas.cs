using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Calender_Anim_ID
{
    NONE = -1,
    ANIM_ID_01 = 8601,
    ANIM_ID_02,
    ANIM_ID_03,
    ANIM_ID_04,
    MAX
}

public class CalenderCanvas : BaseGameCanvas
{
    /// <summary>
    /// カレンダーアニメ―と
    /// </summary>
    Animator animator;

    /// <summary>
    /// カレンダーの
    /// </summary>
    RectTransform root_rect_transform;

    static readonly int max_day = 36;
    /// <summary>
    /// 日付テキスト
    /// </summary>
    TextMeshProUGUI[] calender_day = new TextMeshProUGUI[max_day];
    private bool change_flag;
    private bool inversion_flag;

    public override void BeginInit()
    {
        var chikds_text = gameObject.GetComponentsInChildren<TextMeshProUGUI>();

        string pattern = @"Day(\d+)";
        foreach (var ch in chikds_text)
        {
            string input = ch.gameObject.name;
            // 正規表現パターンに一致する箇所を検索
            Match match = Regex.Match(input, pattern);

            if (match.Success)
            {
                // 一致した部分を取得し、数字を抽出
                string extractedNumber = match.Groups[1].Value;
                int index = int.Parse(extractedNumber);

                calender_day[index] = ch;
            }
        }

        var mask = gameObject.GetComponentInChildren<RectMask2D>();
        if(mask != null)
        {
            root_rect_transform = mask.rectTransform;
        }

        animator = gameObject.GetComponentInChildren<Animator>();
    }

    public void PlayAnim(Calender_Anim_ID id, bool inversion = false)
    {
        int id_num = (int)id;
        if (id_num < (int)Calender_Anim_ID.ANIM_ID_01 || id_num >= (int)Calender_Anim_ID.MAX) return;
        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", -1.0f);
        else animator.SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            animator.SetFloat("Speed", 1.0f);
        }
        animator.Play($"{id_num:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {
        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag)
        {
            if (gameObject.activeInHierarchy == false)
            {
                return true;
            }
            return stateInfo.normalizedTime >= 1.0f;
        }
        else return stateInfo.normalizedTime <= 0.0f;
    }
}
