using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// キャラクターキャンバス
/// </summary>
public class CharacterCanvas : BaseGameCanvas
{
    /// <summary>
    /// キャラクターオブジェ
    /// </summary>
    private List<Image> character_obj_list = new List<Image>();
    // Start is called before the first frame update

    public override void BeginInit()
    {
        character_obj_list.Clear();
        var chileds = gameObject.GetComponentsInChildren<Image>();
        for(int count = 0; count < (int)Novel_Character_ID.MAX;)
        {
            foreach(Image character in chileds)
            {
                if (character.gameObject.name == $"Chara{count + 1:00}")
                {
                    character_obj_list.Add(character);
                }
            }
            ++count;
        }
    }

    static readonly float start_pos_y = -567;

    public void SetCharacter(Novel_Character_ID id,int character_id)
    {
        if (id <= Novel_Character_ID.NONE || id >= Novel_Character_ID.MAX) return;
        if (character_obj_list.Count <= 0) return;
        character_obj_list[(int)id].sprite = gameUiCore.Instance.GetSprite(Novel_Sprite_Type_ID.CHARACTER,character_id);
    }

    public void SetCharacterPosition(Novel_Character_ID id, Vector2 position)
    {
        if (id <= Novel_Character_ID.NONE || id >= Novel_Character_ID.MAX) return;
        if (character_obj_list.Count <= 0) return;
        character_obj_list[(int)id].rectTransform.anchoredPosition = new Vector3((float)position.x, (float)position.y + start_pos_y, 0.0f);
    }

    public RectTransform GetCharacterPosition(Novel_Character_ID id)
    {
        if (id <= Novel_Character_ID.NONE || id >= Novel_Character_ID.MAX) return null;
        if (character_obj_list.Count <= 0) return null;
        return character_obj_list[(int)id].rectTransform;
    }

    public void SetAlpha(Novel_Character_ID id,float value_alpha)
    {
        if (id <= Novel_Character_ID.NONE || id >= Novel_Character_ID.MAX) return;
        if (character_obj_list.Count <= 0) return;
        value_alpha = Mathf.Clamp01(value_alpha);

        Color color = character_obj_list[(int)id].color;
        color.a = value_alpha;
        character_obj_list[(int)id].color = color;
    }

    public void ResetAll()
    {
        for (int count = 0; count < (int)Novel_Character_ID.MAX;)
        {
            character_obj_list[count].sprite = null;
            var color = character_obj_list[count].color;
            color.a = 0.0f;
            character_obj_list[count].color = color;
            ++count;
        }
    }
}
