using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Title_Select_Choice_ID
{
    NONE = -1,
    START,
    NEW_GAME,
    LOAD_GAME,
    MAX
}


public class TitleSelectChoiceCanvas : BaseGameCanvas
{
    /// <summary>
    /// セレクト画像
    /// </summary>
    private Image[] select_sprite_array = new Image[(int)Title_Select_Choice_ID.MAX];

    public override void BeginInit()
    {
        var childs = GetComponentsInChildren<Image>();
        foreach (var child in childs)
        {
            if(child.gameObject.name == "StartImage")
            {
                select_sprite_array[0] = child;
            }
            else if (child.gameObject.name == "NewGameImage")
            {
                select_sprite_array[1] = child;
            }
            else if (child.gameObject.name == "LoadGameImage")
            {
                select_sprite_array[2] = child;
            }
        }

        SetAlpha(Title_Select_Choice_ID.START, 1.0f);
        SetAlpha(Title_Select_Choice_ID.NEW_GAME, 0.0f);
        SetAlpha(Title_Select_Choice_ID.LOAD_GAME, 0.0f);
    }

    public void SetAlpha(Title_Select_Choice_ID id,float value)
    {
        if (id <= Title_Select_Choice_ID.NONE || id >= Title_Select_Choice_ID.MAX) return;
        if (select_sprite_array[(int)id] == null) return;
        var color = select_sprite_array[(int)id].color;
        color.a = value;
        select_sprite_array[(int)id].color = color;
    }

}
