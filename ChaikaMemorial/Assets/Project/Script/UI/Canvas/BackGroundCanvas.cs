using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BackGroundCanvas : BaseGameCanvas
{
    /// <summary>
    /// �w�i�摜
    /// </summary>
    protected Image back_ground_image;

    public override void BeginInit()
    {
        back_ground_image = gameObject.GetComponentsInChildren<Image>().Where(a => a.gameObject.name == "BackGround").First();
    }

    public void SetBackGroundImage(int back_ground_id)
    {
        back_ground_image.sprite = gameUiCore.Instance.GetSprite(Novel_Sprite_Type_ID.BACKGROUND, back_ground_id);
    }

    public void SetAlpha(float value_alpha)
    {
        if (back_ground_image == null) return;

        Color color = back_ground_image.color;
        color.a = value_alpha;
        back_ground_image.color = color;
    }

    public float GetAlpha()
    {
        if (back_ground_image == null) return 1.0f;

        return back_ground_image.color.a;
    }

    public void OnDestroy()
    {
        ResetAll();
    }

    public void ResetAll()
    {
        if(back_ground_image != null)
        {
            back_ground_image.sprite = null;
            back_ground_image = null;
        }
    }
}
