using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum Action_Canvas_Anim_ID
{
    START = 8001,
    SELECT_START = 8002,
    END
}

public enum Parent_Action_Select_Type_ID
{
    NONE = -1,
    ParameterUP,
    SubAction,
    Setting,
    MAX
}

public enum Parent_Anim_Type_ID
{
    NONE = -1,
    StartUP = 8551,
    LoopIdle = 8552,
    MAX,
}

public enum Action_Sub_Obj_ID
{
    NONE = -1,
    Date_Commnad,
    Save_Load,
    MAX
}

public class UpdateMovePosition
{
    public Vector2 start_pos;
    public Vector2 end_pos;
}

public enum Action_Canvas_Anim
{
    NONE = -1,
    StartUP = 8701,
    Scal_BIG,
    Finish,
    MAX
}

public enum Default_Action_Sound_ID
{
    NONE = -1,
    Move_Select = 8101,
    Cansel_Select = 8102,
    Push_Select = 8103, 
    DateCommand_Start = 8104,
    Button_Cursor = 8106,
    Action_BGM = 8902,
    MAX
}

/// <summary>
/// 行動選択キャンバス
/// </summary>
public class ActionCanvas : BaseGameCanvas
{
    /// <summary>
    ///　親セレクト
    /// </summary>
    Animator[] parent_animator = new Animator[(int)Parent_Action_Select_Type_ID.MAX];

    /// <summary>
    /// 座標などの情報データ
    /// </summary>
    RectTransform[] parent_rect_transform = new RectTransform[(int)Parent_Action_Select_Type_ID.MAX];

    /// <summary>
    /// 初期、終了座標
    /// </summary>
    UpdateMovePosition[] parent_move_pos = new UpdateMovePosition[(int)Parent_Action_Select_Type_ID.MAX];

    /// <summary>
    /// ボタン押したとき
    /// </summary>
    Parent_Action_Select_Type_ID on_parent_action_select_type_id = Parent_Action_Select_Type_ID.NONE;
    public Parent_Action_Select_Type_ID OnParentActionSelectTypeID { get { return on_parent_action_select_type_id; } }
    public void ResetParentActionSelectType()
    {
        on_parent_action_select_type_id = Parent_Action_Select_Type_ID.NONE;
    }
    /// <summary>
    /// ボタン押したときのフラグ
    /// </summary>
    bool is_on_button = false;
    public bool IsOnButton { get { return is_on_button; } }
    public void OffIsOnButton()
    {
        is_on_button = false;
    }

    readonly int move_start_add = 823;
    readonly int max_move = 865;
    readonly int min_move = -781;
    /// <summary>
    /// アニメーションチェンジフラグ
    /// </summary>
    private bool change_flag = false;

    private bool inversion_flag = false;

    private static readonly string[] sub_system_assets_array = new string[(int)Action_Sub_Obj_ID.MAX]
    {
        "Assets/Project/Assets/Object/UI/DateCommand.prefab",
        "Assets/Project/Assets/Object/UI/SystemPlayerActionData.prefab"
    };

    /// <summary>
    /// サブシステムのRect
    /// </summary>
    RectTransform sub_system_rect;

    AddressableData<GameObject> sub_system_obj;
    /// <summary>
    /// デートするときの呼び出しボタン
    /// </summary>
    DateCommandObject date_commnad;
    public DateCommandObject DateCommandObject { get { return date_commnad; } }

    /// <summary>
    /// セーブロード
    /// </summary>
    SystemPlayerDataObject system_save_load_object;
    public SystemPlayerDataObject SystemSaveLoadObject { get { return system_save_load_object; } }
    public Animator animator;
    public override void BeginInit()
    {
        var childs = gameObject.GetComponentsInChildren<Animator>();
        animator = gameObject.GetComponentInParent<Animator>();
        foreach (var child in childs)
        {
            if(child.gameObject.name == "ParameterAnimImage")
            {
                parent_animator[0] = child;
                parent_rect_transform[0] = child.transform.parent.GetComponent<RectTransform>();
                parent_move_pos[0] = new UpdateMovePosition();
            }
            else if(child.gameObject.name == "SubActionAnimImage")
            {
                parent_animator[1] = child;
                parent_rect_transform[1] = child.transform.parent.GetComponent<RectTransform>();
                parent_move_pos[1] = new UpdateMovePosition();
            }
            else if(child.gameObject.name == "SettingAnimImage")
            {
                parent_animator[2] = child;
                parent_rect_transform[2] = child.transform.parent.GetComponent<RectTransform>();
                parent_move_pos[2] = new UpdateMovePosition();
            }
        }

        sub_system_rect = gameObject.GetComponentsInChildren<RectTransform>().Where(rt => rt.gameObject.name == "SubSystem").First();

        SoundInit();
    }

    public void SoundInit()
    {
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameSE((int)Default_Action_Sound_ID.Move_Select);
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameSE((int)Default_Action_Sound_ID.Cansel_Select);
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameSE((int)Default_Action_Sound_ID.Push_Select);
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameSE((int)Default_Action_Sound_ID.DateCommand_Start);
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameSE((int)Default_Action_Sound_ID.Button_Cursor);
        SoundGameCore.Instance.EventSummaryData.LoadSelectStartGameBGM((int)Default_Action_Sound_ID.Action_BGM);
    }



    

    public void PlayAnim(Parent_Action_Select_Type_ID id,Parent_Anim_Type_ID anim_id ,bool inversion = false)
    {
        int id_num = (int)id;
        if (id_num < (int)Parent_Action_Select_Type_ID.ParameterUP || id_num >= (int)Parent_Action_Select_Type_ID.MAX) return;
        if (anim_id <= Parent_Anim_Type_ID.NONE || anim_id >= Parent_Anim_Type_ID.MAX) return;
        if (parent_animator[id_num] == null) return;
        change_flag = true;
        if (inversion) parent_animator[id_num].SetFloat("Speed", -1.0f);
        else parent_animator[id_num].SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            parent_animator[id_num].SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            parent_animator[id_num].SetFloat("Speed", 1.0f);
        }
        parent_animator[id_num].Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim(Parent_Action_Select_Type_ID id)
    {
        int id_num = (int)id;
        if (id_num < (int)Parent_Action_Select_Type_ID.ParameterUP || id_num >= (int)Parent_Action_Select_Type_ID.MAX) return true;
        if (parent_animator[id_num] == null) return true;
        if(change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = parent_animator[id_num].GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }

    public void OnDestroy()
    {
        parent_animator = null;
        parent_rect_transform = null;
        parent_move_pos = null;
        ReleaseSubSystem();
    }

    public void OnParameterUp()
    {
        on_parent_action_select_type_id = Parent_Action_Select_Type_ID.ParameterUP;
        is_on_button = true;
    }

    public void OnSubAction()
    {
        on_parent_action_select_type_id = Parent_Action_Select_Type_ID.SubAction;
        is_on_button = true;
    }

    public void OnSetting()
    {
        on_parent_action_select_type_id = Parent_Action_Select_Type_ID.Setting;
        is_on_button = true;
    }

    public void StartMove(bool is_right)
    {
        int add_num = move_start_add;
        if (is_right == false) add_num *= -1;

        for(int count = 0; count < (int)Parent_Action_Select_Type_ID.MAX;)
        {
            parent_move_pos[count].start_pos = parent_rect_transform[count].anchoredPosition;
            parent_move_pos[count].end_pos = parent_rect_transform[count].anchoredPosition;
            parent_move_pos[count].end_pos.x += add_num;
            ++count;
        }
    }

    public void SubSystemLoad(Action_Sub_Obj_ID id)
    {
        if(sub_system_obj != null)
        {
            sub_system_obj.OnAutoRelease();
            sub_system_obj = null;
        }
        sub_system_obj = new AddressableData<GameObject>();
        sub_system_obj.LoadStart(sub_system_assets_array[(int)id]);
    }

    public bool IsSubSystemLoad()
    {
        if(sub_system_obj == null)
        {
            return true;
        }
        bool res = true;
        res &= sub_system_obj.GetFlagSetUpLoading();
        if(date_commnad != null)
        {
            res &= date_commnad.IsLoad();
        }
        return res;
    }

    public void InstanceSubSystem(Action_Sub_Obj_ID id)
    {
        if (sub_system_obj == null) return;
        var obj = GameObject.Instantiate(sub_system_obj.GetAddressableData());
        obj.transform.SetParent(sub_system_rect.transform, false);

        if(id == Action_Sub_Obj_ID.Date_Commnad)
        {
            date_commnad = obj.GetComponent<BaseSystemNovelObject>() as DateCommandObject;
            if(date_commnad != null)
            {
                date_commnad.BaseInit();
            }
        }

        if (id == Action_Sub_Obj_ID.Save_Load)
        {
            system_save_load_object = obj.GetComponent<BaseSystemNovelObject>() as SystemPlayerDataObject;
            if (system_save_load_object != null)
            {
                system_save_load_object.BaseInit();
            }
        }
    }

    public void ReleaseSubSystem()
    {
        if (sub_system_obj != null)
        {
            sub_system_obj.OnAutoRelease();
            sub_system_obj = null;
        }
        if(date_commnad != null)
        {
            date_commnad.Release();
            GameObject.Destroy(date_commnad.gameObject);
            date_commnad = null;
        }
        if(system_save_load_object != null)
        {
            system_save_load_object.Release();
            GameObject.Destroy(system_save_load_object.gameObject);
            system_save_load_object = null;
        }
    }

    public bool UpdateMove(float lerp)
    {
        if (lerp >= 1.0f)
        {
            for (int count = 0; count < (int)Parent_Action_Select_Type_ID.MAX;)
            {
                parent_rect_transform[count].anchoredPosition = Vector2.Lerp(parent_move_pos[count].start_pos, parent_move_pos[count].end_pos, 1.0f);
                if(parent_rect_transform[count].anchoredPosition.x < min_move)
                {
                    parent_rect_transform[count].anchoredPosition = new Vector2(max_move, parent_rect_transform[count].anchoredPosition.y);
                }
                else if(parent_rect_transform[count].anchoredPosition.x > max_move)
                {
                    parent_rect_transform[count].anchoredPosition = new Vector2(min_move, parent_rect_transform[count].anchoredPosition.y);
                }
                ++count;
            }
            return true;
        }
        else
        {
            for (int count = 0; count < (int)Parent_Action_Select_Type_ID.MAX;)
            {
                parent_rect_transform[count].anchoredPosition = Vector2.Lerp(parent_move_pos[count].start_pos, parent_move_pos[count].end_pos, lerp);
                ++count;
            }
        }

        return false;
    }


    public void PlayAnim(Action_Canvas_Anim anim_id, bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Action_Canvas_Anim.StartUP || id_num >= (int)Action_Canvas_Anim.MAX) return;

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", -1.0f);
        else animator.SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", 1.0f);
        }
        animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }
}
