using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public enum Name_Input_Type_ID
{
    NONE = -1,
    SURNAME,
    NAME,
    NICKNAME,
    CURRENTUSER,
    BIRTMONTH,
    BIRTDAY,
    MAX
}

public class NameInputCanvas : BaseGameCanvas
{


    TMP_InputField[] input_array = new TMP_InputField[(int)Name_Input_Type_ID.MAX];
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        SettingUpdate();
    }

    public override void BeginInit()
    {
        var childs = gameObject.GetComponentsInChildren<TMP_InputField>();
        foreach (var child in childs)
        {
            if (child.transform.parent == null) continue;
            string parent_name = child.transform.parent.name;
            if (child.name == "Text (TMP)") continue;
            Name_Input_Type_ID name = Name_Input_Type_ID.NONE;
            if (parent_name == "Surname")
            {
                name = Name_Input_Type_ID.SURNAME;
            }
            else if(parent_name == "Name")
            {
                name = Name_Input_Type_ID.NAME;
            }
            else if (parent_name == "NickName")
            {
                name = Name_Input_Type_ID.NICKNAME;
            }
            else if (parent_name == "BirtMonth")
            {
                name = Name_Input_Type_ID.BIRTMONTH;
            }
            else if (parent_name == "BirtDay")
            {
                name = Name_Input_Type_ID.BIRTDAY;
            }
            else if(parent_name == "CurrentUser")
            {
                name = Name_Input_Type_ID.CURRENTUSER;
            }
            if (name == Name_Input_Type_ID.NONE) continue;

            input_array[(int)name] = child.GetComponent<TMP_InputField>();
            input_array[(int)name].characterLimit = 10;
        }

    }

    public void SettingUpdate()
    {
        int day, month;
        day = 1;
        month = 1;
        for(int count = (int)Name_Input_Type_ID.BIRTMONTH; count < (int)Name_Input_Type_ID.MAX;)
        {
            input_array[count].text = new string(input_array[count].text.Where(char.IsDigit).ToArray());
            if (input_array[count].text == "")
            {
                ++count;
                continue;
            }
            if (count == (int)Name_Input_Type_ID.BIRTDAY)
            {
                day = int.Parse(input_array[count].text);
            }
            if(count == (int)Name_Input_Type_ID.BIRTMONTH)
            {
                month = int.Parse(input_array[count].text);
                if (month <= 0) month = 1;
                if(month > 12) month = 12;
            }
            ++count;
        }

        if(!IsValidDate(2021,month,day))
        {
            int lastDay = DateTime.DaysInMonth(2021, month);
            input_array[(int)Name_Input_Type_ID.BIRTDAY].text = lastDay.ToString();
        }


    }


    private bool IsValidDate(int year, int month, int day)
    {
        try
        {
            DateTime date = new DateTime(year, month, day);
            return true;
        }
        catch (ArgumentOutOfRangeException)
        {
            return false;
        }
    }

    public bool InputValue()
    {
        bool res = true;

        foreach(var input in input_array)
        {
            if (input.text == "") return false;
        }

        return res;
    }

    public (string,string,string,int,int,string) GetInput()
    {
        return (input_array[0].text, input_array[1].text, input_array[2].text, int.Parse(input_array[4].text), int.Parse(input_array[5].text),input_array[3].text);
    }
}
