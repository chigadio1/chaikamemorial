using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public enum Player_Select_ID
{
    NONE = -1,
    Study,
    Exercise,
    Charm,
    Date,
    Call,
    SpecificEvent,
    Save,
    Load,
    Quit,
    MAX
}

/// <summary>
/// セレクトボタンコンポーネント
/// </summary>
/// 

public class SelectButton : MonoBehaviour, IPointerEnterHandler
{
    public Player_Select_ID player_select_id;
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (gameUiCore.Instance.is_button_push) return;
        gameUiCore.Instance.SetPlayerSelectID(player_select_id);
        gameUiCore.Instance.PlayerSelectCanvas.PlayAnim(Play_Select_Anim_ID.ID_8501);
        gameUiCore.Instance.PlayerSelectCanvas.SetRemaindText(ConvertSelect(player_select_id));
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, (int)Default_Action_Sound_ID.Button_Cursor);
    }

    public void OnClick()
    {
        if (gameUiCore.Instance.is_button_push) return;
        gameUiCore.Instance.is_button_push = true;
    }

    public static string ConvertSelect(Player_Select_ID select_id)
    {
        string res = "";

        switch (select_id)
        {
            case Player_Select_ID.Study:
                res = "勉強";
                // Studyを選択した場合の処理
                break;
            case Player_Select_ID.Exercise:
                res = "運動";
                // Exerciseを選択した場合の処理
                break;
            case Player_Select_ID.Charm:
                res = "魅力";
                // Charmを選択した場合の処理
                break;
            case Player_Select_ID.Date:
                res = "デート";
                // Dateを選択した場合の処理
                break;
            case Player_Select_ID.Call:
                res = "社";
                // Callを選択した場合の処理
                break;
            case Player_Select_ID.SpecificEvent:
                res = "固有";
                // SpecificEventを選択した場合の処理
                break;
            case Player_Select_ID.Save:
                res = "セーブ";
                // Saveを選択した場合の処理
                break;
            case Player_Select_ID.Load:
                res = "ロード";
                // Loadを選択した場合の処理
                break;
            case Player_Select_ID.Quit:
                res =  "終了";
                // Quitを選択した場合の処理
                break;
            case Player_Select_ID.NONE:
                // NONEを選択した場合の処理
                break;
            case Player_Select_ID.MAX:
                // MAXを選択した場合の処理
                break;
            default:
                // 未知の選択肢に対するデフォルトの処理
                break;
        }
        res += "?";

        return res;
    }
}
