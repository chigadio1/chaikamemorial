using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum Date_Spot_ID
{
    NONE = -1,
    Park, //公園
    Shrine, //神社
    Home, //家
    Karaoke, //カラオケ
    Shopping, //ショッピング
    ArtMuseum, //美術館
    AmusementPark, //遊園地
    Library, //プール
    MovieTheater, //映画館
    Sea, //海
    MAX
}

public class DateSpotIDConvert
{
    public static string GetJapaneseName(Date_Spot_ID spot)
    {
        switch (spot)
        {
            case Date_Spot_ID.Park:
                return "公園";
            case Date_Spot_ID.Shrine:
                return "神社";
            case Date_Spot_ID.Home:
                return "家";
            case Date_Spot_ID.Karaoke:
                return "カラオケ";
            case Date_Spot_ID.Shopping:
                return "ショッピング";
            case Date_Spot_ID.ArtMuseum:
                return "美術館";
            case Date_Spot_ID.AmusementPark:
                return "遊園地";
            case Date_Spot_ID.Library:
                return "図書館";
            case Date_Spot_ID.MovieTheater:
                return "映画館";
            case Date_Spot_ID.Sea:
                return "海";
            default:
                return "";
        }
    }
}


public class DateSpotButton : MonoBehaviour, IPointerEnterHandler
{
    public Date_Spot_ID date_spot_id = Date_Spot_ID.NONE;
    public void OnPointerEnter(PointerEventData eventData)
    {



        string text = $"{PlayerCore.Instance.GetPlayerToCharacterNickName(gameUiCore.Instance.SelectCharacterID)}" + "と" + $"{DateSpotIDConvert.GetJapaneseName(date_spot_id)}にいこうかな？";
        gameUiCore.Instance.ActionCanvas.DateCommandObject?.SetDateSpotTextTitle(text); ;
    }

    public void OnClick()
    {
        if (gameUiCore.Instance.is_button_push == true) return;
        gameUiCore.Instance.is_button_push = true;
        gameUiCore.Instance.SetSelectDateSpotID(date_spot_id);
    }
}
