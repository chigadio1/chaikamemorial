using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveGridMesh : Graphic
{
    [SerializeField] private int rows = 1;
    [SerializeField] private int columns = 1;

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        if (rows < 1 || columns < 1)
        {
            Debug.LogError("GridMesh => 行と列は1以上に設定してください。");
            return;
        }

        float cellWidth = rectTransform.rect.width / columns;
        float cellHeight = rectTransform.rect.height / rows;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                UIVertex vertex = new UIVertex
                {
                    position = new Vector2(j * cellWidth, -i * cellHeight),
                    color = color
                };
                vh.AddVert(vertex);
            }
        }

        for (int i = 0; i < rows - 1; i++)
        {
            for (int j = 0; j < columns - 1; j++)
            {
                int index0 = i * columns + j;
                int index1 = index0 + 1;
                int index2 = (i + 1) * columns + j;
                int index3 = index2 + 1;

                vh.AddTriangle(index0, index2, index1);
                vh.AddTriangle(index1, index2, index3);
            }
        }
    }

    private void Update()
    {
        SetVerticesDirty();
    }
}
