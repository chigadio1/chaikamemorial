using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NickNameButton : MonoBehaviour
{
    // Start is called before the first frame update
    public Character_NickName_ID nick_name_id;

    public void OnPointerEnter(PointerEventData eventData)
    {
        gameUiCore.Instance.SetCharacterSelectNickNameID(nick_name_id);
    }

    public void OnClick()
    {
        gameUiCore.Instance.is_button_push = true;
        gameUiCore.Instance.SetCharacterSelectNickNameID(nick_name_id);
    }
}
