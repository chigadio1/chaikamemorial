using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アニメーションUI
/// </summary>
public class AnimatorUI<T> where T : Enum
{
    private Animator animator;
    private bool change_flag;
    private bool inversion_flag;

    public void BeginLoad(Animator value)
    {
        animator = value;
    }

    public void PlayAnim(T anim_id, float time = 1.0f, bool inversion = false)
    {
        int id_num = Convert.ToInt32(anim_id);

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", time * -1.0f);
        else animator.SetFloat("Speed", time);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", time * -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", time);
        }
        animator.Play($"{id_num:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }
}
