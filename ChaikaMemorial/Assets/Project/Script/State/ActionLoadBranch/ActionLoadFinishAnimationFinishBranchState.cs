using System.Collections.Generic;
public class ActionLoadFinishAnimationFinishBranchState : BaseActionLoadFinishAnimationFinishBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataAnima.PlayAnim(Player_Data_UI_Anim_ID.StartUP, 1.0f, true);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataAnima.IsPlayAnim())
        {
            is_end_update = true;
            gameUiCore.Instance.is_button_push = gameUiCore.Instance.is_button_cancel_push = false;
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadFinishAnimationFinishBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
