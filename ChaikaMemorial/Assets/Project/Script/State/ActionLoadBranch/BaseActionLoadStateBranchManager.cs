using System.Collections.Generic;
public class BaseActionLoadStateBranchManager : BaseStateBranchManager<ActionLoadInitArgData,ActionLoadUpdateArgData,BaseActionLoadBranchState,ActionLoad_Branch_State_ID > {
    public BaseActionLoadStateBranchManager(ActionLoad_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(ActionLoadInitArgData arg = null){
        switch (state_id)
        {
            case ActionLoad_Branch_State_ID.Nest01_BeginLoad:
                state_id = ActionLoad_Branch_State_ID.Nest02_BeginPhoneAnimationStart;
                state = new ActionLoadBeginPhoneAnimationStartBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest02_BeginPhoneAnimationStart:
                state_id = ActionLoad_Branch_State_ID.Nest03_BeginAnimationStart;
                state = new ActionLoadBeginAnimationStartBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest03_BeginAnimationStart:
                state_id = ActionLoad_Branch_State_ID.Nest07_Update;
                state = new ActionLoadUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest07_Update:
                {
                ActionLoad_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish:
                        state_id = ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish;
                        state = new ActionLoadFinishAnimationFinishBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionLoad_Branch_State_ID.Nest07_SelectAnimationStart:
                        state_id = ActionLoad_Branch_State_ID.Nest07_SelectAnimationStart;
                        state = new ActionLoadSelectAnimationStartBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionLoad_Branch_State_ID.Nest07_SelectAnimationStart:
                state_id = ActionLoad_Branch_State_ID.Nest07_SelectUpdate;
                state = new ActionLoadSelectUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest07_SelectUpdate:
                {
                ActionLoad_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish:
                        state_id = ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish;
                        state = new ActionLoadSelectAnimationFinishBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionLoad_Branch_State_ID.Nest10_Reelase:
                        state_id = ActionLoad_Branch_State_ID.Nest10_Reelase;
                        state = new ActionLoadReelaseBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish:
                state_id = ActionLoad_Branch_State_ID.Nest07_Update;
                state = new ActionLoadUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest09_FinishPhoneAnimationFinish:
                state_id = ActionLoad_Branch_State_ID.Nest10_Reelase;
                state = new ActionLoadReelaseBranchState(state_id);
                state.Start(arg);
                break;
            case ActionLoad_Branch_State_ID.Nest10_Reelase:
                is_finish = true;
                state_id = ActionLoad_Branch_State_ID.Finish;
                break;
            case ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish:
                state_id = ActionLoad_Branch_State_ID.Nest09_FinishPhoneAnimationFinish;
                state = new ActionLoadFinishPhoneAnimationFinishBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new ActionLoadBeginLoadBranchState(ActionLoad_Branch_State_ID.Nest01_BeginLoad);
    }
}
