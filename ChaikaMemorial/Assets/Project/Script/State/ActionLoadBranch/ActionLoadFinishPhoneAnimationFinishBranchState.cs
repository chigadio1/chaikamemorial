using System.Collections.Generic;
public class ActionLoadFinishPhoneAnimationFinishBranchState : BaseActionLoadFinishPhoneAnimationFinishBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Scal_BIG, true);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            gameUiCore.Instance.is_button_push = gameUiCore.Instance.is_button_cancel_push = false;
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
            is_end_update = true;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadFinishPhoneAnimationFinishBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
