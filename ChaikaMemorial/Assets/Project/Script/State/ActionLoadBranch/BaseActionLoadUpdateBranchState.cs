using System.Collections.Generic;
public class BaseActionLoadUpdateBranchState : BaseActionLoadBranchState{
    public BaseActionLoadUpdateBranchState(ActionLoad_Branch_State_ID other):base(other){}
    public virtual ActionLoad_Branch_State_ID BranchNest07_UpdateState(){
        if(BranchNest07_UpdateToFinishAnimationFinish()) return ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish;
        else if(BranchNest07_UpdateToSelectAnimationStart()) return ActionLoad_Branch_State_ID.Nest07_SelectAnimationStart;
     return ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish;
     }
    public virtual bool BranchNest07_UpdateToFinishAnimationFinish(){return true;}
    public virtual bool BranchNest07_UpdateToSelectAnimationStart(){return true;}
    public override ActionLoad_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionLoad_Branch_State_ID.Nest07_Update:
                return BranchNest07_UpdateState();
        }
     return ActionLoad_Branch_State_ID.Nest08_FinishAnimationFinish;
     }
}
