using System.Collections.Generic;
public class ActionLoadSelectUpdateBranchState : BaseActionLoadSelectUpdateBranchState {

    bool is_cancel = false;
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(arg.is_load)
        {
            if(PlayerCore.Instance.is_button_cancel_push)
            {
                is_cancel = true;
                is_end_update = true;
                return;
            }
            if(PlayerCore.Instance.is_button_push)
            {
                if(PlayerCore.Instance.IsSaveData())
                {
                    is_end_update = true;
                    arg.is_game = true;
                }
            }
        }
        else
        {
            if (PlayerCore.Instance.is_button_cancel_push)
            {
                is_cancel = true;
                is_end_update = true;
                return;
            }
            if (PlayerCore.Instance.is_button_push)
            {
                PlayerCore.Instance.SaveData();
                is_end_update = true;
                is_cancel = true;
                gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.SaveDataViewFix();
                return;
            }
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_cancel_push = PlayerCore.Instance.is_button_push = false;
    }
    public ActionLoadSelectUpdateBranchState(ActionLoad_Branch_State_ID other):base(other){}

    public override bool BranchNest07_SelectUpdateToReelase()
    {
        return is_cancel == false;
    }

    public override bool BranchNest07_SelectUpdateToSelectAnimationFinish()
    {
        return is_cancel == true;
    }
}
