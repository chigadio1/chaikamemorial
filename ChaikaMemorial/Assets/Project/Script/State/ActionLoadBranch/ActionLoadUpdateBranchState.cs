using System.Collections.Generic;
public class ActionLoadUpdateBranchState : BaseActionLoadUpdateBranchState {
    bool is_cancel = false;
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(PlayerCore.Instance.is_button_cancel_push)
        {
            is_cancel = true;
            is_end_update = true;
            return;
        }

        if(PlayerCore.Instance.is_button_push)
        {
            is_end_update = true;
            return;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
    }
    public ActionLoadUpdateBranchState(ActionLoad_Branch_State_ID other):base(other){}

    public override bool BranchNest07_UpdateToFinishAnimationFinish()
    {
        return is_cancel == true;
    }

    public override bool BranchNest07_UpdateToSelectAnimationStart()
    {
        return is_cancel == false;
    }
}
