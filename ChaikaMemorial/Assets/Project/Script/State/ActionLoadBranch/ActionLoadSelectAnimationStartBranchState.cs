using System.Collections.Generic;
public class ActionLoadSelectAnimationStartBranchState : BaseActionLoadSelectAnimationStartBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataSelectAnima.PlayAnim(Player_Data_Select_UI_Anim_ID.StartUP);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataSelectAnima.IsPlayAnim())
        {
            is_end_update = true;
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadSelectAnimationStartBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
