using System.Collections.Generic;
using UnityEngine;
public class ActionLoadReelaseBranchState : BaseActionLoadReelaseBranchState {
    public override void Start(ActionLoadInitArgData arg) {

    }
    public override void Update(ActionLoadUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadReelaseBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
