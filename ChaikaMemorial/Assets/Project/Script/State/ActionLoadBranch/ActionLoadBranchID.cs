using System;
public enum ActionLoad_Branch_State_ID {
Begin = -1,
Nest01_BeginLoad,
Nest02_BeginPhoneAnimationStart,
Nest03_BeginAnimationStart,
Nest07_Update,
Nest07_SelectAnimationStart,
Nest07_SelectUpdate,
Nest07_SelectAnimationFinish,
Nest09_FinishPhoneAnimationFinish,
Nest10_Reelase,
Nest08_FinishAnimationFinish,
Finish
}
