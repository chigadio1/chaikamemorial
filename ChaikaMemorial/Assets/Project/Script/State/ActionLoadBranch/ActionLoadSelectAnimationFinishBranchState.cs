using System.Collections.Generic;
public class ActionLoadSelectAnimationFinishBranchState : BaseActionLoadSelectAnimationFinishBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataSelectAnima.PlayAnim(Player_Data_Select_UI_Anim_ID.StartUP, 1.0f, true);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.SystemSaveLoadObject.PlayerDataSelectAnima.IsPlayAnim())
        {
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
            is_end_update = true;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadSelectAnimationFinishBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
