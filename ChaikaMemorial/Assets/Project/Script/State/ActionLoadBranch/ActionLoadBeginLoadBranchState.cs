using System.Collections.Generic;
public class ActionLoadBeginLoadBranchState : BaseActionLoadBeginLoadBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.SubSystemLoad(Action_Sub_Obj_ID.Save_Load);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsSubSystemLoad())
        {
            gameUiCore.Instance.ActionCanvas.InstanceSubSystem(Action_Sub_Obj_ID.Save_Load);
            is_end_update = true;
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadBeginLoadBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
