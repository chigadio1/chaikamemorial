using System.Collections.Generic;
public class ActionLoadBeginPhoneAnimationStartBranchState : BaseActionLoadBeginPhoneAnimationStartBranchState {
    public override void Start(ActionLoadInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Scal_BIG);
    }
    public override void Update(ActionLoadUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadBeginPhoneAnimationStartBranchState(ActionLoad_Branch_State_ID other):base(other){}
}
