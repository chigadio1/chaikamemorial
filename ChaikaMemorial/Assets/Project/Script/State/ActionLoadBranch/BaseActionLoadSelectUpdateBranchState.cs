using System.Collections.Generic;
public class BaseActionLoadSelectUpdateBranchState : BaseActionLoadBranchState{
    public BaseActionLoadSelectUpdateBranchState(ActionLoad_Branch_State_ID other):base(other){}
    public virtual ActionLoad_Branch_State_ID BranchNest07_SelectUpdateState(){
        if(BranchNest07_SelectUpdateToSelectAnimationFinish()) return ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish;
        else if(BranchNest07_SelectUpdateToReelase()) return ActionLoad_Branch_State_ID.Nest10_Reelase;
     return ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish;
     }
    public virtual bool BranchNest07_SelectUpdateToSelectAnimationFinish(){return true;}
    public virtual bool BranchNest07_SelectUpdateToReelase(){return true;}
    public override ActionLoad_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionLoad_Branch_State_ID.Nest07_SelectUpdate:
                return BranchNest07_SelectUpdateState();
        }
     return ActionLoad_Branch_State_ID.Nest07_SelectAnimationFinish;
     }
}
