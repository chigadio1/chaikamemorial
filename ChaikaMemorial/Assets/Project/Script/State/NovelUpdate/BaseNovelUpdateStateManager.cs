using System.Collections.Generic;
public class BaseNovelUpdateStateManager : BaseStateManager<NovelUpdateInitArgData,NovelUpdateUpdateArgData,BaseNovelUpdateState> {
    protected NovelUpdate_State_ID state_id = NovelUpdate_State_ID.BeginInit;
    public NovelUpdate_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NovelUpdateInitArgData arg = null){
        switch (state_id)
        {
            case NovelUpdate_State_ID.Begin:
                state = new NovelUpdateBeginInitState(state);
                state_id = NovelUpdate_State_ID.BeginInit;
                state.Start(arg);
                break;
            case NovelUpdate_State_ID.BeginInit:
                state = new NovelUpdateBeginLoadState(state);
                state_id = NovelUpdate_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case NovelUpdate_State_ID.BeginLoad:
                state = new NovelUpdateNovelUpdateState(state);
                state_id = NovelUpdate_State_ID.NovelUpdate;
                state.Start(arg);
                break;
            case NovelUpdate_State_ID.NovelUpdate:
                state = new NovelUpdateNovelFinishState(state);
                state_id = NovelUpdate_State_ID.NovelFinish;
                state.Start(arg);
                break;
            case NovelUpdate_State_ID.NovelFinish:
                is_finish = true;
                state_id = NovelUpdate_State_ID.Finish;
                break;
            case NovelUpdate_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NovelUpdate_State_ID value_state_id,NovelUpdateInitArgData init_arg = null,NovelUpdateUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NovelUpdate_State_ID.Begin:
                is_finish = true;
                break;
            case NovelUpdate_State_ID.BeginInit:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelUpdateBeginInitState(state);
                state.Start(init_arg);
                break;
            case NovelUpdate_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelUpdateBeginLoadState(state);
                state.Start(init_arg);
                break;
            case NovelUpdate_State_ID.NovelUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelUpdateNovelUpdateState(state);
                state.Start(init_arg);
                break;
            case NovelUpdate_State_ID.NovelFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelUpdateNovelFinishState(state);
                state.Start(init_arg);
                break;
            case NovelUpdate_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NovelUpdateBeginInitState();
    }
}
