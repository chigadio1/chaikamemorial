using System.Collections.Generic;
public class NovelUpdateNovelFinishState : BaseNovelUpdateNovelFinishState {
    public override void Start(NovelUpdateInitArgData arg) {
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateNovelFinishState(BaseNovelUpdateState other):base(other){}
    public NovelUpdateNovelFinishState():base(){}
}
