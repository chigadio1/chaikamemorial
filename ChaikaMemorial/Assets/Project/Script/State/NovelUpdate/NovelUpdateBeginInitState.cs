using System.Collections.Generic;
public class NovelUpdateBeginInitState : BaseNovelUpdateBeginInitState {
    public override void Start(NovelUpdateInitArgData arg) {
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateBeginInitState(BaseNovelUpdateState other):base(other){}
    public NovelUpdateBeginInitState():base(){}
}
