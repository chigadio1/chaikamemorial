using System.Collections.Generic;
public class NovelUpdateBeginLoadState : BaseNovelUpdateBeginLoadState {
    public override void Start(NovelUpdateInitArgData arg) {
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateBeginLoadState(BaseNovelUpdateState other):base(other){}
    public NovelUpdateBeginLoadState():base(){}
}
