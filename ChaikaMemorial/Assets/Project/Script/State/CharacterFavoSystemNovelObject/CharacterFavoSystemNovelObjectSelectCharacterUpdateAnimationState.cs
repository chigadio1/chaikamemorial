using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState : BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState():base(){}
}
