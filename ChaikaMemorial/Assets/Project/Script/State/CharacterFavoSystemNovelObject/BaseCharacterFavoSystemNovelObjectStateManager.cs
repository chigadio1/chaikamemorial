using System.Collections.Generic;
public class BaseCharacterFavoSystemNovelObjectStateManager : BaseStateManager<CharacterFavoSystemNovelObjectInitArgData,CharacterFavoSystemNovelObjectUpdateArgData,BaseCharacterFavoSystemNovelObjectState> {
    protected CharacterFavoSystemNovelObject_State_ID state_id = CharacterFavoSystemNovelObject_State_ID.BeginLoad;
    public CharacterFavoSystemNovelObject_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(CharacterFavoSystemNovelObjectInitArgData arg = null){
        switch (state_id)
        {
            case CharacterFavoSystemNovelObject_State_ID.Begin:
                state = new CharacterFavoSystemNovelObjectBeginLoadState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.BeginLoad:
                state = new CharacterFavoSystemNovelObjectFadeOutState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.FadeOut;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FadeOut:
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdate;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdate:
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateMoveState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateMove;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateMove:
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateAnimation;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateAnimation:
                state = new CharacterFavoSystemNovelObjectFadeInState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.FadeIn;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FadeIn:
                state = new CharacterFavoSystemNovelObjectFinishReleaseState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.FinishRelease;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FinishRelease:
                state = new CharacterFavoSystemNovelObjectIsLoadState(state);
                state_id = CharacterFavoSystemNovelObject_State_ID.IsLoad;
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.IsLoad:
                is_finish = true;
                state_id = CharacterFavoSystemNovelObject_State_ID.Finish;
                break;
            case CharacterFavoSystemNovelObject_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(CharacterFavoSystemNovelObject_State_ID value_state_id,CharacterFavoSystemNovelObjectInitArgData init_arg = null,CharacterFavoSystemNovelObjectUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case CharacterFavoSystemNovelObject_State_ID.Begin:
                is_finish = true;
                break;
            case CharacterFavoSystemNovelObject_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectBeginLoadState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectFadeOutState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateMove:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateMoveState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.SelectCharacterUpdateAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FadeIn:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectFadeInState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.FinishRelease:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectFinishReleaseState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.IsLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new CharacterFavoSystemNovelObjectIsLoadState(state);
                state.Start(init_arg);
                break;
            case CharacterFavoSystemNovelObject_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new CharacterFavoSystemNovelObjectBeginLoadState();
    }
}
