using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectFadeOutState : BaseCharacterFavoSystemNovelObjectFadeOutState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectFadeOutState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectFadeOutState():base(){}
}
