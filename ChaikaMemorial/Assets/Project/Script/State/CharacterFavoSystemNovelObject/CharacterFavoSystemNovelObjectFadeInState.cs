using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectFadeInState : BaseCharacterFavoSystemNovelObjectFadeInState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectFadeInState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectFadeInState():base(){}
}
