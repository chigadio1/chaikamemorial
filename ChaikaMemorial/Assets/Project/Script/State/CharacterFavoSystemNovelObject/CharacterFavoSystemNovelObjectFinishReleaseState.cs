using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectFinishReleaseState : BaseCharacterFavoSystemNovelObjectFinishReleaseState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectFinishReleaseState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectFinishReleaseState():base(){}
}
