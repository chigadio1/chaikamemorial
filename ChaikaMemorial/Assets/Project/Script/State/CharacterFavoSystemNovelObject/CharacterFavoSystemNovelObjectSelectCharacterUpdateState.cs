using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectSelectCharacterUpdateState : BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateState():base(){}
}
