using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectIsLoadState : BaseCharacterFavoSystemNovelObjectIsLoadState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectIsLoadState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectIsLoadState():base(){}
}
