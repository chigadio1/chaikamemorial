using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectBeginLoadState : BaseCharacterFavoSystemNovelObjectBeginLoadState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectBeginLoadState(BaseCharacterFavoSystemNovelObjectState other):base(other){}
    public CharacterFavoSystemNovelObjectBeginLoadState():base(){}
}
