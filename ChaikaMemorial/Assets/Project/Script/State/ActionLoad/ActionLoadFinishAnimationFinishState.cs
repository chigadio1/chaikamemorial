using System.Collections.Generic;
public class ActionLoadFinishAnimationFinishState : BaseActionLoadFinishAnimationFinishState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadFinishAnimationFinishState(BaseActionLoadState other):base(other){}
    public ActionLoadFinishAnimationFinishState():base(){}
}
