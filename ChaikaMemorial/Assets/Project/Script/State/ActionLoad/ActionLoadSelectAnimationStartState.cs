using System.Collections.Generic;
public class ActionLoadSelectAnimationStartState : BaseActionLoadSelectAnimationStartState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadSelectAnimationStartState(BaseActionLoadState other):base(other){}
    public ActionLoadSelectAnimationStartState():base(){}
}
