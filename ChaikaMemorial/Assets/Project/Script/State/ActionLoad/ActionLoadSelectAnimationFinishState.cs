using System.Collections.Generic;
public class ActionLoadSelectAnimationFinishState : BaseActionLoadSelectAnimationFinishState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadSelectAnimationFinishState(BaseActionLoadState other):base(other){}
    public ActionLoadSelectAnimationFinishState():base(){}
}
