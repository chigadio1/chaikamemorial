using System.Collections.Generic;
public class ActionLoadSelectUpdateState : BaseActionLoadSelectUpdateState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadSelectUpdateState(BaseActionLoadState other):base(other){}
    public ActionLoadSelectUpdateState():base(){}
}
