using System.Collections.Generic;
public class ActionLoadUpdateState : BaseActionLoadUpdateState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadUpdateState(BaseActionLoadState other):base(other){}
    public ActionLoadUpdateState():base(){}
}
