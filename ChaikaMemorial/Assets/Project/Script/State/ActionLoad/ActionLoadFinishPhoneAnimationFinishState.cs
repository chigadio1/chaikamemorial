using System.Collections.Generic;
public class ActionLoadFinishPhoneAnimationFinishState : BaseActionLoadFinishPhoneAnimationFinishState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadFinishPhoneAnimationFinishState(BaseActionLoadState other):base(other){}
    public ActionLoadFinishPhoneAnimationFinishState():base(){}
}
