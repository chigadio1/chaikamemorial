using System.Collections.Generic;
public class ActionLoadBeginPhoneAnimationStartState : BaseActionLoadBeginPhoneAnimationStartState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadBeginPhoneAnimationStartState(BaseActionLoadState other):base(other){}
    public ActionLoadBeginPhoneAnimationStartState():base(){}
}
