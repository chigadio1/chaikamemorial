using System.Collections.Generic;
public class ActionLoadBeginAnimationStartState : BaseActionLoadBeginAnimationStartState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadBeginAnimationStartState(BaseActionLoadState other):base(other){}
    public ActionLoadBeginAnimationStartState():base(){}
}
