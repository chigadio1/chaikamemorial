using System.Collections.Generic;
public class ActionLoadBeginLoadState : BaseActionLoadBeginLoadState {
    public override void Start(ActionLoadInitArgData arg) {
    }
    public override void Update(ActionLoadUpdateArgData arg) {
    }
    public override void Finish(ActionLoadUpdateArgData arg) {
    }
    public ActionLoadBeginLoadState(BaseActionLoadState other):base(other){}
    public ActionLoadBeginLoadState():base(){}
}
