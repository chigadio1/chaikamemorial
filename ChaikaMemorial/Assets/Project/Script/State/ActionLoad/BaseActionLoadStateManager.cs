using System.Collections.Generic;
public class BaseActionLoadStateManager : BaseStateManager<ActionLoadInitArgData,ActionLoadUpdateArgData,BaseActionLoadState> {
    protected ActionLoad_State_ID state_id = ActionLoad_State_ID.BeginLoad;
    public ActionLoad_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(ActionLoadInitArgData arg = null){
        switch (state_id)
        {
            case ActionLoad_State_ID.Begin:
                state = new ActionLoadBeginLoadState(state);
                state_id = ActionLoad_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.BeginLoad:
                state = new ActionLoadBeginPhoneAnimationStartState(state);
                state_id = ActionLoad_State_ID.BeginPhoneAnimationStart;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.BeginPhoneAnimationStart:
                state = new ActionLoadBeginAnimationStartState(state);
                state_id = ActionLoad_State_ID.BeginAnimationStart;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.BeginAnimationStart:
                state = new ActionLoadUpdateState(state);
                state_id = ActionLoad_State_ID.Update;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.Update:
                state = new ActionLoadSelectAnimationStartState(state);
                state_id = ActionLoad_State_ID.SelectAnimationStart;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.SelectAnimationStart:
                state = new ActionLoadSelectUpdateState(state);
                state_id = ActionLoad_State_ID.SelectUpdate;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.SelectUpdate:
                state = new ActionLoadSelectAnimationFinishState(state);
                state_id = ActionLoad_State_ID.SelectAnimationFinish;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.SelectAnimationFinish:
                state = new ActionLoadFinishPhoneAnimationFinishState(state);
                state_id = ActionLoad_State_ID.FinishPhoneAnimationFinish;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.FinishPhoneAnimationFinish:
                state = new ActionLoadReelaseState(state);
                state_id = ActionLoad_State_ID.Reelase;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.Reelase:
                state = new ActionLoadFinishAnimationFinishState(state);
                state_id = ActionLoad_State_ID.FinishAnimationFinish;
                state.Start(arg);
                break;
            case ActionLoad_State_ID.FinishAnimationFinish:
                is_finish = true;
                state_id = ActionLoad_State_ID.Finish;
                break;
            case ActionLoad_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(ActionLoad_State_ID value_state_id,ActionLoadInitArgData init_arg = null,ActionLoadUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case ActionLoad_State_ID.Begin:
                is_finish = true;
                break;
            case ActionLoad_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadBeginLoadState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.BeginPhoneAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadBeginPhoneAnimationStartState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.BeginAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadBeginAnimationStartState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.Update:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.SelectAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadSelectAnimationStartState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.SelectUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadSelectUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.SelectAnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadSelectAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.FinishPhoneAnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadFinishPhoneAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.Reelase:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadReelaseState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.FinishAnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionLoadFinishAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case ActionLoad_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new ActionLoadBeginLoadState();
    }
}
