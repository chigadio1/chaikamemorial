using System.Collections.Generic;
public class BaseNickNameChoiceStateManager : BaseStateManager<NickNameChoiceInitArgData,NickNameChoiceUpdateArgData,BaseNickNameChoiceState> {
    protected NickNameChoice_State_ID state_id = NickNameChoice_State_ID.BeginLoad;
    public NickNameChoice_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NickNameChoiceInitArgData arg = null){
        switch (state_id)
        {
            case NickNameChoice_State_ID.Begin:
                state = new NickNameChoiceBeginLoadState(state);
                state_id = NickNameChoice_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case NickNameChoice_State_ID.BeginLoad:
                state = new NickNameChoiceStartAnimationState(state);
                state_id = NickNameChoice_State_ID.StartAnimation;
                state.Start(arg);
                break;
            case NickNameChoice_State_ID.StartAnimation:
                state = new NickNameChoiceUpdateState(state);
                state_id = NickNameChoice_State_ID.Update;
                state.Start(arg);
                break;
            case NickNameChoice_State_ID.Update:
                state = new NickNameChoiceEndAnimationState(state);
                state_id = NickNameChoice_State_ID.EndAnimation;
                state.Start(arg);
                break;
            case NickNameChoice_State_ID.EndAnimation:
                state = new NickNameChoiceReleaseState(state);
                state_id = NickNameChoice_State_ID.Release;
                state.Start(arg);
                break;
            case NickNameChoice_State_ID.Release:
                is_finish = true;
                state_id = NickNameChoice_State_ID.Finish;
                break;
            case NickNameChoice_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NickNameChoice_State_ID value_state_id,NickNameChoiceInitArgData init_arg = null,NickNameChoiceUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NickNameChoice_State_ID.Begin:
                is_finish = true;
                break;
            case NickNameChoice_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new NickNameChoiceBeginLoadState(state);
                state.Start(init_arg);
                break;
            case NickNameChoice_State_ID.StartAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new NickNameChoiceStartAnimationState(state);
                state.Start(init_arg);
                break;
            case NickNameChoice_State_ID.Update:
                if(state != null) state.Finish(updatet_arg);
                state = new NickNameChoiceUpdateState(state);
                state.Start(init_arg);
                break;
            case NickNameChoice_State_ID.EndAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new NickNameChoiceEndAnimationState(state);
                state.Start(init_arg);
                break;
            case NickNameChoice_State_ID.Release:
                if(state != null) state.Finish(updatet_arg);
                state = new NickNameChoiceReleaseState(state);
                state.Start(init_arg);
                break;
            case NickNameChoice_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NickNameChoiceBeginLoadState();
    }
}
