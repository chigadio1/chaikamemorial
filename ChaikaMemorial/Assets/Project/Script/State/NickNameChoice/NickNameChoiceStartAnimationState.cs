using System.Collections.Generic;
public class NickNameChoiceStartAnimationState : BaseNickNameChoiceStartAnimationState {
    public override void Start(NickNameChoiceInitArgData arg) {
        gameUiCore.Instance.SettingCanvas?.NickNameChoiceObject.PlayAnim(Nick_Name_Choice_Anim_ID.StartUP);
    }
    public override void Update(NickNameChoiceUpdateArgData arg) {
        if(gameUiCore.Instance.SettingCanvas.NickNameChoiceObject.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(NickNameChoiceUpdateArgData arg) {
    }
    public NickNameChoiceStartAnimationState(BaseNickNameChoiceState other):base(other){}
    public NickNameChoiceStartAnimationState():base(){}
}
