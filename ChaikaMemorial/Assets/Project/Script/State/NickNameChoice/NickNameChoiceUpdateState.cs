using System.Collections.Generic;
public class NickNameChoiceUpdateState : BaseNickNameChoiceUpdateState {
    public override void Start(NickNameChoiceInitArgData arg) {
    }
    public override void Update(NickNameChoiceUpdateArgData arg) {
        if(gameUiCore.Instance.is_button_push)
        {
            is_end_update = true;
            
            ///好感度を取得
            var data = PlayerCore.Instance.CharacterNickNameStorage.GetNickNameData(arg.chara_id,gameUiCore.Instance.SelectNickNameID,PlayerCore.Instance.GetPlayerCoreData.GetCharacterFavorabilityData.GetFavorability(arg.chara_id));
            if (data != null)
            {
                //成功したので、入れ替える
                PlayerCore.Instance.GetPlayerCoreData.AddValueCharacterNickName(arg.chara_id, gameUiCore.Instance.SelectNickNameID);
                arg.is_success = true;
            }
            gameUiCore.Instance.is_button_push = false;
        }
    }
    public override void Finish(NickNameChoiceUpdateArgData arg) {
    }
    public NickNameChoiceUpdateState(BaseNickNameChoiceState other):base(other){}
    public NickNameChoiceUpdateState():base(){}
}
