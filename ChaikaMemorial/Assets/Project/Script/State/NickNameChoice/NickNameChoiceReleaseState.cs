using System.Collections.Generic;
using UnityEngine;

public class NickNameChoiceReleaseState : BaseNickNameChoiceReleaseState {
    public override void Start(NickNameChoiceInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.Release();
    }
    public override void Update(NickNameChoiceUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NickNameChoiceUpdateArgData arg) {
        Resources.UnloadUnusedAssets();
    }
    public NickNameChoiceReleaseState(BaseNickNameChoiceState other):base(other){}
    public NickNameChoiceReleaseState():base(){}
}
