using System.Collections.Generic;
public class NickNameChoiceEndAnimationState : BaseNickNameChoiceEndAnimationState {
    public override void Start(NickNameChoiceInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.NickNameChoiceObject.PlayAnim(Nick_Name_Choice_Anim_ID.StartUP, true);
    }
    public override void Update(NickNameChoiceUpdateArgData arg) {
        if(gameUiCore.Instance.SettingCanvas.NickNameChoiceObject.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(NickNameChoiceUpdateArgData arg) {
    }
    public NickNameChoiceEndAnimationState(BaseNickNameChoiceState other):base(other){}
    public NickNameChoiceEndAnimationState():base(){}
}
