using System.Collections.Generic;
public class NickNameChoiceBeginLoadState : BaseNickNameChoiceBeginLoadState {
    public override void Start(NickNameChoiceInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.SystemLoad(Setting_System_ID.NickNameChoice);
    }
    public override void Update(NickNameChoiceUpdateArgData arg) {
        if (gameUiCore.Instance.SettingCanvas.IsSystemLoad())
        {
            gameUiCore.Instance.SettingCanvas.ValueSystemDataInstance(Setting_System_ID.NickNameChoice);
            gameUiCore.Instance.SettingCanvas?.NickNameChoiceObject?.SetChoiceText(PlayerCore.Instance.CharacterNickNameStorage.GetNickNameDataStock(arg.chara_id));
            is_end_update = true;
        }

    }
    public override void Finish(NickNameChoiceUpdateArgData arg) {
    }
    public NickNameChoiceBeginLoadState(BaseNickNameChoiceState other):base(other){}
    public NickNameChoiceBeginLoadState():base(){}
}
