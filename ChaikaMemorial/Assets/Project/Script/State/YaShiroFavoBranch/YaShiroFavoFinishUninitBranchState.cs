using System.Collections.Generic;
public class YaShiroFavoFinishUninitBranchState : BaseYaShiroFavoFinishUninitBranchState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
        is_end_update = true;
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoFinishUninitBranchState(YaShiroFavo_Branch_State_ID other):base(other){}
}
