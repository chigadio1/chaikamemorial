using System.Collections.Generic;
public class BaseYaShiroFavoStateBranchManager : BaseStateBranchManager<YaShiroFavoInitArgData,YaShiroFavoUpdateArgData,BaseYaShiroFavoBranchState,YaShiroFavo_Branch_State_ID > {
    public BaseYaShiroFavoStateBranchManager(YaShiroFavo_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(YaShiroFavoInitArgData arg = null){
        switch (state_id)
        {
            case YaShiroFavo_Branch_State_ID.Nest01_BeginInit:
                state_id = YaShiroFavo_Branch_State_ID.Nest02_FadeIn;
                state = new YaShiroFavoFadeInBranchState(state_id);
                state.Start(arg);
                break;
            case YaShiroFavo_Branch_State_ID.Nest04_FadeOut:
                state_id = YaShiroFavo_Branch_State_ID.Nest05_FinishUninit;
                state = new YaShiroFavoFinishUninitBranchState(state_id);
                state.Start(arg);
                break;
            case YaShiroFavo_Branch_State_ID.Nest03_NovelUpdate:
                state_id = YaShiroFavo_Branch_State_ID.Nest04_FadeOut;
                state = new YaShiroFavoFadeOutBranchState(state_id);
                state.Start(arg);
                break;
            case YaShiroFavo_Branch_State_ID.Nest02_FadeIn:
                state_id = YaShiroFavo_Branch_State_ID.Nest03_NovelUpdate;
                state = new YaShiroFavoNovelUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case YaShiroFavo_Branch_State_ID.Nest05_FinishUninit:
                is_finish = true;
                state_id = YaShiroFavo_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new YaShiroFavoBeginInitBranchState(YaShiroFavo_Branch_State_ID.Nest01_BeginInit);
    }
}
