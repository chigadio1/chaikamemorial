using System.Collections.Generic;
using UnityEngine;

public class YaShiroFavoFadeInBranchState : BaseYaShiroFavoFadeInBranchState {

    public override void Start(YaShiroFavoInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.StartUP, true);
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoFadeInBranchState(YaShiroFavo_Branch_State_ID other):base(other){}
}
