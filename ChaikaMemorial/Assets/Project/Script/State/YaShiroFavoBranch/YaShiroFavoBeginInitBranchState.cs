using System.Collections.Generic;
public class YaShiroFavoBeginInitBranchState : BaseYaShiroFavoBeginInitBranchState {
    public override void Start(YaShiroFavoInitArgData arg) {
        gameUiCore.Instance.LoadGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
        if(gameUiCore.Instance.IsLoadFinishGameUIObject(gameUiCore.UI_Object_ID.NOVEL))
        {
            is_end_update = true;
        }
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoBeginInitBranchState(YaShiroFavo_Branch_State_ID other):base(other){}
}
