using System.Collections.Generic;
public class YaShiroFavoNovelUpdateBranchState : BaseYaShiroFavoNovelUpdateBranchState {
    public override void Start(YaShiroFavoInitArgData arg) {
        gameUiCore.Instance.InstanceGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
        gameUiCore.Instance.PlayEvent(9002);
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
        if(gameUiCore.Instance.IsLoadingEnd())
        {
            gameUiCore.Instance.NovelUpdate();
            if(gameUiCore.Instance.IsNovelEnd)
            {
                is_end_update = true;
            }
        }
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
        gameUiCore.Instance.AllRelease();
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
        gameUiCore.Instance.PlayEnd();
    }
    public YaShiroFavoNovelUpdateBranchState(YaShiroFavo_Branch_State_ID other):base(other){}
}
