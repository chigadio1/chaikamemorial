using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 基礎管理遷移クラス
/// </summary>

public class BaseStateManager<T,E,S>  where T : BaseStateArgData where E : BaseStateArgData where S : BaseState<T,E>
{
    protected S state; //ステート
    public S getState { get { return state; } }
    protected bool is_finish; //終了フラグ
    public bool IsFinish { get { return is_finish;} }
    protected bool is_start; //開始フラグ

    // Start is called before the first frame update
    public  void Start(T arg = null)
    {
        if (is_start) return;
        if (state == null) StartInstance();
        state.Start(arg);
        is_start = true;
        ManualStart(arg);
    }

    public virtual void StartInstance() { }

    public virtual void ManualStart(T arg = null)
    {

    }


    // Update is called once per frame
    public  void Update(T init_arg = null,E update_arg = null)
    {
        if (is_start == false) Start(init_arg);
        if (is_finish) return;
        state.Update(update_arg);
        ManualUpdate(update_arg);
        if(state.getIsEndUpdate)
        {
            state.Finish(update_arg);
            NextStateChange(init_arg);
        }

    }



    public virtual void ManualUpdate(E arg = null)
    {

    }

    public virtual void NextStateChange(T init_arg = null)
    {

    }

    



}

public class BaseStateBranchManager<T, E, S, P> : BaseStateManager<T, E, S> where T : BaseStateArgData where E : BaseStateArgData where S : BaseState<T, E> where P : Enum
{

    /// 前のステート
    /// </summary>
    protected P state_id;

    public BaseStateBranchManager(P value_id) { state_id = value_id; }






    public BaseStateBranchManager() :base(){}

}
