using System.Collections.Generic;
public class NovelUpdateBeginInitBranchState : BaseNovelUpdateBeginInitBranchState {
    public override void Start(NovelUpdateInitArgData arg) {
        gameUiCore.Instance.LoadGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
        if(gameUiCore.Instance.IsLoadFinishGameUIObject(gameUiCore.UI_Object_ID.NOVEL))
        {
            gameUiCore.Instance.InstanceGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
            is_end_update = true;
        }
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateBeginInitBranchState(NovelUpdate_Branch_State_ID other):base(other){}
}
