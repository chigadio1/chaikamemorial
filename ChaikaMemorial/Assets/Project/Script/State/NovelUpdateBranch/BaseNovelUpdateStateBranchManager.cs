using System.Collections.Generic;
public class BaseNovelUpdateStateBranchManager : BaseStateBranchManager<NovelUpdateInitArgData,NovelUpdateUpdateArgData,BaseNovelUpdateBranchState,NovelUpdate_Branch_State_ID > {
    public BaseNovelUpdateStateBranchManager(NovelUpdate_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(NovelUpdateInitArgData arg = null){
        switch (state_id)
        {
            case NovelUpdate_Branch_State_ID.Nest01_BeginInit:
                state_id = NovelUpdate_Branch_State_ID.Nest02_BeginLoad;
                state = new NovelUpdateBeginLoadBranchState(state_id);
                state.Start(arg);
                break;
            case NovelUpdate_Branch_State_ID.Nest02_BeginLoad:
                state_id = NovelUpdate_Branch_State_ID.Nest03_NovelUpdate;
                state = new NovelUpdateNovelUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case NovelUpdate_Branch_State_ID.Nest03_NovelUpdate:
                state_id = NovelUpdate_Branch_State_ID.Nest04_NovelFinish;
                state = new NovelUpdateNovelFinishBranchState(state_id);
                state.Start(arg);
                break;
            case NovelUpdate_Branch_State_ID.Nest04_NovelFinish:
                is_finish = true;
                state_id = NovelUpdate_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new NovelUpdateBeginInitBranchState(NovelUpdate_Branch_State_ID.Nest01_BeginInit);
    }
}
