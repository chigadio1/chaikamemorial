using System.Collections.Generic;
using UnityEngine;

public class NovelUpdateNovelFinishBranchState : BaseNovelUpdateNovelFinishBranchState {
    public override void Start(NovelUpdateInitArgData arg) {
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
        gameUiCore.Instance.AllRelease();
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
        Resources.UnloadUnusedAssets();
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateNovelFinishBranchState(NovelUpdate_Branch_State_ID other):base(other){}
}
