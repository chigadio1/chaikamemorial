using System.Collections.Generic;
using UnityEngine;

public class NovelUpdateNovelUpdateBranchState : BaseNovelUpdateNovelUpdateBranchState {


    public override void Start(NovelUpdateInitArgData arg) {
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
        gameUiCore.Instance.NovelUpdate();
        if(gameUiCore.Instance.IsNovelEnd)
        {
            is_end_update = true;
        }
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
        gameUiCore.Instance.AllRelease();
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
    }
    public NovelUpdateNovelUpdateBranchState(NovelUpdate_Branch_State_ID other):base(other){}


}
