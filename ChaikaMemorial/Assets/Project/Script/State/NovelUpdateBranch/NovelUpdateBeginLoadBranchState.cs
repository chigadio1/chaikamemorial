using System.Collections.Generic;
public class NovelUpdateBeginLoadBranchState : BaseNovelUpdateBeginLoadBranchState {
    public override void Start(NovelUpdateInitArgData arg) {
        gameUiCore.Instance.PlayEvent(arg.event_id);
    }
    public override void Update(NovelUpdateUpdateArgData arg) {
        if(gameUiCore.Instance.IsLoadingEnd())
        {
            is_end_update = true;
        }
    }
    public override void Finish(NovelUpdateUpdateArgData arg) {
    }
    public NovelUpdateBeginLoadBranchState(NovelUpdate_Branch_State_ID other):base(other){}
}
