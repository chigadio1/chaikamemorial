using System.Collections.Generic;
public class MainGameBeginGameState : BaseMainGameBeginGameState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginGameState(BaseMainGameState other):base(other){}
    public MainGameBeginGameState():base(){}
}
