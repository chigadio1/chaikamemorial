using System.Collections.Generic;
public class MainGameGameUpdateState : BaseMainGameGameUpdateState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameGameUpdateState(BaseMainGameState other):base(other){}
    public MainGameGameUpdateState():base(){}
}
