using System.Collections.Generic;
public class MainGameBeginStartState : BaseMainGameBeginStartState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginStartState(BaseMainGameState other):base(other){}
    public MainGameBeginStartState():base(){}
}
