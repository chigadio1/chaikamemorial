using System.Collections.Generic;
public class MainGameNameUpdateState : BaseMainGameNameUpdateState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameNameUpdateState(BaseMainGameState other):base(other){}
    public MainGameNameUpdateState():base(){}
}
