using System.Collections.Generic;
public class MainGameFinishTitleState : BaseMainGameFinishTitleState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameFinishTitleState(BaseMainGameState other):base(other){}
    public MainGameFinishTitleState():base(){}
}
