using System;
public enum MainGame_State_ID {
    Begin, //初期
    BeginStart, //
    BeginTitle, //
    TitleUpdate, //
    FinishTitle, //
    BeginGame, //
    GameUpdate, //
    FinishGame, //
    BeginName, //
    NameUpdate, //
    FinishName, //
    Finish //終了
}
