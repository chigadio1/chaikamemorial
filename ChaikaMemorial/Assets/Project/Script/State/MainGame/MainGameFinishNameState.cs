using System.Collections.Generic;
public class MainGameFinishNameState : BaseMainGameFinishNameState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameFinishNameState(BaseMainGameState other):base(other){}
    public MainGameFinishNameState():base(){}
}
