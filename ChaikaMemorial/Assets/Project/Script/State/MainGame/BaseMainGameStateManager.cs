using System.Collections.Generic;
public class BaseMainGameStateManager : BaseStateManager<MainGameInitArgData,MainGameUpdateArgData,BaseMainGameState> {
    protected MainGame_State_ID state_id = MainGame_State_ID.BeginStart;
    public MainGame_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(MainGameInitArgData arg = null){
        switch (state_id)
        {
            case MainGame_State_ID.Begin:
                state = new MainGameBeginStartState(state);
                state_id = MainGame_State_ID.BeginStart;
                state.Start(arg);
                break;
            case MainGame_State_ID.BeginStart:
                state = new MainGameBeginTitleState(state);
                state_id = MainGame_State_ID.BeginTitle;
                state.Start(arg);
                break;
            case MainGame_State_ID.BeginTitle:
                state = new MainGameTitleUpdateState(state);
                state_id = MainGame_State_ID.TitleUpdate;
                state.Start(arg);
                break;
            case MainGame_State_ID.TitleUpdate:
                state = new MainGameFinishTitleState(state);
                state_id = MainGame_State_ID.FinishTitle;
                state.Start(arg);
                break;
            case MainGame_State_ID.FinishTitle:
                state = new MainGameBeginGameState(state);
                state_id = MainGame_State_ID.BeginGame;
                state.Start(arg);
                break;
            case MainGame_State_ID.BeginGame:
                state = new MainGameGameUpdateState(state);
                state_id = MainGame_State_ID.GameUpdate;
                state.Start(arg);
                break;
            case MainGame_State_ID.GameUpdate:
                state = new MainGameFinishGameState(state);
                state_id = MainGame_State_ID.FinishGame;
                state.Start(arg);
                break;
            case MainGame_State_ID.FinishGame:
                state = new MainGameBeginNameState(state);
                state_id = MainGame_State_ID.BeginName;
                state.Start(arg);
                break;
            case MainGame_State_ID.BeginName:
                state = new MainGameNameUpdateState(state);
                state_id = MainGame_State_ID.NameUpdate;
                state.Start(arg);
                break;
            case MainGame_State_ID.NameUpdate:
                state = new MainGameFinishNameState(state);
                state_id = MainGame_State_ID.FinishName;
                state.Start(arg);
                break;
            case MainGame_State_ID.FinishName:
                is_finish = true;
                state_id = MainGame_State_ID.Finish;
                break;
            case MainGame_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(MainGame_State_ID value_state_id,MainGameInitArgData init_arg = null,MainGameUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case MainGame_State_ID.Begin:
                is_finish = true;
                break;
            case MainGame_State_ID.BeginStart:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameBeginStartState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.BeginTitle:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameBeginTitleState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.TitleUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameTitleUpdateState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.FinishTitle:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameFinishTitleState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.BeginGame:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameBeginGameState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.GameUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameGameUpdateState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.FinishGame:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameFinishGameState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.BeginName:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameBeginNameState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.NameUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameNameUpdateState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.FinishName:
                if(state != null) state.Finish(updatet_arg);
                state = new MainGameFinishNameState(state);
                state.Start(init_arg);
                break;
            case MainGame_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new MainGameBeginStartState();
    }
}
