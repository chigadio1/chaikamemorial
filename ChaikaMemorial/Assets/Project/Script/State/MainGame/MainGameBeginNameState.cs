using System.Collections.Generic;
public class MainGameBeginNameState : BaseMainGameBeginNameState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginNameState(BaseMainGameState other):base(other){}
    public MainGameBeginNameState():base(){}
}
