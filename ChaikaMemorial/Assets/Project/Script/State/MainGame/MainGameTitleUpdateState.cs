using System.Collections.Generic;
public class MainGameTitleUpdateState : BaseMainGameTitleUpdateState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameTitleUpdateState(BaseMainGameState other):base(other){}
    public MainGameTitleUpdateState():base(){}
}
