using System.Collections.Generic;
public class MainGameBeginTitleState : BaseMainGameBeginTitleState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginTitleState(BaseMainGameState other):base(other){}
    public MainGameBeginTitleState():base(){}
}
