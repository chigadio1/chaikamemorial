using System.Collections.Generic;
public class MainGameFinishGameState : BaseMainGameFinishGameState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameFinishGameState(BaseMainGameState other):base(other){}
    public MainGameFinishGameState():base(){}
}
