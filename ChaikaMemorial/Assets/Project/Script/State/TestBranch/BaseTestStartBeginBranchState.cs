using System.Collections.Generic;
public class BaseTestStartBeginBranchState : BaseTestBranchState{
    public BaseTestStartBeginBranchState(Test_Branch_State_ID other):base(other){}
    public virtual Test_Branch_State_ID BranchStartBeginState(){
        if(BranchStartBeginToUpdateExec()) return Test_Branch_State_ID.StartBeginUpdateExec;
        else if(BranchStartBeginToEndFinish()) return Test_Branch_State_ID.StartBeginEndFinish;
     return Test_Branch_State_ID.StartBeginUpdateExec;
     }
    public virtual bool BranchStartBeginToUpdateExec(){return true;}
    public virtual bool BranchStartBeginToEndFinish(){return true;}
    public override Test_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case Test_Branch_State_ID.StartBegin:
                return BranchStartBeginState();
        }
     return Test_Branch_State_ID.StartBeginUpdateExec;
     }
}
