using System.Collections.Generic;
public class BaseTestStateBranchManager : BaseStateBranchManager<TestInitArgData,TestUpdateArgData,BaseTestBranchState,Test_Branch_State_ID > {
    public override void NextStateChange(TestInitArgData arg = null){
        switch (state_id)
        {
            case Test_Branch_State_ID.StartBegin:
                Test_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case Test_Branch_State_ID.StartBeginUpdateExec:
                        state = new TestUpdateExecBranchState(state_id);
                        state_id = Test_Branch_State_ID.StartBeginUpdateExec;
                        break;
                    case Test_Branch_State_ID.StartBeginEndFinish:
                        state = new TestEndFinishBranchState(state_id);
                        state_id = Test_Branch_State_ID.StartBeginEndFinish;
                        break;
                 }
                break;
            case Test_Branch_State_ID.StartBeginUpdateExec:
                state = new TestEndFinishBranchState(state_id);
                state_id = Test_Branch_State_ID.StartBeginUpdateExecEndFinish;
                state.Start(arg);
                break;
            case Test_Branch_State_ID.StartBeginEndFinish:
                state = new TestUpdateExecBranchState(state_id);
                state_id = Test_Branch_State_ID.StartBeginEndFinishUpdateExec;
                state.Start(arg);
                break;
            case Test_Branch_State_ID.StartBeginEndFinishUpdateExec:
                is_finish = true;
                state_id = Test_Branch_State_ID.Finish;
                break;
            case Test_Branch_State_ID.StartBeginUpdateExecEndFinish:
                is_finish = true;
                state_id = Test_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new TestStartBeginBranchState(Test_Branch_State_ID.StartBegin);
    }
}
