using System.Collections.Generic;
public class BaseTitleUINoLoadGameInputUpdateBranchState : BaseTitleUIBranchState{
    public BaseTitleUINoLoadGameInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
    public virtual TitleUI_Branch_State_ID BranchNest13_NoLoadGameInputUpdateState(){
        if(BranchNest13_NoLoadGameInputUpdateToGameStart()) return TitleUI_Branch_State_ID.Nest14_GameStart;
        else if(BranchNest13_NoLoadGameInputUpdateToNewLoadInputUpdate()) return TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate;
     return TitleUI_Branch_State_ID.Nest14_GameStart;
     }
    public virtual bool BranchNest13_NoLoadGameInputUpdateToGameStart(){return true;}
    public virtual bool BranchNest13_NoLoadGameInputUpdateToNewLoadInputUpdate(){return true;}
    public override TitleUI_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleUI_Branch_State_ID.Nest13_NoLoadGameInputUpdate:
                return BranchNest13_NoLoadGameInputUpdateState();
        }
     return TitleUI_Branch_State_ID.Nest14_GameStart;
     }
}
