using System.Collections.Generic;
public class TitleUIBeginStartUpBranchState : BaseTitleUIBeginStartUpBranchState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, 0.0f);
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(0.0f);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        if (SoundGameCore.Instance.IsLoadEnd() == false) return;

        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.BGM, (int)Default_Title_Sound_ID.Title_BGM);
        is_end_update = true;
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIBeginStartUpBranchState(TitleUI_Branch_State_ID other):base(other){}
}
