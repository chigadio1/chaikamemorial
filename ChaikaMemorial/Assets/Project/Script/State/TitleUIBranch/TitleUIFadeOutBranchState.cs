using System.Collections.Generic;
using UnityEngine;

public class TitleUIFadeOutBranchState : BaseTitleUIFadeOutBranchState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(1.0f);

        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.BGM, (int)Default_Title_Sound_ID.Title_BGM);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime;
        if (add_calc >= 1.0f)
        {
            is_end_update = true;
        }
        add_calc = Mathf.Clamp(add_calc, 0.0f, 1.0f);

        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(Mathf.Abs(add_calc / 1.0f - 1.0f));
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        add_calc = 0.0f;
        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(0.0f);
    }
    public TitleUIFadeOutBranchState(TitleUI_Branch_State_ID other):base(other){}
}
