using System.Collections.Generic;
using UnityEngine;

public class TitleUINoLoadGameInputUpdateBranchState : BaseTitleUINoLoadGameInputUpdateBranchState {
    TitleLoadStateBranchManager manager = new TitleLoadStateBranchManager();
    TitleLoadUpdateArgData update_arg = new TitleLoadUpdateArgData();

    bool is_game = false;
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        manager.Update(null,update_arg);
        if(manager.IsFinish)
        {
            is_end_update = true;
            is_game = update_arg.is_game;
        }

    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINoLoadGameInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}

    public override bool BranchNest13_NoLoadGameInputUpdateToGameStart()
    {
        return is_game == true;
    }

    public override bool BranchNest13_NoLoadGameInputUpdateToNewLoadInputUpdate()
    {
        return is_game == false;
    }
}
