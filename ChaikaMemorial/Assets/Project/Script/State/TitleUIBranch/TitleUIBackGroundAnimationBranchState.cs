using System.Collections.Generic;
using UnityEngine;

public class TitleUIBackGroundAnimationBranchState : BaseTitleUIBackGroundAnimationBranchState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime / 2.0f;
        add_calc = Mathf.Clamp(add_calc, 0.0f, 0.5f);

        if (add_calc >= 0.5f) is_end_update = true;

        TitleUiCore.Instance.TitleBackGroundCanvas.SetChange(add_calc);
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, Mathf.Abs(add_calc / 0.5f) - 1.0f);
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.NEW_GAME, Mathf.Abs(add_calc / 0.5f));
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.LOAD_GAME, Mathf.Abs(add_calc / 0.5f));
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(Mathf.Lerp(2.0f, 0.0f,add_calc / 0.5f));
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIBackGroundAnimationBranchState(TitleUI_Branch_State_ID other):base(other){}
}
