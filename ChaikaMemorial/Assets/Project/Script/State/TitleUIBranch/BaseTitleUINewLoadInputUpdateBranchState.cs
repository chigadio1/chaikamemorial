using System.Collections.Generic;
public class BaseTitleUINewLoadInputUpdateBranchState : BaseTitleUIBranchState{
    public BaseTitleUINewLoadInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
    public virtual TitleUI_Branch_State_ID BranchNest13_NewLoadInputUpdateState(){
        if(BranchNest13_NewLoadInputUpdateToBackGroundAnimationEnd()) return TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd;
        else if(BranchNest13_NewLoadInputUpdateToNewGameIsSaveData()) return TitleUI_Branch_State_ID.Nest13_NewGameIsSaveData;
        else if(BranchNest13_NewLoadInputUpdateToLoadGameUpdate()) return TitleUI_Branch_State_ID.Nest13_LoadGameUpdate;
     return TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd;
     }
    public virtual bool BranchNest13_NewLoadInputUpdateToBackGroundAnimationEnd(){return true;}
    public virtual bool BranchNest13_NewLoadInputUpdateToNewGameIsSaveData(){return true;}
    public virtual bool BranchNest13_NewLoadInputUpdateToLoadGameUpdate(){return true;}
    public override TitleUI_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate:
                return BranchNest13_NewLoadInputUpdateState();
        }
     return TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd;
     }
}
