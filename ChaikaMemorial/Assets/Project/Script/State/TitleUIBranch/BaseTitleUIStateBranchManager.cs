using System.Collections.Generic;
public class BaseTitleUIStateBranchManager : BaseStateBranchManager<TitleUIInitArgData,TitleUIUpdateArgData,BaseTitleUIBranchState,TitleUI_Branch_State_ID > {
    public BaseTitleUIStateBranchManager(TitleUI_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(TitleUIInitArgData arg = null){
        switch (state_id)
        {
            case TitleUI_Branch_State_ID.Nest02_FadeOut:
                state_id = TitleUI_Branch_State_ID.Nest03_StartImageAnimation;
                state = new TitleUIStartImageAnimationBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest03_StartImageAnimation:
                state_id = TitleUI_Branch_State_ID.Nest13_StartInputUpdate;
                state = new TitleUIStartInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest13_StartInputUpdate:
                state_id = TitleUI_Branch_State_ID.Nest07_BackGroundAnimation;
                state = new TitleUIBackGroundAnimationBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest07_BackGroundAnimation:
                state_id = TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate;
                state = new TitleUINewLoadInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate:
                {
                TitleUI_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd:
                        state_id = TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd;
                        state = new TitleUIBackGroundAnimationEndBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleUI_Branch_State_ID.Nest13_NewGameIsSaveData:
                        state_id = TitleUI_Branch_State_ID.Nest13_NewGameIsSaveData;
                        state = new TitleUINewGameIsSaveDataBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleUI_Branch_State_ID.Nest13_LoadGameUpdate:
                        state_id = TitleUI_Branch_State_ID.Nest13_LoadGameUpdate;
                        state = new TitleUILoadGameUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleUI_Branch_State_ID.Nest13_NewImageAnimation:
                state_id = TitleUI_Branch_State_ID.Nest13_NewInputUpdate;
                state = new TitleUINewInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest13_NewInputUpdate:
                {
                TitleUI_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd:
                        state_id = TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd;
                        state = new TitleUINewImageAnimationEndBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleUI_Branch_State_ID.Nest14_FadeIn:
                        state_id = TitleUI_Branch_State_ID.Nest14_FadeIn;
                        state = new TitleUIFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleUI_Branch_State_ID.Nest15_GameStart:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
            case TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd:
                state_id = TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate;
                state = new TitleUINewLoadInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest13_LoadGameUpdate:
                state_id = TitleUI_Branch_State_ID.Nest13_NoLoadGameInputUpdate;
                state = new TitleUINoLoadGameInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest01_NoLoadGameImageAnimation:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
            case TitleUI_Branch_State_ID.Nest13_NoLoadGameInputUpdate:
                {
                TitleUI_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleUI_Branch_State_ID.Nest14_GameStart:
                        state_id = TitleUI_Branch_State_ID.Nest14_GameStart;
                        state = new TitleUIGameStartBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate:
                        state_id = TitleUI_Branch_State_ID.Nest13_NewLoadInputUpdate;
                        state = new TitleUINewLoadInputUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleUI_Branch_State_ID.Nest01_NoLoadGameImageAnimationEnd:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
            case TitleUI_Branch_State_ID.Nest13_BackGroundAnimationEnd:
                state_id = TitleUI_Branch_State_ID.Nest13_StartInputUpdate;
                state = new TitleUIStartInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest14_FadeIn:
                state_id = TitleUI_Branch_State_ID.Nest15_GameStart;
                state = new TitleUIGameStartBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest01_BeginStartUp:
                state_id = TitleUI_Branch_State_ID.Nest02_FadeOut;
                state = new TitleUIFadeOutBranchState(state_id);
                state.Start(arg);
                break;
            case TitleUI_Branch_State_ID.Nest13_NewGameIsSaveData:
                {
                TitleUI_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleUI_Branch_State_ID.Nest13_NewImageAnimation:
                        state_id = TitleUI_Branch_State_ID.Nest13_NewImageAnimation;
                        state = new TitleUINewImageAnimationBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleUI_Branch_State_ID.Nest14_FadeIn:
                        state_id = TitleUI_Branch_State_ID.Nest14_FadeIn;
                        state = new TitleUIFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleUI_Branch_State_ID.Nest01_FadeIn:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
            case TitleUI_Branch_State_ID.Nest01_GameStart:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
            case TitleUI_Branch_State_ID.Nest14_GameStart:
                is_finish = true;
                state_id = TitleUI_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new TitleUIBeginStartUpBranchState(TitleUI_Branch_State_ID.Nest01_BeginStartUp);
    }
}
