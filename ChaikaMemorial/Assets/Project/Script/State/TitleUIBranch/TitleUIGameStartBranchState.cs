using System.Collections.Generic;
public class TitleUIGameStartBranchState : BaseTitleUIGameStartBranchState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        if (PlayerCore.Instance.GetIsLoadFinish == true)
        {
            is_end_update = true;
        }
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIGameStartBranchState(TitleUI_Branch_State_ID other):base(other){}
}
