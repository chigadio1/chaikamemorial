using System.Collections.Generic;
using UnityEngine;

public class TitleUINewLoadInputUpdateBranchState : BaseTitleUINewLoadInputUpdateBranchState {

    int select_index = 0;
    bool cancel_flag = false;

    float[] select_pos_y = new float[2] {-110.0f,-438.0f };
    public enum Select_ID
    {
        NewGame,
        LoadGame,
        Max
    }
    public override void Start(TitleUIInitArgData arg) {
        cancel_flag = false;
        select_index = 0;
        is_end_update = false;
        add_calc = 0.0f;
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime / 2.0f;

        add_calc = Mathf.Clamp(add_calc, 0.0f, 0.5f);
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(Mathf.Lerp(0.0f, 2.0f, add_calc / 0.5f));
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            ++select_index;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Select);

        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            --select_index;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Select);
        }
        else if (Input.GetKeyDown(KeyCode.Backspace))
        {
            cancel_flag = true;
            is_end_update = true;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Select);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            is_end_update = true;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Sub_Select_Space);
        }
        if (select_index < 0)
        {
            select_index = (int)Select_ID.LoadGame;
        }
        select_index = Mathf.Abs(select_index);

        select_index = select_index % (int)Select_ID.Max;

        TitleUiCore.Instance.TitleSelectCanvas.SetPositionY(select_pos_y[select_index]);


       
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewLoadInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}

    public override bool BranchNest13_NewLoadInputUpdateToBackGroundAnimationEnd()
    {
        return cancel_flag == true;
    }

    public override bool BranchNest13_NewLoadInputUpdateToLoadGameUpdate()
    {
        return select_index == (int)Select_ID.LoadGame && cancel_flag == false;
    }

    public override bool BranchNest13_NewLoadInputUpdateToNewGameIsSaveData()
    {
        return select_index == (int)Select_ID.NewGame && cancel_flag == false;
    }
}
