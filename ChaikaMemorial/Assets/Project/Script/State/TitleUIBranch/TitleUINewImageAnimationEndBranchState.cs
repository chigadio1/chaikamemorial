using System.Collections.Generic;
using UnityEngine;

public class TitleUINewImageAnimationEndBranchState : BaseTitleUINewImageAnimationEndBranchState {
    public float max_time = 0.5f;
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime;

        add_calc = Mathf.Clamp(add_calc, 0.0f, max_time);
        float value_alpha = Mathf.Lerp(1.0f, 0.0f, add_calc / max_time);
        float value_alpha_fade = Mathf.Lerp(0.5f, 0.0f, add_calc / max_time);
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.NOTE, value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeAlpha(Title_Select_Type_ID.YES, value_alpha);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeAlpha(Title_Select_Type_ID.NO, value_alpha);

        if(add_calc >= max_time) is_end_update = true;
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        add_calc = 0.0f;
        TitleUiCore.Instance.TitleSelectSubCanvas.OffSelectMessage(Title_Select_Message_ID.NOTE);
        float value_alpha = 0.0f;
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(0.0f);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.NOTE, 0.0f);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeAlpha(Title_Select_Type_ID.YES, value_alpha);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeAlpha(Title_Select_Type_ID.NO, value_alpha);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.YES, 1.0f);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.NO, 1.0f);
    }
    public TitleUINewImageAnimationEndBranchState(TitleUI_Branch_State_ID other):base(other){}
}
