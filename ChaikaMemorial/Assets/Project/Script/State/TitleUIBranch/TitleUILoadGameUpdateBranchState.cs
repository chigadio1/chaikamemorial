using System.Collections.Generic;
public class TitleUILoadGameUpdateBranchState : BaseTitleUILoadGameUpdateBranchState {

    bool is_save_data = false;
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        is_save_data = false;
        if(is_save_data == true)
        {
            GameManagerCore.Instance.OffNewGame();
        }
        is_end_update = true;
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }


    public TitleUILoadGameUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
}
