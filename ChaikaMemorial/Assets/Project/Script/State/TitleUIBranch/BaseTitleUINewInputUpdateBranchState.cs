using System.Collections.Generic;
public class BaseTitleUINewInputUpdateBranchState : BaseTitleUIBranchState{
    public BaseTitleUINewInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
    public virtual TitleUI_Branch_State_ID BranchNest13_NewInputUpdateState(){
        if(BranchNest13_NewInputUpdateToNewImageAnimationEnd()) return TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd;
        else if(BranchNest13_NewInputUpdateToFadeIn()) return TitleUI_Branch_State_ID.Nest14_FadeIn;
     return TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd;
     }
    public virtual bool BranchNest13_NewInputUpdateToNewImageAnimationEnd(){return true;}
    public virtual bool BranchNest13_NewInputUpdateToFadeIn(){return true;}
    public override TitleUI_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleUI_Branch_State_ID.Nest13_NewInputUpdate:
                return BranchNest13_NewInputUpdateState();
        }
     return TitleUI_Branch_State_ID.Nest11_NewImageAnimationEnd;
     }
}
