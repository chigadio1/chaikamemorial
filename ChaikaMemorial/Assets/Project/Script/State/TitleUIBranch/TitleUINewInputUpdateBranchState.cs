using System.Collections.Generic;
using UnityEngine;

public class TitleUINewInputUpdateBranchState : BaseTitleUINewInputUpdateBranchState {

    int select_index = 0;
    public override void Start(TitleUIInitArgData arg) {
        select_index = 0;
    }
    public override void Update(TitleUIUpdateArgData arg) {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            select_index--;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Sub_Select);
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            select_index++;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Sub_Select);
        }
        select_index = select_index % (int)Title_Select_Type_ID.MAX;
        select_index = Mathf.Abs(select_index);
        select_index = Mathf.Clamp(select_index, 0, (int)Title_Select_Type_ID.NO);

        if(select_index == (int)Title_Select_Type_ID.YES)
        {
            TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.YES, 1.0f);
            TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.NO, 0.5f);
        }
        else
        {
            TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.YES, 0.5f);
            TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectTypeColor(Title_Select_Type_ID.NO, 1.0f);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(select_index == (int)Title_Select_Type_ID.YES)
            {
                GameManagerCore.Instance.OnNewGame();
            }
            else
            {
                GameManagerCore.Instance.OffNewGame();
            }
            is_end_update = true;
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Sub_Select_Space);
        }
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }

    public override bool BranchNest13_NewInputUpdateToFadeIn()
    {
        return select_index == (int)Title_Select_Type_ID.YES;
    }

    public override bool BranchNest13_NewInputUpdateToNewImageAnimationEnd()
    {
        return select_index == (int)Title_Select_Type_ID.NO;
    }

    public TitleUINewInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
}
