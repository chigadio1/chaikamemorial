using System.Collections.Generic;
using UnityEngine;

public class TitleUINoLoadGameImageAnimationEndBranchState : BaseTitleUINoLoadGameImageAnimationEndBranchState {
    float max_time = 0.5f;
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime;

        add_calc = Mathf.Clamp(add_calc, 0.0f, max_time);
        float value_alpha_fade = Mathf.Lerp(0.5f, 0.0f, add_calc / max_time);
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.ERROR, value_alpha_fade);
        if (add_calc >= max_time) is_end_update = true;

    }
    public override void Finish(TitleUIUpdateArgData arg) {
        float value_alpha_fade = 0.0f;
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.ERROR, value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.OffSelectMessage(Title_Select_Message_ID.ERROR);
    }
    public TitleUINoLoadGameImageAnimationEndBranchState(TitleUI_Branch_State_ID other):base(other){}
}
