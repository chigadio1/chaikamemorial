using System.Collections.Generic;
public class TitleUINewGameIsSaveDataBranchState : BaseTitleUINewGameIsSaveDataBranchState {
    public enum IS_TYPE
    {
        NONE,
        FIND
    }
    IS_TYPE is_type_id = IS_TYPE.NONE;
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {

            is_type_id = IS_TYPE.NONE;
            is_end_update = true;
            GameManagerCore.Instance.OnNewGame();
        
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }

    public override bool BranchNest13_NewGameIsSaveDataToFadeIn()
    {
        return is_type_id == IS_TYPE.NONE;
    }
    public override bool BranchNest13_NewGameIsSaveDataToNewImageAnimation()
    {
        return is_type_id == IS_TYPE.FIND;
    }
    public TitleUINewGameIsSaveDataBranchState(TitleUI_Branch_State_ID other):base(other){}
}
