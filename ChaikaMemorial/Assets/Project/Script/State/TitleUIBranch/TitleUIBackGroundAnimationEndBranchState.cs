using System.Collections.Generic;
using UnityEngine;

public class TitleUIBackGroundAnimationEndBranchState : BaseTitleUIBackGroundAnimationEndBranchState {

    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime / 2.0f;
        add_calc = Mathf.Clamp(add_calc, 0.0f, 0.5f);

        if (add_calc >= 0.5f) is_end_update = true;

        float value = Mathf.Lerp(0.5f, 0.0f, add_calc / 0.5f);
        float value_start_alpha = Mathf.Lerp(0.0f, 1.0f, add_calc / 0.5f);
        float value_alpha = Mathf.Lerp(1.0f, 0.0f, add_calc / 0.5f);
        TitleUiCore.Instance.TitleBackGroundCanvas.SetChange(value);
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START,value_start_alpha);
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.NEW_GAME,  value);
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.LOAD_GAME, value);
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        TitleUiCore.Instance.TitleBackGroundCanvas.SetChange(0.0f);
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(2.0f);
        TitleUiCore.Instance.TitleSelectCanvas.SetPositionY(-296.0f);
    }
    public TitleUIBackGroundAnimationEndBranchState(TitleUI_Branch_State_ID other):base(other){}
}
