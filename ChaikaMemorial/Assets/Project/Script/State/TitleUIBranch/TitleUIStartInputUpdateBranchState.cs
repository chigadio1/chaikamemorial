using System.Collections.Generic;
using UnityEngine;

public class TitleUIStartInputUpdateBranchState : BaseTitleUIStartInputUpdateBranchState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
    }
    public override void Update(TitleUIUpdateArgData arg) {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, (int)Default_Title_Sound_ID.Sub_Select_Space);
            is_end_update = true;
        }
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIStartInputUpdateBranchState(TitleUI_Branch_State_ID other):base(other){}
}
