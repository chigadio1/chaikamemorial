using System.Collections.Generic;
using UnityEngine;

public class TitleUIStartImageAnimationBranchState : BaseTitleUIStartImageAnimationBranchState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, 0.0f);
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(0.0f);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime / 2.0f;
        add_calc = Mathf.Clamp(add_calc, 0.0f, 0.5f);

        if (add_calc >= 0.5f) is_end_update = true;

        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, Mathf.Abs(add_calc / 0.5f));
        TitleUiCore.Instance.TitleSelectCanvas.SetWidthScale(Mathf.Lerp(0.0f, 2.0f, add_calc / 0.5f));
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIStartImageAnimationBranchState(TitleUI_Branch_State_ID other):base(other){}
}
