using System.Collections.Generic;
using UnityEngine;

public class TitleUINoLoadGameImageAnimationBranchState : BaseTitleUINoLoadGameImageAnimationBranchState {
    public float max_time = 0.5f;
    public override void Start(TitleUIInitArgData arg) {
        TitleUiCore.Instance.TitleSelectSubCanvas.OnSelectMessage(Title_Select_Message_ID.ERROR);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime;

        add_calc = Mathf.Clamp(add_calc, 0.0f, max_time);
        float value_alpha_fade = Mathf.Lerp(0.0f, 0.5f, add_calc / max_time);
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.ERROR, value_alpha_fade);
        if (add_calc >= max_time) is_end_update = true;
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        float value_alpha_fade =0.5f;
        TitleUiCore.Instance.SelectFadeCanvas.SetFadeAlpha(value_alpha_fade);
        TitleUiCore.Instance.TitleSelectSubCanvas.SetSelectMessageAlpha(Title_Select_Message_ID.ERROR, value_alpha_fade);
    }
    public TitleUINoLoadGameImageAnimationBranchState(TitleUI_Branch_State_ID other):base(other){}
}
