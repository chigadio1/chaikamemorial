using System.Collections.Generic;
public class BaseTitleUINewGameIsSaveDataBranchState : BaseTitleUIBranchState{
    public BaseTitleUINewGameIsSaveDataBranchState(TitleUI_Branch_State_ID other):base(other){}
    public virtual TitleUI_Branch_State_ID BranchNest13_NewGameIsSaveDataState(){
        if(BranchNest13_NewGameIsSaveDataToNewImageAnimation()) return TitleUI_Branch_State_ID.Nest13_NewImageAnimation;
        else if(BranchNest13_NewGameIsSaveDataToFadeIn()) return TitleUI_Branch_State_ID.Nest14_FadeIn;
     return TitleUI_Branch_State_ID.Nest13_NewImageAnimation;
     }
    public virtual bool BranchNest13_NewGameIsSaveDataToNewImageAnimation(){return true;}
    public virtual bool BranchNest13_NewGameIsSaveDataToFadeIn(){return true;}
    public override TitleUI_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleUI_Branch_State_ID.Nest13_NewGameIsSaveData:
                return BranchNest13_NewGameIsSaveDataState();
        }
     return TitleUI_Branch_State_ID.Nest13_NewImageAnimation;
     }
}
