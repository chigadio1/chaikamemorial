using System.Collections.Generic;
public class YaShiroFavoBeginInitState : BaseYaShiroFavoBeginInitState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoBeginInitState(BaseYaShiroFavoState other):base(other){}
    public YaShiroFavoBeginInitState():base(){}
}
