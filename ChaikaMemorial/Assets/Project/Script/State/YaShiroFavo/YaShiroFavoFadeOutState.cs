using System.Collections.Generic;
public class YaShiroFavoFadeOutState : BaseYaShiroFavoFadeOutState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoFadeOutState(BaseYaShiroFavoState other):base(other){}
    public YaShiroFavoFadeOutState():base(){}
}
