using System.Collections.Generic;
public class BaseYaShiroFavoStateManager : BaseStateManager<YaShiroFavoInitArgData,YaShiroFavoUpdateArgData,BaseYaShiroFavoState> {
    protected YaShiroFavo_State_ID state_id = YaShiroFavo_State_ID.BeginInit;
    public YaShiroFavo_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(YaShiroFavoInitArgData arg = null){
        switch (state_id)
        {
            case YaShiroFavo_State_ID.Begin:
                state = new YaShiroFavoBeginInitState(state);
                state_id = YaShiroFavo_State_ID.BeginInit;
                state.Start(arg);
                break;
            case YaShiroFavo_State_ID.BeginInit:
                state = new YaShiroFavoFadeOutState(state);
                state_id = YaShiroFavo_State_ID.FadeOut;
                state.Start(arg);
                break;
            case YaShiroFavo_State_ID.FadeOut:
                state = new YaShiroFavoNovelUpdateState(state);
                state_id = YaShiroFavo_State_ID.NovelUpdate;
                state.Start(arg);
                break;
            case YaShiroFavo_State_ID.NovelUpdate:
                state = new YaShiroFavoFadeInState(state);
                state_id = YaShiroFavo_State_ID.FadeIn;
                state.Start(arg);
                break;
            case YaShiroFavo_State_ID.FadeIn:
                state = new YaShiroFavoFinishUninitState(state);
                state_id = YaShiroFavo_State_ID.FinishUninit;
                state.Start(arg);
                break;
            case YaShiroFavo_State_ID.FinishUninit:
                is_finish = true;
                state_id = YaShiroFavo_State_ID.Finish;
                break;
            case YaShiroFavo_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(YaShiroFavo_State_ID value_state_id,YaShiroFavoInitArgData init_arg = null,YaShiroFavoUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case YaShiroFavo_State_ID.Begin:
                is_finish = true;
                break;
            case YaShiroFavo_State_ID.BeginInit:
                if(state != null) state.Finish(updatet_arg);
                state = new YaShiroFavoBeginInitState(state);
                state.Start(init_arg);
                break;
            case YaShiroFavo_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new YaShiroFavoFadeOutState(state);
                state.Start(init_arg);
                break;
            case YaShiroFavo_State_ID.NovelUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new YaShiroFavoNovelUpdateState(state);
                state.Start(init_arg);
                break;
            case YaShiroFavo_State_ID.FadeIn:
                if(state != null) state.Finish(updatet_arg);
                state = new YaShiroFavoFadeInState(state);
                state.Start(init_arg);
                break;
            case YaShiroFavo_State_ID.FinishUninit:
                if(state != null) state.Finish(updatet_arg);
                state = new YaShiroFavoFinishUninitState(state);
                state.Start(init_arg);
                break;
            case YaShiroFavo_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new YaShiroFavoBeginInitState();
    }
}
