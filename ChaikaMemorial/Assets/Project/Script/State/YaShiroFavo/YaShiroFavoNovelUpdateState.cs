using System.Collections.Generic;
public class YaShiroFavoNovelUpdateState : BaseYaShiroFavoNovelUpdateState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoNovelUpdateState(BaseYaShiroFavoState other):base(other){}
    public YaShiroFavoNovelUpdateState():base(){}
}
