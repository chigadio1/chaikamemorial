using System.Collections.Generic;
public class YaShiroFavoFinishUninitState : BaseYaShiroFavoFinishUninitState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoFinishUninitState(BaseYaShiroFavoState other):base(other){}
    public YaShiroFavoFinishUninitState():base(){}
}
