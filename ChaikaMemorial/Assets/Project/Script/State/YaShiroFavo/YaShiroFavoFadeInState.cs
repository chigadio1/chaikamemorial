using System.Collections.Generic;
public class YaShiroFavoFadeInState : BaseYaShiroFavoFadeInState {
    public override void Start(YaShiroFavoInitArgData arg) {
    }
    public override void Update(YaShiroFavoUpdateArgData arg) {
    }
    public override void Finish(YaShiroFavoUpdateArgData arg) {
    }
    public YaShiroFavoFadeInState(BaseYaShiroFavoState other):base(other){}
    public YaShiroFavoFadeInState():base(){}
}
