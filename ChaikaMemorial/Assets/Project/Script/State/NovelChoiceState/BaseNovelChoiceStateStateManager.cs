using System.Collections.Generic;
public class BaseNovelChoiceStateStateManager : BaseStateManager<NovelChoiceStateInitArgData,NovelChoiceStateUpdateArgData,BaseNovelChoiceStateState> {
    protected NovelChoiceState_State_ID state_id = NovelChoiceState_State_ID.BeginLoad;
    public NovelChoiceState_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NovelChoiceStateInitArgData arg = null){
        switch (state_id)
        {
            case NovelChoiceState_State_ID.Begin:
                state = new NovelChoiceStateBeginLoadState(state);
                state_id = NovelChoiceState_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.BeginLoad:
                state = new NovelChoiceStateFadeINState(state);
                state_id = NovelChoiceState_State_ID.FadeIN;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.FadeIN:
                state = new NovelChoiceStateStartAnimationState(state);
                state_id = NovelChoiceState_State_ID.StartAnimation;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.StartAnimation:
                state = new NovelChoiceStateChoiceSelectUpdateState(state);
                state_id = NovelChoiceState_State_ID.ChoiceSelectUpdate;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.ChoiceSelectUpdate:
                state = new NovelChoiceStateEnterChoiceAnimationState(state);
                state_id = NovelChoiceState_State_ID.EnterChoiceAnimation;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.EnterChoiceAnimation:
                state = new NovelChoiceStateFinishAnimationState(state);
                state_id = NovelChoiceState_State_ID.FinishAnimation;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.FinishAnimation:
                state = new NovelChoiceStateFadeOutState(state);
                state_id = NovelChoiceState_State_ID.FadeOut;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.FadeOut:
                state = new NovelChoiceStateFinishReleaseState(state);
                state_id = NovelChoiceState_State_ID.FinishRelease;
                state.Start(arg);
                break;
            case NovelChoiceState_State_ID.FinishRelease:
                is_finish = true;
                state_id = NovelChoiceState_State_ID.Finish;
                break;
            case NovelChoiceState_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NovelChoiceState_State_ID value_state_id,NovelChoiceStateInitArgData init_arg = null,NovelChoiceStateUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NovelChoiceState_State_ID.Begin:
                is_finish = true;
                break;
            case NovelChoiceState_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateBeginLoadState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.FadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateFadeINState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.StartAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateStartAnimationState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.ChoiceSelectUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateChoiceSelectUpdateState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.EnterChoiceAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateEnterChoiceAnimationState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.FinishAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateFinishAnimationState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateFadeOutState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.FinishRelease:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelChoiceStateFinishReleaseState(state);
                state.Start(init_arg);
                break;
            case NovelChoiceState_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NovelChoiceStateBeginLoadState();
    }
}
