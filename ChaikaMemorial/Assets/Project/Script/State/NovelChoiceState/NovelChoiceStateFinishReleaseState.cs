using System.Collections.Generic;
using UnityEngine;

public class NovelChoiceStateFinishReleaseState : BaseNovelChoiceStateFinishReleaseState {
    public override void Start(NovelChoiceStateInitArgData arg) {
    }
    public override void Update(NovelChoiceStateUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NovelChoiceStateUpdateArgData arg) {
        Resources.UnloadUnusedAssets();
    }
    public NovelChoiceStateFinishReleaseState(BaseNovelChoiceStateState other):base(other){}
    public NovelChoiceStateFinishReleaseState():base(){}
}
