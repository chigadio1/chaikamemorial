using System.Collections.Generic;
public class NovelChoiceStateBeginLoadState : BaseNovelChoiceStateBeginLoadState {
    public override void Start(NovelChoiceStateInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.SystemLoad(Setting_System_ID.Choice);
    }
    public override void Update(NovelChoiceStateUpdateArgData arg) {
        if(gameUiCore.Instance.SettingCanvas.IsSystemLoad())
        {
            gameUiCore.Instance.SettingCanvas.ValueSystemDataInstance(Setting_System_ID.Choice);

            List<string> list = new List<string>();
            foreach(var data in arg.choice_summary.list_choice)
            {
                list.Add(data.text);
            }

            gameUiCore.Instance.SettingCanvas.ChoiceObject.BeginInitButton(list.ToArray());
            is_end_update = true;
        }
    }
    public override void Finish(NovelChoiceStateUpdateArgData arg) {
    }
    public NovelChoiceStateBeginLoadState(BaseNovelChoiceStateState other):base(other){}
    public NovelChoiceStateBeginLoadState():base(){}
}
