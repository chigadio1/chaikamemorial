using System;
public enum NovelChoiceState_State_ID {
    Begin, //初期
    BeginLoad, //
    FadeIN, //
    StartAnimation, //
    ChoiceSelectUpdate, //
    EnterChoiceAnimation, //
    FinishAnimation, //
    FadeOut, //
    FinishRelease, //
    Finish //終了
}
