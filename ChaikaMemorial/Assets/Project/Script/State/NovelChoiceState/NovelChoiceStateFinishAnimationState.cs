using System.Collections.Generic;
using UnityEngine;

public class NovelChoiceStateFinishAnimationState : BaseNovelChoiceStateFinishAnimationState {
    float calc_time = 0.0f;
    readonly float max_time = 0.5f;
    public override void Start(NovelChoiceStateInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.ChoiceObject.PlayAnim(Choice_Commnad_Anim_ID.StartUP,1.0f, true);
    }
    public override void Update(NovelChoiceStateUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(0.0f, 0.5f, calc_time / max_time);
        if (gameUiCore.Instance.SettingCanvas.ChoiceObject.IsPlayAnim() && calc_time >= max_time)
        {
            is_end_update = true;
        }
        gameUiCore.Instance.SettingCanvas.ChoiceObject.ButtonSelectFinishAnimation(calc_time / max_time);
    }
    public override void Finish(NovelChoiceStateUpdateArgData arg) {
    }
    public NovelChoiceStateFinishAnimationState(BaseNovelChoiceStateState other):base(other){}
    public NovelChoiceStateFinishAnimationState():base(){}
}
