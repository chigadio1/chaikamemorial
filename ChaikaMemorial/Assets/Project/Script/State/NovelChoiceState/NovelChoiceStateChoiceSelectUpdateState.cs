using System.Collections.Generic;
using UnityEngine;

public class NovelChoiceStateChoiceSelectUpdateState : BaseNovelChoiceStateChoiceSelectUpdateState {
    public override void Start(NovelChoiceStateInitArgData arg) {
    }
    public override void Update(NovelChoiceStateUpdateArgData arg) {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            gameUiCore.Instance.SettingCanvas.ChoiceObject.SelectMove(true);
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            gameUiCore.Instance.SettingCanvas.ChoiceObject.SelectMove(false);
        }

        if(gameUiCore.Instance.is_button_push == true || Input.GetKeyDown(KeyCode.Space))
        {
            if(!gameUiCore.Instance.SettingCanvas.ChoiceObject.IsFinish())
            {
                gameUiCore.Instance.is_button_push = false;
                return;
            }
            else
            {
                int select_index = gameUiCore.Instance.SettingCanvas.ChoiceObject.GetSelectIndex();

                arg.jump_exec_id = arg.choice_summary.list_choice[select_index].jump_exec_id;
                arg.jump_order_id = arg.choice_summary.list_choice[select_index].jump_order_id;
                is_end_update = true;
            }
        }
    }
    public override void Finish(NovelChoiceStateUpdateArgData arg) {
    }
    public NovelChoiceStateChoiceSelectUpdateState(BaseNovelChoiceStateState other):base(other){}
    public NovelChoiceStateChoiceSelectUpdateState():base(){}
}
