using System.Collections.Generic;
using UnityEngine;

public class NovelChoiceStateFadeOutState : BaseNovelChoiceStateFadeOutState {
    float calc_time = 0.0f;
    readonly float max_time = 0.5f;
    public override void Start(NovelChoiceStateInitArgData arg) {

    }
    public override void Update(NovelChoiceStateUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(0.5f, 0.0f, calc_time / max_time);
        if (calc_time >= max_time)
        {
            is_end_update = true;
        }
        gameUiCore.Instance.SettingCanvas.ChoiceObject.SetFadeAlpha(lerp);
    }
    public override void Finish(NovelChoiceStateUpdateArgData arg) {
        gameUiCore.Instance.SettingCanvas.Release();
        gameUiCore.Instance.is_button_push = false;
    }
    public NovelChoiceStateFadeOutState(BaseNovelChoiceStateState other):base(other){}
    public NovelChoiceStateFadeOutState():base(){}
}
