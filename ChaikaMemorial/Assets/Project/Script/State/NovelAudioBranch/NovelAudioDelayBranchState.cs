using System.Collections.Generic;
public class NovelAudioDelayBranchState : BaseNovelAudioDelayBranchState {
    public override void Start(NovelAudioInitArgData arg) {
    }
    public override void Update(NovelAudioUpdateArgData arg) {
    }
    public override void Finish(NovelAudioUpdateArgData arg) {
    }
    public NovelAudioDelayBranchState(NovelAudio_Branch_State_ID other):base(other){}
}
