using System.Collections.Generic;
public class BaseNovelAudioStateBranchManager : BaseStateBranchManager<NovelAudioInitArgData,NovelAudioUpdateArgData,BaseNovelAudioBranchState,NovelAudio_Branch_State_ID > {
    public BaseNovelAudioStateBranchManager(NovelAudio_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(NovelAudioInitArgData arg = null){
        switch (state_id)
        {
            case NovelAudio_Branch_State_ID.Nest02_Delay:
                is_finish = true;
                state_id = NovelAudio_Branch_State_ID.Finish;
                break;
            case NovelAudio_Branch_State_ID.Nest01_Fade:
                state_id = NovelAudio_Branch_State_ID.Nest02_Delay;
                state = new NovelAudioDelayBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new NovelAudioFadeBranchState(NovelAudio_Branch_State_ID.Nest01_Fade);
    }
}
