using System.Collections.Generic;
public class BaseGamePartGameSelectBranchState : BaseGamePartBranchState{
    public BaseGamePartGameSelectBranchState(GamePart_Branch_State_ID other):base(other){}
    public virtual GamePart_Branch_State_ID BranchNest11_GameSelectState(){
        if(BranchNest11_GameSelectToActionSelectUpdate()) return GamePart_Branch_State_ID.Nest08_ActionSelectUpdate;
        else if(BranchNest11_GameSelectToBeginDate()) return GamePart_Branch_State_ID.Nest11_BeginDate;
        else if(BranchNest11_GameSelectToGameLoad()) return GamePart_Branch_State_ID.Nest11_GameLoad;
     return GamePart_Branch_State_ID.Nest08_ActionSelectUpdate;
     }
    public virtual bool BranchNest11_GameSelectToActionSelectUpdate(){return true;}
    public virtual bool BranchNest11_GameSelectToBeginDate(){return true;}
    public virtual bool BranchNest11_GameSelectToGameLoad(){return true;}
    public override GamePart_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case GamePart_Branch_State_ID.Nest11_GameSelect:
                return BranchNest11_GameSelectState();
        }
     return GamePart_Branch_State_ID.Nest08_ActionSelectUpdate;
     }
}
