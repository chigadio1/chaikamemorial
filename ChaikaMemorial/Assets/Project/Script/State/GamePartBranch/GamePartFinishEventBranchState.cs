using System.Collections.Generic;
public class GamePartFinishEventBranchState : BaseGamePartFinishEventBranchState {
    
    public override void Start(GamePartInitArgData arg) {

    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
        PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.FixTurn();
    }
    public GamePartFinishEventBranchState(GamePart_Branch_State_ID other):base(other){}


}
