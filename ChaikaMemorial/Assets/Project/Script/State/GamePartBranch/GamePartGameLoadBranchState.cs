using System.Collections.Generic;
public class GamePartGameLoadBranchState : BaseGamePartGameLoadBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
        PlayerCore.Instance.LoadData();
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartGameLoadBranchState(GamePart_Branch_State_ID other):base(other){}
}
