using System.Collections.Generic;
public class GamePartFinishDateBranchState : BaseGamePartFinishDateBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartFinishDateBranchState(GamePart_Branch_State_ID other):base(other){}
}
