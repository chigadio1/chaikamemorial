using System.Collections.Generic;
public class GamePartBeginDateBranchState : BaseGamePartBeginDateBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginDateBranchState(GamePart_Branch_State_ID other):base(other){}


}
