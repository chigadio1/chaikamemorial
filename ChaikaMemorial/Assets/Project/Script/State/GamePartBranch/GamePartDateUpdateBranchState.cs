using System.Collections.Generic;
public class GamePartDateUpdateBranchState : BaseGamePartDateUpdateBranchState {

    NovelUpdateStateBranchManager manager = new NovelUpdateStateBranchManager();
    NovelUpdateInitArgData init_arg = new NovelUpdateInitArgData();
    bool is_novel = false;
    public override void Start(GamePartInitArgData arg) {
        init_arg.event_id = 9801;
        var data = DateUnit.DateMeetingLocationEventID(gameUiCore.Instance.SelectCharacterID, gameUiCore.Instance.SelectDateSpotID);

        if(init_arg.event_id == data)
        {
            is_end_update = false;
            return;
        }
        is_novel = true;
        init_arg.event_id = data;
        is_end_update = false;
    }
    public override void Update(GamePartUpdateArgData arg) {
        if(!is_novel)
        {
            is_end_update = true;
            return;
        }
        manager.Update(init_arg);
        if(manager.IsFinish)
        {
            is_end_update = true;
            return;
        }
    }
    public override void Finish(GamePartUpdateArgData arg) {
        //次のStopまでWhileでルール
        bool res = true;
        while(res)
        {
            PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.AddNumTurn();
            var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetNumFixTurn);
            if(data == null)
            {
                break;
            }
            if(data.IsStop)
            {
                break;
            }
        }
    }
    public GamePartDateUpdateBranchState(GamePart_Branch_State_ID other):base(other){}
}
