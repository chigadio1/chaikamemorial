using System.Collections.Generic;
public class GamePartBeginEventBranchState : BaseGamePartBeginEventBranchState {
    bool is_novel = false;

    CalenderEventStateBranchManager manager = new CalenderEventStateBranchManager();
    CalenderEventUpdateArgData update_arg  = new CalenderEventUpdateArgData();
    public override void Start(GamePartInitArgData arg) {
        is_novel = false;

        var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn,Event_Occurrence_ID.StartOfTheDay);
        if (data != null)
        {
            if(data.Length > 0)
            {
                update_arg.data_details_array = data;
                update_arg.occurrence_id = Event_Occurrence_ID.StartOfTheDay;
                is_novel = true;
            }
        }
    }
    public override void Update(GamePartUpdateArgData arg) {
        if(is_novel)
        {
            manager.Update(null,update_arg);
            if(manager.IsFinish)
            {
                is_end_update = true;
            }
        }
        else
        {
            is_end_update = true;
        }
    }
    public override void Finish(GamePartUpdateArgData arg) {
        PlayerCore.Instance.event_id = 0;
        PlayerCore.Instance.is_next_event = false;
    }
    public GamePartBeginEventBranchState(GamePart_Branch_State_ID other):base(other){}
}
