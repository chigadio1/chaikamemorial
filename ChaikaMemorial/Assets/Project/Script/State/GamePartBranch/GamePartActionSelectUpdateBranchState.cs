using System.Collections.Generic;
public class GamePartActionSelectUpdateBranchState : BaseGamePartActionSelectUpdateBranchState {

    TalkActionEventStateBranchManager manager = new TalkActionEventStateBranchManager();
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        manager.Update();
        if(manager.IsFinish)
        {
            is_end_update = true;
        }

    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartActionSelectUpdateBranchState(GamePart_Branch_State_ID other):base(other){}
}
