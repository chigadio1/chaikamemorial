using System.Collections.Generic;
public class BaseGamePartStateBranchManager : BaseStateBranchManager<GamePartInitArgData,GamePartUpdateArgData,BaseGamePartBranchState,GamePart_Branch_State_ID > {
    public BaseGamePartStateBranchManager(GamePart_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(GamePartInitArgData arg = null){
        switch (state_id)
        {
            case GamePart_Branch_State_ID.Nest01_BeginStart:
                state_id = GamePart_Branch_State_ID.Nest11_BeginUpdate;
                state = new GamePartBeginUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_BeginEvent:
                state_id = GamePart_Branch_State_ID.Nest11_GameSelect;
                state = new GamePartGameSelectBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_BeginUpdate:
                state_id = GamePart_Branch_State_ID.Nest11_BeginEvent;
                state = new GamePartBeginEventBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_GameSelect:
                {
                GamePart_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case GamePart_Branch_State_ID.Nest08_ActionSelectUpdate:
                        state_id = GamePart_Branch_State_ID.Nest08_ActionSelectUpdate;
                        state = new GamePartActionSelectUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case GamePart_Branch_State_ID.Nest11_BeginDate:
                        state_id = GamePart_Branch_State_ID.Nest11_BeginDate;
                        state = new GamePartBeginDateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case GamePart_Branch_State_ID.Nest11_GameLoad:
                        state_id = GamePart_Branch_State_ID.Nest11_GameLoad;
                        state = new GamePartGameLoadBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case GamePart_Branch_State_ID.Nest08_ActionSelectUpdate:
                state_id = GamePart_Branch_State_ID.Nest11_FinishUpdate;
                state = new GamePartFinishUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest10_FinishEvent:
                state_id = GamePart_Branch_State_ID.Nest11_BeginUpdate;
                state = new GamePartBeginUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_FinishUpdate:
                {
                GamePart_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case GamePart_Branch_State_ID.Nest10_FinishEvent:
                        state_id = GamePart_Branch_State_ID.Nest10_FinishEvent;
                        state = new GamePartFinishEventBranchState(state_id);
                        state.Start(arg);
                        break;
                    case GamePart_Branch_State_ID.Nest12_EndGame:
                        state_id = GamePart_Branch_State_ID.Nest12_EndGame;
                        state = new GamePartEndGameBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case GamePart_Branch_State_ID.Nest12_EndGame:
                is_finish = true;
                state_id = GamePart_Branch_State_ID.Finish;
                break;
            case GamePart_Branch_State_ID.Nest01_EndGame:
                is_finish = true;
                state_id = GamePart_Branch_State_ID.Finish;
                break;
            case GamePart_Branch_State_ID.Nest11_BeginDate:
                state_id = GamePart_Branch_State_ID.Nest11_DateUpdate;
                state = new GamePartDateUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_DateUpdate:
                state_id = GamePart_Branch_State_ID.Nest11_FinishDate;
                state = new GamePartFinishDateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_FinishDate:
                state_id = GamePart_Branch_State_ID.Nest11_FinishUpdate;
                state = new GamePartFinishUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case GamePart_Branch_State_ID.Nest11_GameLoad:
                state_id = GamePart_Branch_State_ID.Nest11_BeginUpdate;
                state = new GamePartBeginUpdateBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new GamePartBeginStartBranchState(GamePart_Branch_State_ID.Nest01_BeginStart);
    }
}
