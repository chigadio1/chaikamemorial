using System.Collections.Generic;
public class GamePartEndGameBranchState : BaseGamePartEndGameBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartEndGameBranchState(GamePart_Branch_State_ID other):base(other){}
}
