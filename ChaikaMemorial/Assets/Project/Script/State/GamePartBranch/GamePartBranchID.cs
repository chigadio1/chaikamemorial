using System;
public enum GamePart_Branch_State_ID {
Begin = -1,
Nest01_BeginStart,
Nest11_BeginEvent,
Nest11_BeginUpdate,
Nest11_GameSelect,
Nest08_ActionSelectUpdate,
Nest10_FinishEvent,
Nest11_FinishUpdate,
Nest12_EndGame,
Nest01_EndGame,
Nest11_BeginDate,
Nest11_DateUpdate,
Nest11_FinishDate,
Nest11_GameLoad,
Finish
}
