using System.Collections.Generic;
public class GamePartBeginUpdateBranchState : BaseGamePartBeginUpdateBranchState {
    public override void Start(GamePartInitArgData arg) {
        PlayerCore.Instance.is_next_event = false;
        PlayerCore.Instance.event_id = 0;
        gameUiCore.Instance.is_load_game_retry = false;
    }
    public override void Update(GamePartUpdateArgData arg) {

        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginUpdateBranchState(GamePart_Branch_State_ID other):base(other){}
}
