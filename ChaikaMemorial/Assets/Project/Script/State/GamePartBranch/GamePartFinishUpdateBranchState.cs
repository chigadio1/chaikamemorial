using System.Collections.Generic;
public class GamePartFinishUpdateBranchState : BaseGamePartFinishUpdateBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartFinishUpdateBranchState(GamePart_Branch_State_ID other):base(other){}

    public override bool BranchNest11_FinishUpdateToFinishEvent()
    {
        return true;
    }
}
