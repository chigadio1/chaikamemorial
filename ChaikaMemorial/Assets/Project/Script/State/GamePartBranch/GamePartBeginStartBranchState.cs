using System.Collections.Generic;
public class GamePartBeginStartBranchState : BaseGamePartBeginStartBranchState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginStartBranchState(GamePart_Branch_State_ID other):base(other){}
}
