using System.Collections.Generic;
public class GamePartGameSelectBranchState : BaseGamePartGameSelectBranchState {
    ActionSelectStateBranchManager actionSelect = new ActionSelectStateBranchManager();
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
        actionSelect.Update();
        if(actionSelect.IsFinish ==true)
        {
            is_end_update = true;
        }


    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartGameSelectBranchState(GamePart_Branch_State_ID other):base(other){}

    public override bool BranchNest11_GameSelectToBeginDate()
    {
        return PlayerCore.Instance.IsDate == true;
    }

    public override bool BranchNest11_GameSelectToActionSelectUpdate()
    {
        return PlayerCore.Instance.IsDate == false && gameUiCore.Instance.is_load_game_retry == false;
    }

    public override bool BranchNest11_GameSelectToGameLoad()
    {
        return gameUiCore.Instance.is_load_game_retry == true;
    }
}
