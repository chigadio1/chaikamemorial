using System.Collections.Generic;
public class BaseGamePartFinishUpdateBranchState : BaseGamePartBranchState{
    public BaseGamePartFinishUpdateBranchState(GamePart_Branch_State_ID other):base(other){}
    public virtual GamePart_Branch_State_ID BranchNest11_FinishUpdateState(){
        if(BranchNest11_FinishUpdateToFinishEvent()) return GamePart_Branch_State_ID.Nest10_FinishEvent;
        else if(BranchNest11_FinishUpdateToEndGame()) return GamePart_Branch_State_ID.Nest12_EndGame;
     return GamePart_Branch_State_ID.Nest10_FinishEvent;
     }
    public virtual bool BranchNest11_FinishUpdateToFinishEvent(){return true;}
    public virtual bool BranchNest11_FinishUpdateToEndGame(){return true;}
    public override GamePart_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case GamePart_Branch_State_ID.Nest11_FinishUpdate:
                return BranchNest11_FinishUpdateState();
        }
     return GamePart_Branch_State_ID.Nest10_FinishEvent;
     }
}
