using System;
public enum ActionSelect_State_ID {
    Begin, //初期
    BeginInit, //
    FadeOut, //
    SelectParentUpdate, //
    SelectParentMoveUpdate, //
    SelectParentBackUpdate, //
    ParameterUpUpdate, //
    SubActionUpdate, //
    SettingUpdate, //
    FadeIn, //
    FinishSelect, //
    GameFinish, //
    SelectParentChangeAnimation, //
    YashiroFavo, //
    DateCharaUpdate, //
    LoadUpdate, //
    SaveUpdate, //
    Finish //終了
}
