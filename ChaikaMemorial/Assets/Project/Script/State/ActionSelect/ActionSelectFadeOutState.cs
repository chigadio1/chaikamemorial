using System.Collections.Generic;
public class ActionSelectFadeOutState : BaseActionSelectFadeOutState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectFadeOutState(BaseActionSelectState other):base(other){}
    public ActionSelectFadeOutState():base(){}
}
