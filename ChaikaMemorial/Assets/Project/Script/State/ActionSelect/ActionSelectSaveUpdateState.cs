using System.Collections.Generic;
public class ActionSelectSaveUpdateState : BaseActionSelectSaveUpdateState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSaveUpdateState(BaseActionSelectState other):base(other){}
    public ActionSelectSaveUpdateState():base(){}
}
