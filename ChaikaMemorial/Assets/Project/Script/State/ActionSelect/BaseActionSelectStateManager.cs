using System.Collections.Generic;
public class BaseActionSelectStateManager : BaseStateManager<ActionSelectInitArgData,ActionSelectUpdateArgData,BaseActionSelectState> {
    protected ActionSelect_State_ID state_id = ActionSelect_State_ID.BeginInit;
    public ActionSelect_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(ActionSelectInitArgData arg = null){
        switch (state_id)
        {
            case ActionSelect_State_ID.Begin:
                state = new ActionSelectBeginInitState(state);
                state_id = ActionSelect_State_ID.BeginInit;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.BeginInit:
                state = new ActionSelectFadeOutState(state);
                state_id = ActionSelect_State_ID.FadeOut;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.FadeOut:
                state = new ActionSelectSelectParentUpdateState(state);
                state_id = ActionSelect_State_ID.SelectParentUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SelectParentUpdate:
                state = new ActionSelectSelectParentMoveUpdateState(state);
                state_id = ActionSelect_State_ID.SelectParentMoveUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SelectParentMoveUpdate:
                state = new ActionSelectSelectParentBackUpdateState(state);
                state_id = ActionSelect_State_ID.SelectParentBackUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SelectParentBackUpdate:
                state = new ActionSelectParameterUpUpdateState(state);
                state_id = ActionSelect_State_ID.ParameterUpUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.ParameterUpUpdate:
                state = new ActionSelectSubActionUpdateState(state);
                state_id = ActionSelect_State_ID.SubActionUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SubActionUpdate:
                state = new ActionSelectSettingUpdateState(state);
                state_id = ActionSelect_State_ID.SettingUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SettingUpdate:
                state = new ActionSelectFadeInState(state);
                state_id = ActionSelect_State_ID.FadeIn;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.FadeIn:
                state = new ActionSelectFinishSelectState(state);
                state_id = ActionSelect_State_ID.FinishSelect;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.FinishSelect:
                state = new ActionSelectGameFinishState(state);
                state_id = ActionSelect_State_ID.GameFinish;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.GameFinish:
                state = new ActionSelectSelectParentChangeAnimationState(state);
                state_id = ActionSelect_State_ID.SelectParentChangeAnimation;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SelectParentChangeAnimation:
                state = new ActionSelectYashiroFavoState(state);
                state_id = ActionSelect_State_ID.YashiroFavo;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.YashiroFavo:
                state = new ActionSelectDateCharaUpdateState(state);
                state_id = ActionSelect_State_ID.DateCharaUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.DateCharaUpdate:
                state = new ActionSelectLoadUpdateState(state);
                state_id = ActionSelect_State_ID.LoadUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.LoadUpdate:
                state = new ActionSelectSaveUpdateState(state);
                state_id = ActionSelect_State_ID.SaveUpdate;
                state.Start(arg);
                break;
            case ActionSelect_State_ID.SaveUpdate:
                is_finish = true;
                state_id = ActionSelect_State_ID.Finish;
                break;
            case ActionSelect_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(ActionSelect_State_ID value_state_id,ActionSelectInitArgData init_arg = null,ActionSelectUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case ActionSelect_State_ID.Begin:
                is_finish = true;
                break;
            case ActionSelect_State_ID.BeginInit:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectBeginInitState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectFadeOutState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SelectParentUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSelectParentUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SelectParentMoveUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSelectParentMoveUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SelectParentBackUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSelectParentBackUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.ParameterUpUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectParameterUpUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SubActionUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSubActionUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SettingUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSettingUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.FadeIn:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectFadeInState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.FinishSelect:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectFinishSelectState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.GameFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectGameFinishState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SelectParentChangeAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSelectParentChangeAnimationState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.YashiroFavo:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectYashiroFavoState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.DateCharaUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectDateCharaUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.LoadUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectLoadUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.SaveUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new ActionSelectSaveUpdateState(state);
                state.Start(init_arg);
                break;
            case ActionSelect_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new ActionSelectBeginInitState();
    }
}
