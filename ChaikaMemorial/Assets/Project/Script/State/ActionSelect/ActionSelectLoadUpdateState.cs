using System.Collections.Generic;
public class ActionSelectLoadUpdateState : BaseActionSelectLoadUpdateState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectLoadUpdateState(BaseActionSelectState other):base(other){}
    public ActionSelectLoadUpdateState():base(){}
}
