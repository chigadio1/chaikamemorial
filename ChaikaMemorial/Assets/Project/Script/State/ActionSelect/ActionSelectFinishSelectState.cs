using System.Collections.Generic;
public class ActionSelectFinishSelectState : BaseActionSelectFinishSelectState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectFinishSelectState(BaseActionSelectState other):base(other){}
    public ActionSelectFinishSelectState():base(){}
}
