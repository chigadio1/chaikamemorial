using System.Collections.Generic;
public class ActionSelectSettingUpdateState : BaseActionSelectSettingUpdateState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSettingUpdateState(BaseActionSelectState other):base(other){}
    public ActionSelectSettingUpdateState():base(){}
}
