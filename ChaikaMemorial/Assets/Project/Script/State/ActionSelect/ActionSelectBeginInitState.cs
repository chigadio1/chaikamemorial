using System.Collections.Generic;
public class ActionSelectBeginInitState : BaseActionSelectBeginInitState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectBeginInitState(BaseActionSelectState other):base(other){}
    public ActionSelectBeginInitState():base(){}
}
