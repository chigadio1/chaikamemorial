using System.Collections.Generic;
public class ActionSelectParameterUpUpdateState : BaseActionSelectParameterUpUpdateState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectParameterUpUpdateState(BaseActionSelectState other):base(other){}
    public ActionSelectParameterUpUpdateState():base(){}
}
