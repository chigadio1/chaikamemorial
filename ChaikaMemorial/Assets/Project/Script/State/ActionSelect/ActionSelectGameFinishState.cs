using System.Collections.Generic;
public class ActionSelectGameFinishState : BaseActionSelectGameFinishState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectGameFinishState(BaseActionSelectState other):base(other){}
    public ActionSelectGameFinishState():base(){}
}
