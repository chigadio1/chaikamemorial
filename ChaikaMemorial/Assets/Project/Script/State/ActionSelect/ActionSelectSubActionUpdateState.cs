using System.Collections.Generic;
public class ActionSelectSubActionUpdateState : BaseActionSelectSubActionUpdateState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSubActionUpdateState(BaseActionSelectState other):base(other){}
    public ActionSelectSubActionUpdateState():base(){}
}
