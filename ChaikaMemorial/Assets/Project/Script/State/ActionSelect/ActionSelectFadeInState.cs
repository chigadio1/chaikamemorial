using System.Collections.Generic;
public class ActionSelectFadeInState : BaseActionSelectFadeInState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectFadeInState(BaseActionSelectState other):base(other){}
    public ActionSelectFadeInState():base(){}
}
