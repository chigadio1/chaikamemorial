using System.Collections.Generic;
public class TitleUIFadeInState : BaseTitleUIFadeInState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIFadeInState(BaseTitleUIState other):base(other){}
    public TitleUIFadeInState():base(){}
}
