using System.Collections.Generic;
public class TitleUIBeginStartUpState : BaseTitleUIBeginStartUpState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIBeginStartUpState(BaseTitleUIState other):base(other){}
    public TitleUIBeginStartUpState():base(){}
}
