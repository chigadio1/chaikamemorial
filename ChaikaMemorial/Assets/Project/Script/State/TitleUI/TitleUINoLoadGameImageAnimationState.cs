using System.Collections.Generic;
public class TitleUINoLoadGameImageAnimationState : BaseTitleUINoLoadGameImageAnimationState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINoLoadGameImageAnimationState(BaseTitleUIState other):base(other){}
    public TitleUINoLoadGameImageAnimationState():base(){}
}
