using System.Collections.Generic;
public class TitleUIBackGroundAnimationEndState : BaseTitleUIBackGroundAnimationEndState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIBackGroundAnimationEndState(BaseTitleUIState other):base(other){}
    public TitleUIBackGroundAnimationEndState():base(){}
}
