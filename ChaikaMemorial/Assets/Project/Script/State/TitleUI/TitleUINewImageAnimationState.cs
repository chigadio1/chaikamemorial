using System.Collections.Generic;
public class TitleUINewImageAnimationState : BaseTitleUINewImageAnimationState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewImageAnimationState(BaseTitleUIState other):base(other){}
    public TitleUINewImageAnimationState():base(){}
}
