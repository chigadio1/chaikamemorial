using System.Collections.Generic;
public class TitleUIStartImageAnimationState : BaseTitleUIStartImageAnimationState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, 1.0f);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIStartImageAnimationState(BaseTitleUIState other):base(other){}
    public TitleUIStartImageAnimationState():base(){}
}
