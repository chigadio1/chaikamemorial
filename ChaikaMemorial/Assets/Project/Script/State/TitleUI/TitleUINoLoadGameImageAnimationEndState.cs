using System.Collections.Generic;
public class TitleUINoLoadGameImageAnimationEndState : BaseTitleUINoLoadGameImageAnimationEndState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINoLoadGameImageAnimationEndState(BaseTitleUIState other):base(other){}
    public TitleUINoLoadGameImageAnimationEndState():base(){}
}
