using System.Collections.Generic;
public class TitleUINewLoadInputUpdateState : BaseTitleUINewLoadInputUpdateState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewLoadInputUpdateState(BaseTitleUIState other):base(other){}
    public TitleUINewLoadInputUpdateState():base(){}
}
