using System.Collections.Generic;
public class TitleUIGameStartState : BaseTitleUIGameStartState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUIGameStartState(BaseTitleUIState other):base(other){}
    public TitleUIGameStartState():base(){}
}
