using System.Collections.Generic;
using UnityEngine;

public class TitleUIBackGroundAnimationState : BaseTitleUIBackGroundAnimationState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
    }
    public override void Update(TitleUIUpdateArgData arg) {

        add_calc += Time.deltaTime / 2.0f;
        add_calc = Mathf.Clamp(add_calc, 0.0f, 0.5f);

        if (add_calc >= 0.5f) is_end_update = true;

        TitleUiCore.Instance.TitleBackGroundCanvas.SetChange(add_calc);

    }
    public override void Finish(TitleUIUpdateArgData arg) {
        TitleUiCore.Instance.TitleBackGroundCanvas.SetChange(0.5f);
    }
    public TitleUIBackGroundAnimationState(BaseTitleUIState other):base(other){}
    public TitleUIBackGroundAnimationState():base(){}
}
