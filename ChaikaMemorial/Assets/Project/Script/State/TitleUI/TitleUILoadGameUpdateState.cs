using System.Collections.Generic;
public class TitleUILoadGameUpdateState : BaseTitleUILoadGameUpdateState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUILoadGameUpdateState(BaseTitleUIState other):base(other){}
    public TitleUILoadGameUpdateState():base(){}
}
