using System;
public enum TitleUI_State_ID {
    Begin, //初期
    FadeOut, //
    StartImageAnimation, //
    StartInputUpdate, //
    BackGroundAnimation, //
    NewLoadInputUpdate, //
    NewImageAnimation, //
    NewInputUpdate, //
    GameStart, //
    NewImageAnimationEnd, //
    LoadGameUpdate, //
    NoLoadGameImageAnimation, //
    NoLoadGameInputUpdate, //
    NoLoadGameImageAnimationEnd, //
    BackGroundAnimationEnd, //
    FadeIn, //
    BeginStartUp, //
    NewGameIsSaveData, //
    Finish //終了
}
