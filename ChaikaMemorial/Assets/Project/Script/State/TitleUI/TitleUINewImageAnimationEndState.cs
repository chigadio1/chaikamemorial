using System.Collections.Generic;
public class TitleUINewImageAnimationEndState : BaseTitleUINewImageAnimationEndState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewImageAnimationEndState(BaseTitleUIState other):base(other){}
    public TitleUINewImageAnimationEndState():base(){}
}
