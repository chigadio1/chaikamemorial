using System.Collections.Generic;
public class TitleUINoLoadGameInputUpdateState : BaseTitleUINoLoadGameInputUpdateState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINoLoadGameInputUpdateState(BaseTitleUIState other):base(other){}
    public TitleUINoLoadGameInputUpdateState():base(){}
}
