using System.Collections.Generic;
public class TitleUINewInputUpdateState : BaseTitleUINewInputUpdateState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewInputUpdateState(BaseTitleUIState other):base(other){}
    public TitleUINewInputUpdateState():base(){}
}
