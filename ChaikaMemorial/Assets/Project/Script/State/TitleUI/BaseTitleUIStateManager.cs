using System.Collections.Generic;
public class BaseTitleUIStateManager : BaseStateManager<TitleUIInitArgData,TitleUIUpdateArgData,BaseTitleUIState> {
    protected TitleUI_State_ID state_id = TitleUI_State_ID.FadeOut;
    public TitleUI_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(TitleUIInitArgData arg = null){
        switch (state_id)
        {
            case TitleUI_State_ID.Begin:
                state = new TitleUIFadeOutState(state);
                state_id = TitleUI_State_ID.FadeOut;
                state.Start(arg);
                break;
            case TitleUI_State_ID.FadeOut:
                state = new TitleUIStartImageAnimationState(state);
                state_id = TitleUI_State_ID.StartImageAnimation;
                state.Start(arg);
                break;
            case TitleUI_State_ID.StartImageAnimation:
                state = new TitleUIStartInputUpdateState(state);
                state_id = TitleUI_State_ID.StartInputUpdate;
                state.Start(arg);
                break;
            case TitleUI_State_ID.StartInputUpdate:
                state = new TitleUIBackGroundAnimationState(state);
                state_id = TitleUI_State_ID.BackGroundAnimation;
                state.Start(arg);
                break;
            case TitleUI_State_ID.BackGroundAnimation:
                state = new TitleUINewLoadInputUpdateState(state);
                state_id = TitleUI_State_ID.NewLoadInputUpdate;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NewLoadInputUpdate:
                state = new TitleUINewImageAnimationState(state);
                state_id = TitleUI_State_ID.NewImageAnimation;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NewImageAnimation:
                state = new TitleUINewInputUpdateState(state);
                state_id = TitleUI_State_ID.NewInputUpdate;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NewInputUpdate:
                state = new TitleUIGameStartState(state);
                state_id = TitleUI_State_ID.GameStart;
                state.Start(arg);
                break;
            case TitleUI_State_ID.GameStart:
                state = new TitleUINewImageAnimationEndState(state);
                state_id = TitleUI_State_ID.NewImageAnimationEnd;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NewImageAnimationEnd:
                state = new TitleUILoadGameUpdateState(state);
                state_id = TitleUI_State_ID.LoadGameUpdate;
                state.Start(arg);
                break;
            case TitleUI_State_ID.LoadGameUpdate:
                state = new TitleUINoLoadGameImageAnimationState(state);
                state_id = TitleUI_State_ID.NoLoadGameImageAnimation;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NoLoadGameImageAnimation:
                state = new TitleUINoLoadGameInputUpdateState(state);
                state_id = TitleUI_State_ID.NoLoadGameInputUpdate;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NoLoadGameInputUpdate:
                state = new TitleUINoLoadGameImageAnimationEndState(state);
                state_id = TitleUI_State_ID.NoLoadGameImageAnimationEnd;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NoLoadGameImageAnimationEnd:
                state = new TitleUIBackGroundAnimationEndState(state);
                state_id = TitleUI_State_ID.BackGroundAnimationEnd;
                state.Start(arg);
                break;
            case TitleUI_State_ID.BackGroundAnimationEnd:
                state = new TitleUIFadeInState(state);
                state_id = TitleUI_State_ID.FadeIn;
                state.Start(arg);
                break;
            case TitleUI_State_ID.FadeIn:
                state = new TitleUIBeginStartUpState(state);
                state_id = TitleUI_State_ID.BeginStartUp;
                state.Start(arg);
                break;
            case TitleUI_State_ID.BeginStartUp:
                state = new TitleUINewGameIsSaveDataState(state);
                state_id = TitleUI_State_ID.NewGameIsSaveData;
                state.Start(arg);
                break;
            case TitleUI_State_ID.NewGameIsSaveData:
                is_finish = true;
                state_id = TitleUI_State_ID.Finish;
                break;
            case TitleUI_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(TitleUI_State_ID value_state_id,TitleUIInitArgData init_arg = null,TitleUIUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case TitleUI_State_ID.Begin:
                is_finish = true;
                break;
            case TitleUI_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIFadeOutState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.StartImageAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIStartImageAnimationState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.StartInputUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIStartInputUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.BackGroundAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIBackGroundAnimationState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NewLoadInputUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINewLoadInputUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NewImageAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINewImageAnimationState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NewInputUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINewInputUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.GameStart:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIGameStartState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NewImageAnimationEnd:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINewImageAnimationEndState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.LoadGameUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUILoadGameUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NoLoadGameImageAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINoLoadGameImageAnimationState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NoLoadGameInputUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINoLoadGameInputUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NoLoadGameImageAnimationEnd:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINoLoadGameImageAnimationEndState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.BackGroundAnimationEnd:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIBackGroundAnimationEndState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.FadeIn:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIFadeInState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.BeginStartUp:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUIBeginStartUpState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.NewGameIsSaveData:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleUINewGameIsSaveDataState(state);
                state.Start(init_arg);
                break;
            case TitleUI_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new TitleUIFadeOutState();
    }
}
