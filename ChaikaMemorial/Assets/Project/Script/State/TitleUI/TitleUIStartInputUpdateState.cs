using System.Collections.Generic;
using UnityEngine;

public class TitleUIStartInputUpdateState : BaseTitleUIStartInputUpdateState {
   
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            is_end_update = true;
        }
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        TitleUiCore.Instance.TitleSelectChoiceCanvas.SetAlpha(Title_Select_Choice_ID.START, 0.0f);
    }
    public TitleUIStartInputUpdateState(BaseTitleUIState other):base(other){}
    public TitleUIStartInputUpdateState():base(){}
}
