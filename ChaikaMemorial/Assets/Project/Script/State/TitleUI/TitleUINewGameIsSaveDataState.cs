using System.Collections.Generic;
public class TitleUINewGameIsSaveDataState : BaseTitleUINewGameIsSaveDataState {
    public override void Start(TitleUIInitArgData arg) {
    }
    public override void Update(TitleUIUpdateArgData arg) {
    }
    public override void Finish(TitleUIUpdateArgData arg) {
    }
    public TitleUINewGameIsSaveDataState(BaseTitleUIState other):base(other){}
    public TitleUINewGameIsSaveDataState():base(){}
}
