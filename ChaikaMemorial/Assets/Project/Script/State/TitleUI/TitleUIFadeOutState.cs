using System.Collections.Generic;
using UnityEngine;

public class TitleUIFadeOutState : BaseTitleUIFadeOutState {
    public override void Start(TitleUIInitArgData arg) {
        is_end_update = false;
        add_calc = 0.0f;
        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(1.0f);
    }
    public override void Update(TitleUIUpdateArgData arg) {
        add_calc += Time.deltaTime;
        if(add_calc >= 1.0f)
        {
            is_end_update = true;
        }
        add_calc = Mathf.Clamp(add_calc, 0.0f, 1.0f);

        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(Mathf.Abs(add_calc / 1.0f - 1.0f));
    }
    public override void Finish(TitleUIUpdateArgData arg) {
        add_calc = 0.0f;
        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(0.0f);
    }
    public TitleUIFadeOutState(BaseTitleUIState other):base(other){}
    public TitleUIFadeOutState():base(){}
}
