using System.Collections.Generic;
public class BaseNameInputStateManager : BaseStateManager<NameInputInitArgData,NameInputUpdateArgData,BaseNameInputState> {
    protected NameInput_State_ID state_id = NameInput_State_ID.BeginStart;
    public NameInput_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NameInputInitArgData arg = null){
        switch (state_id)
        {
            case NameInput_State_ID.Begin:
                state = new NameInputBeginStartState(state);
                state_id = NameInput_State_ID.BeginStart;
                state.Start(arg);
                break;
            case NameInput_State_ID.BeginStart:
                state = new NameInputNameInputUpdateState(state);
                state_id = NameInput_State_ID.NameInputUpdate;
                state.Start(arg);
                break;
            case NameInput_State_ID.NameInputUpdate:
                state = new NameInputFinishEndState(state);
                state_id = NameInput_State_ID.FinishEnd;
                state.Start(arg);
                break;
            case NameInput_State_ID.FinishEnd:
                is_finish = true;
                state_id = NameInput_State_ID.Finish;
                break;
            case NameInput_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NameInput_State_ID value_state_id,NameInputInitArgData init_arg = null,NameInputUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NameInput_State_ID.Begin:
                is_finish = true;
                break;
            case NameInput_State_ID.BeginStart:
                if(state != null) state.Finish(updatet_arg);
                state = new NameInputBeginStartState(state);
                state.Start(init_arg);
                break;
            case NameInput_State_ID.NameInputUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new NameInputNameInputUpdateState(state);
                state.Start(init_arg);
                break;
            case NameInput_State_ID.FinishEnd:
                if(state != null) state.Finish(updatet_arg);
                state = new NameInputFinishEndState(state);
                state.Start(init_arg);
                break;
            case NameInput_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NameInputBeginStartState();
    }
}
