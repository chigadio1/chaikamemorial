using System.Collections.Generic;
using UnityEngine;

public class NameInputNameInputUpdateState : BaseNameInputNameInputUpdateState {
    public override void Start(NameInputInitArgData arg) {
    }
    public override void Update(NameInputUpdateArgData arg) {
        if(NameUICore.Instance.IsEnter)
        {
            if(NameUICore.Instance.NameInputCanvas.InputValue())
            {
                GameManagerCore.Instance.OffBackNameTitle();
                is_end_update = true;

                var input_data = NameUICore.Instance.NameInputCanvas.GetInput();
                PlayerCore.Instance.GetPlayerCoreData.ResetAll();
                PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.SetValuePlayerInput(input_data.Item1,input_data.Item2,input_data.Item3,input_data.Item4,input_data.Item5,input_data.Item6);
            }
            else
            {
                NameUICore.Instance.OffEnter();
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            is_end_update = true;
            GameManagerCore.Instance.OnBackNameTitle();
        }

    }
    public override void Finish(NameInputUpdateArgData arg) {
    }
    public NameInputNameInputUpdateState(BaseNameInputState other):base(other){}
    public NameInputNameInputUpdateState():base(){}
}
