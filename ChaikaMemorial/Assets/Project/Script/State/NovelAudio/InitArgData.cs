using System.Collections.Generic;
using System;
public class NovelAudioInitArgData : BaseStateArgData{
    public Sound_Event_Type_ID sound_event_type_id; //
    public Sound_Type_ID sound_type_id; //
    public int sound_id; //
    public float fade_time; //
    public float delay_time; //
    public float volume; //
    public bool is_bgm; //
    public bool is_play; //
}
