using System.Collections.Generic;
public class BaseNovelAudioState : BaseState<NovelAudioInitArgData,NovelAudioUpdateArgData>{
    public float calc_time; //
    public BaseNovelAudioState(BaseNovelAudioState other){
        this.calc_time=other.calc_time;
    }
    public BaseNovelAudioState(){
    }
}
