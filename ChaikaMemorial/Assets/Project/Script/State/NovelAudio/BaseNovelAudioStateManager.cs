using System.Collections.Generic;
public class BaseNovelAudioStateManager : BaseStateManager<NovelAudioInitArgData,NovelAudioUpdateArgData,BaseNovelAudioState> {
    protected NovelAudio_State_ID state_id = NovelAudio_State_ID.Delay;
    public NovelAudio_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NovelAudioInitArgData arg = null){
        switch (state_id)
        {
            case NovelAudio_State_ID.Begin:
                state = new NovelAudioDelayState(state);
                state_id = NovelAudio_State_ID.Delay;
                state.Start(arg);
                break;
            case NovelAudio_State_ID.Delay:
                state = new NovelAudioFadeState(state);
                state_id = NovelAudio_State_ID.Fade;
                state.Start(arg);
                break;
            case NovelAudio_State_ID.Fade:
                is_finish = true;
                state_id = NovelAudio_State_ID.Finish;
                break;
            case NovelAudio_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NovelAudio_State_ID value_state_id,NovelAudioInitArgData init_arg = null,NovelAudioUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NovelAudio_State_ID.Begin:
                is_finish = true;
                break;
            case NovelAudio_State_ID.Delay:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelAudioDelayState(state);
                state.Start(init_arg);
                break;
            case NovelAudio_State_ID.Fade:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelAudioFadeState(state);
                state.Start(init_arg);
                break;
            case NovelAudio_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NovelAudioDelayState();
    }
}
