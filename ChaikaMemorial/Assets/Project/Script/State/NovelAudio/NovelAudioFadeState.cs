using System.Collections.Generic;
using UnityEngine;

public class NovelAudioFadeState : BaseNovelAudioFadeState {

    float fade_time = 0.0f;
    bool is_play = false;
    float volume = 0.0f;
    bool is_bgm = false;
    float fade_in_volume = 0.0f;
    public override void Start(NovelAudioInitArgData arg) {
        calc_time = 0.0f;
        is_play = arg.is_play;
        fade_time = arg.fade_time;
        volume = arg.volume;
        is_bgm = arg.is_bgm;
        if(is_play == false)
        {
            if(is_bgm)
            {
                fade_in_volume = SoundGameCore.Instance.GetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM);
            }
            else
            {
                fade_in_volume = SoundGameCore.Instance.GetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE);
            }
        }
    }
    public override void Update(NovelAudioUpdateArgData arg) {

        if(fade_time <= 0.0f)
        {
            if (is_play)
            {
                if (is_bgm)
                {
                    SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, 1.0f);
                }
                else
                {
                    SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, 1.0f);
                }
            }
            else
            {
                if (is_bgm)
                {
                    SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, 0.0f);
                }
                else
                {
                    SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, 0.0f);
                }
            }
            is_end_update = true;
            return;
        }

        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, fade_time);
        float lerp_time = calc_time / fade_time;
        lerp_time = Mathf.Clamp(lerp_time, 0.0f, 1.0f);
        float lerp_value = 0.0f;
        if(is_play)
        {
            lerp_value = Mathf.Lerp(0.0f, volume, lerp_time);
        }
        else
        {
            lerp_value = Mathf.Lerp(0.0f, fade_in_volume, 0.0f);
        }
        if (is_bgm)
        {
            SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, lerp_value);
        }
        else
        {
            SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, lerp_value);
        }

        if(calc_time >= fade_time)
        {
            is_end_update = true;
            if(is_bgm)
            {
                if (is_play == false) SoundGameCore.Instance.StopBGM();
            }
        }


    }
    public override void Finish(NovelAudioUpdateArgData arg) {
    }
    public NovelAudioFadeState(BaseNovelAudioState other):base(other){}
    public NovelAudioFadeState():base(){}
}
