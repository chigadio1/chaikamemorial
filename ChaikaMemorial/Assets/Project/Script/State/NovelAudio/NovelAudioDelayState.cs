using System.Collections.Generic;
using UnityEngine;

public class NovelAudioDelayState : BaseNovelAudioDelayState {

    float delay_time = 0.0f;
    bool is_bgm = false;
    bool is_play = false;
    int sound_id = 0;
    float fade_time = 0.0f;
    float volume;
    public override void Start(NovelAudioInitArgData arg) {
        calc_time = 0.0f;
        delay_time = arg.delay_time;
        is_bgm = arg.is_bgm;
        sound_id = arg.sound_id;
        is_play = arg.is_play;
        fade_time = arg.fade_time;
        volume = arg.volume;
    }
    public override void Update(NovelAudioUpdateArgData arg) {
        calc_time += Time.deltaTime;
        if (calc_time >= delay_time)
        {
            if(is_bgm)
            {
                if (is_play)
                {
                    if (fade_time <= 0.0f)
                    {
                        SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, volume);
                    }
                    else
                    {
                        SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, 0.0f);
                    }
                    SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, sound_id);
                }
            }
            else
            {
                if (is_play)
                {
                    if (fade_time <= 0.0f)
                    {
                        SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, volume);
                    }
                    else SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, 0.0f);
                    SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, sound_id);
                }
            }
            is_end_update = true;
        }
    }
    public override void Finish(NovelAudioUpdateArgData arg) {
    }
    public NovelAudioDelayState(BaseNovelAudioState other):base(other){}
    public NovelAudioDelayState():base(){}
}
