using System.Collections.Generic;
public class TalkActionEventFramePopBranchState : BaseTalkActionEventFramePopBranchState {
    public override void Start(TalkActionEventInitArgData arg) {
        PlayerCore.Instance.StatusCalculatorResult.CalcStatus(PlayerCore.Instance.select_status_id,
            PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetStatus(PlayerCore.Instance.select_status_id));


        gameUiCore.Instance?.StatusCanvas?.AddChatData();
        gameUiCore.Instance?.StatusCanvas?.SetTextTalk("");
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFramePopBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
