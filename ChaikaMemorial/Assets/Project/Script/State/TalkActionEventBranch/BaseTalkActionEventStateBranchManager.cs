using System.Collections.Generic;
public class BaseTalkActionEventStateBranchManager : BaseStateBranchManager<TalkActionEventInitArgData,TalkActionEventUpdateArgData,BaseTalkActionEventBranchState,TalkActionEvent_Branch_State_ID > {
    public BaseTalkActionEventStateBranchManager(TalkActionEvent_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(TalkActionEventInitArgData arg = null){
        switch (state_id)
        {
            case TalkActionEvent_Branch_State_ID.Nest01_BeginInit:
                state_id = TalkActionEvent_Branch_State_ID.Nest02_FadeOut;
                state = new TalkActionEventFadeOutBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_FramePop:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_FrameTextUpdate;
                state = new TalkActionEventFrameTextUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_FrameTextUpdate:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_FrameResultIcon;
                state = new TalkActionEventFrameResultIconBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_FrameResultIcon:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_StatusUP;
                state = new TalkActionEventStatusUPBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_StatusUP:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_EventUpdate;
                state = new TalkActionEventEventUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_FadeIN:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_EvenInit:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_EventUpdate:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_EventGenericUpdate;
                state = new TalkActionEventEventGenericUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_EventFadeOut:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest02_FadeOut:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck;
                state = new TalkActionEventSchoolCheckBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_StartEventUpdate;
                state = new TalkActionEventStartEventUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_StartEventInit:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_StartEventUpdate:
                state_id = TalkActionEvent_Branch_State_ID.Nest10_FramePop;
                state = new TalkActionEventFramePopBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_StartEventFadeOut:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest01_Index02_FadeIN:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest12_GameFinish:
                is_finish = true;
                state_id = TalkActionEvent_Branch_State_ID.Finish;
                break;
            case TalkActionEvent_Branch_State_ID.Nest11_FinishFadeIn:
                state_id = TalkActionEvent_Branch_State_ID.Nest12_GameFinish;
                state = new TalkActionEventGameFinishBranchState(state_id);
                state.Start(arg);
                break;
            case TalkActionEvent_Branch_State_ID.Nest10_EventGenericUpdate:
                {
                TalkActionEvent_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck:
                        state_id = TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck;
                        state = new TalkActionEventSchoolCheckBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TalkActionEvent_Branch_State_ID.Nest11_FinishFadeIn:
                        state_id = TalkActionEvent_Branch_State_ID.Nest11_FinishFadeIn;
                        state = new TalkActionEventFinishFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
             }
    }

    public override void StartInstance() {
        state = new TalkActionEventBeginInitBranchState(TalkActionEvent_Branch_State_ID.Nest01_BeginInit);
    }
}
