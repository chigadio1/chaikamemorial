using System.Collections.Generic;
public class TalkActionEventStartEventInitBranchState : BaseTalkActionEventStartEventInitBranchState {
    public override void Start(TalkActionEventInitArgData arg) {

    }
    public override void Update(TalkActionEventUpdateArgData arg) {
            is_end_update = true;

    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventStartEventInitBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
