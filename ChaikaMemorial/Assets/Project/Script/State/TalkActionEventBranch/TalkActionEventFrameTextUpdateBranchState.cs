using System.Collections.Generic;
using UnityEngine;

public class TalkActionEventFrameTextUpdateBranchState : BaseTalkActionEventFrameTextUpdateBranchState {

    public int max_load_count = 3;
    public int load_count = 0;
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        add_calc_time += Time.deltaTime;

        if (add_calc_time >= 0.5f)
        {
            ++load_count;
            add_calc_time = 0.0f;
            string text = StatusStringConverter.GetStatusString(PlayerCore.Instance.select_status_id);
            for (int count = 0; count < load_count;)
            {
                text += "�E";
                ++count;
            }
            if (load_count >= max_load_count)
            {
                is_end_update = true;
            }
            gameUiCore.Instance?.StatusCanvas?.SetTextTalk(text);
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFrameTextUpdateBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
