using System.Collections.Generic;
public class BaseTalkActionEventEventGenericUpdateBranchState : BaseTalkActionEventBranchState{
    public BaseTalkActionEventEventGenericUpdateBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
    public virtual TalkActionEvent_Branch_State_ID BranchNest10_EventGenericUpdateState(){
        if(BranchNest10_EventGenericUpdateToSchoolCheck()) return TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck;
        else if(BranchNest10_EventGenericUpdateToFinishFadeIn()) return TalkActionEvent_Branch_State_ID.Nest11_FinishFadeIn;
     return TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck;
     }
    public virtual bool BranchNest10_EventGenericUpdateToSchoolCheck(){return true;}
    public virtual bool BranchNest10_EventGenericUpdateToFinishFadeIn(){return true;}
    public override TalkActionEvent_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TalkActionEvent_Branch_State_ID.Nest10_EventGenericUpdate:
                return BranchNest10_EventGenericUpdateState();
        }
     return TalkActionEvent_Branch_State_ID.Nest10_SchoolCheck;
     }
}
