using System.Collections.Generic;
public class TalkActionEventSchoolCheckBranchState : BaseTalkActionEventSchoolCheckBranchState {


    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if (gameUiCore.Instance.StatusCanvas.IsPlayAnim() == false) return;

        //カレンダーにある固定イベントがあれば発生
        is_end_update = true;

    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventSchoolCheckBranchState(TalkActionEvent_Branch_State_ID other):base(other){}

}
