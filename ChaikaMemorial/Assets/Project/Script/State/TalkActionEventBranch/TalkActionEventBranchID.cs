using System;
public enum TalkActionEvent_Branch_State_ID {
Begin = -1,
Nest01_BeginInit,
Nest10_FramePop,
Nest10_FrameTextUpdate,
Nest10_FrameResultIcon,
Nest10_StatusUP,
Nest01_FadeIN,
Nest01_EvenInit,
Nest10_EventUpdate,
Nest01_EventFadeOut,
Nest02_FadeOut,
Nest10_SchoolCheck,
Nest01_StartEventInit,
Nest10_StartEventUpdate,
Nest01_StartEventFadeOut,
Nest01_Index02_FadeIN,
Nest12_GameFinish,
Nest11_FinishFadeIn,
Nest10_EventGenericUpdate,
Finish
}
