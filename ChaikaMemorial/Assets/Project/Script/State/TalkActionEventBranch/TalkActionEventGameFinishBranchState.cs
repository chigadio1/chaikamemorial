using System.Collections.Generic;
using UnityEngine;

public class TalkActionEventGameFinishBranchState : BaseTalkActionEventGameFinishBranchState {
    public override void Start(TalkActionEventInitArgData arg) {
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.BEHAVIOR);
        gameUiCore.Instance.AllRelease();
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.ScheduleAction);
        Resources.UnloadUnusedAssets();
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventGameFinishBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
