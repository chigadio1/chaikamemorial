using System.Collections.Generic;
using UnityEngine;

public class TalkActionEventStartEventFadeOutBranchState : BaseTalkActionEventStartEventFadeOutBranchState {
    float calc_time = 0.0f;
    readonly float max_time = 1.0f;
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(1.0f, 0.0f, calc_time / max_time);
        if (calc_time >= max_time)
        {
            is_end_update = true;
        }
        gameUiCore.Instance.FadeCanvas.SetFadeAlpha(lerp);
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
        gameUiCore.Instance.FadeCanvas.SetFadeAlpha(0.0f);
    }
    public TalkActionEventStartEventFadeOutBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
