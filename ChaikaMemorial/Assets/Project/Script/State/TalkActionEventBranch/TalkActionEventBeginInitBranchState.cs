using System.Collections.Generic;
public class TalkActionEventBeginInitBranchState : BaseTalkActionEventBeginInitBranchState {
    public override void Start(TalkActionEventInitArgData arg) {
        gameUiCore.Instance.LoadGameUIObject(gameUiCore.UI_Object_ID.BEHAVIOR);
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(gameUiCore.Instance.IsLoadFinishGameUIObject(gameUiCore.UI_Object_ID.BEHAVIOR))
        {
            gameUiCore.Instance.InstanceGameUIObject(gameUiCore.UI_Object_ID.BEHAVIOR);
            gameUiCore.Instance.StatusCanvas.SetStatusNumInit((int)Status_ID.STUDY, PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetStatus(Status_ID.STUDY));
            gameUiCore.Instance.StatusCanvas.SetStatusNumInit((int)Status_ID.EXERCISE, PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetStatus(Status_ID.EXERCISE));
            gameUiCore.Instance.StatusCanvas.SetStatusNumInit((int)Status_ID.CHARM, PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetStatus(Status_ID.CHARM));
            is_end_update = true;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventBeginInitBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
