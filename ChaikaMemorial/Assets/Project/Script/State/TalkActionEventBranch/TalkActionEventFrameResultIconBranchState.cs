using System.Collections.Generic;
public class TalkActionEventFrameResultIconBranchState : BaseTalkActionEventFrameResultIconBranchState {
    public override void Start(TalkActionEventInitArgData arg) {
        gameUiCore.Instance?.StatusCanvas?.AddResultData();


        int id = gameUiCore.result_action_names[(int)PlayerCore.Instance.StatusCalculatorResult.CalcResultID];
        var sprite = gameUiCore.Instance.GetDefaultSprite(Novel_Sprite_Type_ID.ICON, id);
        gameUiCore.Instance?.StatusCanvas?.SetResultSptite(sprite);
        is_end_update = false;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFrameResultIconBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
