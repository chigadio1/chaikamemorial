using System.Collections.Generic;
public class TalkActionEventEvenInitBranchState : BaseTalkActionEventEvenInitBranchState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventEvenInitBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
