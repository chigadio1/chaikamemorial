using System.Collections.Generic;
public class TalkActionEventStatusUPBranchState : BaseTalkActionEventStatusUPBranchState {


    public override void Start(TalkActionEventInitArgData arg) {
        is_end_update = false;
        gameUiCore.Instance?.StatusCanvas?.SetStatusNum((int)PlayerCore.Instance.select_status_id,PlayerCore.Instance.StatusCalculatorResult.CalcStatusNum);
        PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.SetStatus(PlayerCore.Instance.select_status_id, PlayerCore.Instance.StatusCalculatorResult.CalcStatusNum);
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if (gameUiCore.Instance.StatusCanvas.GetIsNumFix(PlayerCore.Instance.select_status_id))
        {

            is_end_update = true;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {

    }
    public TalkActionEventStatusUPBranchState(TalkActionEvent_Branch_State_ID other):base(other){}


}
