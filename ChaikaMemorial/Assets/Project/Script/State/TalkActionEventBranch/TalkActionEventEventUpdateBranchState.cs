using System.Collections.Generic;
public class TalkActionEventEventUpdateBranchState : BaseTalkActionEventEventUpdateBranchState {
    CalenderEventStateBranchManager manager = new CalenderEventStateBranchManager();
    CalenderEventUpdateArgData update_arg = new CalenderEventUpdateArgData();
    bool is_novel = false;
    public override void Start(TalkActionEventInitArgData arg) {
        is_novel = false;

        var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn, Event_Occurrence_ID.FinishOfAction);
        if (data != null)
        {
            if (data.Length > 0)
            {
                update_arg.data_details_array = data;
                update_arg.occurrence_id = Event_Occurrence_ID.FinishOfAction;
                is_novel = true;
                foreach(var item in data)
                {
                    PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.On(EventIDExtensions.EventIDConvert(item.event_id));
                }
            }
        }

        //Tod:ここで各キャラの誕生日があるかどうか、プレイヤーの誕生日があるかどうか調べる
    }
    public override void Update(TalkActionEventUpdateArgData arg) {

        if (is_novel == false)
        {
            is_end_update = true;
            return;
        }
        update_arg.occurrence_id = Event_Occurrence_ID.StartOfAction;
        manager.Update();
        if (manager.IsFinish == true)
        {
            is_end_update = true;
            return;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventEventUpdateBranchState(TalkActionEvent_Branch_State_ID other):base(other){}


}
