using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TalkActionEventEventGenericUpdateBranchState : BaseTalkActionEventEventGenericUpdateBranchState {
    NovelLoopUpdateStateBranchManager manager = new NovelLoopUpdateStateBranchManager();
    NovelLoopUpdateUpdateArgData update_arg = new NovelLoopUpdateUpdateArgData();
    bool is_novel = false;
    bool is_stop = false;
    private float calc_time;
    private float max_time = 0.5f;

    public override void Start(TalkActionEventInitArgData arg) {
        is_novel = false;
        calc_time = 0.0f;
        var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);
        if (data != null)
        {
            var holiday_array = data.GetListDayDetails.Where(a => a.day_holiday_id > Day_Holiday_ID.NONE && a.day_holiday_id < Day_Holiday_ID.MAX).ToArray();
            //土日、祝なら休日用のイベント発生
            if (data.GetDayWeekID == Day_Week_ID.Sunday || data.GetDayWeekID == Day_Week_ID.Saturday || holiday_array.Length > 0)
            {
                int event_id = GenericEventUnit.GenericEventHolidayID();
                if(event_id > 0)
                {
                    update_arg.list_event_id.Add(event_id);
                    is_novel = true;
                }
            }
            //学校用
            else
            {
                //授業
                if (GenericEventUnit.OccurrenceSchool(Generic_Event_School_Type_ID.Lesson))
                {
                    int event_id = GenericEventUnit.GenericEventSchoolLessonID();
                    if (event_id > 0)
                    {
                        update_arg.list_event_id.Add(event_id);
                        is_novel = true;
                    }
                }
                //昼休み
                if (GenericEventUnit.OccurrenceSchool(Generic_Event_School_Type_ID.Lunch_Break))
                {
                    int event_id = GenericEventUnit.GenericEventSchoolLunch_BreakID();
                    if (event_id > 0)
                    {
                        update_arg.list_event_id.Add(event_id);
                        is_novel = true;
                    }
                }
                //放課後
                if (GenericEventUnit.OccurrenceSchool(Generic_Event_School_Type_ID.After_School))
                {
                    int event_id = GenericEventUnit.GenericEventSchoolAfterID();
                    if (event_id > 0)
                    {
                        update_arg.list_event_id.Add(event_id);
                        is_novel = true;
                    }
                }
            }
        }
        else
        {
            return;
        }
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(!is_novel)
        {
            //次の日を調べる
            PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.AddNumTurn();
            var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn + PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetFixTurn);
            if (data != null)
            {
                is_stop = data.IsStop;
            }
            is_end_update = true;
            return;
        }
        else
        {
            manager.Update(null, update_arg);
            if (manager.IsFinish == true)
            {
                calc_time += Time.deltaTime;
                calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
                float lerp = Mathf.Lerp(1.0f, 0.0f, calc_time / max_time);
                if (calc_time >= max_time)
                {
                    is_end_update = true;

                    gameUiCore.Instance.FadeCanvas.SetFadeAlpha(lerp);
                    PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.AddNumTurn();
                    var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn + PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetFixTurn);
                    if (data != null)
                    {
                        is_stop = data.IsStop;
                    }
                    is_end_update = true;
                    return;
                }
            }
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {

    }
    public TalkActionEventEventGenericUpdateBranchState(TalkActionEvent_Branch_State_ID other):base(other){}


    public override bool BranchNest10_EventGenericUpdateToFinishFadeIn()
    {
        return is_stop == true; ;
    }

    public override bool BranchNest10_EventGenericUpdateToSchoolCheck()
    {
        return is_stop == false ;
    }
}
