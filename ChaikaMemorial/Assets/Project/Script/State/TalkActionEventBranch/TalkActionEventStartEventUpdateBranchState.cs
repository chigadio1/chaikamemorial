using System.Collections.Generic;
public class TalkActionEventStartEventUpdateBranchState : BaseTalkActionEventStartEventUpdateBranchState {
    CalenderEventStateBranchManager manager = new CalenderEventStateBranchManager();
    CalenderEventUpdateArgData update_arg = new CalenderEventUpdateArgData();
    bool is_novel = false;
    public override void Start(TalkActionEventInitArgData arg) {

        var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn, Event_Occurrence_ID.StartOfAction);
        if (data != null)
        {
            if (data.Length > 0)
            {
                update_arg.data_details_array = data;
                update_arg.occurrence_id = Event_Occurrence_ID.StartOfAction;
                is_novel = true;
            }
        }
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(is_novel == false)
        {
            is_end_update = true;
            return;
        }
        update_arg.occurrence_id = Event_Occurrence_ID.StartOfAction;
        manager.Update();
        if(manager.IsFinish == true)
        {
            is_end_update = true;
            return;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
        gameUiCore.Instance.PlayEnd();
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
    }
    public TalkActionEventStartEventUpdateBranchState(TalkActionEvent_Branch_State_ID other):base(other){}
}
