using System.Collections.Generic;
public class DateCharaCommandAnimationStartBranchState : BaseDateCharaCommandAnimationStartBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.PlayAnim(Date_Commnad_Anim_ID.StartUP);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.DateCommandObject.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandAnimationStartBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
