using System.Collections.Generic;
public class BaseDateCharaCommandStateBranchManager : BaseStateBranchManager<DateCharaCommandInitArgData,DateCharaCommandUpdateArgData,BaseDateCharaCommandBranchState,DateCharaCommand_Branch_State_ID > {
    public BaseDateCharaCommandStateBranchManager(DateCharaCommand_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(DateCharaCommandInitArgData arg = null){
        switch (state_id)
        {
            case DateCharaCommand_Branch_State_ID.Nest01_BeginLoad:
                state_id = DateCharaCommand_Branch_State_ID.Nest02_BeginSubLoad;
                state = new DateCharaCommandBeginSubLoadBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest02_BeginSubLoad:
                state_id = DateCharaCommand_Branch_State_ID.Nest03_AnimationStart;
                state = new DateCharaCommandAnimationStartBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_Update:
                {
                DateCharaCommand_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart:
                        state_id = DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart;
                        state = new DateCharaCommandPhoneAnimationStartBranchState(state_id);
                        state.Start(arg);
                        break;
                    case DateCharaCommand_Branch_State_ID.Nest15_ButtonFinishAnimation:
                        state_id = DateCharaCommand_Branch_State_ID.Nest15_ButtonFinishAnimation;
                        state = new DateCharaCommandButtonFinishAnimationBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN:
                state_id = DateCharaCommand_Branch_State_ID.Nest16_NovelUpdate;
                state = new DateCharaCommandNovelUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest16_NovelUpdate:
                state_id = DateCharaCommand_Branch_State_ID.Nest17_NovelFinish;
                state = new DateCharaCommandNovelFinishBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest17_NovelFinish:
                is_finish = true;
                state_id = DateCharaCommand_Branch_State_ID.Finish;
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_CancelGameFinish:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_CancelPhoneAnimationImpFinish;
                state = new DateCharaCommandCancelPhoneAnimationImpFinishBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest17_GameFinish:
                is_finish = true;
                state_id = DateCharaCommand_Branch_State_ID.Finish;
                break;
            case DateCharaCommand_Branch_State_ID.Nest03_AnimationStart:
                state_id = DateCharaCommand_Branch_State_ID.Nest04_ButtonStartAnimation;
                state = new DateCharaCommandButtonStartAnimationBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest16_AnimationFinish:
                state_id = DateCharaCommand_Branch_State_ID.Nest17_GameFinish;
                state = new DateCharaCommandGameFinishBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest04_ButtonStartAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_Update;
                state = new DateCharaCommandUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest15_ButtonFinishAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest16_AnimationFinish;
                state = new DateCharaCommandAnimationFinishBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_DateSpotStartAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_DateSpotUpdate;
                state = new DateCharaCommandDateSpotUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_DateSpotFinishAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationReStart;
                state = new DateCharaCommandPhoneAnimationReStartBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_DateSpotUpdate:
                {
                DateCharaCommand_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN:
                        state_id = DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN;
                        state = new DateCharaCommandNovelFadeINBranchState(state_id);
                        state.Start(arg);
                        break;
                    case DateCharaCommand_Branch_State_ID.Nest14_DateSpotFinishAnimation:
                        state_id = DateCharaCommand_Branch_State_ID.Nest14_DateSpotFinishAnimation;
                        state = new DateCharaCommandDateSpotFinishAnimationBranchState(state_id);
                        state.Start(arg);
                        break;
                    case DateCharaCommand_Branch_State_ID.Nest14_CancelStartAnimation:
                        state_id = DateCharaCommand_Branch_State_ID.Nest14_CancelStartAnimation;
                        state = new DateCharaCommandCancelStartAnimationBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_DateSpotStartAnimation;
                state = new DateCharaCommandDateSpotStartAnimationBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationReStart:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_Update;
                state = new DateCharaCommandUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_CancelStartAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_CancelGameFinish;
                state = new DateCharaCommandCancelGameFinishBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest12_DateSpotFinishAnimation:
                state_id = DateCharaCommand_Branch_State_ID.Nest14_Update;
                state = new DateCharaCommandUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case DateCharaCommand_Branch_State_ID.Nest14_CancelPhoneAnimationImpFinish:
                state_id = DateCharaCommand_Branch_State_ID.Nest12_DateSpotFinishAnimation;
                state = new DateCharaCommandDateSpotFinishAnimationBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new DateCharaCommandBeginLoadBranchState(DateCharaCommand_Branch_State_ID.Nest01_BeginLoad);
    }
}
