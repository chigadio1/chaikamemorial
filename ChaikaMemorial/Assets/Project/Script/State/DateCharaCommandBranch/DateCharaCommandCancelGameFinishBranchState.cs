using System.Collections.Generic;
public class DateCharaCommandCancelGameFinishBranchState : BaseDateCharaCommandCancelGameFinishBranchState {

    NovelUpdateStateBranchManager manager = new NovelUpdateStateBranchManager();
    NovelUpdateInitArgData init_arg = new NovelUpdateInitArgData();
    public override void Start(DateCharaCommandInitArgData arg) {
        init_arg.event_id = 0;
        init_arg.event_id = DateUnit.CancelCharacterDateEventIDArray[(int)gameUiCore.Instance.SelectCharacterID];
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        manager.Update(init_arg);
        if(manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandCancelGameFinishBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
