using System.Collections.Generic;
public class BaseDateCharaCommandDateSpotUpdateBranchState : BaseDateCharaCommandBranchState{
    public BaseDateCharaCommandDateSpotUpdateBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
    public virtual DateCharaCommand_Branch_State_ID BranchNest14_DateSpotUpdateState(){
        if(BranchNest14_DateSpotUpdateToNovelFadeIN()) return DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN;
        else if(BranchNest14_DateSpotUpdateToDateSpotFinishAnimation()) return DateCharaCommand_Branch_State_ID.Nest14_DateSpotFinishAnimation;
        else if(BranchNest14_DateSpotUpdateToCancelStartAnimation()) return DateCharaCommand_Branch_State_ID.Nest14_CancelStartAnimation;
     return DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN;
     }
    public virtual bool BranchNest14_DateSpotUpdateToNovelFadeIN(){return true;}
    public virtual bool BranchNest14_DateSpotUpdateToDateSpotFinishAnimation(){return true;}
    public virtual bool BranchNest14_DateSpotUpdateToCancelStartAnimation(){return true;}
    public override DateCharaCommand_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case DateCharaCommand_Branch_State_ID.Nest14_DateSpotUpdate:
                return BranchNest14_DateSpotUpdateState();
        }
     return DateCharaCommand_Branch_State_ID.Nest15_NovelFadeIN;
     }
}
