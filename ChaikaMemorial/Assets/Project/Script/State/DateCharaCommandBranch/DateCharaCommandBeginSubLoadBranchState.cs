using System.Collections.Generic;
public class DateCharaCommandBeginSubLoadBranchState : BaseDateCharaCommandBeginSubLoadBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.DateCommandObject.IsLoad())
        {
            gameUiCore.Instance.ActionCanvas.DateCommandObject.SubInstance();
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandBeginSubLoadBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
