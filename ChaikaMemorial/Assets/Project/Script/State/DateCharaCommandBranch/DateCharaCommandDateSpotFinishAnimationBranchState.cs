using System.Collections.Generic;
public class DateCharaCommandDateSpotFinishAnimationBranchState : BaseDateCharaCommandDateSpotFinishAnimationBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.PlayAnim(Date_Spot_Commnad_Anim_ID.StartUP,true);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.DateCommandObject.IsPlayAnimDateSpot())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public DateCharaCommandDateSpotFinishAnimationBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
