using System.Collections.Generic;
public class DateCharaCommandPhoneAnimationReStartBranchState : BaseDateCharaCommandPhoneAnimationReStartBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Scal_BIG,true);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
            gameUiCore.Instance.is_button_push = false;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandPhoneAnimationReStartBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
