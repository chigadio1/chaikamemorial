using System.Collections.Generic;
public class DateCharaCommandButtonStartAnimationBranchState : BaseDateCharaCommandButtonStartAnimationBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.ButtonStartAnimation();
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.DateCommandObject.ButtonAnimation())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public DateCharaCommandButtonStartAnimationBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
