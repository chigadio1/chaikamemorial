using System.Collections.Generic;
public class DateCharaCommandPhoneAnimationStartBranchState : BaseDateCharaCommandPhoneAnimationStartBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Scal_BIG);
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, (int)Default_Action_Sound_ID.DateCommand_Start);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandPhoneAnimationStartBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
