using System.Collections.Generic;
public class DateCharaCommandCancelPhoneAnimationImpFinishBranchState : BaseDateCharaCommandCancelPhoneAnimationImpFinishBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.PlayAnim(Date_Spot_Commnad_Anim_ID.StartUP,5.0f,true);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.DateCommandObject.IsPlayAnimDateSpot())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandCancelPhoneAnimationImpFinishBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
