using System.Collections.Generic;
using UnityEngine;

public class DateCharaCommandAnimationFinishBranchState : BaseDateCharaCommandAnimationFinishBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.PlayAnim(Date_Commnad_Anim_ID.StartUP, true);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.DateCommandObject.IsPlayAnim())
        {

            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandAnimationFinishBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
