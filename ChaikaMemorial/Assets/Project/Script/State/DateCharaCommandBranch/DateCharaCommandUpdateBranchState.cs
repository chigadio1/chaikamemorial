using System.Collections.Generic;
using UnityEngine;

public class DateCharaCommandUpdateBranchState : BaseDateCharaCommandUpdateBranchState {

    bool is_back = false;
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (Input.GetMouseButtonDown(1)) // 1は右クリックのボタンを表します
        {
            is_back = is_end_update = true;
            // 右クリックが検出された場合の処理をここに追加
            return;
        }
        if(gameUiCore.Instance.is_button_push == true)
        {
            is_end_update = true;

            return;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandUpdateBranchState(DateCharaCommand_Branch_State_ID other):base(other){}

    public override bool BranchNest14_UpdateToButtonFinishAnimation()
    {
        return is_back == true;
    }

    public override bool BranchNest14_UpdateToPhoneAnimationStart()
    {
        return is_back == false;
    }
}
