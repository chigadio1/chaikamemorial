using System.Collections.Generic;
public class DateCharaCommandBeginLoadBranchState : BaseDateCharaCommandBeginLoadBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.SubSystemLoad(Action_Sub_Obj_ID.Date_Commnad);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsSubSystemLoad())
        {
            gameUiCore.Instance.ActionCanvas.InstanceSubSystem(Action_Sub_Obj_ID.Date_Commnad);
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandBeginLoadBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
