using System.Collections.Generic;
using UnityEngine;

public class DateCharaCommandDateSpotUpdateBranchState : BaseDateCharaCommandDateSpotUpdateBranchState
{
    private bool is_back;
    private bool is_next;
    private bool is_success;
    public override void Start(DateCharaCommandInitArgData arg)
    {
    }
    public override void Update(DateCharaCommandUpdateArgData arg)
    {
        if (Input.GetMouseButtonDown(1)) // 1は右クリックのボタンを表します
        {
            is_back = is_end_update = true;
            // 右クリックが検出された場合の処理をここに追加
            return;
        }
        if(gameUiCore.Instance.is_button_push)
        {
            is_next = is_end_update = true;
            is_success = DateUnit.CanInviteForDate(gameUiCore.Instance.SelectCharacterID);
            PlayerCore.Instance.SetIsDate(is_success);
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg)
    {
    }
    public DateCharaCommandDateSpotUpdateBranchState(DateCharaCommand_Branch_State_ID other) : base(other) { }

    public override bool BranchNest14_DateSpotUpdateToDateSpotFinishAnimation()
    {
        return is_back == true;
    }

    public override bool BranchNest14_DateSpotUpdateToNovelFadeIN()
    {
        return is_back == false && is_next == true && is_success == true;
    }

    public override bool BranchNest14_DateSpotUpdateToCancelStartAnimation()
    {
        return is_back == false && is_next == true && is_success == false;
    }
}
