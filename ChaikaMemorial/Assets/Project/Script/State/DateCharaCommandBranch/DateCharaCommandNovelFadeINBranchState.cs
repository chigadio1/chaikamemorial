using System.Collections.Generic;
public class DateCharaCommandNovelFadeINBranchState : BaseDateCharaCommandNovelFadeINBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Finish);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandNovelFadeINBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
