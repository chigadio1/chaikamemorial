using System.Collections.Generic;
public class BaseDateCharaCommandUpdateBranchState : BaseDateCharaCommandBranchState{
    public BaseDateCharaCommandUpdateBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
    public virtual DateCharaCommand_Branch_State_ID BranchNest14_UpdateState(){
        if(BranchNest14_UpdateToPhoneAnimationStart()) return DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart;
        else if(BranchNest14_UpdateToButtonFinishAnimation()) return DateCharaCommand_Branch_State_ID.Nest15_ButtonFinishAnimation;
     return DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart;
     }
    public virtual bool BranchNest14_UpdateToPhoneAnimationStart(){return true;}
    public virtual bool BranchNest14_UpdateToButtonFinishAnimation(){return true;}
    public override DateCharaCommand_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case DateCharaCommand_Branch_State_ID.Nest14_Update:
                return BranchNest14_UpdateState();
        }
     return DateCharaCommand_Branch_State_ID.Nest14_PhoneAnimationStart;
     }
}
