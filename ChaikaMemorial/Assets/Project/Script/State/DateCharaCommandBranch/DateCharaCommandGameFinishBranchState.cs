using System.Collections.Generic;
using UnityEngine;

public class DateCharaCommandGameFinishBranchState : BaseDateCharaCommandGameFinishBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.ReleaseSubSystem();
        Resources.UnloadUnusedAssets();
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandGameFinishBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
