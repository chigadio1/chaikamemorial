using System.Collections.Generic;
public class DateCharaCommandCancelStartAnimationBranchState : BaseDateCharaCommandCancelStartAnimationBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(Action_Canvas_Anim.Finish);
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public DateCharaCommandCancelStartAnimationBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
