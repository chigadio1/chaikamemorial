using System.Collections.Generic;
public class DateCharaCommandButtonFinishAnimationBranchState : BaseDateCharaCommandButtonFinishAnimationBranchState {
    public override void Start(DateCharaCommandInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.DateCommandObject.ButtonFinishAnimation();
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.DateCommandObject.ButtonAnimation())
        {
            is_end_update = true;
        }
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandButtonFinishAnimationBranchState(DateCharaCommand_Branch_State_ID other):base(other){}
}
