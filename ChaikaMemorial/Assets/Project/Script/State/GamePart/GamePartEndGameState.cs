using System.Collections.Generic;
public class GamePartEndGameState : BaseGamePartEndGameState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartEndGameState(BaseGamePartState other):base(other){}
    public GamePartEndGameState():base(){}
}
