using System.Collections.Generic;
public class GamePartBeginDateState : BaseGamePartBeginDateState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginDateState(BaseGamePartState other):base(other){}
    public GamePartBeginDateState():base(){}
}
