using System.Collections.Generic;
public class GamePartBeginUpdateState : BaseGamePartBeginUpdateState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginUpdateState(BaseGamePartState other):base(other){}
    public GamePartBeginUpdateState():base(){}
}
