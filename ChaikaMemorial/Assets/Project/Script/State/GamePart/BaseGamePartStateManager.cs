using System.Collections.Generic;
public class BaseGamePartStateManager : BaseStateManager<GamePartInitArgData,GamePartUpdateArgData,BaseGamePartState> {
    protected GamePart_State_ID state_id = GamePart_State_ID.BeginStart;
    public GamePart_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(GamePartInitArgData arg = null){
        switch (state_id)
        {
            case GamePart_State_ID.Begin:
                state = new GamePartBeginStartState(state);
                state_id = GamePart_State_ID.BeginStart;
                state.Start(arg);
                break;
            case GamePart_State_ID.BeginStart:
                state = new GamePartBeginUpdateState(state);
                state_id = GamePart_State_ID.BeginUpdate;
                state.Start(arg);
                break;
            case GamePart_State_ID.BeginUpdate:
                state = new GamePartBeginEventState(state);
                state_id = GamePart_State_ID.BeginEvent;
                state.Start(arg);
                break;
            case GamePart_State_ID.BeginEvent:
                state = new GamePartGameSelectState(state);
                state_id = GamePart_State_ID.GameSelect;
                state.Start(arg);
                break;
            case GamePart_State_ID.GameSelect:
                state = new GamePartActionSelectUpdateState(state);
                state_id = GamePart_State_ID.ActionSelectUpdate;
                state.Start(arg);
                break;
            case GamePart_State_ID.ActionSelectUpdate:
                state = new GamePartFinishUpdateState(state);
                state_id = GamePart_State_ID.FinishUpdate;
                state.Start(arg);
                break;
            case GamePart_State_ID.FinishUpdate:
                state = new GamePartFinishEventState(state);
                state_id = GamePart_State_ID.FinishEvent;
                state.Start(arg);
                break;
            case GamePart_State_ID.FinishEvent:
                state = new GamePartEndGameState(state);
                state_id = GamePart_State_ID.EndGame;
                state.Start(arg);
                break;
            case GamePart_State_ID.EndGame:
                state = new GamePartBeginDateState(state);
                state_id = GamePart_State_ID.BeginDate;
                state.Start(arg);
                break;
            case GamePart_State_ID.BeginDate:
                state = new GamePartDateUpdateState(state);
                state_id = GamePart_State_ID.DateUpdate;
                state.Start(arg);
                break;
            case GamePart_State_ID.DateUpdate:
                state = new GamePartFinishDateState(state);
                state_id = GamePart_State_ID.FinishDate;
                state.Start(arg);
                break;
            case GamePart_State_ID.FinishDate:
                state = new GamePartGameLoadState(state);
                state_id = GamePart_State_ID.GameLoad;
                state.Start(arg);
                break;
            case GamePart_State_ID.GameLoad:
                is_finish = true;
                state_id = GamePart_State_ID.Finish;
                break;
            case GamePart_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(GamePart_State_ID value_state_id,GamePartInitArgData init_arg = null,GamePartUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case GamePart_State_ID.Begin:
                is_finish = true;
                break;
            case GamePart_State_ID.BeginStart:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartBeginStartState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.BeginUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartBeginUpdateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.BeginEvent:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartBeginEventState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.GameSelect:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartGameSelectState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.ActionSelectUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartActionSelectUpdateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.FinishUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartFinishUpdateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.FinishEvent:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartFinishEventState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.EndGame:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartEndGameState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.BeginDate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartBeginDateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.DateUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartDateUpdateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.FinishDate:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartFinishDateState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.GameLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new GamePartGameLoadState(state);
                state.Start(init_arg);
                break;
            case GamePart_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new GamePartBeginStartState();
    }
}
