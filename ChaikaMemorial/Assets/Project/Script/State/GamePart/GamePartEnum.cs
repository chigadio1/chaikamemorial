using System;
public enum GamePart_State_ID {
    Begin, //初期
    BeginStart, //
    BeginUpdate, //
    BeginEvent, //
    GameSelect, //
    ActionSelectUpdate, //
    FinishUpdate, //
    FinishEvent, //
    EndGame, //
    BeginDate, //
    DateUpdate, //
    FinishDate, //
    GameLoad, //
    Finish //終了
}
