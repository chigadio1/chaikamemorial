using System.Collections.Generic;
public class GamePartDateUpdateState : BaseGamePartDateUpdateState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartDateUpdateState(BaseGamePartState other):base(other){}
    public GamePartDateUpdateState():base(){}
}
