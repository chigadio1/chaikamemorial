using System.Collections.Generic;
public class GamePartActionSelectUpdateState : BaseGamePartActionSelectUpdateState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartActionSelectUpdateState(BaseGamePartState other):base(other){}
    public GamePartActionSelectUpdateState():base(){}
}
