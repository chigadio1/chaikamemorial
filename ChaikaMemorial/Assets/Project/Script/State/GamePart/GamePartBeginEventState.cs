using System.Collections.Generic;
public class GamePartBeginEventState : BaseGamePartBeginEventState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginEventState(BaseGamePartState other):base(other){}
    public GamePartBeginEventState():base(){}
}
