using System.Collections.Generic;
public class GamePartFinishUpdateState : BaseGamePartFinishUpdateState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartFinishUpdateState(BaseGamePartState other):base(other){}
    public GamePartFinishUpdateState():base(){}
}
