using System.Collections.Generic;
public class GamePartBeginStartState : BaseGamePartBeginStartState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartBeginStartState(BaseGamePartState other):base(other){}
    public GamePartBeginStartState():base(){}
}
