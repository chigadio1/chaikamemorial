using System.Collections.Generic;
public class GamePartGameLoadState : BaseGamePartGameLoadState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartGameLoadState(BaseGamePartState other):base(other){}
    public GamePartGameLoadState():base(){}
}
