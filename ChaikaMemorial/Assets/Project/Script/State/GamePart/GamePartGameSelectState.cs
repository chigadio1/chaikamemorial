using System.Collections.Generic;
public class GamePartGameSelectState : BaseGamePartGameSelectState {
    public override void Start(GamePartInitArgData arg) {
    }
    public override void Update(GamePartUpdateArgData arg) {
    }
    public override void Finish(GamePartUpdateArgData arg) {
    }
    public GamePartGameSelectState(BaseGamePartState other):base(other){}
    public GamePartGameSelectState():base(){}
}
