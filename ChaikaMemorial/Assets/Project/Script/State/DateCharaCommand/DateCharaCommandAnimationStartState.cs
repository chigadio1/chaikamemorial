using System.Collections.Generic;
public class DateCharaCommandAnimationStartState : BaseDateCharaCommandAnimationStartState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandAnimationStartState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandAnimationStartState():base(){}
}
