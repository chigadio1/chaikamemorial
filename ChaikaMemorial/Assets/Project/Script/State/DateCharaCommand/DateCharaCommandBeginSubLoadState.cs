using System.Collections.Generic;
public class DateCharaCommandBeginSubLoadState : BaseDateCharaCommandBeginSubLoadState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandBeginSubLoadState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandBeginSubLoadState():base(){}
}
