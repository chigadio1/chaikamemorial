using System.Collections.Generic;
public class DateCharaCommandCancelGameFinishState : BaseDateCharaCommandCancelGameFinishState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandCancelGameFinishState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandCancelGameFinishState():base(){}
}
