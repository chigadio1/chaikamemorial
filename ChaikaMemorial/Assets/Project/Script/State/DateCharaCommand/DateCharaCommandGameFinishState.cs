using System.Collections.Generic;
public class DateCharaCommandGameFinishState : BaseDateCharaCommandGameFinishState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandGameFinishState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandGameFinishState():base(){}
}
