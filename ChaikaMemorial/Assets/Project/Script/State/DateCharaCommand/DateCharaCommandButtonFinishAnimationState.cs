using System.Collections.Generic;
public class DateCharaCommandButtonFinishAnimationState : BaseDateCharaCommandButtonFinishAnimationState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandButtonFinishAnimationState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandButtonFinishAnimationState():base(){}
}
