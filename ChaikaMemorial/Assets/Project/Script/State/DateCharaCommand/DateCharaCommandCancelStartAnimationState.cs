using System.Collections.Generic;
public class DateCharaCommandCancelStartAnimationState : BaseDateCharaCommandCancelStartAnimationState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandCancelStartAnimationState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandCancelStartAnimationState():base(){}
}
