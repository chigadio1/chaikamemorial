using System.Collections.Generic;
public class DateCharaCommandButtonStartAnimationState : BaseDateCharaCommandButtonStartAnimationState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandButtonStartAnimationState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandButtonStartAnimationState():base(){}
}
