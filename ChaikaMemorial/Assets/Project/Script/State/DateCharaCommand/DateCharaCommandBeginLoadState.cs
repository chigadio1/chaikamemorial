using System.Collections.Generic;
public class DateCharaCommandBeginLoadState : BaseDateCharaCommandBeginLoadState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandBeginLoadState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandBeginLoadState():base(){}
}
