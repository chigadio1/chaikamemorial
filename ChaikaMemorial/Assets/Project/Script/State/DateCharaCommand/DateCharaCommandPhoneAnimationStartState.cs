using System.Collections.Generic;
public class DateCharaCommandPhoneAnimationStartState : BaseDateCharaCommandPhoneAnimationStartState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandPhoneAnimationStartState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandPhoneAnimationStartState():base(){}
}
