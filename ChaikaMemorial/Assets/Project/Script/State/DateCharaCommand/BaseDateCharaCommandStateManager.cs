using System.Collections.Generic;
public class BaseDateCharaCommandStateManager : BaseStateManager<DateCharaCommandInitArgData,DateCharaCommandUpdateArgData,BaseDateCharaCommandState> {
    protected DateCharaCommand_State_ID state_id = DateCharaCommand_State_ID.BeginLoad;
    public DateCharaCommand_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(DateCharaCommandInitArgData arg = null){
        switch (state_id)
        {
            case DateCharaCommand_State_ID.Begin:
                state = new DateCharaCommandBeginLoadState(state);
                state_id = DateCharaCommand_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.BeginLoad:
                state = new DateCharaCommandBeginSubLoadState(state);
                state_id = DateCharaCommand_State_ID.BeginSubLoad;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.BeginSubLoad:
                state = new DateCharaCommandUpdateState(state);
                state_id = DateCharaCommand_State_ID.Update;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.Update:
                state = new DateCharaCommandNovelFadeINState(state);
                state_id = DateCharaCommand_State_ID.NovelFadeIN;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.NovelFadeIN:
                state = new DateCharaCommandNovelUpdateState(state);
                state_id = DateCharaCommand_State_ID.NovelUpdate;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.NovelUpdate:
                state = new DateCharaCommandNovelFinishState(state);
                state_id = DateCharaCommand_State_ID.NovelFinish;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.NovelFinish:
                state = new DateCharaCommandCancelGameFinishState(state);
                state_id = DateCharaCommand_State_ID.CancelGameFinish;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.CancelGameFinish:
                state = new DateCharaCommandGameFinishState(state);
                state_id = DateCharaCommand_State_ID.GameFinish;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.GameFinish:
                state = new DateCharaCommandAnimationStartState(state);
                state_id = DateCharaCommand_State_ID.AnimationStart;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.AnimationStart:
                state = new DateCharaCommandAnimationFinishState(state);
                state_id = DateCharaCommand_State_ID.AnimationFinish;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.AnimationFinish:
                state = new DateCharaCommandButtonStartAnimationState(state);
                state_id = DateCharaCommand_State_ID.ButtonStartAnimation;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.ButtonStartAnimation:
                state = new DateCharaCommandButtonFinishAnimationState(state);
                state_id = DateCharaCommand_State_ID.ButtonFinishAnimation;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.ButtonFinishAnimation:
                state = new DateCharaCommandDateSpotStartAnimationState(state);
                state_id = DateCharaCommand_State_ID.DateSpotStartAnimation;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.DateSpotStartAnimation:
                state = new DateCharaCommandDateSpotFinishAnimationState(state);
                state_id = DateCharaCommand_State_ID.DateSpotFinishAnimation;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.DateSpotFinishAnimation:
                state = new DateCharaCommandDateSpotUpdateState(state);
                state_id = DateCharaCommand_State_ID.DateSpotUpdate;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.DateSpotUpdate:
                state = new DateCharaCommandPhoneAnimationStartState(state);
                state_id = DateCharaCommand_State_ID.PhoneAnimationStart;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.PhoneAnimationStart:
                state = new DateCharaCommandPhoneAnimationReStartState(state);
                state_id = DateCharaCommand_State_ID.PhoneAnimationReStart;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.PhoneAnimationReStart:
                state = new DateCharaCommandCancelStartAnimationState(state);
                state_id = DateCharaCommand_State_ID.CancelStartAnimation;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.CancelStartAnimation:
                state = new DateCharaCommandCancelPhoneAnimationImpFinishState(state);
                state_id = DateCharaCommand_State_ID.CancelPhoneAnimationImpFinish;
                state.Start(arg);
                break;
            case DateCharaCommand_State_ID.CancelPhoneAnimationImpFinish:
                is_finish = true;
                state_id = DateCharaCommand_State_ID.Finish;
                break;
            case DateCharaCommand_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(DateCharaCommand_State_ID value_state_id,DateCharaCommandInitArgData init_arg = null,DateCharaCommandUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case DateCharaCommand_State_ID.Begin:
                is_finish = true;
                break;
            case DateCharaCommand_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandBeginLoadState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.BeginSubLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandBeginSubLoadState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.Update:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandUpdateState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.NovelFadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandNovelFadeINState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.NovelUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandNovelUpdateState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.NovelFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandNovelFinishState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.CancelGameFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandCancelGameFinishState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.GameFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandGameFinishState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.AnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandAnimationStartState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.AnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.ButtonStartAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandButtonStartAnimationState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.ButtonFinishAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandButtonFinishAnimationState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.DateSpotStartAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandDateSpotStartAnimationState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.DateSpotFinishAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandDateSpotFinishAnimationState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.DateSpotUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandDateSpotUpdateState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.PhoneAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandPhoneAnimationStartState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.PhoneAnimationReStart:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandPhoneAnimationReStartState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.CancelStartAnimation:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandCancelStartAnimationState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.CancelPhoneAnimationImpFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new DateCharaCommandCancelPhoneAnimationImpFinishState(state);
                state.Start(init_arg);
                break;
            case DateCharaCommand_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new DateCharaCommandBeginLoadState();
    }
}
