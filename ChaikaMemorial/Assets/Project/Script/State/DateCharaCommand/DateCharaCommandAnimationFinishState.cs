using System.Collections.Generic;
public class DateCharaCommandAnimationFinishState : BaseDateCharaCommandAnimationFinishState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandAnimationFinishState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandAnimationFinishState():base(){}
}
