using System.Collections.Generic;
public class DateCharaCommandPhoneAnimationReStartState : BaseDateCharaCommandPhoneAnimationReStartState {
    public override void Start(DateCharaCommandInitArgData arg) {
    }
    public override void Update(DateCharaCommandUpdateArgData arg) {
    }
    public override void Finish(DateCharaCommandUpdateArgData arg) {
    }
    public DateCharaCommandPhoneAnimationReStartState(BaseDateCharaCommandState other):base(other){}
    public DateCharaCommandPhoneAnimationReStartState():base(){}
}
