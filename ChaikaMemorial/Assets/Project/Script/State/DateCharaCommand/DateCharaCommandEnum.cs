using System;
public enum DateCharaCommand_State_ID {
    Begin, //初期
    BeginLoad, //
    BeginSubLoad, //
    Update, //
    NovelFadeIN, //
    NovelUpdate, //
    NovelFinish, //
    CancelGameFinish, //
    GameFinish, //
    AnimationStart, //
    AnimationFinish, //
    ButtonStartAnimation, //
    ButtonFinishAnimation, //
    DateSpotStartAnimation, //
    DateSpotFinishAnimation, //
    DateSpotUpdate, //
    PhoneAnimationStart, //
    PhoneAnimationReStart, //
    CancelStartAnimation, //
    CancelPhoneAnimationImpFinish, //
    Finish //終了
}
