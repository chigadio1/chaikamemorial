using System.Collections.Generic;
public class BaseTestStateManager : BaseStateManager<TestInitArgData,TestUpdateArgData,BaseTestState> {
    protected Test_State_ID state_id = Test_State_ID.StartBegin;
    public Test_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(TestInitArgData arg = null){
        switch (state_id)
        {
            case Test_State_ID.Begin:
                state = new TestStartBeginState(state);
                state_id = Test_State_ID.StartBegin;
                state.Start(arg);
                break;
            case Test_State_ID.StartBegin:
                state = new TestUpdateExecState(state);
                state_id = Test_State_ID.UpdateExec;
                state.Start(arg);
                break;
            case Test_State_ID.UpdateExec:
                state = new TestEndFinishState(state);
                state_id = Test_State_ID.EndFinish;
                state.Start(arg);
                break;
            case Test_State_ID.EndFinish:
                is_finish = true;
                state_id = Test_State_ID.Finish;
                break;
            case Test_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(Test_State_ID value_state_id,TestInitArgData init_arg = null,TestUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case Test_State_ID.Begin:
                is_finish = true;
                break;
            case Test_State_ID.StartBegin:
                if(state != null) state.Finish(updatet_arg);
                state = new TestStartBeginState(state);
                state.Start(init_arg);
                break;
            case Test_State_ID.UpdateExec:
                if(state != null) state.Finish(updatet_arg);
                state = new TestUpdateExecState(state);
                state.Start(init_arg);
                break;
            case Test_State_ID.EndFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new TestEndFinishState(state);
                state.Start(init_arg);
                break;
            case Test_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new TestStartBeginState();
    }
}
