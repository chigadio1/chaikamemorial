using System.Collections.Generic;
public class TestStartBeginState : BaseTestStartBeginState {
    public override void Start(TestInitArgData arg) {
    }
    public override void Update(TestUpdateArgData arg) {
    }
    public override void Finish(TestUpdateArgData arg) {
    }
    public TestStartBeginState(BaseTestState other):base(other){}
    public TestStartBeginState():base(){}
}
