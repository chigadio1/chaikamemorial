using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationBranchState : BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationBranchState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject?.PlayAnim(Character_Favo_Anim_ID.LooPIdle);
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
}
