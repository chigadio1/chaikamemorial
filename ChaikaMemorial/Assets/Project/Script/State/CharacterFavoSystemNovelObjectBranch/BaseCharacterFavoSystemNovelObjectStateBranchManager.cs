using System.Collections.Generic;
public class BaseCharacterFavoSystemNovelObjectStateBranchManager : BaseStateBranchManager<CharacterFavoSystemNovelObjectInitArgData,CharacterFavoSystemNovelObjectUpdateArgData,BaseCharacterFavoSystemNovelObjectBranchState,CharacterFavoSystemNovelObject_Branch_State_ID > {
    public BaseCharacterFavoSystemNovelObjectStateBranchManager(CharacterFavoSystemNovelObject_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(CharacterFavoSystemNovelObjectInitArgData arg = null){
        switch (state_id)
        {
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest01_BeginLoad:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest02_IsLoad;
                state = new CharacterFavoSystemNovelObjectIsLoadBranchState(state_id);
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest03_FadeOut:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdate;
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdate:
                {
                CharacterFavoSystemNovelObject_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove:
                        state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove;
                        state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateMoveBranchState(state_id);
                        state.Start(arg);
                        break;
                    case CharacterFavoSystemNovelObject_Branch_State_ID.Nest07_FadeIn:
                        state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest07_FadeIn;
                        state = new CharacterFavoSystemNovelObjectFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateAnimation;
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateAnimationBranchState(state_id);
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateAnimation:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdate;
                state = new CharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest07_FadeIn:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest08_FinishRelease;
                state = new CharacterFavoSystemNovelObjectFinishReleaseBranchState(state_id);
                state.Start(arg);
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest08_FinishRelease:
                is_finish = true;
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Finish;
                break;
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest02_IsLoad:
                state_id = CharacterFavoSystemNovelObject_Branch_State_ID.Nest03_FadeOut;
                state = new CharacterFavoSystemNovelObjectFadeOutBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new CharacterFavoSystemNovelObjectBeginLoadBranchState(CharacterFavoSystemNovelObject_Branch_State_ID.Nest01_BeginLoad);
    }
}
