using System.Collections.Generic;
using UnityEngine;

public class CharacterFavoSystemNovelObjectSelectCharacterUpdateMoveBranchState : BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateMoveBranchState {
    float calc_time = 0.0f;
    float max_time = 0.3f;
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject?.StartMove(gameUiCore.Instance.IsMoveSelect);
        gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject?.PlayAnim(Character_Favo_Anim_ID.StartUP);
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(0.0f, 1.0f, calc_time / max_time);
        bool res = gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject.UpdateMove(lerp);
        if (res && gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject.IsPlayAnim())
        {
            is_end_update = true;
        }

    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateMoveBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
}
