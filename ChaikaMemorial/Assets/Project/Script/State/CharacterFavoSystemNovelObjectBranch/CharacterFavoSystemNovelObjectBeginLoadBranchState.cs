using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectBeginLoadBranchState : BaseCharacterFavoSystemNovelObjectBeginLoadBranchState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.SystemLoad(Setting_System_ID.CharacterFovo);
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        if(gameUiCore.Instance.SettingCanvas.IsSystemLoad())
        {
            gameUiCore.Instance.SettingCanvas.ValueSystemDataInstance(Setting_System_ID.CharacterFovo);
            is_end_update = true;
        }
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectBeginLoadBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
}
