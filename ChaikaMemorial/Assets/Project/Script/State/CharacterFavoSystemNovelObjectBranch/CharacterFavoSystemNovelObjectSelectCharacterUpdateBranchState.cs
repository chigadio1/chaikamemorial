using System.Collections.Generic;
using UnityEngine;

public class CharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState : BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState {

    bool is_end_back = false;
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            gameUiCore.Instance.IsMoveSelect = true;
            is_end_update = true;
            gameUiCore.Instance.GeneralPurposeIndex -= 1;
            if(gameUiCore.Instance.GeneralPurposeIndex < 0)
            {
                gameUiCore.Instance.GeneralPurposeIndex = (int)Character_ID.MAX - 1;
            }
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            gameUiCore.Instance.IsMoveSelect = false;
            is_end_update = true;
            gameUiCore.Instance.GeneralPurposeIndex = gameUiCore.Instance.GeneralPurposeIndex % (int)Character_ID.MAX;
        }
        else if(Input.GetKeyDown(KeyCode.Backspace))
        {
            is_end_back = true;
            is_end_update = true;
        }
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}

    public override bool BranchNest06_SelectCharacterUpdateToFadeIn()
    {
        return is_end_back == true;
    }

    public override bool BranchNest06_SelectCharacterUpdateToSelectCharacterUpdateMove()
    {
        return is_end_back == false;
    }
}
