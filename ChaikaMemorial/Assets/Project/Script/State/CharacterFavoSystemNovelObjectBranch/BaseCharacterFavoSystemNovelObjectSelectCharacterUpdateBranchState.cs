using System.Collections.Generic;
public class BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState : BaseCharacterFavoSystemNovelObjectBranchState{
    public BaseCharacterFavoSystemNovelObjectSelectCharacterUpdateBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
    public virtual CharacterFavoSystemNovelObject_Branch_State_ID BranchNest06_SelectCharacterUpdateState(){
        if(BranchNest06_SelectCharacterUpdateToSelectCharacterUpdateMove()) return CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove;
        else if(BranchNest06_SelectCharacterUpdateToFadeIn()) return CharacterFavoSystemNovelObject_Branch_State_ID.Nest07_FadeIn;
     return CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove;
     }
    public virtual bool BranchNest06_SelectCharacterUpdateToSelectCharacterUpdateMove(){return true;}
    public virtual bool BranchNest06_SelectCharacterUpdateToFadeIn(){return true;}
    public override CharacterFavoSystemNovelObject_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdate:
                return BranchNest06_SelectCharacterUpdateState();
        }
     return CharacterFavoSystemNovelObject_Branch_State_ID.Nest06_SelectCharacterUpdateMove;
     }
}
