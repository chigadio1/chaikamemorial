using System.Collections.Generic;
using UnityEngine;

public class CharacterFavoSystemNovelObjectFinishReleaseBranchState : BaseCharacterFavoSystemNovelObjectFinishReleaseBranchState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
        gameUiCore.Instance.SettingCanvas.Release();
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        Resources.UnloadUnusedAssets();
    }
    public CharacterFavoSystemNovelObjectFinishReleaseBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
}
