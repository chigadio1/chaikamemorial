using System.Collections.Generic;
public class CharacterFavoSystemNovelObjectIsLoadBranchState : BaseCharacterFavoSystemNovelObjectIsLoadBranchState {
    public override void Start(CharacterFavoSystemNovelObjectInitArgData arg) {
    }
    public override void Update(CharacterFavoSystemNovelObjectUpdateArgData arg) {
        if(gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject.IsLoad())
        {
            gameUiCore.Instance.SettingCanvas.CharacterFavoSystemNovelObject?.PlayAnim(Character_Favo_Anim_ID.StartUP);
            is_end_update = true;
        }
    }
    public override void Finish(CharacterFavoSystemNovelObjectUpdateArgData arg) {
    }
    public CharacterFavoSystemNovelObjectIsLoadBranchState(CharacterFavoSystemNovelObject_Branch_State_ID other):base(other){}
}
