using System.Collections.Generic;
public class TalkActionEventEvenInitState : BaseTalkActionEventEvenInitState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventEvenInitState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventEvenInitState():base(){}
}
