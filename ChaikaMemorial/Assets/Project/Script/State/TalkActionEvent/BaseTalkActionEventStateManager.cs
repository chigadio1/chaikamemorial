using System.Collections.Generic;
public class BaseTalkActionEventStateManager : BaseStateManager<TalkActionEventInitArgData,TalkActionEventUpdateArgData,BaseTalkActionEventState> {
    protected TalkActionEvent_State_ID state_id = TalkActionEvent_State_ID.BeginInit;
    public TalkActionEvent_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(TalkActionEventInitArgData arg = null){
        switch (state_id)
        {
            case TalkActionEvent_State_ID.Begin:
                state = new TalkActionEventBeginInitState(state);
                state_id = TalkActionEvent_State_ID.BeginInit;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.BeginInit:
                state = new TalkActionEventFramePopState(state);
                state_id = TalkActionEvent_State_ID.FramePop;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FramePop:
                state = new TalkActionEventFrameTextUpdateState(state);
                state_id = TalkActionEvent_State_ID.FrameTextUpdate;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FrameTextUpdate:
                state = new TalkActionEventFrameResultIconState(state);
                state_id = TalkActionEvent_State_ID.FrameResultIcon;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FrameResultIcon:
                state = new TalkActionEventStatusUPState(state);
                state_id = TalkActionEvent_State_ID.StatusUP;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.StatusUP:
                state = new TalkActionEventFadeINState(state);
                state_id = TalkActionEvent_State_ID.FadeIN;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FadeIN:
                state = new TalkActionEventEvenInitState(state);
                state_id = TalkActionEvent_State_ID.EvenInit;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.EvenInit:
                state = new TalkActionEventEventUpdateState(state);
                state_id = TalkActionEvent_State_ID.EventUpdate;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.EventUpdate:
                state = new TalkActionEventEventFadeOutState(state);
                state_id = TalkActionEvent_State_ID.EventFadeOut;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.EventFadeOut:
                state = new TalkActionEventFadeOutState(state);
                state_id = TalkActionEvent_State_ID.FadeOut;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FadeOut:
                state = new TalkActionEventSchoolCheckState(state);
                state_id = TalkActionEvent_State_ID.SchoolCheck;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.SchoolCheck:
                state = new TalkActionEventStartEventInitState(state);
                state_id = TalkActionEvent_State_ID.StartEventInit;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.StartEventInit:
                state = new TalkActionEventStartEventUpdateState(state);
                state_id = TalkActionEvent_State_ID.StartEventUpdate;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.StartEventUpdate:
                state = new TalkActionEventStartEventFadeOutState(state);
                state_id = TalkActionEvent_State_ID.StartEventFadeOut;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.StartEventFadeOut:
                state = new TalkActionEventGameFinishState(state);
                state_id = TalkActionEvent_State_ID.GameFinish;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.GameFinish:
                state = new TalkActionEventFinishFadeInState(state);
                state_id = TalkActionEvent_State_ID.FinishFadeIn;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.FinishFadeIn:
                state = new TalkActionEventEventGenericUpdateState(state);
                state_id = TalkActionEvent_State_ID.EventGenericUpdate;
                state.Start(arg);
                break;
            case TalkActionEvent_State_ID.EventGenericUpdate:
                is_finish = true;
                state_id = TalkActionEvent_State_ID.Finish;
                break;
            case TalkActionEvent_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(TalkActionEvent_State_ID value_state_id,TalkActionEventInitArgData init_arg = null,TalkActionEventUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case TalkActionEvent_State_ID.Begin:
                is_finish = true;
                break;
            case TalkActionEvent_State_ID.BeginInit:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventBeginInitState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FramePop:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFramePopState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FrameTextUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFrameTextUpdateState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FrameResultIcon:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFrameResultIconState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.StatusUP:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventStatusUPState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFadeINState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.EvenInit:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventEvenInitState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.EventUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventEventUpdateState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.EventFadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventEventFadeOutState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFadeOutState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.SchoolCheck:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventSchoolCheckState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.StartEventInit:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventStartEventInitState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.StartEventUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventStartEventUpdateState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.StartEventFadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventStartEventFadeOutState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.GameFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventGameFinishState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.FinishFadeIn:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventFinishFadeInState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.EventGenericUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TalkActionEventEventGenericUpdateState(state);
                state.Start(init_arg);
                break;
            case TalkActionEvent_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new TalkActionEventBeginInitState();
    }
}
