using System.Collections.Generic;
public class TalkActionEventGameFinishState : BaseTalkActionEventGameFinishState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventGameFinishState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventGameFinishState():base(){}
}
