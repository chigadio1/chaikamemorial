using System.Collections.Generic;
public class TalkActionEventEventGenericUpdateState : BaseTalkActionEventEventGenericUpdateState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventEventGenericUpdateState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventEventGenericUpdateState():base(){}
}
