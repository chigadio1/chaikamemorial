using System.Collections.Generic;
public class TalkActionEventStatusUPState : BaseTalkActionEventStatusUPState {
    public override void Start(TalkActionEventInitArgData arg) {
        is_end_update = false;
        gameUiCore.Instance?.StatusCanvas?.SetStatusNum((int)arg.status_id, arg.status_num);
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(gameUiCore.Instance.StatusCanvas.GetIsNumFix(arg.status_id))
        {
            is_end_update=true;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventStatusUPState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventStatusUPState():base(){}
}
