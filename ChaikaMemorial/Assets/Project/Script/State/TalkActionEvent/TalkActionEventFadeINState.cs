using System.Collections.Generic;
using UnityEngine;

public class TalkActionEventFadeINState : BaseTalkActionEventFadeINState {
    public override void Start(TalkActionEventInitArgData arg) {
        add_calc_time = 0.0f;
        is_end_update = false;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(arg.novel_id <= 0)
        {
            is_end_update = true;
        }

        add_calc_time += Time.deltaTime;
        add_calc_time = Mathf.Clamp(add_calc_time, 0.0f, 1.0f);
        gameUiCore.Instance?.FadeCanvas?.SetFadeAlpha(add_calc_time / 1.0f);
        if(add_calc_time >= 1.0f)
        {
            is_end_update = true;
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg)
    {
        if (arg.novel_id > 0)
        {
            gameUiCore.Instance?.FadeCanvas?.SetFadeAlpha(1.0f);
            gameUiCore.Instance?.SetActive(gameUiCore.UI_Object_ID.NOVEL, true);
            gameUiCore.Instance?.SetActive(gameUiCore.UI_Object_ID.BEHAVIOR, false);
        }
        add_calc_time = 0.0f;
    }
    public TalkActionEventFadeINState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFadeINState():base(){}
}
