using System.Collections.Generic;
public class TalkActionEventBeginInitState : BaseTalkActionEventBeginInitState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventBeginInitState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventBeginInitState():base(){}
}
