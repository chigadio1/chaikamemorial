using System.Collections.Generic;
public class TalkActionEventFinishFadeInState : BaseTalkActionEventFinishFadeInState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFinishFadeInState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFinishFadeInState():base(){}
}
