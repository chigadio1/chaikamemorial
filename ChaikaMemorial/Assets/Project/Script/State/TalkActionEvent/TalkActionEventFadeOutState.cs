using System.Collections.Generic;
public class TalkActionEventFadeOutState : BaseTalkActionEventFadeOutState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFadeOutState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFadeOutState():base(){}
}
