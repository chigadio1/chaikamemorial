using System.Collections.Generic;
public class TalkActionEventFramePopState : BaseTalkActionEventFramePopState {
    public override void Start(TalkActionEventInitArgData arg) {
        gameUiCore.Instance?.StatusCanvas?.AddChatData();
        gameUiCore.Instance?.StatusCanvas?.SetTextTalk("");
        is_end_update = false;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {

        
        is_end_update = true;

    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFramePopState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFramePopState():base(){}
}
