using System.Collections.Generic;
using System;
public class TalkActionEventInitArgData : BaseStateArgData{
    public int novel_id; //
    public int chat_talk_index; //
    public int status_num; //
    public Status_ID status_id; //
    public Result_Action_TypeID result_id; //
}
