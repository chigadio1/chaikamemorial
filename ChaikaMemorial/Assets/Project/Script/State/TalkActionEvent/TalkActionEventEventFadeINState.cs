using System.Collections.Generic;
public class TalkActionEventEventFadeINState : BaseTalkActionEventEventFadeINState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventEventFadeINState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventEventFadeINState():base(){}
}
