using System.Collections.Generic;
public class TalkActionEventStartEventInitState : BaseTalkActionEventStartEventInitState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventStartEventInitState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventStartEventInitState():base(){}
}
