using System.Collections.Generic;
public class TalkActionEventStartEventFadeOutState : BaseTalkActionEventStartEventFadeOutState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventStartEventFadeOutState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventStartEventFadeOutState():base(){}
}
