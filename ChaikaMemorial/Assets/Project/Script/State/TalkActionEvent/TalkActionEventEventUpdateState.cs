using System.Collections.Generic;
public class TalkActionEventEventUpdateState : BaseTalkActionEventEventUpdateState {

   
    public override void Start(TalkActionEventInitArgData arg) {
        if(arg.novel_id > 0)
        {
            gameUiCore.Instance?.PlayEvent(arg.novel_id);
        }
        is_end_update = false;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        if(arg.novel_id <= 0)
        {
            is_end_update = true;
            return;
        }
        else
        {
            gameUiCore.Instance?.NovelUpdate();
            if(gameUiCore.Instance.IsNovelEnd)
            {
                is_end_update = true;
            }
        }
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
        if (arg.novel_id > 0)
        {
            gameUiCore.Instance?.PlayEnd();
        }
    }
    public TalkActionEventEventUpdateState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventEventUpdateState():base(){}
}
