using System.Collections.Generic;
using UnityEngine;

public class TalkActionEventFrameTextUpdateState : BaseTalkActionEventFrameTextUpdateState {

    public int max_load_count = 3;
    public int load_count = 0;
    public readonly string[] status_name_array = new string[(int)Status_ID.MAX]
    {
        "べんきょう中","うんどう中","みりょく中"
    };
    public override void Start(TalkActionEventInitArgData arg) {
        is_end_update = false;
        load_count = 0;
        is_end_update = false;
        add_calc_time = 0.0f;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        add_calc_time += Time.deltaTime;

        if(add_calc_time >= 1.0f)
        {
            ++load_count;
            add_calc_time = 0.0f;
            string text = status_name_array[(int)arg.status_id];
            for(int count = 0; count < load_count;)
            {
                text += "・";
                ++count;
            }
            if(load_count >= max_load_count)
            {
                is_end_update = true;
            }
            gameUiCore.Instance?.StatusCanvas?.SetTextTalk(text);
        }

    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
        load_count = 0;
        add_calc_time = 0.0f;
    }
    public TalkActionEventFrameTextUpdateState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFrameTextUpdateState():base(){}
}
