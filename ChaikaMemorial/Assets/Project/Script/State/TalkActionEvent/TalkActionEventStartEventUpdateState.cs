using System.Collections.Generic;
public class TalkActionEventStartEventUpdateState : BaseTalkActionEventStartEventUpdateState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventStartEventUpdateState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventStartEventUpdateState():base(){}
}
