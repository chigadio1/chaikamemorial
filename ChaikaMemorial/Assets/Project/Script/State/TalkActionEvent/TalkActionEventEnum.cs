using System;
public enum TalkActionEvent_State_ID {
    Begin, //初期
    BeginInit, //
    FramePop, //
    FrameTextUpdate, //
    FrameResultIcon, //
    StatusUP, //
    FadeIN, //
    EvenInit, //
    EventUpdate, //
    EventFadeOut, //
    FadeOut, //
    SchoolCheck, //
    StartEventInit, //
    StartEventUpdate, //
    StartEventFadeOut, //
    GameFinish, //
    FinishFadeIn, //
    EventGenericUpdate, //
    Finish //終了
}
