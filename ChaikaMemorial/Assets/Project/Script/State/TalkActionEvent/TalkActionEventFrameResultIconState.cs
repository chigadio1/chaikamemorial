using System.Collections.Generic;
public class TalkActionEventFrameResultIconState : BaseTalkActionEventFrameResultIconState {
    public override void Start(TalkActionEventInitArgData arg) {
        //gameUiCore.Instance?.StatusCanvas?.AddResultData(arg.chat_talk_index);


        int id = gameUiCore.result_action_names[(int)arg.result_id];
        var sprite = gameUiCore.Instance.GetDefaultSprite(Novel_Sprite_Type_ID.ICON, id);
        gameUiCore.Instance?.StatusCanvas?.SetResultSptite(sprite);
        is_end_update = false;
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventFrameResultIconState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventFrameResultIconState():base(){}
}
