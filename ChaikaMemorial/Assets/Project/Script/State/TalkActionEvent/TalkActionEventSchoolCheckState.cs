using System.Collections.Generic;
public class TalkActionEventSchoolCheckState : BaseTalkActionEventSchoolCheckState {
    public override void Start(TalkActionEventInitArgData arg) {
    }
    public override void Update(TalkActionEventUpdateArgData arg) {
    }
    public override void Finish(TalkActionEventUpdateArgData arg) {
    }
    public TalkActionEventSchoolCheckState(BaseTalkActionEventState other):base(other){}
    public TalkActionEventSchoolCheckState():base(){}
}
