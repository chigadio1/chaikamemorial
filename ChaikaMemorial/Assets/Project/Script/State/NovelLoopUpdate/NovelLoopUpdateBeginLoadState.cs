using System.Collections.Generic;
public class NovelLoopUpdateBeginLoadState : BaseNovelLoopUpdateBeginLoadState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateBeginLoadState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateBeginLoadState():base(){}
}
