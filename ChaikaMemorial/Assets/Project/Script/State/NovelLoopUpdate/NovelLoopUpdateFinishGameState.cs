using System.Collections.Generic;
public class NovelLoopUpdateFinishGameState : BaseNovelLoopUpdateFinishGameState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateFinishGameState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateFinishGameState():base(){}
}
