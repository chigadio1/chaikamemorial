using System.Collections.Generic;
public class NovelLoopUpdateBeginInitState : BaseNovelLoopUpdateBeginInitState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateBeginInitState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateBeginInitState():base(){}
}
