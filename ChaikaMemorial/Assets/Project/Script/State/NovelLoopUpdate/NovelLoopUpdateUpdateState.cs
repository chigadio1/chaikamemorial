using System.Collections.Generic;
public class NovelLoopUpdateUpdateState : BaseNovelLoopUpdateUpdateState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateUpdateState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateUpdateState():base(){}
}
