using System.Collections.Generic;
public class NovelLoopUpdateEventCheckState : BaseNovelLoopUpdateEventCheckState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateEventCheckState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateEventCheckState():base(){}
}
