using System.Collections.Generic;
public class NovelLoopUpdateFadeOutState : BaseNovelLoopUpdateFadeOutState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateFadeOutState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateFadeOutState():base(){}
}
