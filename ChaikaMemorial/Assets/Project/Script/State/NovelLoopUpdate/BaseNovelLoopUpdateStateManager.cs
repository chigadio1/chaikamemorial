using System.Collections.Generic;
public class BaseNovelLoopUpdateStateManager : BaseStateManager<NovelLoopUpdateInitArgData,NovelLoopUpdateUpdateArgData,BaseNovelLoopUpdateState> {
    protected NovelLoopUpdate_State_ID state_id = NovelLoopUpdate_State_ID.BeginLoad;
    public NovelLoopUpdate_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(NovelLoopUpdateInitArgData arg = null){
        switch (state_id)
        {
            case NovelLoopUpdate_State_ID.Begin:
                state = new NovelLoopUpdateBeginLoadState(state);
                state_id = NovelLoopUpdate_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.BeginLoad:
                state = new NovelLoopUpdateBeginInitState(state);
                state_id = NovelLoopUpdate_State_ID.BeginInit;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.BeginInit:
                state = new NovelLoopUpdateUpdateState(state);
                state_id = NovelLoopUpdate_State_ID.Update;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.Update:
                state = new NovelLoopUpdateEventCheckState(state);
                state_id = NovelLoopUpdate_State_ID.EventCheck;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.EventCheck:
                state = new NovelLoopUpdateFinishGameState(state);
                state_id = NovelLoopUpdate_State_ID.FinishGame;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.FinishGame:
                state = new NovelLoopUpdateFadeINState(state);
                state_id = NovelLoopUpdate_State_ID.FadeIN;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.FadeIN:
                state = new NovelLoopUpdateBeginFadeINState(state);
                state_id = NovelLoopUpdate_State_ID.BeginFadeIN;
                state.Start(arg);
                break;
            case NovelLoopUpdate_State_ID.BeginFadeIN:
                is_finish = true;
                state_id = NovelLoopUpdate_State_ID.Finish;
                break;
            case NovelLoopUpdate_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(NovelLoopUpdate_State_ID value_state_id,NovelLoopUpdateInitArgData init_arg = null,NovelLoopUpdateUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case NovelLoopUpdate_State_ID.Begin:
                is_finish = true;
                break;
            case NovelLoopUpdate_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateBeginLoadState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.BeginInit:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateBeginInitState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.Update:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateUpdateState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.EventCheck:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateEventCheckState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.FinishGame:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateFinishGameState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.FadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateFadeINState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.BeginFadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new NovelLoopUpdateBeginFadeINState(state);
                state.Start(init_arg);
                break;
            case NovelLoopUpdate_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new NovelLoopUpdateBeginLoadState();
    }
}
