using System.Collections.Generic;
public class NovelLoopUpdateFadeINState : BaseNovelLoopUpdateFadeINState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateFadeINState(BaseNovelLoopUpdateState other):base(other){}
    public NovelLoopUpdateFadeINState():base(){}
}
