using System.Collections.Generic;
public class BaseChaikaStateManager : BaseStateManager<ChaikaInitArgData,ChaikaUpdateArgData,BaseChaikaState> {
    protected Chaika_State_ID state_id = Chaika_State_ID.Init;
    public Chaika_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(ChaikaInitArgData arg = null){
        switch (state_id)
        {
            case Chaika_State_ID.Begin:
                state = new ChaikaInitState(state);
                state_id = Chaika_State_ID.Init;
                state.Start(arg);
                break;
            case Chaika_State_ID.Init:
                is_finish = true;
                state_id = Chaika_State_ID.Finish;
                break;
            case Chaika_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(Chaika_State_ID value_state_id,ChaikaInitArgData init_arg = null,ChaikaUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case Chaika_State_ID.Begin:
                is_finish = true;
                break;
            case Chaika_State_ID.Init:
                if(state != null) state.Finish(updatet_arg);
                state = new ChaikaInitState(state);
                state.Start(init_arg);
                break;
            case Chaika_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new ChaikaInitState();
    }
}
