using System.Collections.Generic;
public class ChaikaInitState : BaseChaikaInitState {
    public override void Start(ChaikaInitArgData arg) {
    }
    public override void Update(ChaikaUpdateArgData arg) {
    }
    public override void Finish(ChaikaUpdateArgData arg) {
    }
    public ChaikaInitState(BaseChaikaState other):base(other){}
    public ChaikaInitState():base(){}
}
