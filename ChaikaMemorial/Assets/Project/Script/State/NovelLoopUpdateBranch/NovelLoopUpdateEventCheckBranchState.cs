using System.Collections.Generic;
public class NovelLoopUpdateEventCheckBranchState : BaseNovelLoopUpdateEventCheckBranchState {
    bool is_finish = false;
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
        if(arg.list_event_id.Count > 0)
        {
            arg.list_event_id.RemoveAt(0);
            if(arg.list_event_id.Count <= 0)
            {
                is_finish = true;
            }
        }
        if(arg.list_event_id.Count == 0)
        {
            is_finish = true;
        }
        is_end_update = true;
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateEventCheckBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}

    public override bool BranchNest06_EventCheckToBeginInit()
    {
        return is_finish == false;
    }

    public override bool BranchNest06_EventCheckToFinishGame()
    {
        return is_finish == true;
    }
}
