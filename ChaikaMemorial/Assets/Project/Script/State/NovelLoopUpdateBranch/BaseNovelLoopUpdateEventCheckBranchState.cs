using System.Collections.Generic;
public class BaseNovelLoopUpdateEventCheckBranchState : BaseNovelLoopUpdateBranchState{
    public BaseNovelLoopUpdateEventCheckBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
    public virtual NovelLoopUpdate_Branch_State_ID BranchNest06_EventCheckState(){
        if(BranchNest06_EventCheckToBeginInit()) return NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit;
        else if(BranchNest06_EventCheckToFinishGame()) return NovelLoopUpdate_Branch_State_ID.Nest07_FinishGame;
     return NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit;
     }
    public virtual bool BranchNest06_EventCheckToBeginInit(){return true;}
    public virtual bool BranchNest06_EventCheckToFinishGame(){return true;}
    public override NovelLoopUpdate_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case NovelLoopUpdate_Branch_State_ID.Nest06_EventCheck:
                return BranchNest06_EventCheckState();
        }
     return NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit;
     }
}
