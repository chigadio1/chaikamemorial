using System.Collections.Generic;
public class BaseNovelLoopUpdateStateBranchManager : BaseStateBranchManager<NovelLoopUpdateInitArgData,NovelLoopUpdateUpdateArgData,BaseNovelLoopUpdateBranchState,NovelLoopUpdate_Branch_State_ID > {
    public BaseNovelLoopUpdateStateBranchManager(NovelLoopUpdate_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(NovelLoopUpdateInitArgData arg = null){
        switch (state_id)
        {
            case NovelLoopUpdate_Branch_State_ID.Nest02_BeginLoad:
                state_id = NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit;
                state = new NovelLoopUpdateBeginInitBranchState(state_id);
                state.Start(arg);
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit:
                state_id = NovelLoopUpdate_Branch_State_ID.Nest06_FadeIN;
                state = new NovelLoopUpdateFadeINBranchState(state_id);
                state.Start(arg);
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest06_Update:
                state_id = NovelLoopUpdate_Branch_State_ID.Nest06_EventCheck;
                state = new NovelLoopUpdateEventCheckBranchState(state_id);
                state.Start(arg);
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest06_EventCheck:
                {
                NovelLoopUpdate_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit:
                        state_id = NovelLoopUpdate_Branch_State_ID.Nest06_BeginInit;
                        state = new NovelLoopUpdateBeginInitBranchState(state_id);
                        state.Start(arg);
                        break;
                    case NovelLoopUpdate_Branch_State_ID.Nest07_FinishGame:
                        state_id = NovelLoopUpdate_Branch_State_ID.Nest07_FinishGame;
                        state = new NovelLoopUpdateFinishGameBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest07_FinishGame:
                is_finish = true;
                state_id = NovelLoopUpdate_Branch_State_ID.Finish;
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest06_FadeIN:
                state_id = NovelLoopUpdate_Branch_State_ID.Nest06_Update;
                state = new NovelLoopUpdateUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case NovelLoopUpdate_Branch_State_ID.Nest01_BeginFadeIN:
                state_id = NovelLoopUpdate_Branch_State_ID.Nest02_BeginLoad;
                state = new NovelLoopUpdateBeginLoadBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new NovelLoopUpdateBeginFadeINBranchState(NovelLoopUpdate_Branch_State_ID.Nest01_BeginFadeIN);
    }
}
