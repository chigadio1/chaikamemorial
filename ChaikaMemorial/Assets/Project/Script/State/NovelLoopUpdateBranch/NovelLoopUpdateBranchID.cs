using System;
public enum NovelLoopUpdate_Branch_State_ID {
Begin = -1,
Nest02_BeginLoad,
Nest06_BeginInit,
Nest06_Update,
Nest06_EventCheck,
Nest07_FinishGame,
Nest06_FadeIN,
Nest01_BeginFadeIN,
Finish
}
