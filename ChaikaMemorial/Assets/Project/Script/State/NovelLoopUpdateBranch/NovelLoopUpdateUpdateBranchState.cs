using System.Collections.Generic;
public class NovelLoopUpdateUpdateBranchState : BaseNovelLoopUpdateUpdateBranchState {
    NovelUpdateStateBranchManager manager = new NovelUpdateStateBranchManager();
    NovelUpdateInitArgData init_arg = new NovelUpdateInitArgData();
    public override void Start(NovelLoopUpdateInitArgData arg) {
      
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
        if(arg == null)
        {
            is_end_update = true;
            return;
        }
        if(arg.list_event_id.Count <= 0)
        {
            is_end_update = true;
            return;
        }
        if(arg.list_event_id[0] <= 0)
        {
            is_end_update=true;
            return;
        }
        init_arg.event_id = arg.list_event_id[0];
        manager.Update(init_arg);
        if(manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateUpdateBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
}
