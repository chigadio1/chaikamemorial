using System.Collections.Generic;
public class NovelLoopUpdateBeginInitBranchState : BaseNovelLoopUpdateBeginInitBranchState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateBeginInitBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
}
