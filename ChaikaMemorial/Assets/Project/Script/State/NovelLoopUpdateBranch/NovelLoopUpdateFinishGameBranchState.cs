using System.Collections.Generic;
public class NovelLoopUpdateFinishGameBranchState : BaseNovelLoopUpdateFinishGameBranchState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateFinishGameBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
}
