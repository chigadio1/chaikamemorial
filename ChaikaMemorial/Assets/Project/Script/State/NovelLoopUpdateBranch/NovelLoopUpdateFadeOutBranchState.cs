using System.Collections.Generic;
public class NovelLoopUpdateFadeOutBranchState : BaseNovelLoopUpdateFadeOutBranchState {
    public override void Start(NovelLoopUpdateInitArgData arg) {
    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateFadeOutBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
}
