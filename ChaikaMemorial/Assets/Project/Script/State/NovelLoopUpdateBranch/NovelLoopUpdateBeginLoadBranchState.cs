using System.Collections.Generic;
public class NovelLoopUpdateBeginLoadBranchState : BaseNovelLoopUpdateBeginLoadBranchState {
    public override void Start(NovelLoopUpdateInitArgData arg) {

    }
    public override void Update(NovelLoopUpdateUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(NovelLoopUpdateUpdateArgData arg) {
    }
    public NovelLoopUpdateBeginLoadBranchState(NovelLoopUpdate_Branch_State_ID other):base(other){}
}
