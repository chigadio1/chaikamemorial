using System.Collections.Generic;
public class TitleLoadUpdateState : BaseTitleLoadUpdateState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadUpdateState(BaseTitleLoadState other):base(other){}
    public TitleLoadUpdateState():base(){}
}
