using System.Collections.Generic;
public class BaseTitleLoadStateManager : BaseStateManager<TitleLoadInitArgData,TitleLoadUpdateArgData,BaseTitleLoadState> {
    protected TitleLoad_State_ID state_id = TitleLoad_State_ID.BeginAnimationStart;
    public TitleLoad_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(TitleLoadInitArgData arg = null){
        switch (state_id)
        {
            case TitleLoad_State_ID.Begin:
                state = new TitleLoadBeginAnimationStartState(state);
                state_id = TitleLoad_State_ID.BeginAnimationStart;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.BeginAnimationStart:
                state = new TitleLoadUpdateState(state);
                state_id = TitleLoad_State_ID.Update;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.Update:
                state = new TitleLoadYesNoSelectAnimationStartState(state);
                state_id = TitleLoad_State_ID.YesNoSelectAnimationStart;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.YesNoSelectAnimationStart:
                state = new TitleLoadYesNoSelectUpdateState(state);
                state_id = TitleLoad_State_ID.YesNoSelectUpdate;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.YesNoSelectUpdate:
                state = new TitleLoadYesNoSelectAnimationFinishState(state);
                state_id = TitleLoad_State_ID.YesNoSelectAnimationFinish;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.YesNoSelectAnimationFinish:
                state = new TitleLoadAnimationFinishState(state);
                state_id = TitleLoad_State_ID.AnimationFinish;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.AnimationFinish:
                state = new TitleLoadFadeINState(state);
                state_id = TitleLoad_State_ID.FadeIN;
                state.Start(arg);
                break;
            case TitleLoad_State_ID.FadeIN:
                is_finish = true;
                state_id = TitleLoad_State_ID.Finish;
                break;
            case TitleLoad_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(TitleLoad_State_ID value_state_id,TitleLoadInitArgData init_arg = null,TitleLoadUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case TitleLoad_State_ID.Begin:
                is_finish = true;
                break;
            case TitleLoad_State_ID.BeginAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadBeginAnimationStartState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.Update:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.YesNoSelectAnimationStart:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadYesNoSelectAnimationStartState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.YesNoSelectUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadYesNoSelectUpdateState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.YesNoSelectAnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadYesNoSelectAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.AnimationFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadAnimationFinishState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.FadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new TitleLoadFadeINState(state);
                state.Start(init_arg);
                break;
            case TitleLoad_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new TitleLoadBeginAnimationStartState();
    }
}
