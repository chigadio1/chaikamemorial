using System.Collections.Generic;
public class TitleLoadYesNoSelectAnimationFinishState : BaseTitleLoadYesNoSelectAnimationFinishState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadYesNoSelectAnimationFinishState(BaseTitleLoadState other):base(other){}
    public TitleLoadYesNoSelectAnimationFinishState():base(){}
}
