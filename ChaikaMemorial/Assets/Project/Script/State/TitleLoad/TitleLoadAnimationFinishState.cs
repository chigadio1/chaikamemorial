using System.Collections.Generic;
public class TitleLoadAnimationFinishState : BaseTitleLoadAnimationFinishState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadAnimationFinishState(BaseTitleLoadState other):base(other){}
    public TitleLoadAnimationFinishState():base(){}
}
