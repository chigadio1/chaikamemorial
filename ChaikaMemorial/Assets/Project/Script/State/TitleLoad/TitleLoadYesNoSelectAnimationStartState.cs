using System.Collections.Generic;
public class TitleLoadYesNoSelectAnimationStartState : BaseTitleLoadYesNoSelectAnimationStartState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadYesNoSelectAnimationStartState(BaseTitleLoadState other):base(other){}
    public TitleLoadYesNoSelectAnimationStartState():base(){}
}
