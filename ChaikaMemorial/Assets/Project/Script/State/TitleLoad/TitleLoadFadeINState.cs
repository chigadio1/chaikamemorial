using System.Collections.Generic;
public class TitleLoadFadeINState : BaseTitleLoadFadeINState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadFadeINState(BaseTitleLoadState other):base(other){}
    public TitleLoadFadeINState():base(){}
}
