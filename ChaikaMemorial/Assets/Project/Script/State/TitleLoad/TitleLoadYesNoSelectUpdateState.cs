using System.Collections.Generic;
public class TitleLoadYesNoSelectUpdateState : BaseTitleLoadYesNoSelectUpdateState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadYesNoSelectUpdateState(BaseTitleLoadState other):base(other){}
    public TitleLoadYesNoSelectUpdateState():base(){}
}
