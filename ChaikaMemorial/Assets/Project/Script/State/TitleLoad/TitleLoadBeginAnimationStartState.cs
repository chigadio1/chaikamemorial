using System.Collections.Generic;
public class TitleLoadBeginAnimationStartState : BaseTitleLoadBeginAnimationStartState {
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadBeginAnimationStartState(BaseTitleLoadState other):base(other){}
    public TitleLoadBeginAnimationStartState():base(){}
}
