using System.Collections.Generic;
public class TitleLoadYesNoSelectAnimationStartBranchState : BaseTitleLoadYesNoSelectAnimationStartBranchState {
    public override void Start(TitleLoadInitArgData arg) {
        TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataSelectAnima.PlayAnim(Player_Data_Select_UI_Anim_ID.StartUP);
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        if(TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataSelectAnima.IsPlayAnim())
        {
            is_end_update = true;
            PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
        }
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadYesNoSelectAnimationStartBranchState(TitleLoad_Branch_State_ID other):base(other){}
}
