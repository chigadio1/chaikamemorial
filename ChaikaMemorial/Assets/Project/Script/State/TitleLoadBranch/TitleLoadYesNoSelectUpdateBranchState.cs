using System.Collections.Generic;
public class TitleLoadYesNoSelectUpdateBranchState : BaseTitleLoadYesNoSelectUpdateBranchState {

    bool is_cancel = false;
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        if(PlayerCore.Instance.is_button_cancel_push)
        {
            is_cancel = true;
            is_end_update = true;
            return;
        }
        if(PlayerCore.Instance.is_button_push)
        {
            is_end_update = true;
            return;
        }
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
    }
    public TitleLoadYesNoSelectUpdateBranchState(TitleLoad_Branch_State_ID other):base(other){}

    public override bool BranchNest05_YesNoSelectUpdateToFadeIN()
    {
        return is_cancel == false;
    }

    public override bool BranchNest05_YesNoSelectUpdateToYesNoSelectAnimationFinish()
    {
        return is_cancel == true;
    }
}
