using System.Collections.Generic;
public class BaseTitleLoadUpdateBranchState : BaseTitleLoadBranchState{
    public BaseTitleLoadUpdateBranchState(TitleLoad_Branch_State_ID other):base(other){}
    public virtual TitleLoad_Branch_State_ID BranchNest05_UpdateState(){
        if(BranchNest05_UpdateToAnimationFinish()) return TitleLoad_Branch_State_ID.Nest06_AnimationFinish;
        else if(BranchNest05_UpdateToYesNoSelectAnimationStart()) return TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationStart;
     return TitleLoad_Branch_State_ID.Nest06_AnimationFinish;
     }
    public virtual bool BranchNest05_UpdateToAnimationFinish(){return true;}
    public virtual bool BranchNest05_UpdateToYesNoSelectAnimationStart(){return true;}
    public override TitleLoad_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleLoad_Branch_State_ID.Nest05_Update:
                return BranchNest05_UpdateState();
        }
     return TitleLoad_Branch_State_ID.Nest06_AnimationFinish;
     }
}
