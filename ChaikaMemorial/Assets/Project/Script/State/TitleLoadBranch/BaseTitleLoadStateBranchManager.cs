using System.Collections.Generic;
public class BaseTitleLoadStateBranchManager : BaseStateBranchManager<TitleLoadInitArgData,TitleLoadUpdateArgData,BaseTitleLoadBranchState,TitleLoad_Branch_State_ID > {
    public BaseTitleLoadStateBranchManager(TitleLoad_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(TitleLoadInitArgData arg = null){
        switch (state_id)
        {
            case TitleLoad_Branch_State_ID.Nest01_BeginAnimationStart:
                state_id = TitleLoad_Branch_State_ID.Nest05_Update;
                state = new TitleLoadUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleLoad_Branch_State_ID.Nest05_Update:
                {
                TitleLoad_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleLoad_Branch_State_ID.Nest06_AnimationFinish:
                        state_id = TitleLoad_Branch_State_ID.Nest06_AnimationFinish;
                        state = new TitleLoadAnimationFinishBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationStart:
                        state_id = TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationStart;
                        state = new TitleLoadYesNoSelectAnimationStartBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationStart:
                state_id = TitleLoad_Branch_State_ID.Nest05_YesNoSelectUpdate;
                state = new TitleLoadYesNoSelectUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleLoad_Branch_State_ID.Nest05_YesNoSelectUpdate:
                {
                TitleLoad_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish:
                        state_id = TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish;
                        state = new TitleLoadYesNoSelectAnimationFinishBranchState(state_id);
                        state.Start(arg);
                        break;
                    case TitleLoad_Branch_State_ID.Nest06_FadeIN:
                        state_id = TitleLoad_Branch_State_ID.Nest06_FadeIN;
                        state = new TitleLoadFadeINBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish:
                state_id = TitleLoad_Branch_State_ID.Nest05_Update;
                state = new TitleLoadUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case TitleLoad_Branch_State_ID.Nest06_AnimationFinish:
                is_finish = true;
                state_id = TitleLoad_Branch_State_ID.Finish;
                break;
            case TitleLoad_Branch_State_ID.Nest06_FadeIN:
                is_finish = true;
                state_id = TitleLoad_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new TitleLoadBeginAnimationStartBranchState(TitleLoad_Branch_State_ID.Nest01_BeginAnimationStart);
    }
}
