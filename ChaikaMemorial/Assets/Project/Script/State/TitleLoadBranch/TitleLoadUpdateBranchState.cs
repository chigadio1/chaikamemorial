using System.Collections.Generic;
public class TitleLoadUpdateBranchState : BaseTitleLoadUpdateBranchState {

    bool is_cancel = false;
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        if(PlayerCore.Instance.is_button_cancel_push)
        {
            is_cancel = true;
            is_end_update = true;
            return;
        }

        if(PlayerCore.Instance.is_button_push)
        {
            if(PlayerCore.Instance.IsSaveData())
            {
                TitleUiCore.Instance.SystemPlayerDataObject.SetTextConfig($"PlayerData {PlayerCore.Instance.player_save_id:00}をLoadしますか?");
                is_end_update = true;
                return;
            }
            else
            {
                PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
            }
        }
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
    }
    public TitleLoadUpdateBranchState(TitleLoad_Branch_State_ID other):base(other){}

    public override bool BranchNest05_UpdateToAnimationFinish()
    {
        return is_cancel == true;
    }

    public override bool BranchNest05_UpdateToYesNoSelectAnimationStart()
    {
        return is_cancel == false;
    }
}
