using System.Collections.Generic;
public class BaseTitleLoadYesNoSelectUpdateBranchState : BaseTitleLoadBranchState{
    public BaseTitleLoadYesNoSelectUpdateBranchState(TitleLoad_Branch_State_ID other):base(other){}
    public virtual TitleLoad_Branch_State_ID BranchNest05_YesNoSelectUpdateState(){
        if(BranchNest05_YesNoSelectUpdateToYesNoSelectAnimationFinish()) return TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish;
        else if(BranchNest05_YesNoSelectUpdateToFadeIN()) return TitleLoad_Branch_State_ID.Nest06_FadeIN;
     return TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish;
     }
    public virtual bool BranchNest05_YesNoSelectUpdateToYesNoSelectAnimationFinish(){return true;}
    public virtual bool BranchNest05_YesNoSelectUpdateToFadeIN(){return true;}
    public override TitleLoad_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case TitleLoad_Branch_State_ID.Nest05_YesNoSelectUpdate:
                return BranchNest05_YesNoSelectUpdateState();
        }
     return TitleLoad_Branch_State_ID.Nest05_YesNoSelectAnimationFinish;
     }
}
