using System.Collections.Generic;
using UnityEngine;

public class TitleLoadFadeINBranchState : BaseTitleLoadFadeINBranchState {
    float add_calc = 0.0f;
    public override void Start(TitleLoadInitArgData arg) {
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        add_calc += Time.deltaTime;
        if (add_calc >= 1.0f)
        {
            is_end_update = true;
        }
        add_calc = Mathf.Clamp(add_calc, 0.0f, 1.0f);
        arg.is_game = true;
        TitleUiCore.Instance.FadeCanvas.SetFadeAlpha(Mathf.Lerp(0.0f, 1.0f, add_calc));
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
        PlayerCore.Instance.LoadData();
    }
    public TitleLoadFadeINBranchState(TitleLoad_Branch_State_ID other):base(other){}
}
