using System.Collections.Generic;
public class TitleLoadAnimationFinishBranchState : BaseTitleLoadAnimationFinishBranchState {
    public override void Start(TitleLoadInitArgData arg) {
        TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataAnima.PlayAnim(Player_Data_UI_Anim_ID.StartUP, 1.0f, true);
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        if(TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataAnima.IsPlayAnim())
        {
            is_end_update = true;
        }
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
        PlayerCore.Instance.is_button_push = PlayerCore.Instance.is_button_cancel_push = false;
    }
    public TitleLoadAnimationFinishBranchState(TitleLoad_Branch_State_ID other):base(other){}
}
