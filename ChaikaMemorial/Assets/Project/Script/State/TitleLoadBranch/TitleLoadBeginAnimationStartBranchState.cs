using System.Collections.Generic;
public class TitleLoadBeginAnimationStartBranchState : BaseTitleLoadBeginAnimationStartBranchState {
    public override void Start(TitleLoadInitArgData arg) {
        TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataAnima.PlayAnim(Player_Data_UI_Anim_ID.StartUP);
    }
    public override void Update(TitleLoadUpdateArgData arg) {
        if(TitleUiCore.Instance.SystemPlayerDataObject.PlayerDataAnima.IsPlayAnim())
        {
            is_end_update = true;
            PlayerCore.Instance.is_button_cancel_push = PlayerCore.Instance.is_button_push = false;
            TitleUiCore.Instance.SystemPlayerDataObject.SetTextConfig("Load���܂���?");
        }
    }
    public override void Finish(TitleLoadUpdateArgData arg) {
    }
    public TitleLoadBeginAnimationStartBranchState(TitleLoad_Branch_State_ID other):base(other){}
}
