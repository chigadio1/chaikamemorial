using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// ��b�J�ڃN���X
/// </summary>
public class BaseState<T,S> where T : BaseStateArgData where S : BaseStateArgData
{
  
    protected bool is_end_update = false; //�I���t���O

    public bool getIsEndUpdate { get { return is_end_update; } }
    // Start is called before the first frame update
    public virtual void Start(T�@arg = null)
    {
        
    }

    // Update is called once per frame
    public virtual void Update(S arg = null)
    {
        
    }

    public virtual void Finish(S arg = null)
    {

    }

 
}


/// <summary>
/// ��b����J�ڃN���X
/// </summary>
public class BaseBranchState<T, S, P> : BaseState<T, S> where T : BaseStateArgData where S : BaseStateArgData where P : Enum
{
    protected P state_id;
    public BaseBranchState(P value)
    {
        state_id = value;
    }

    public virtual P GenerateNextState() { return default(P); }




}



