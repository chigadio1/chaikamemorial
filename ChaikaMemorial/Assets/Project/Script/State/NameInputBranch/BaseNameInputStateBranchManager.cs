using System.Collections.Generic;
public class BaseNameInputStateBranchManager : BaseStateBranchManager<NameInputInitArgData,NameInputUpdateArgData,BaseNameInputBranchState,NameInput_Branch_State_ID > {
    public BaseNameInputStateBranchManager(NameInput_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(NameInputInitArgData arg = null){
        switch (state_id)
        {
            case NameInput_Branch_State_ID.Nest01_BeginStart:
                state_id = NameInput_Branch_State_ID.Nest02_NameInputUpdate;
                state = new NameInputNameInputUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case NameInput_Branch_State_ID.Nest02_NameInputUpdate:
                state_id = NameInput_Branch_State_ID.Nest03_FinishEnd;
                state = new NameInputFinishEndBranchState(state_id);
                state.Start(arg);
                break;
            case NameInput_Branch_State_ID.Nest03_FinishEnd:
                is_finish = true;
                state_id = NameInput_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new NameInputBeginStartBranchState(NameInput_Branch_State_ID.Nest01_BeginStart);
    }
}
