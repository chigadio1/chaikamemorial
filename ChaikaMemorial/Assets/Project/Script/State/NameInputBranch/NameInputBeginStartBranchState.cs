using System.Collections.Generic;
using UnityEngine;

public class NameInputBeginStartBranchState : BaseNameInputBeginStartBranchState {

    float calc_time = 0.0f;
    float max_fade_time = 2.0f;
    public override void Start(NameInputInitArgData arg) {
        NameUICore.Instance.FadeCanvas.SetFadeAlpha(1.0f);
    }
    public override void Update(NameInputUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_fade_time);
        if(calc_time >= max_fade_time)
        {
            is_end_update = true;
        }

        float lerp = calc_time / max_fade_time;
        lerp = Mathf.Lerp(1.0f, 0.0f, lerp);
        NameUICore.Instance.FadeCanvas.SetFadeAlpha(lerp);
    }
    public override void Finish(NameInputUpdateArgData arg) {
    }
    public NameInputBeginStartBranchState(NameInput_Branch_State_ID other):base(other){}
}
