using System.Collections.Generic;
public class CalenderEventNovelFinishState : BaseCalenderEventNovelFinishState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventNovelFinishState(BaseCalenderEventState other):base(other){}
    public CalenderEventNovelFinishState():base(){}
}
