using System.Collections.Generic;
public class CalenderEventBeginLoadState : BaseCalenderEventBeginLoadState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventBeginLoadState(BaseCalenderEventState other):base(other){}
    public CalenderEventBeginLoadState():base(){}
}
