using System.Collections.Generic;
public class CalenderEventFinishEventState : BaseCalenderEventFinishEventState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventFinishEventState(BaseCalenderEventState other):base(other){}
    public CalenderEventFinishEventState():base(){}
}
