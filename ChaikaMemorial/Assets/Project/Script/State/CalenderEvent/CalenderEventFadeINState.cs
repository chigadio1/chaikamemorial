using System.Collections.Generic;
public class CalenderEventFadeINState : BaseCalenderEventFadeINState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventFadeINState(BaseCalenderEventState other):base(other){}
    public CalenderEventFadeINState():base(){}
}
