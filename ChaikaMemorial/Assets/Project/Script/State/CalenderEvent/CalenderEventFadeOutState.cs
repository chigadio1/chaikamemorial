using System.Collections.Generic;
public class CalenderEventFadeOutState : BaseCalenderEventFadeOutState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventFadeOutState(BaseCalenderEventState other):base(other){}
    public CalenderEventFadeOutState():base(){}
}
