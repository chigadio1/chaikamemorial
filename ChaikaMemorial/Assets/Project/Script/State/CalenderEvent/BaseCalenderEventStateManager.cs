using System.Collections.Generic;
public class BaseCalenderEventStateManager : BaseStateManager<CalenderEventInitArgData,CalenderEventUpdateArgData,BaseCalenderEventState> {
    protected CalenderEvent_State_ID state_id = CalenderEvent_State_ID.BeginLoad;
    public CalenderEvent_State_ID getStateID {get{ return state_id; } }
    public override void NextStateChange(CalenderEventInitArgData arg = null){
        switch (state_id)
        {
            case CalenderEvent_State_ID.Begin:
                state = new CalenderEventBeginLoadState(state);
                state_id = CalenderEvent_State_ID.BeginLoad;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.BeginLoad:
                state = new CalenderEventCheckState(state);
                state_id = CalenderEvent_State_ID.Check;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.Check:
                state = new CalenderEventFadeINState(state);
                state_id = CalenderEvent_State_ID.FadeIN;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.FadeIN:
                state = new CalenderEventNovelUpdateState(state);
                state_id = CalenderEvent_State_ID.NovelUpdate;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.NovelUpdate:
                state = new CalenderEventFadeOutState(state);
                state_id = CalenderEvent_State_ID.FadeOut;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.FadeOut:
                state = new CalenderEventNovelFinishState(state);
                state_id = CalenderEvent_State_ID.NovelFinish;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.NovelFinish:
                state = new CalenderEventFinishEventState(state);
                state_id = CalenderEvent_State_ID.FinishEvent;
                state.Start(arg);
                break;
            case CalenderEvent_State_ID.FinishEvent:
                is_finish = true;
                state_id = CalenderEvent_State_ID.Finish;
                break;
            case CalenderEvent_State_ID.Finish:
                is_finish = true;
                break;
            default:
                is_finish = true;
                break;
        }
    }
    public void NextStateChange(CalenderEvent_State_ID value_state_id,CalenderEventInitArgData init_arg = null,CalenderEventUpdateArgData updatet_arg = null){
        state_id = value_state_id;
        switch (state_id)
        {
            case CalenderEvent_State_ID.Begin:
                is_finish = true;
                break;
            case CalenderEvent_State_ID.BeginLoad:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventBeginLoadState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.Check:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventCheckState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.FadeIN:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventFadeINState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.NovelUpdate:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventNovelUpdateState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.FadeOut:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventFadeOutState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.NovelFinish:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventNovelFinishState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.FinishEvent:
                if(state != null) state.Finish(updatet_arg);
                state = new CalenderEventFinishEventState(state);
                state.Start(init_arg);
                break;
            case CalenderEvent_State_ID.Finish:
                break;
            default:
                break;
        }
    }

    public override void StartInstance() {
        state = new CalenderEventBeginLoadState();
    }
}
