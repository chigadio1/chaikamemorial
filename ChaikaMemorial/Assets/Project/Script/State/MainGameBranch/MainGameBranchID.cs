using System;
public enum MainGame_Branch_State_ID {
Begin = -1,
Nest10_BeginTitle,
Nest01_BeginStart,
Nest10_TitleUpdate,
Nest10_FinishTitle,
Nest10_BeginGame,
Nest10_GameUpdate,
Nest10_FinishGame,
Nest10_BeginName,
Nest10_NameUpdate,
Nest10_FinishName,
Finish
}
