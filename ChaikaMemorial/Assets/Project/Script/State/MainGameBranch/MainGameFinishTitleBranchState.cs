using System.Collections.Generic;
using UnityEngine;

public class MainGameFinishTitleBranchState : BaseMainGameFinishTitleBranchState {
    public override void Start(MainGameInitArgData arg) {
        GameManagerCore.Instance.ReleaseObj();
        GameObject.Destroy(TitleUiCore.Instance.gameObject);
        SoundGameCore.Instance.GetVolume(Sound_Event_Type_ID.Title, Sound_Type_ID.SE);
    }
    public override void Update(MainGameUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(MainGameUpdateArgData arg) {
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.Title);
    }
    public MainGameFinishTitleBranchState(MainGame_Branch_State_ID other):base(other){}

    public override bool BranchNest10_FinishTitleToBeginGame()
    {
        return GameManagerCore.Instance.IsNewGame == false;
    }

    public override bool BranchNest10_FinishTitleToBeginName()
    {
        return GameManagerCore.Instance.IsNewGame == true;
    }
}
