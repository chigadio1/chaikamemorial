using System.Collections.Generic;
using UnityEngine;

public class MainGameFinishNameBranchState : BaseMainGameFinishNameBranchState {
    public override void Start(MainGameInitArgData arg) {
        GameManagerCore.Instance.ReleaseObj();
        GameObject.Destroy(NameUICore.Instance.gameObject);
    }
    public override void Update(MainGameUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameFinishNameBranchState(MainGame_Branch_State_ID other):base(other){}

    public override bool BranchNest10_FinishNameToBeginTitle()
    {
        return GameManagerCore.Instance.IsBackNameTitle == true;
    }

    public override bool BranchNest10_FinishNameToBeginGame()
    {
        return GameManagerCore.Instance.IsBackNameTitle == false;
    }
}
