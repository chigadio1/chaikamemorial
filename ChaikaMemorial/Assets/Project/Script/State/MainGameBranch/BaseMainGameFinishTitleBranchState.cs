using System.Collections.Generic;
public class BaseMainGameFinishTitleBranchState : BaseMainGameBranchState{
    public BaseMainGameFinishTitleBranchState(MainGame_Branch_State_ID other):base(other){}
    public virtual MainGame_Branch_State_ID BranchNest10_FinishTitleState(){
        if(BranchNest10_FinishTitleToBeginGame()) return MainGame_Branch_State_ID.Nest10_BeginGame;
        else if(BranchNest10_FinishTitleToBeginName()) return MainGame_Branch_State_ID.Nest10_BeginName;
     return MainGame_Branch_State_ID.Nest10_BeginGame;
     }
    public virtual bool BranchNest10_FinishTitleToBeginGame(){return true;}
    public virtual bool BranchNest10_FinishTitleToBeginName(){return true;}
    public override MainGame_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case MainGame_Branch_State_ID.Nest10_FinishTitle:
                return BranchNest10_FinishTitleState();
        }
     return MainGame_Branch_State_ID.Nest10_BeginGame;
     }
}
