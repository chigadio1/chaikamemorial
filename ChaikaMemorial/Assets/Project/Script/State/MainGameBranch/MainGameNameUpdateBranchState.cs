using System.Collections.Generic;
public class MainGameNameUpdateBranchState : BaseMainGameNameUpdateBranchState {
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
        if(NameUICore.Instance.IsEnd)
        {
            is_end_update = true;
        }
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameNameUpdateBranchState(MainGame_Branch_State_ID other):base(other){}
}
