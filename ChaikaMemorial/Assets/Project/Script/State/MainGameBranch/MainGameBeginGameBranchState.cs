using System.Collections.Generic;
public class MainGameBeginGameBranchState : BaseMainGameBeginGameBranchState {
    public override void Start(MainGameInitArgData arg) {
        GameManagerCore.Instance.LoadCoreObj(GameManagerCore.Core_Obj_ID.GAME);
    }
    public override void Update(MainGameUpdateArgData arg) {
        if(GameManagerCore.Instance.IsLoadEnd())
        {
            GameManagerCore.Instance.InstanceCoreObj(GameManagerCore.Core_Obj_ID.GAME);
            is_end_update = true;
        }
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginGameBranchState(MainGame_Branch_State_ID other):base(other){}
}
