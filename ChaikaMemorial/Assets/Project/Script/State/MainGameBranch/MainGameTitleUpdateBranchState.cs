using System.Collections.Generic;
public class MainGameTitleUpdateBranchState : BaseMainGameTitleUpdateBranchState {
    /// <summary>
    /// タイトルマネージャー
    /// </summary>
    private TitleUIStateBranchManager title_manager = new TitleUIStateBranchManager();
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
        title_manager.Update();
        if(title_manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameTitleUpdateBranchState(MainGame_Branch_State_ID other):base(other){}
}
