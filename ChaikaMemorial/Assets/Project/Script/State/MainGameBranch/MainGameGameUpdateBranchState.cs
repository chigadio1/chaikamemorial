using System.Collections.Generic;
public class MainGameGameUpdateBranchState : BaseMainGameGameUpdateBranchState {
    GamePartStateBranchManager gamePart = new GamePartStateBranchManager();
    public override void Start(MainGameInitArgData arg) {
    }
    public override void Update(MainGameUpdateArgData arg) {
        gamePart.Update();
        if(gamePart.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameGameUpdateBranchState(MainGame_Branch_State_ID other):base(other){}
}
