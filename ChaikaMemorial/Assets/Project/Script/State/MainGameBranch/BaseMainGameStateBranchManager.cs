using System.Collections.Generic;
public class BaseMainGameStateBranchManager : BaseStateBranchManager<MainGameInitArgData,MainGameUpdateArgData,BaseMainGameBranchState,MainGame_Branch_State_ID > {
    public BaseMainGameStateBranchManager(MainGame_Branch_State_ID value):base(value) {
    }

#if UNITY_EDITOR
    public void ChangeDebug(GameManagerCore.Debug_State_ID id)
    {
        if(id == GameManagerCore.Debug_State_ID.Name)
        {
            state_id = MainGame_Branch_State_ID.Nest10_BeginName;
            state = new MainGameBeginNameBranchState(state_id);
            state.Start(null);
        }
        else if(id == GameManagerCore.Debug_State_ID.Game)
        {
            state_id = MainGame_Branch_State_ID.Nest10_BeginGame;
            state = new MainGameBeginGameBranchState(state_id);
            state.Start(null);
        }
    }
#endif
    public override void NextStateChange(MainGameInitArgData arg = null){
        switch (state_id)
        {
            case MainGame_Branch_State_ID.Nest10_BeginTitle:
                state_id = MainGame_Branch_State_ID.Nest10_TitleUpdate;
                state = new MainGameTitleUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest01_BeginStart:
                state_id = MainGame_Branch_State_ID.Nest10_BeginTitle;
                state = new MainGameBeginTitleBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_TitleUpdate:
                state_id = MainGame_Branch_State_ID.Nest10_FinishTitle;
                state = new MainGameFinishTitleBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_FinishTitle:
                {
                MainGame_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case MainGame_Branch_State_ID.Nest10_BeginGame:
                        state_id = MainGame_Branch_State_ID.Nest10_BeginGame;
                        state = new MainGameBeginGameBranchState(state_id);
                        state.Start(arg);
                        break;
                    case MainGame_Branch_State_ID.Nest10_BeginName:
                        state_id = MainGame_Branch_State_ID.Nest10_BeginName;
                        state = new MainGameBeginNameBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case MainGame_Branch_State_ID.Nest10_BeginGame:
                state_id = MainGame_Branch_State_ID.Nest10_GameUpdate;
                state = new MainGameGameUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_GameUpdate:
                state_id = MainGame_Branch_State_ID.Nest10_FinishGame;
                state = new MainGameFinishGameBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_FinishGame:
                state_id = MainGame_Branch_State_ID.Nest10_BeginTitle;
                state = new MainGameBeginTitleBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_BeginName:
                state_id = MainGame_Branch_State_ID.Nest10_NameUpdate;
                state = new MainGameNameUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_NameUpdate:
                state_id = MainGame_Branch_State_ID.Nest10_FinishName;
                state = new MainGameFinishNameBranchState(state_id);
                state.Start(arg);
                break;
            case MainGame_Branch_State_ID.Nest10_FinishName:
                {
                MainGame_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case MainGame_Branch_State_ID.Nest10_BeginGame:
                        state_id = MainGame_Branch_State_ID.Nest10_BeginGame;
                        state = new MainGameBeginGameBranchState(state_id);
                        state.Start(arg);
                        break;
                    case MainGame_Branch_State_ID.Nest10_BeginTitle:
                        state_id = MainGame_Branch_State_ID.Nest10_BeginTitle;
                        state = new MainGameBeginTitleBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
             }
    }

    public override void StartInstance() {
        state = new MainGameBeginStartBranchState(MainGame_Branch_State_ID.Nest01_BeginStart);
    }
}
