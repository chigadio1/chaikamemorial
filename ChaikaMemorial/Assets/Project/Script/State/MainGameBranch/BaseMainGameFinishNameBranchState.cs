using System.Collections.Generic;
public class BaseMainGameFinishNameBranchState : BaseMainGameBranchState{
    public BaseMainGameFinishNameBranchState(MainGame_Branch_State_ID other):base(other){}
    public virtual MainGame_Branch_State_ID BranchNest10_FinishNameState(){
        if(BranchNest10_FinishNameToBeginGame()) return MainGame_Branch_State_ID.Nest10_BeginGame;
        else if(BranchNest10_FinishNameToBeginTitle()) return MainGame_Branch_State_ID.Nest10_BeginTitle;
     return MainGame_Branch_State_ID.Nest10_BeginGame;
     }
    public virtual bool BranchNest10_FinishNameToBeginGame(){return true;}
    public virtual bool BranchNest10_FinishNameToBeginTitle(){return true;}
    public override MainGame_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case MainGame_Branch_State_ID.Nest10_FinishName:
                return BranchNest10_FinishNameState();
        }
     return MainGame_Branch_State_ID.Nest10_BeginGame;
     }
}
