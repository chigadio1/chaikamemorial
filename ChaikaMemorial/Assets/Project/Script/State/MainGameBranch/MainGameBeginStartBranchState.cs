using System.Collections.Generic;
public class MainGameBeginStartBranchState : BaseMainGameBeginStartBranchState {
    public override void Start(MainGameInitArgData arg) {
        is_end_update = false;
    }
    public override void Update(MainGameUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginStartBranchState(MainGame_Branch_State_ID other):base(other){}
}
