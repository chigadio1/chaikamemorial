using System.Collections.Generic;
public class MainGameBeginNameBranchState : BaseMainGameBeginNameBranchState {
    public override void Start(MainGameInitArgData arg) {
        GameManagerCore.Instance.LoadCoreObj(GameManagerCore.Core_Obj_ID.NAMEINPUT);
    }
    public override void Update(MainGameUpdateArgData arg) {
        if (GameManagerCore.Instance.IsLoadEnd())
        {
            GameManagerCore.Instance.InstanceCoreObj(GameManagerCore.Core_Obj_ID.NAMEINPUT);
            is_end_update = true;
        }
    }
    public override void Finish(MainGameUpdateArgData arg) {
    }
    public MainGameBeginNameBranchState(MainGame_Branch_State_ID other):base(other){}
}
