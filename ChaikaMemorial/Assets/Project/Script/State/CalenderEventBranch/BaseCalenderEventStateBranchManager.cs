using System.Collections.Generic;
public class BaseCalenderEventStateBranchManager : BaseStateBranchManager<CalenderEventInitArgData,CalenderEventUpdateArgData,BaseCalenderEventBranchState,CalenderEvent_Branch_State_ID > {
    public BaseCalenderEventStateBranchManager(CalenderEvent_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(CalenderEventInitArgData arg = null){
        switch (state_id)
        {
            case CalenderEvent_Branch_State_ID.Nest01_BeginLoad:
                state_id = CalenderEvent_Branch_State_ID.Nest06_Check;
                state = new CalenderEventCheckBranchState(state_id);
                state.Start(arg);
                break;
            case CalenderEvent_Branch_State_ID.Nest06_Check:
                {
                CalenderEvent_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case CalenderEvent_Branch_State_ID.Nest06_FadeIN:
                        state_id = CalenderEvent_Branch_State_ID.Nest06_FadeIN;
                        state = new CalenderEventFadeINBranchState(state_id);
                        state.Start(arg);
                        break;
                    case CalenderEvent_Branch_State_ID.Nest07_FinishEvent:
                        state_id = CalenderEvent_Branch_State_ID.Nest07_FinishEvent;
                        state = new CalenderEventFinishEventBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case CalenderEvent_Branch_State_ID.Nest06_FadeIN:
                state_id = CalenderEvent_Branch_State_ID.Nest06_NovelUpdate;
                state = new CalenderEventNovelUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case CalenderEvent_Branch_State_ID.Nest06_NovelUpdate:
                state_id = CalenderEvent_Branch_State_ID.Nest06_FadeOut;
                state = new CalenderEventFadeOutBranchState(state_id);
                state.Start(arg);
                break;
            case CalenderEvent_Branch_State_ID.Nest06_FadeOut:
                state_id = CalenderEvent_Branch_State_ID.Nest06_NovelFinish;
                state = new CalenderEventNovelFinishBranchState(state_id);
                state.Start(arg);
                break;
            case CalenderEvent_Branch_State_ID.Nest06_NovelFinish:
                state_id = CalenderEvent_Branch_State_ID.Nest06_Check;
                state = new CalenderEventCheckBranchState(state_id);
                state.Start(arg);
                break;
            case CalenderEvent_Branch_State_ID.Nest07_FinishEvent:
                is_finish = true;
                state_id = CalenderEvent_Branch_State_ID.Finish;
                break;
             }
    }

    public override void StartInstance() {
        state = new CalenderEventBeginLoadBranchState(CalenderEvent_Branch_State_ID.Nest01_BeginLoad);
    }
}
