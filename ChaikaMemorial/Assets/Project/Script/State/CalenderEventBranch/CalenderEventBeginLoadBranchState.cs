using System.Collections.Generic;
public class CalenderEventBeginLoadBranchState : BaseCalenderEventBeginLoadBranchState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
        var data = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn,
            arg.occurrence_id);
        if(data.Length <= 0)
        {
            return;
        }
        is_end_update = true;
        arg.data_details_array = data;
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventBeginLoadBranchState(CalenderEvent_Branch_State_ID other):base(other){}
}
