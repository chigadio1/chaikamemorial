using System.Collections.Generic;
using UnityEngine;

public class CalenderEventFinishEventBranchState : BaseCalenderEventFinishEventBranchState {
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
        Resources.UnloadUnusedAssets();
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.NOVEL);
        is_end_update = true;
    }
    public override void Finish(CalenderEventUpdateArgData arg) {

    }
    public CalenderEventFinishEventBranchState(CalenderEvent_Branch_State_ID other):base(other){}
}
