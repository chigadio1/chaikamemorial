using System;
using System.Collections.Generic;
public class CalenderEventNovelFinishBranchState : BaseCalenderEventNovelFinishBranchState {

    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
        if(arg.data_details_array != null)
        {
            Array.Resize(ref arg.data_details_array, arg.data_details_array.Length - 1);

        }
        is_end_update = true;
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventNovelFinishBranchState(CalenderEvent_Branch_State_ID other):base(other){}


}
