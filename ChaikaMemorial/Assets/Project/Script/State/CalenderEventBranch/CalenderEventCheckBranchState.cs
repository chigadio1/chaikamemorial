using System.Collections.Generic;
public class CalenderEventCheckBranchState : BaseCalenderEventCheckBranchState {

    bool is_event = false;
    public override void Start(CalenderEventInitArgData arg) {
    }
    public override void Update(CalenderEventUpdateArgData arg) {
        is_end_update = true;
        if(arg.data_details_array.Length <= 0)
        {
            return;
        }
        is_event = true;
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventCheckBranchState(CalenderEvent_Branch_State_ID other):base(other){}

    public override bool BranchNest06_CheckToFadeIN()
    {
        return is_event == true;
    }

    public override bool BranchNest06_CheckToFinishEvent()
    {
        return is_event == false;
    }
}
