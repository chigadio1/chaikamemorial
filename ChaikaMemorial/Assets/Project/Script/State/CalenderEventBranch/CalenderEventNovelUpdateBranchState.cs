using System.Collections.Generic;
public class CalenderEventNovelUpdateBranchState : BaseCalenderEventNovelUpdateBranchState {

    NovelUpdateStateBranchManager manager = new NovelUpdateStateBranchManager();
    NovelUpdateInitArgData novel_arg = new NovelUpdateInitArgData();
    public override void Start(CalenderEventInitArgData arg) {
      
    }
    public override void Update(CalenderEventUpdateArgData arg) {
        novel_arg.event_id = arg.data_details_array[0].event_id;
        manager.Update(novel_arg);
        if(manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(CalenderEventUpdateArgData arg) {
    }
    public CalenderEventNovelUpdateBranchState(CalenderEvent_Branch_State_ID other):base(other){}
}
