using System.Collections.Generic;
public class BaseCalenderEventCheckBranchState : BaseCalenderEventBranchState{
    public BaseCalenderEventCheckBranchState(CalenderEvent_Branch_State_ID other):base(other){}
    public virtual CalenderEvent_Branch_State_ID BranchNest06_CheckState(){
        if(BranchNest06_CheckToFadeIN()) return CalenderEvent_Branch_State_ID.Nest06_FadeIN;
        else if(BranchNest06_CheckToFinishEvent()) return CalenderEvent_Branch_State_ID.Nest07_FinishEvent;
     return CalenderEvent_Branch_State_ID.Nest06_FadeIN;
     }
    public virtual bool BranchNest06_CheckToFadeIN(){return true;}
    public virtual bool BranchNest06_CheckToFinishEvent(){return true;}
    public override CalenderEvent_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case CalenderEvent_Branch_State_ID.Nest06_Check:
                return BranchNest06_CheckState();
        }
     return CalenderEvent_Branch_State_ID.Nest06_FadeIN;
     }
}
