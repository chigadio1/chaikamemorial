using System.Collections.Generic;
using UnityEngine;

public class ActionSelectSelectParentMoveUpdateBranchState : BaseActionSelectSelectParentMoveUpdateBranchState {

    float calc_time = 0.0f;
    float max_time = 0.5f;
    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.StartMove(gameUiCore.Instance.IsMoveSelect);
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, (int)Default_Action_Sound_ID.Move_Select);
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        calc_time += Time.deltaTime * 2.0f;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(0.0f, 1.0f, calc_time / max_time);
        bool res = gameUiCore.Instance.ActionCanvas.UpdateMove(lerp);
        if(res)
        {
            is_end_update = true;
        }

    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSelectParentMoveUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
