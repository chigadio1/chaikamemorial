using System.Collections.Generic;
public class ActionSelectYashiroFavoBranchState : BaseActionSelectYashiroFavoBranchState {

    YaShiroFavoStateBranchManager manager = new YaShiroFavoStateBranchManager();
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        manager.Update();
        if(manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectYashiroFavoBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
