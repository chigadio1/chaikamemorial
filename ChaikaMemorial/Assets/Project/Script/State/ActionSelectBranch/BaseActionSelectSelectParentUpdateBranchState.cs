using System.Collections.Generic;
public class BaseActionSelectSelectParentUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectSelectParentUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest09_SelectParentUpdateState(){
        if(BranchNest09_SelectParentUpdateToSelectParentMoveUpdate()) return ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate;
        else if(BranchNest09_SelectParentUpdateToSelectParentChangeAnimation()) return ActionSelect_Branch_State_ID.Nest09_SelectParentChangeAnimation;
     return ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate;
     }
    public virtual bool BranchNest09_SelectParentUpdateToSelectParentMoveUpdate(){return true;}
    public virtual bool BranchNest09_SelectParentUpdateToSelectParentChangeAnimation(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest09_SelectParentUpdate:
                return BranchNest09_SelectParentUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate;
     }
}
