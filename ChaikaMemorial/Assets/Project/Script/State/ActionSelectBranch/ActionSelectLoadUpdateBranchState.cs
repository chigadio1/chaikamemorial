using System.Collections.Generic;
public class ActionSelectLoadUpdateBranchState : BaseActionSelectLoadUpdateBranchState {

    ActionLoadStateBranchManager manager = new ActionLoadStateBranchManager();
    ActionLoadUpdateArgData update_arg = new ActionLoadUpdateArgData();

    bool is_load_game = false;
    public override void Start(ActionSelectInitArgData arg) {
        update_arg.is_load = true;
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        manager.Update(null, update_arg);
        if(manager.IsFinish)
        {
            gameUiCore.Instance.is_load_game_retry = is_load_game = update_arg.is_game;
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
        gameUiCore.Instance.is_button_cancel_push = gameUiCore.Instance.is_button_push = false;
        PlayerCore.Instance.is_button_cancel_push = PlayerCore.Instance.is_button_push = false;
    }
    public ActionSelectLoadUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest10_LoadUpdateToFadeIn()
    {
        return is_load_game == true; ;
    }

    public override bool BranchNest10_LoadUpdateToSettingUpdate()
    {
        return is_load_game == false;
    }
}
