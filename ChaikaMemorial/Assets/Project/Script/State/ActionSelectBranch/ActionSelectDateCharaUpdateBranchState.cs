using System.Collections.Generic;
public class ActionSelectDateCharaUpdateBranchState : BaseActionSelectDateCharaUpdateBranchState {
    DateCharaCommandStateBranchManager manager = new DateCharaCommandStateBranchManager();
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        manager.Update();
        if(manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public ActionSelectDateCharaUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest10_DateCharaUpdateToSubActionUpdate()
    {
        return PlayerCore.Instance.IsDate == false;
    }

    public override bool BranchNest10_DateCharaUpdateToGameFinish()
    {
        return PlayerCore.Instance.IsDate == true;
    }
}
