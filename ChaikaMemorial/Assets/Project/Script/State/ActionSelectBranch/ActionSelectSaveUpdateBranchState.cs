using System.Collections.Generic;
public class ActionSelectSaveUpdateBranchState : BaseActionSelectSaveUpdateBranchState {
    ActionLoadStateBranchManager manager = new ActionLoadStateBranchManager();
    ActionLoadUpdateArgData update_arg = new ActionLoadUpdateArgData();
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        manager.Update(null, update_arg);
        if (manager.IsFinish)
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
        gameUiCore.Instance.is_button_cancel_push = gameUiCore.Instance.is_button_push = false;
        PlayerCore.Instance.is_button_cancel_push = PlayerCore.Instance.is_button_push = false;
    }
    public ActionSelectSaveUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
