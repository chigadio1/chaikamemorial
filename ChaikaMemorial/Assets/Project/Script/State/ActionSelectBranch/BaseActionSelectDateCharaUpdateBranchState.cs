using System.Collections.Generic;
public class BaseActionSelectDateCharaUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectDateCharaUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest10_DateCharaUpdateState(){
        if(BranchNest10_DateCharaUpdateToSubActionUpdate()) return ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
        else if(BranchNest10_DateCharaUpdateToGameFinish()) return ActionSelect_Branch_State_ID.Nest14_GameFinish;
     return ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
     }
    public virtual bool BranchNest10_DateCharaUpdateToSubActionUpdate(){return true;}
    public virtual bool BranchNest10_DateCharaUpdateToGameFinish(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest10_DateCharaUpdate:
                return BranchNest10_DateCharaUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
     }
}
