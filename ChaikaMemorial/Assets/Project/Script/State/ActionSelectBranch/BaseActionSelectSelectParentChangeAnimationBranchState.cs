using System.Collections.Generic;
public class BaseActionSelectSelectParentChangeAnimationBranchState : BaseActionSelectBranchState{
    public BaseActionSelectSelectParentChangeAnimationBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest09_SelectParentChangeAnimationState(){
        if(BranchNest09_SelectParentChangeAnimationToParameterUpUpdate()) return ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate;
        else if(BranchNest09_SelectParentChangeAnimationToSubActionUpdate()) return ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
        else if(BranchNest09_SelectParentChangeAnimationToSettingUpdate()) return ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
     return ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate;
     }
    public virtual bool BranchNest09_SelectParentChangeAnimationToParameterUpUpdate(){return true;}
    public virtual bool BranchNest09_SelectParentChangeAnimationToSubActionUpdate(){return true;}
    public virtual bool BranchNest09_SelectParentChangeAnimationToSettingUpdate(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest09_SelectParentChangeAnimation:
                return BranchNest09_SelectParentChangeAnimationState();
        }
     return ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate;
     }
}
