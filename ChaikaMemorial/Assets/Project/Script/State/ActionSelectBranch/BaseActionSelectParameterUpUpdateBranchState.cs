using System.Collections.Generic;
public class BaseActionSelectParameterUpUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectParameterUpUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest07_ParameterUpUpdateState(){
        if(BranchNest07_ParameterUpUpdateToSelectParentBackUpdate()) return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
        else if(BranchNest07_ParameterUpUpdateToFadeIn()) return ActionSelect_Branch_State_ID.Nest10_FadeIn;
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
    public virtual bool BranchNest07_ParameterUpUpdateToSelectParentBackUpdate(){return true;}
    public virtual bool BranchNest07_ParameterUpUpdateToFadeIn(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate:
                return BranchNest07_ParameterUpUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
}
