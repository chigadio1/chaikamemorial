using System.Collections.Generic;
public class ActionSelectSelectParentBackUpdateBranchState : BaseActionSelectSelectParentBackUpdateBranchState {
    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID, Parent_Anim_Type_ID.StartUP,true);
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, (int)Default_Action_Sound_ID.Cansel_Select);
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        if (gameUiCore.Instance.ActionCanvas.IsPlayAnim(gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID))
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID,Parent_Anim_Type_ID.LoopIdle);
        gameUiCore.Instance.ActionCanvas.OffIsOnButton();
        gameUiCore.Instance.ActionCanvas.ResetParentActionSelectType();
    }
    public ActionSelectSelectParentBackUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
