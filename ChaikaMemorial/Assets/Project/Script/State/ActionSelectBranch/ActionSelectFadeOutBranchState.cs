using System.Collections.Generic;
using UnityEngine;
public class ActionSelectFadeOutBranchState : BaseActionSelectFadeOutBranchState {
    float calc_time = 0.0f;
    readonly float max_time = 1.0f;
    public override void Start(ActionSelectInitArgData arg) {
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.BGM,(int)Default_Action_Sound_ID.Action_BGM);
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, max_time);
        float lerp = Mathf.Lerp(1.0f, 0.0f, calc_time / max_time);
        float volume = Mathf.Lerp(0.0f, 0.75f, calc_time / max_time);
        if(calc_time >= max_time)
        {
            is_end_update = true;
        }
       SoundGameCore.Instance.SetVolume(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.BGM, volume);
       gameUiCore.Instance.FadeCanvas.SetFadeAlpha(lerp);
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
        gameUiCore.Instance.FadeCanvas.SetFadeAlpha(0.0f);
    }
    public ActionSelectFadeOutBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
