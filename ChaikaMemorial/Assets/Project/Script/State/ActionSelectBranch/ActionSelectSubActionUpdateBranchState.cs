using System.Collections.Generic;
using UnityEngine;

public class ActionSelectSubActionUpdateBranchState : BaseActionSelectSubActionUpdateBranchState {
    private bool is_back = false;

    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public override void Update(ActionSelectUpdateArgData arg) {

        if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetMouseButtonDown(1))
        {
            is_back = true;
            is_end_update = true;
        }
        else if(gameUiCore.Instance.is_button_push)
        {
            gameUiCore.Instance.is_button_push = false;
            is_back = false;
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSubActionUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest10_SubActionUpdateToYashiroFavo()
    {
        return is_back == false && gameUiCore.Instance.SelectPlayerID == Player_Select_ID.Call;
    }

    public override bool BranchNest10_SubActionUpdateToSelectParentBackUpdate()
    {
        return is_back == true;
    }

    public override bool BranchNest10_SubActionUpdateToDateCharaUpdate()
    {
        return is_back == false && gameUiCore.Instance.SelectPlayerID == Player_Select_ID.Date;
    }
}
