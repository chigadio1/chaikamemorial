using System.Collections.Generic;
public class ActionSelectBeginInitBranchState : BaseActionSelectBeginInitBranchState {

    private bool is_next = false;
    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.LoadGameUIObject(gameUiCore.UI_Object_ID.SCHOOL);
        gameUiCore.Instance.FadeCanvas.gameObject.SetActive(true);
        gameUiCore.Instance.FadeCanvas.SetFadeAlpha(1.0f);
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        if(is_next)
        {
            if(SoundGameCore.Instance.IsLoadEnd())
            {
                is_end_update = true;
            }
            return;
        }
        if(gameUiCore.Instance.IsLoadFinishGameUIObject(gameUiCore.UI_Object_ID.SCHOOL) && is_next == false)
        {
            gameUiCore.Instance.InstanceGameUIObject(gameUiCore.UI_Object_ID.SCHOOL);
            is_next = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectBeginInitBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
