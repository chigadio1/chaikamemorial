using System.Collections.Generic;
public class BaseActionSelectLoadUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectLoadUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest10_LoadUpdateState(){
        if(BranchNest10_LoadUpdateToSettingUpdate()) return ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
        else if(BranchNest10_LoadUpdateToFadeIn()) return ActionSelect_Branch_State_ID.Nest10_FadeIn;
     return ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
     }
    public virtual bool BranchNest10_LoadUpdateToSettingUpdate(){return true;}
    public virtual bool BranchNest10_LoadUpdateToFadeIn(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest10_LoadUpdate:
                return BranchNest10_LoadUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
     }
}
