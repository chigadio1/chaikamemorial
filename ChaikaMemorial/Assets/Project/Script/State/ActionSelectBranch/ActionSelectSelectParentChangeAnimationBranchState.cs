using System.Collections.Generic;
using UnityEngine;

public class ActionSelectSelectParentChangeAnimationBranchState : BaseActionSelectSelectParentChangeAnimationBranchState {
    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.ActionCanvas.PlayAnim(gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID, Parent_Anim_Type_ID.StartUP);
        SoundGameCore.Instance.PlaySound(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, (int)Default_Action_Sound_ID.Push_Select);
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        if(gameUiCore.Instance.ActionCanvas.IsPlayAnim(gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID))
        {

            is_end_update = true;
        }

    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSelectParentChangeAnimationBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest09_SelectParentChangeAnimationToParameterUpUpdate()
    {
        return gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID == Parent_Action_Select_Type_ID.ParameterUP;
    }

    public override bool BranchNest09_SelectParentChangeAnimationToSettingUpdate()
    {
        return gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID == Parent_Action_Select_Type_ID.Setting;
    }

    public override bool BranchNest09_SelectParentChangeAnimationToSubActionUpdate()
    {
        return gameUiCore.Instance.ActionCanvas.OnParentActionSelectTypeID == Parent_Action_Select_Type_ID.SubAction;
    }
}
