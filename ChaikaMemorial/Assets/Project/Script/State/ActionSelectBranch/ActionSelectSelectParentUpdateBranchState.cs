using System.Collections.Generic;
using UnityEngine;

public class ActionSelectSelectParentUpdateBranchState : BaseActionSelectSelectParentUpdateBranchState {

    /// <summary>
    /// �ړ�  
    /// </summary>
    bool is_move = false;
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {

        var mouseAxis = Input.GetAxis("Mouse ScrollWheel");
        if (Input.GetKeyDown(KeyCode.RightArrow) || mouseAxis <= -0.1f)
        {
            int index = (int)gameUiCore.Instance.ParentActionSelectTypeID;
            index++;
            index = index % (int)Parent_Action_Select_Type_ID.MAX;
            is_move = true;
            is_end_update = true;
            gameUiCore.Instance.IsMoveSelect = true;
            gameUiCore.Instance.ParentActionSelectTypeID = (Parent_Action_Select_Type_ID)index;
        }
        else if(Input.GetKeyDown(KeyCode.LeftArrow) || mouseAxis >= 0.1f)
        {
            int index = (int)gameUiCore.Instance.ParentActionSelectTypeID;
            index--;
            if (index < 0) index = (int)Parent_Action_Select_Type_ID.Setting;
            is_move = true;
            is_end_update = true;
            gameUiCore.Instance.IsMoveSelect = false;
            gameUiCore.Instance.ParentActionSelectTypeID = (Parent_Action_Select_Type_ID)index;
        }

        if (gameUiCore.Instance.ActionCanvas.IsOnButton)
        {
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectSelectParentUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest09_SelectParentUpdateToSelectParentMoveUpdate()
    {
        return is_move == true;
    }

    public override bool BranchNest09_SelectParentUpdateToSelectParentChangeAnimation()
    {
        return is_move == false;
    }
}
