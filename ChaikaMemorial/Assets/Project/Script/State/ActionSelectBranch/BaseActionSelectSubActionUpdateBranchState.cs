using System.Collections.Generic;
public class BaseActionSelectSubActionUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectSubActionUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest10_SubActionUpdateState(){
        if(BranchNest10_SubActionUpdateToSelectParentBackUpdate()) return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
        else if(BranchNest10_SubActionUpdateToYashiroFavo()) return ActionSelect_Branch_State_ID.Nest09_YashiroFavo;
        else if(BranchNest10_SubActionUpdateToDateCharaUpdate()) return ActionSelect_Branch_State_ID.Nest10_DateCharaUpdate;
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
    public virtual bool BranchNest10_SubActionUpdateToSelectParentBackUpdate(){return true;}
    public virtual bool BranchNest10_SubActionUpdateToYashiroFavo(){return true;}
    public virtual bool BranchNest10_SubActionUpdateToDateCharaUpdate(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest10_SubActionUpdate:
                return BranchNest10_SubActionUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
}
