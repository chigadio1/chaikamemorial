using System.Collections.Generic;
public class ActionSelectFinishSelectBranchState : BaseActionSelectFinishSelectBranchState {
    public override void Start(ActionSelectInitArgData arg) {
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectFinishSelectBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
