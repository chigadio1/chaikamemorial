using System;
public enum ActionSelect_Branch_State_ID {
Begin = -1,
Nest01_BeginInit,
Nest02_FadeOut,
Nest09_SelectParentUpdate,
Nest04_SelectParentMoveUpdate,
Nest09_SelectParentBackUpdate,
Nest07_ParameterUpUpdate,
Nest10_SubActionUpdate,
Nest11_SettingUpdate,
Nest10_FadeIn,
Nest11_FinishSelect,
Nest14_GameFinish,
Nest09_SelectParentChangeAnimation,
Nest09_YashiroFavo,
Nest10_DateCharaUpdate,
Nest10_LoadUpdate,
Nest11_SaveUpdate,
Finish
}
