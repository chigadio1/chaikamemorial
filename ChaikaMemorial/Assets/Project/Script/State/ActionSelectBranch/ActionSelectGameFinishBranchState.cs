using System.Collections.Generic;
using UnityEngine;

public class ActionSelectGameFinishBranchState : BaseActionSelectGameFinishBranchState {
    public override void Start(ActionSelectInitArgData arg) {
        if(gameUiCore.Instance.is_load_game_retry)
        {
            gameUiCore.Instance.ActionCanvas.ReleaseSubSystem();
        }
        gameUiCore.Instance.ReleaseGameUIObject(gameUiCore.UI_Object_ID.SCHOOL);
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.SelectStartGame);
        Resources.UnloadUnusedAssets();

    }
    public override void Update(ActionSelectUpdateArgData arg) {
        is_end_update = true;
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectGameFinishBranchState(ActionSelect_Branch_State_ID other):base(other){}
}
