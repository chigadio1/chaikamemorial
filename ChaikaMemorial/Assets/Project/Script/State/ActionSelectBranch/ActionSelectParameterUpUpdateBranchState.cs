using System.Collections.Generic;
using UnityEngine;

public class ActionSelectParameterUpUpdateBranchState : BaseActionSelectParameterUpUpdateBranchState {

    bool is_back = false;
    public override void Start(ActionSelectInitArgData arg) {
        gameUiCore.Instance.is_button_push = false;
    }
    public override void Update(ActionSelectUpdateArgData arg) {
        if(Input.GetKeyDown(KeyCode.Backspace) || Input.GetMouseButtonDown(1))
        {
            is_back = true;
            is_end_update = true;
        }
        if(gameUiCore.Instance.is_button_push == true)
        {
            switch(gameUiCore.Instance.SelectPlayerID)
            {
                case Player_Select_ID.Study:
                    PlayerCore.Instance.select_status_id = Status_ID.STUDY;
                    break;
                case Player_Select_ID.Charm:
                    PlayerCore.Instance.select_status_id = Status_ID.CHARM;
                    break;
                case Player_Select_ID.Exercise:
                    PlayerCore.Instance.select_status_id = Status_ID.EXERCISE;
                    break;
                default:
                    gameUiCore.Instance.is_button_push = false;
                    return;
            }
            is_back = false;
            is_end_update = true;
        }
    }
    public override void Finish(ActionSelectUpdateArgData arg) {
    }
    public ActionSelectParameterUpUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}

    public override bool BranchNest07_ParameterUpUpdateToSelectParentBackUpdate()
    {
        return is_back == true && gameUiCore.Instance.is_button_push == false;
    }

    public override bool BranchNest07_ParameterUpUpdateToFadeIn()
    {
        return is_back == false && gameUiCore.Instance.is_button_push == true;
    }
}
