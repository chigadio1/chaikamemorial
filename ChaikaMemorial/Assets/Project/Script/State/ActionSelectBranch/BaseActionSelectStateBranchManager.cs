using System.Collections.Generic;
public class BaseActionSelectStateBranchManager : BaseStateBranchManager<ActionSelectInitArgData,ActionSelectUpdateArgData,BaseActionSelectBranchState,ActionSelect_Branch_State_ID > {
    public BaseActionSelectStateBranchManager(ActionSelect_Branch_State_ID value):base(value) {
    }
    public override void NextStateChange(ActionSelectInitArgData arg = null){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest01_BeginInit:
                state_id = ActionSelect_Branch_State_ID.Nest02_FadeOut;
                state = new ActionSelectFadeOutBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest02_FadeOut:
                state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentUpdate;
                state = new ActionSelectSelectParentUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest09_SelectParentUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate;
                        state = new ActionSelectSelectParentMoveUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest09_SelectParentChangeAnimation:
                        state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentChangeAnimation;
                        state = new ActionSelectSelectParentChangeAnimationBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest04_SelectParentMoveUpdate:
                state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentUpdate;
                state = new ActionSelectSelectParentUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate:
                state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentUpdate;
                state = new ActionSelectSelectParentUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
                        state = new ActionSelectSelectParentBackUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_FadeIn:
                        state_id = ActionSelect_Branch_State_ID.Nest10_FadeIn;
                        state = new ActionSelectFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest10_SubActionUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
                        state = new ActionSelectSelectParentBackUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest09_YashiroFavo:
                        state_id = ActionSelect_Branch_State_ID.Nest09_YashiroFavo;
                        state = new ActionSelectYashiroFavoBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_DateCharaUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest10_DateCharaUpdate;
                        state = new ActionSelectDateCharaUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest11_SettingUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
                        state = new ActionSelectSelectParentBackUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_FadeIn:
                        state_id = ActionSelect_Branch_State_ID.Nest10_FadeIn;
                        state = new ActionSelectFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_LoadUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest10_LoadUpdate;
                        state = new ActionSelectLoadUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest11_SaveUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest11_SaveUpdate;
                        state = new ActionSelectSaveUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest10_FadeIn:
                state_id = ActionSelect_Branch_State_ID.Nest11_FinishSelect;
                state = new ActionSelectFinishSelectBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest11_FinishSelect:
                state_id = ActionSelect_Branch_State_ID.Nest14_GameFinish;
                state = new ActionSelectGameFinishBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest14_GameFinish:
                is_finish = true;
                state_id = ActionSelect_Branch_State_ID.Finish;
                break;
            case ActionSelect_Branch_State_ID.Nest09_SelectParentChangeAnimation:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest07_ParameterUpUpdate;
                        state = new ActionSelectParameterUpUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_SubActionUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
                        state = new ActionSelectSubActionUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest11_SettingUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
                        state = new ActionSelectSettingUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest09_YashiroFavo:
                state_id = ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
                state = new ActionSelectSubActionUpdateBranchState(state_id);
                state.Start(arg);
                break;
            case ActionSelect_Branch_State_ID.Nest10_DateCharaUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest10_SubActionUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest10_SubActionUpdate;
                        state = new ActionSelectSubActionUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest14_GameFinish:
                        state_id = ActionSelect_Branch_State_ID.Nest14_GameFinish;
                        state = new ActionSelectGameFinishBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest10_LoadUpdate:
                {
                ActionSelect_Branch_State_ID next_id = state.GenerateNextState();
                 switch (next_id)
                 {
                    case ActionSelect_Branch_State_ID.Nest11_SettingUpdate:
                        state_id = ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
                        state = new ActionSelectSettingUpdateBranchState(state_id);
                        state.Start(arg);
                        break;
                    case ActionSelect_Branch_State_ID.Nest10_FadeIn:
                        state_id = ActionSelect_Branch_State_ID.Nest10_FadeIn;
                        state = new ActionSelectFadeInBranchState(state_id);
                        state.Start(arg);
                        break;
                 }
                }
                break;
            case ActionSelect_Branch_State_ID.Nest11_SaveUpdate:
                state_id = ActionSelect_Branch_State_ID.Nest11_SettingUpdate;
                state = new ActionSelectSettingUpdateBranchState(state_id);
                state.Start(arg);
                break;
             }
    }

    public override void StartInstance() {
        state = new ActionSelectBeginInitBranchState(ActionSelect_Branch_State_ID.Nest01_BeginInit);
    }
}
