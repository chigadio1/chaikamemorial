using System.Collections.Generic;
public class BaseActionSelectSettingUpdateBranchState : BaseActionSelectBranchState{
    public BaseActionSelectSettingUpdateBranchState(ActionSelect_Branch_State_ID other):base(other){}
    public virtual ActionSelect_Branch_State_ID BranchNest11_SettingUpdateState(){
        if(BranchNest11_SettingUpdateToSelectParentBackUpdate()) return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
        else if(BranchNest11_SettingUpdateToFadeIn()) return ActionSelect_Branch_State_ID.Nest10_FadeIn;
        else if(BranchNest11_SettingUpdateToLoadUpdate()) return ActionSelect_Branch_State_ID.Nest10_LoadUpdate;
        else if(BranchNest11_SettingUpdateToSaveUpdate()) return ActionSelect_Branch_State_ID.Nest11_SaveUpdate;
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
    public virtual bool BranchNest11_SettingUpdateToSelectParentBackUpdate(){return true;}
    public virtual bool BranchNest11_SettingUpdateToFadeIn(){return true;}
    public virtual bool BranchNest11_SettingUpdateToLoadUpdate(){return true;}
    public virtual bool BranchNest11_SettingUpdateToSaveUpdate(){return true;}
    public override ActionSelect_Branch_State_ID GenerateNextState(){
        switch (state_id)
        {
            case ActionSelect_Branch_State_ID.Nest11_SettingUpdate:
                return BranchNest11_SettingUpdateState();
        }
     return ActionSelect_Branch_State_ID.Nest09_SelectParentBackUpdate;
     }
}
