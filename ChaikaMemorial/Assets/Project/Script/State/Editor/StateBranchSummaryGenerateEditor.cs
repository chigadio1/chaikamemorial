using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using System;
using System.Linq;
using System.IO;
using static StateSummaryGenerateEditor;

public class StateBranchSummaryGenerateEditor : EditorWindow
{
    public static StateBranchSummaryGenerateEditor instance;
    public ExampleGraphView graphView;


    void OnEnable()
    {
        graphView = new ExampleGraphView(this);
        rootVisualElement.Add(graphView);
    }

    public void Save()
    {
        instance.graphView.SaveValue();
    }


}

public class ExampleNode : Node
{
    public ExampleNode()
    {
        title = "Test2";      // 入力用のポートを作成
        var inputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(float)); // 第三引数をPort.Capacity.Multipleにすると複数のポートへの接続が可能になる
        inputPort.portName = "Input";
        inputContainer.Add(inputPort); // 入力用ポートはinputContainerに追加する

        // 出力用のポートを作る
        var outputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(float));
        outputPort.portName = "Value";
        outputContainer.Add(outputPort); // 出力用ポートはoutputContainerに追加する
    }
}

public class StateBranchEditorNode : Node
{
    /// <summary>
    /// ステートID(エディタ用)
    /// </summary>
    public int state_branch_id = -1;
    public Port inputPort;
    public Port outputPort;
    public List<StateSystemSummaryEditor.NodeData> input_list = new List<StateSystemSummaryEditor.NodeData>();
    public List<StateSystemSummaryEditor.NodeData> output_list = new List<StateSystemSummaryEditor.NodeData>();
    public StateSystemSummaryEditor.SystemDataEditor data = new StateSystemSummaryEditor.SystemDataEditor();
    public void SetTitle(string name)
    {
        title = name;
    }
    public StateBranchEditorNode()
    {
        title = "aaaa";
        // 入力用のポートを作成
        inputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Multi, typeof(Port)); // 第三引数をPort.Capacity.Multipleにすると複数のポートへの接続が可能になる
        inputPort.portName = "Begin";





        inputContainer.Add(inputPort); // 入力用ポートはinputContainerに追加する

        // 出力用のポートを作る
        outputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(Port));
        outputPort.portName = "Next";
        outputContainer.Add(outputPort); // 出力用ポートはoutputContainerに追加する

        // ボタンを作成
        var button = new Button(() => HandleButtonClick());
        button.text = "CopyName";

        // ボタンをNodeに追加
        this.mainContainer.Add(button);
    }


    private void HandleButtonClick()
    {
        // スクリプトのアセットパスを指定
        string scriptPath = Application.dataPath + $"/Project/Script/State/{StateSummaryGenerateEditor.instance.GetSelectState().state_name}Branch";
        if (System.IO.Directory.Exists(scriptPath) == false) return;
        if (System.IO.File.Exists(scriptPath + $"/{StateSummaryGenerateEditor.instance.GetSelectState().state_name}{title}BranchState.cs") == false) return ;
        GUIUtility.systemCopyBuffer = $"{StateSummaryGenerateEditor.instance.GetSelectState().state_name}{title}BranchState";
        System.Diagnostics.Process.Start("devenv", "/edit " + $"Assets/Project/Script/State/{StateSummaryGenerateEditor.instance.GetSelectState().state_name}Branch/{StateSummaryGenerateEditor.instance.GetSelectState().state_name}{title}BranchState.cs");
    }

}

public class ExampleGraphView : GraphView
{

    /// <summary>
    /// リストノード
    /// </summary>
    public Dictionary<int, StateBranchEditorNode> list_editor_node = new Dictionary<int, StateBranchEditorNode>();

    public int LastIndex()
    {
        int count = 0;
        foreach(var data in list_editor_node.Values)
        {
            if (count < data.state_branch_id) count = data.state_branch_id;
        }
        return count + 1;
    }
 
    public ExampleGraphView(EditorWindow editorWindow)
    {
        if (StateBranchSummaryGenerateEditor.instance != null) return;
        InitNodeCreate();

        // 親のサイズに合わせてGraphViewのサイズを設定
        this.StretchToParentSize();

        // MMBスクロールでズームインアウトができるように
        SetupZoom(ContentZoomer.DefaultMinScale, ContentZoomer.DefaultMaxScale);
        // MMBドラッグで描画範囲を動かせるように
        this.AddManipulator(new ContentDragger());
        // LMBドラッグで選択した要素を動かせるように
        this.AddManipulator(new SelectionDragger());
        // LMBドラッグで範囲選択ができるように
        this.AddManipulator(new RectangleSelector());

        // 右クリックメニューを追加
        var menuWindowProvider = ScriptableObject.CreateInstance<SearchMenuWindowProvider>();
        menuWindowProvider.Initialize(this, editorWindow);
        nodeCreationRequest += context =>
        {
            SearchWindow.Open(new SearchWindowContext(context.screenMousePosition), menuWindowProvider);
        };
    }




    public void SaveValue()
    {
        for(int count = 0; count < StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data.Count;count++)
        {
            StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].input_state_branch.Clear();
            StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].output_state_branch.Clear();
            if (list_editor_node == null) continue;
            if (list_editor_node.ContainsKey(StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].state_branch_id) == false) continue;
            if (list_editor_node[StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].state_branch_id] == null) continue;
            var value_data = list_editor_node[StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].state_branch_id];
            {

                StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].position = value_data.GetPosition().position;
                foreach (var input in value_data.inputPort.connections.ToList())
                {
                   
                    var node = input.output.node as StateBranchEditorNode;
                    if (node.state_branch_id == value_data.state_branch_id) continue;
                    if (StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].input_state_branch.Find(i_data => i_data.node_id == node.state_branch_id) != null) continue;
                    var data = new StateSystemSummaryEditor.NodeData();
                    data.node_id = node.state_branch_id;
                    data.position = node.GetPosition().position;
                    data.state_name = node.title;
                    StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].input_state_branch.Add(data);
                }

                foreach (var output in value_data.outputPort.connections.ToList())
                {

                    var node = output.input.node as StateBranchEditorNode;
                    var childrens = node.outputContainer.Children().ToList();
                    if (StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].output_state_branch.Find(i_data => i_data.node_id == node.state_branch_id) != null) continue;
                    if (node.state_branch_id == value_data.state_branch_id) continue;
                    var data = new StateSystemSummaryEditor.NodeData();
                    data.node_id = node.state_branch_id;
                    data.position = node.GetPosition().position;
                    data.state_name = node.title;
                    StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data[count].output_state_branch.Add(data);
                }
            }


        }



    }

    public void InitNodeCreate()
    {
        list_editor_node.Clear();
        list_editor_node = new Dictionary<int, StateBranchEditorNode>();
        int count = 0;
        if (StateSummaryGenerateEditor.instance == null) return;
        foreach (var data in StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data)
        {
            var add_data = new StateBranchEditorNode();
            add_data.state_branch_id = data.state_branch_id;
            add_data.title = data.name;
            add_data.state_branch_id = count;
            ++count;
            add_data.SetPosition(new Rect(data.position, new Vector2(100.0f, 100.0f)));

            foreach(var inpu in data.input_state_branch)
            {
                var add_sub = new StateSystemSummaryEditor.NodeData();
                add_sub.state_name = inpu.state_name;
                add_sub.node_id = inpu.node_id;
                add_sub.position = inpu.position;
                add_data.data.input_state_branch.Add(add_sub);
            }
            foreach (var inpu in data.output_state_branch)
            {
                var add_sub = new StateSystemSummaryEditor.NodeData();
                add_sub.state_name = inpu.state_name;
                add_sub.node_id = inpu.node_id;
                add_sub.position = inpu.position;
                add_data.data.output_state_branch.Add(add_sub);
            }
            list_editor_node[add_data.state_branch_id] = add_data;

            AddElement(add_data);
        }
        var input_array = new Dictionary<int, StateBranchEditorNode>();
        var output_array = new Dictionary<int, StateBranchEditorNode>();
        foreach (var data in list_editor_node)
        { 

            foreach (var input in data.Value.data.input_state_branch)
            {
                StateBranchEditorNode add_data;
                if (list_editor_node.ContainsKey(input.node_id) == false)
                {
                    add_data = new StateBranchEditorNode();
                    add_data.state_branch_id = input.node_id;
                    add_data.state_branch_id = count;
                    add_data.title = input.state_name;
                    ++count;
                    add_data.SetPosition(new Rect(input.position, new Vector2(100.0f, 100.0f)));
                    input_array[add_data.state_branch_id] = add_data;
                    AddElement(add_data);
                }
                else
                {
                    add_data = list_editor_node[input.node_id];
                
                }
                // ノードを接続
                var edge = new Edge { output = (Port)add_data.outputContainer[0], input = (Port)data.Value.inputContainer[0] };
                var node_add = new StateSystemSummaryEditor.NodeData();
                node_add.node_id = add_data.state_branch_id;
                node_add.state_name = add_data.title;
                node_add.position = add_data.GetPosition().position;
                data.Value.input_list.Add(node_add);
                edge.input.Connect(edge);
                edge.output.Connect(edge);
                AddElement(edge);

            }

            foreach (var output in data.Value.data.output_state_branch)
            {
                StateBranchEditorNode add_data;
                if (list_editor_node.ContainsKey(output.node_id) == false)
                {
                    add_data = new StateBranchEditorNode();
                    add_data.state_branch_id = output.node_id;
                    add_data.title = output.state_name;
                    add_data.state_branch_id = count;
                    ++count;
                    add_data.SetPosition(new Rect(output.position, new Vector2(100.0f, 100.0f)));
                    output_array[add_data.state_branch_id] = add_data;
                    AddElement(add_data);
                }
                else
                {
                    add_data = list_editor_node[output.node_id];
                }

                // ノードを接続
                var edge = new Edge { output = (Port)data.Value.outputContainer[0], input = (Port)add_data.inputContainer[0] };
                edge.output.Connect(edge);
                edge.input.Connect(edge);
                var node_add = new StateSystemSummaryEditor.NodeData();
                node_add.node_id = add_data.state_branch_id;
                node_add.state_name = add_data.title;
                node_add.position = add_data.GetPosition().position;
                data.Value.output_list.Add(node_add);
                AddElement(edge);

            }

        }

        list_editor_node = list_editor_node.Union(input_array).ToDictionary(pair => pair.Key, pairt => pairt.Value);
        list_editor_node = list_editor_node.Union(output_array).ToDictionary(pair => pair.Key, pairt => pairt.Value);


    }

    public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
    {
        // Create Nodeメニュー
        if (evt.target is GraphView && nodeCreationRequest != null)
        {
            evt.menu.AppendAction("Create Node", OnContextMenuNodeCreate, DropdownMenuAction.AlwaysEnabled);
            evt.menu.AppendSeparator();
        }
        // Copyメニュー
        if ((evt.target is Node))
        {            // ノードが選択されている
            evt.menu.AppendAction(            // menu選択後のAction追加
                "Copy",                        // menu名
                copy => { CopyAction(); },    // 実際に呼び出されれるアクション
                                              // canCopySelection:コピーが成功/失敗 => (メニュー活性/非活性)
                (Func<DropdownMenuAction, DropdownMenuAction.Status>)(copy => (this.canCopySelection ? DropdownMenuAction.Status.Normal : DropdownMenuAction.Status.Disabled)),
                (object)null);
        }
        // Pasteメニュー
        if (evt.target is UnityEditor.Experimental.GraphView.GraphView)
        {
            evt.menu.AppendAction(
                "Paste",
                paste => { PasteAction(); },
                (Func<DropdownMenuAction, DropdownMenuAction.Status>)(paste => (this.canPaste ? DropdownMenuAction.Status.Normal : DropdownMenuAction.Status.Disabled)),
                (object)null);
        }
        // Deleteメニュー
        if (evt.target is GraphView || evt.target is Node || evt.target is Group || evt.target is Edge)
        {
            evt.menu.AppendAction("Delete", (a) => { DeleteCallback(AskUser.DontAskUser); },
                (a) => { return canDeleteSelection ? DropdownMenuAction.Status.Normal : DropdownMenuAction.Status.Disabled; });
        }
    }
    // Create Nodeメニュー作成
    void OnContextMenuNodeCreate(DropdownMenuAction a)
    {
        RequestNodeCreation(null, -1, a.eventInfo.mousePosition);
    }

    private void RequestNodeCreation(VisualElement target, int index, Vector2 position)
    {
        if (nodeCreationRequest == null)
            return;

        // Editorウィンドウ取得
        var graphEditorWindow = StateBranchSummaryGenerateEditor.instance;

        Vector2 screenPoint = graphEditorWindow.position.position + position;
        nodeCreationRequest(new NodeCreationContext() { screenMousePosition = screenPoint, target = target, index = index });
    }

    private void CopyAction()
    {

        // 1.コピー用シリアライズ登録
        serializeGraphElements = new SerializeGraphElementsDelegate(Copy);

        // 2.コピー用コールバック呼び出し
        CopySelectionCallback();

    }


    private void PasteAction()
    {

        // 1.貼り付け用デシリアライズ登録
        unserializeAndPaste = new UnserializeAndPasteDelegate(Paste);
        // 2.ペースト用コールバック呼び出し
        PasteCallback();
    }

    string Copy(IEnumerable<GraphElement> elements)
    {
        foreach(var data_sub in elements.ToList())
        {
            StateBranchEditorNode node = data_sub as StateBranchEditorNode;
            Debug.Log(node.title);
        }
        string data = "";
        // コピー処理 ノードをstringにシリアライズ
        return data;
    }
    void Paste(string operationName, string data)
    {
        // ペースト処理 シリアライズからノード生成
    }
    public void DeleteCallback(AskUser askUser)
    {
        var selectedElements = selection.OfType<GraphElement>().ToList();

        foreach (var element in selectedElements)
        {
            StateBranchEditorNode node = element as StateBranchEditorNode;
            StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data.RemoveAll(data => data.state_branch_id == node.state_branch_id);
            RemoveElement(element);
        }
    }


    // GetCompatiblePortsをオーバーライドする
    public override List<Port> GetCompatiblePorts(Port startAnchor, NodeAdapter nodeAdapter)
    {
        var compatiblePorts = new List<Port>();
        foreach (var port in ports.ToList())
        {
            if (startAnchor.node == port.node ||
                startAnchor.direction == port.direction ||
                startAnchor.portType != port.portType)
            {
                continue;
            }
            compatiblePorts.Add(port);
        }


        return compatiblePorts;
    }
}

public class SearchMenuWindowProvider : ScriptableObject, ISearchWindowProvider
{
    private ExampleGraphView _graphView;
    private EditorWindow _editorWindow;

    public void Initialize(ExampleGraphView graphView, EditorWindow editorWindow)
    {
        _graphView = graphView;
        _editorWindow = editorWindow;
    }

    List<SearchTreeEntry> ISearchWindowProvider.CreateSearchTree(SearchWindowContext context)
    {
        var entries = new List<SearchTreeEntry>();
        entries.Add(new SearchTreeGroupEntry(new GUIContent("Create Node")));



        var file_path = Application.dataPath + "/Project/Assets/Editor/Data/State/";
        var files = System.IO.Directory.GetFiles(file_path, "*.json", System.IO.SearchOption.AllDirectories);


        entries.Add(new SearchTreeEntry(new GUIContent("Begin")) { level = 1, userData = typeof(StateBranchEditorNode) });

        foreach(var data in StateSummaryGenerateEditor.instance.GetSelectState().list_state)
        {
            // Exampleグループの下に各ノードを作るためのメニューを追加
            entries.Add(new SearchTreeEntry(new GUIContent(data.name)) { level = 1, userData = typeof(StateBranchEditorNode) });
        }
        entries.Add(new SearchTreeEntry(new GUIContent("Finish")) { level = 2, userData = typeof(StateBranchEditorNode) });
        




        return entries;
    }

    bool ISearchWindowProvider.OnSelectEntry(SearchTreeEntry searchTreeEntry, SearchWindowContext context)
    {
      
        var type = searchTreeEntry.userData as Type;
        var node = Activator.CreateInstance(type) as StateBranchEditorNode;

        // マウスの位置にノードを追加
        var worldMousePosition = _editorWindow.rootVisualElement.ChangeCoordinatesTo(_editorWindow.rootVisualElement.parent, context.screenMousePosition - _editorWindow.position.position);
        var localMousePosition = _graphView.contentViewContainer.WorldToLocal(worldMousePosition);
        node.SetPosition(new Rect(localMousePosition, new Vector2(100, 100)));

        node.title = searchTreeEntry.name;
        int index = 0;
        if (StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data.Count > 0) index = StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data.Last().state_branch_id + 1;
        node.state_branch_id = index;
        StateBranchSummaryData.StateBranchData add_data = new StateBranchSummaryData.StateBranchData();
        add_data.position = localMousePosition;
        add_data.state_branch_id = index;
        add_data.name = node.title;
        StateSummaryGenerateEditor.instance.summary_state_branch.state_branch_data.Add(add_data);
        StateBranchSummaryGenerateEditor.instance.graphView.list_editor_node[node.state_branch_id] = node;


        _graphView.AddElement(node);
        return true;
    }
}