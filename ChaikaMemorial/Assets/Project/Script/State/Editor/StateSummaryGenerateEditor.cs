using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class StateSummaryGenerateEditor : EditorWindow
{
    public enum Create_State
    {
        Enum,
        ArgData,
        State,
        Manager
    }
    [System.Serializable]
    public class StateSystemSummaryEditor
    {
        [System.Serializable]
        public class NodeData
        {
            [SerializeField]
            public int node_id = -1;
            [SerializeField]
            public string state_name = "";
            [SerializeField]
            public Vector2 position = Vector2.zero;

        }
        [System.Serializable]
        public class SystemDataEditor
        {
            [System.Serializable]
            public class SystemColumnDataEditor
            {
                [SerializeField]
                public string type_name = "";//変数
                [SerializeField]
                public string value_name = "";//変数名
                [SerializeField]
                public string explanation_text = "";//説明文
                [SerializeField]
                public int length = 0; //配列数 -1でList
            }
            [SerializeField]
            public string name = ""; //遷移名
            [SerializeField]
            public List<SystemColumnDataEditor> list_column = new List<SystemColumnDataEditor>(); //カラム
            [SerializeField]
            public string explanation_text = "";//説明文
            [SerializeField]
            public List<NodeData> input_state_branch = new List<NodeData>(); //分岐ステート
            [SerializeField]
            public List<NodeData> output_state_branch = new List<NodeData>(); //分岐ステート
            [SerializeField]
            public Vector2 state_editor_position = Vector2.zero;

            [SerializeField]
            public int state_branch_id = -1;
        }
        [SerializeField]
        public string state_name = ""; //遷移名
        [SerializeField]
        public string explanation_text = "";//説明文
        [SerializeField]
        public List<SystemDataEditor> list_state = new List<SystemDataEditor>();//遷移リスト


        [SerializeField]
        public SystemDataEditor initialize_arg = new SystemDataEditor(); //初期化関数の引数データクラス

        [SerializeField]
        public SystemDataEditor update_arg = new SystemDataEditor(); //更新関数の引数データクラス

        [SerializeField]
        public SystemDataEditor manager_column = new SystemDataEditor(); //管理クラスの変数

        [SerializeField]
        public SystemDataEditor base_state_column = new SystemDataEditor(); //基礎ステート変数

    }
    [System.Serializable]
    public class StateBranchSummaryData
    {
        [System.Serializable]
        public class StateBranchData
        {
            [SerializeField]
            public int state_branch_id = -1;
            [SerializeField]
            public string name = ""; //遷移名
            [SerializeField]
            public Vector2 position = Vector2.zero;
            [SerializeField]
            public List<StateSystemSummaryEditor.NodeData> input_state_branch = new List<StateSystemSummaryEditor.NodeData>(); //分岐ステート
            [SerializeField]
            public List<StateSystemSummaryEditor.NodeData> output_state_branch = new List<StateSystemSummaryEditor.NodeData>(); //分岐ステート
            [SerializeField]
            public string nest_name = "";
            [SerializeField]
            public bool remove_flag = false;
        }
        [SerializeField]
        public List<StateBranchData> state_branch_data = new List<StateBranchData>();
    }
    public List<StateSystemSummaryEditor> list_summary = new List<StateSystemSummaryEditor>();
    [SerializeField]
    public StateBranchSummaryData summary_state_branch = new StateBranchSummaryData();

    public string file_path = "";
    public static StateSummaryGenerateEditor instance; //インスタンス
    public string text_state_generate_name;
    public string text_state_module_generate_name;

    private Vector2 left_scroll = Vector2.zero;
    private Vector2 right_scroll = Vector2.zero;
    private int selected_index = -1; // 選択されたリストアイテムのインデックス
    private Vector2 left_module_scroll = Vector2.zero;
    private int selected_module_index = -1; // 選択されたリストアイテムのインデックス


    private Vector2 left_module_column_scroll = Vector2.zero;

    private Vector2 left_init_scroll = Vector2.zero;
    private Vector2 left_update_scroll = Vector2.zero;
    private Vector2 left_manager_scroll = Vector2.zero;
    private Vector2 left_base_state_scroll = Vector2.zero;

    public int a;

    public StateSystemSummaryEditor GetSelectState()
    {
        if (selected_index < 0) return null;
        return list_summary[selected_index];
    }


    //using取得
    void GetUsingType(string type_name,ref List<string> usingNamesList)
    {
        string usingName = "";

        // 正規表現パターンを定義
        string pattern = @"(\w+)<(\w+)>";

        // 正規表現にマッチするかを確認
        bool isMatch = Regex.IsMatch(type_name, pattern);

        if (isMatch)
        {
            // 正規表現にマッチした部分を抽出
            Match match = Regex.Match(type_name, pattern);

            // パターン内のキャプチャグループを取得
            string typeName = match.Groups[1].Value;
            string className = typeName; // 取得したいクラス名
            Type classType = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .FirstOrDefault(type => type.Name == className);
            if (classType != null)
            {
                string namespaceName = classType.Namespace;
                usingName = namespaceName;
            }
            string parameter = match.Groups[2].Value;
            className = parameter; // 取得したいクラス名
            classType = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .FirstOrDefault(type => type.Name == className);
            if (classType != null)
            {
                string namespaceName = classType.Namespace;
                usingName = namespaceName; 
            }
        }
        else
        {

            // パターン内のキャプチャグループを取得
            string typeName = type_name;
            string className = typeName; // 取得したいクラス名
            Type classType = AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .FirstOrDefault(type => type.Name == className);
            if (classType != null)
            {
                string namespaceName = classType.Namespace;
                usingName = namespaceName;
            }
        }




        if (usingName == "") return;

        foreach(var name in usingNamesList)
        {
            if(usingName == name)
            {
                return;
            }
        }
        usingNamesList.Add(usingName);
    }


    [MenuItem("Custom/遷移SummaryGenerate")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            Debug.Log("すでにウィンドウは存在しています");
            return;
        }
        // ウィンドウを作成または表示します
        StateSummaryGenerateEditor.instance = EditorWindow.GetWindow(typeof(StateSummaryGenerateEditor)) as StateSummaryGenerateEditor;
        StateSummaryGenerateEditor.instance.Init();


    }

    public void Init()
    {
        file_path = Application.dataPath + "/Project/Assets/Editor/Data/State/";

        var files = System.IO.Directory.GetFiles(file_path, "*.json", System.IO.SearchOption.AllDirectories);
        foreach(var file in files)
        {
            if(file.IndexOf("json") < 0)
            {
                continue;
            }
            StreamReader reader = new StreamReader(file); //受け取ったパスのファイルを読み込む
            string datastr = reader.ReadToEnd();//ファイルの中身をすべて読み込む
            reader.Close();//ファイルを閉じるs
            var addData = JsonUtility.FromJson<StateSystemSummaryEditor>(datastr);
            int count = 0;
            foreach(var data in addData.list_state)
            {
                data.state_branch_id = count;
                ++count;
            }
            list_summary.Add(addData);



        }


    }

    /// <summary>
    /// 分岐遷移データ作成
    /// </summary>
    /// <returns></returns>
    public void InitBranchCreate()
    {
        int count = 0;
        var local_old =list_summary[selected_index].list_state.ToList();
        foreach (var data in list_summary[selected_index].list_state)
        {
            data.state_branch_id = count;
            ++count;
        }

        Dictionary<string, (int old_id,int next_id)> change_state_branch_id = new Dictionary<string, (int old_id, int next_id)>();
        for(count = 0; count < local_old.Count;)
        {
            if(local_old[count].name == list_summary[selected_index].list_state[count].name)
            {
                if(local_old[count].state_branch_id != list_summary[selected_index].list_state[count].state_branch_id)
                {
                    change_state_branch_id[local_old[count].name] = (local_old[count].state_branch_id, list_summary[selected_index].list_state[count].state_branch_id);
                }
            }
            ++count;
        }
        string branch_file = Application.dataPath + "/Project/Assets/Editor/Data";
        if (Directory.Exists(branch_file + $"/Branch") == false)
        {
            Directory.CreateDirectory(branch_file + $"/Branch");
        }

        //ブランチデータ
        string path = branch_file + $"/Branch/{list_summary[selected_index].state_name}_Branch.json";

        summary_state_branch.state_branch_data.Clear();
        if (System.IO.File.Exists(path) == false)
        {
            foreach (var state in list_summary[selected_index].list_state)
            {
                StateBranchSummaryData.StateBranchData add_data = new StateBranchSummaryData.StateBranchData();
                add_data.name = state.name;
                add_data.state_branch_id = state.state_branch_id;
                summary_state_branch.state_branch_data.Add(add_data);
            }
            string jsonstr = JsonUtility.ToJson(summary_state_branch);
            StreamWriter writer = new StreamWriter(path, false);//初めに指定したデータの保存先を開く
            writer.WriteLine(jsonstr);//JSONデータを書き込み
            writer.Flush();//バッファをクリアする
            writer.Close();//ファイルをクローズする
            return;
        }
        else
        {
            //すでにある情報を取得
            StreamReader reader = new StreamReader(path); //受け取ったパスのファイルを読み込む
            string datastr = reader.ReadToEnd();//ファイルの中身をすべて読み込む
            reader.Close();//ファイルを閉じるs
            summary_state_branch = JsonUtility.FromJson<StateBranchSummaryData>(datastr);
            //入れ替えたステートの変更
            foreach(var data in summary_state_branch.state_branch_data)
            {
                if (change_state_branch_id.ContainsKey(data.name) == false) continue;
                if (data.state_branch_id == change_state_branch_id[data.name].old_id)
                {
                    data.state_branch_id = change_state_branch_id[data.name].next_id;
                }
            }
            //新しく追加されたステートがあれば追加
            foreach (var state in list_summary[selected_index].list_state)
            {
                var find_data = summary_state_branch.state_branch_data.Find(data => data.name == state.name);
                if(find_data == null)
                {
                    StateBranchSummaryData.StateBranchData add_data = new StateBranchSummaryData.StateBranchData();
                    add_data.name = state.name;
                    add_data.state_branch_id = state.state_branch_id;
                    summary_state_branch.state_branch_data.Add(add_data);
                    foreach(var node in summary_state_branch.state_branch_data)
                    {
                        foreach(var data in node.input_state_branch)
                        {
                            if (FIndData(data) == false) data.node_id += 1;
                        }
                        foreach (var data in node.output_state_branch)
                        {
                            if (FIndData(data) == false) data.node_id += 1;
                        }
                    }
                }
            }
        
            //消されたステートを削除
            List<string> remove_state_names = new List<string>();
            for(count = 0; count < summary_state_branch.state_branch_data.Count;)
            {
                var find_data = list_summary[selected_index].list_state.Find(data => data.name == summary_state_branch.state_branch_data[count].name);
                if(find_data == null)
                {
                    remove_state_names.Add(summary_state_branch.state_branch_data[count].name);
                    list_summary[selected_index] = null;
                }
                ++count;
            }
            summary_state_branch.state_branch_data.RemoveAll(data => data == null);
            foreach (var branch in summary_state_branch.state_branch_data)
            {
                foreach (var remove_name in remove_state_names)
                {
                    branch.input_state_branch.RemoveAll(data => data.state_name == remove_name);
                    branch.output_state_branch.RemoveAll(data => data.state_name == remove_name);
                }
            }
            summary_state_branch.state_branch_data.Sort((a, b) => a.state_branch_id.CompareTo(b.state_branch_id));
            //
            //同じ被っているIDがあればインクリメント
            var change_state_list_branch_id = new Dictionary<string, List<(int old_id, int next_id)>>();
            for (count = 0; count + 1 < summary_state_branch.state_branch_data.Count;)
            {
                if(summary_state_branch.state_branch_data[count].state_branch_id == summary_state_branch.state_branch_data[count + 1].state_branch_id)
                {
                    if (change_state_list_branch_id.ContainsKey(summary_state_branch.state_branch_data[count].name) == false) change_state_list_branch_id[summary_state_branch.state_branch_data[count].name] = new List<(int old_id, int next_id)>();
                    change_state_list_branch_id[summary_state_branch.state_branch_data[count].name].Add(new(summary_state_branch.state_branch_data[count + 1].state_branch_id, summary_state_branch.state_branch_data[count + 1].state_branch_id + 1));
                    summary_state_branch.state_branch_data[count + 1].state_branch_id += 1;
                }
                ++count;
            }

            foreach(var change_name in change_state_branch_id)
            {
                foreach (var state in summary_state_branch.state_branch_data)
                {
                    for (count = 0; count < state.input_state_branch.Count;)
                    {
                        if (state.input_state_branch[count].state_name == change_name.Key && state.input_state_branch[count].node_id == change_name.Value.old_id)
                        {
                            state.input_state_branch[count].node_id = change_name.Value.next_id;
                        }
                        ++count;
                    }
                    for (count = 0; count < state.output_state_branch.Count;)
                    {
                        if (state.output_state_branch[count].state_name == change_name.Key && state.output_state_branch[count].node_id == change_name.Value.old_id)
                        {
                            state.output_state_branch[count].node_id = change_name.Value.next_id;
                        }
                        ++count;
                    }
                }
            }

            foreach(var change_name in change_state_list_branch_id)
            {
                foreach(var data_value in change_name.Value)
                {
                    foreach(var state in summary_state_branch.state_branch_data)
                    {
                        for(count = 0; count < state.input_state_branch.Count;)
                        {
                            if (state.input_state_branch[count].state_name == change_name.Key && state.input_state_branch[count].node_id == data_value.old_id)
                            {
                                state.input_state_branch[count].node_id = data_value.next_id;
                            }
                            ++count;
                        }
                        for (count = 0; count < state.output_state_branch.Count;)
                        {
                            if (state.output_state_branch[count].state_name == change_name.Key && state.output_state_branch[count].node_id == data_value.old_id)
                            {
                                state.output_state_branch[count].node_id = data_value.next_id;
                            }
                            ++count;
                        }
                    }
                }
            }
        }
    }

    public  bool FIndData(StateSystemSummaryEditor.NodeData  node)
    {
        foreach(var state in list_summary[selected_index].list_state)
        {
            if (state.state_branch_id == node.node_id) return true;
        }

        return false;
    }



    public void AddData(string name)
    {
        if (name == "") return;

        foreach(var data in list_summary)
        {
            if (data.state_name == name)
            {
                Debug.LogWarning("すでに存在しています");
                return;
            }
        }

        //保存
        var addData = new StateSystemSummaryEditor();
        addData.state_name = name;

        list_summary.Add(addData);

        string jsonstr = JsonUtility.ToJson(addData);
        StreamWriter writer = new StreamWriter(file_path + $"/{name}.json", false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする
    }

    public void AddModuleData(string name)
    {
        if (name == "") return;

        var stateData = list_summary[selected_index];
        foreach (var data in stateData.list_state)
        {
            if (data.name == name)
            {
                Debug.LogWarning("すでに存在しています");
                return;
            }
        }

        //保存
        var addData = new StateSummaryGenerateEditor.StateSystemSummaryEditor.SystemDataEditor();
        addData.name = name;

        list_summary[selected_index].list_state.Add(addData);

        string jsonstr = JsonUtility.ToJson(list_summary[selected_index]);
        StreamWriter writer = new StreamWriter(file_path + $"/{list_summary[selected_index].state_name}.json", false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする


    }

    public void AddModuleColumn()
    {
        list_summary[selected_index].list_state[selected_module_index].list_column.Add(new StateSystemSummaryEditor.SystemDataEditor.SystemColumnDataEditor());
    }

    public void AddModuleColumn(ref StateSystemSummaryEditor.SystemDataEditor data)
    {
        data.list_column.Add(new StateSystemSummaryEditor.SystemDataEditor.SystemColumnDataEditor());
    }

    public void MoveModuleData(int index,int movePos)
    {
        if(movePos > 0)
        {
            if(index + movePos >= list_summary[selected_index].list_state.Count)
            {
                return;
            }
        }
        else
        {
            if(index == 0)
            {
                return;
            }
            if(index - movePos <= 0)
            {
                return;
            }
        }

        var saveOrijine = list_summary[selected_index].list_state[index];
        list_summary[selected_index].list_state[index] = list_summary[selected_index].list_state[index + movePos];
        list_summary[selected_index].list_state[index + movePos] = saveOrijine;

        string jsonstr = JsonUtility.ToJson(list_summary[selected_index]);
        StreamWriter writer = new StreamWriter(file_path + $"/{list_summary[selected_index].state_name}.json", false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする

    }

    public void DeleteData(int index)
    {

        File.Delete(file_path + $"/{list_summary[index].state_name}.json");
        list_summary.RemoveAt(index);

        selected_index = selected_module_index =  -1;
    }

    public void DeleteModuleData(int index)
    {
        list_summary[selected_index].list_state.RemoveAt(index);

        string jsonstr = JsonUtility.ToJson(list_summary[selected_index]);
        StreamWriter writer = new StreamWriter(file_path + $"/{list_summary[selected_index].state_name}.json", false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする

        selected_module_index = -1;
    }

    public void DeleteModuleColumnData(int index)
    {
        list_summary[selected_index].list_state[selected_module_index].list_column.RemoveAt(index);



    }

    public void DeleteModuleColumnData(ref StateSystemSummaryEditor.SystemDataEditor data,int index )
    {
        data.list_column.RemoveAt(index);
    }

    public void ColumnDataGUI(ref StateSystemSummaryEditor.SystemDataEditor data, string panel_name,ref Vector2 scroll)
    {
        GUILayout.Label(panel_name, EditorStyles.boldLabel);
        scroll = EditorGUILayout.BeginScrollView(scroll);
        GUILayout.BeginHorizontal();
        GUILayout.Label("変数の型", GUILayout.Width(100));
        GUILayout.Label("変数名", GUILayout.Width(150));
        GUILayout.Label("配列数", GUILayout.Width(50));
        GUILayout.Label("説明", GUILayout.Width(250));
        GUILayout.EndHorizontal();



        for (int i = 0; i < data.list_column.Count; i++)
        {
            GUILayout.BeginHorizontal();

            data.list_column[i].type_name = EditorGUILayout.TextField("",        data.list_column[i].type_name, GUILayout.Width(100));
            data.list_column[i].value_name = EditorGUILayout.TextField("",       data.list_column[i].value_name, GUILayout.Width(150));
            data.list_column[i].length = EditorGUILayout.IntField("",            data.list_column[i].length, GUILayout.Width(50));
            data.list_column[i].explanation_text = EditorGUILayout.TextField("", data.list_column[i].explanation_text, GUILayout.Width(250));

            if (GUILayout.Button("×", GUILayout.Width(100)))
            {
                DeleteModuleColumnData(ref data, i);
            }

            GUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add", GUILayout.Width(100)))
        {
            AddModuleColumn(ref data);
        }
        GUILayout.EndHorizontal();
    }

    public void SaveState()
    {

        if(StateBranchSummaryGenerateEditor.instance != null) StateBranchSummaryGenerateEditor.instance.Save();

        string branch_file = Application.dataPath + "/Project/Assets/Editor/Data";
        if (Directory.Exists(branch_file + $"/Branch") == false)
        {
            Directory.CreateDirectory(branch_file + $"/Branch");
        }

        //ブランチデータ
        string path = branch_file + $"/Branch/{list_summary[selected_index].state_name}_Branch.json";
        string jsonstr = JsonUtility.ToJson(summary_state_branch);
        StreamWriter writer = new StreamWriter(path, false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする

        jsonstr = JsonUtility.ToJson(list_summary[selected_index]);

        int count = 0;
        foreach (var data in list_summary[selected_index].list_state)
        {
            data.state_branch_id = count;
            ++count;
        }

        writer = new StreamWriter(file_path + $"/{list_summary[selected_index].state_name}.json", false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする



    }

    //enumファイル作成
    public void GenerateEnumFile(string path)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;

        string filePath = path + $"/{list_summary[selected_index].state_name}Enum.cs";

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            writer.WriteLine("using System;");
            writer.WriteLine($"public enum {list_summary[selected_index].state_name}_State_ID " + "{");

            writer.WriteLine($"    Begin, //初期");
            foreach (var data in list_summary[selected_index].list_state)
            {
                writer.WriteLine($"    {data.name}, //{data.explanation_text}");
                

            }
            writer.WriteLine($"    Finish //終了");
            writer.WriteLine("}");
        }
    }

    public void GenerateArgFile(string path,string argName,StateSystemSummaryEditor.SystemDataEditor systemData)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;


        
        string filePath = path + $"/{systemData.name}{argName}ArgData.cs";

        using (StreamWriter writer = new StreamWriter(filePath))
        {

            List<string> usingNames = new List<string>();
            usingNames.Add("System.Collections.Generic");
            foreach (var data in systemData.list_column)
            {

                // 正規表現にマッチするかを確認
                GetUsingType(data.type_name,ref usingNames);
            }
            foreach (var name in usingNames)
            {
                if (name == "" || name == null || name == "null") continue;
                writer.WriteLine($"using {name};");
            }

            writer.WriteLine("using System;");
            writer.WriteLine($"public class {list_summary[selected_index].state_name}{argName}ArgData : BaseStateArgData" + "{");
            foreach (var data in systemData.list_column)
            {
                if(data.type_name == "" || data.value_name == "")
                {
                    continue;
                }

                if (data.length < 0)
                {
                    writer.WriteLine($"    public List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                }
                else if (data.length > 0)
                {
                    writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new  {data.type_name} [  {data.length}]; //{data.explanation_text}");
                }
                else
                {
                    writer.WriteLine($"    public {data.type_name} {data.value_name}; //{data.explanation_text}");
                }
                

            }

            writer.WriteLine("}");
        }

    }

    public void GenerateState(string path)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;

        //基礎Stateクラス作成
        string fileBaseStatePath = path + $"/Base{list_summary[selected_index].state_name}State.cs";
        using (StreamWriter writer = new StreamWriter(fileBaseStatePath))
        {
            List<string> usingNames = new List<string>();
            usingNames.Add("System.Collections.Generic");
            foreach (var data in list_summary[selected_index].base_state_column.list_column)
            {
                // 正規表現にマッチするかを確認
                GetUsingType(data.type_name, ref usingNames);
            }
            foreach (var name in usingNames)
            {
                if (name == "" || name == null || name == "null") continue;
                writer.WriteLine($"using {name};");
            }
            writer.WriteLine($"public class Base{list_summary[selected_index].state_name}State : BaseState<{list_summary[selected_index].state_name}InitArgData,{list_summary[selected_index].state_name}UpdateArgData>" + "{");
            foreach (var data in list_summary[selected_index].base_state_column.list_column)
            {
                if (data.type_name == "" || data.value_name == "")
                {
                    continue;
                }

                if (data.length < 0)
                {
                    writer.WriteLine($"    public List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                }
                else if (data.length > 0)
                {
                    writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new  {data.type_name} [  {data.length}]; //{data.explanation_text}");
                }
                else
                {
                    writer.WriteLine($"    public {data.type_name} {data.value_name}; //{data.explanation_text}");
                }
            }
            writer.WriteLine($"    public Base{list_summary[selected_index].state_name}State(Base{list_summary[selected_index].state_name}State other)"+"{");
            foreach (var data in list_summary[selected_index].base_state_column.list_column)
            {
                if (data.type_name == "" || data.value_name == "")
                {
                    continue;
                }
                writer.WriteLine($"        this.{data.value_name}=other.{data.value_name};");

            }
            writer.WriteLine("    }");

            writer.WriteLine($"    public Base{list_summary[selected_index].state_name}State()" + "{");
            writer.WriteLine("    }");
            writer.WriteLine("}");
        }

        foreach (var state in list_summary[selected_index].list_state)
        {
            string fileBasePath = path + $"/Base{list_summary[selected_index].state_name}{state.name}State.cs";
            string filePath = path + $"/{list_summary[selected_index].state_name}{state.name}State.cs";

            using (StreamWriter writer = new StreamWriter(fileBasePath))
            {


                List<string> usingNames = new List<string>();
                usingNames.Add("System.Collections.Generic");
                foreach (var data in state.list_column)
                {
                    // 正規表現にマッチするかを確認
                    GetUsingType(data.type_name, ref usingNames);
                }
                foreach (var name in usingNames)
                { 
                    if (name == "" || name == null || name == "null") continue;
                    writer.WriteLine($"using {name};");
                }


                writer.WriteLine($"public class Base{list_summary[selected_index].state_name}{state.name}State : Base{list_summary[selected_index].state_name}State" + "{");

                foreach(var data in state.list_column)
                {
                    if (data.type_name == "" || data.value_name == "")
                    {
                        continue;
                    }

                    if (data.length < 0)
                    {
                        writer.WriteLine($"    protected List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                    }
                    else if (data.length > 0)
                    {
                        writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new {data.type_name}[{data.length}]; //{data.explanation_text}");
                    }
                    else
                    {
                        writer.WriteLine($"    protected {data.type_name} {data.value_name}; //{data.explanation_text}");
                    }
                }
                writer.WriteLine($"    public Base{list_summary[selected_index].state_name}{state.name}State(Base{list_summary[selected_index].state_name}State other):base(other)" + "{}");

                writer.WriteLine($"    public Base{list_summary[selected_index].state_name}{state.name}State():base()" + "{}");
                writer.WriteLine("}");
            }

            if (File.Exists(filePath) == false)
            {
                using (StreamWriter writer = new StreamWriter(filePath))
                {

                    List<string> usingNames = new List<string>();
                    usingNames.Add("System.Collections.Generic");
                    foreach (var data in state.list_column)
                    {
                        // 正規表現にマッチするかを確認
                        GetUsingType(data.type_name, ref usingNames);
                    }
                    foreach (var name in usingNames)
                    {
                        if (name == "" || name == null || name == "null") continue;
                        writer.WriteLine($"using {name};");
                    }
                    writer.WriteLine($"public class {list_summary[selected_index].state_name}{state.name}State : Base{list_summary[selected_index].state_name}{state.name}State " + "{");


                    writer.WriteLine($"    public override void Start({list_summary[selected_index].state_name}InitArgData arg)" + " {");
                    writer.WriteLine("    }");
                    writer.WriteLine($"    public override void Update({list_summary[selected_index].state_name}UpdateArgData arg)" + " {");
                    writer.WriteLine("    }");
                    writer.WriteLine($"    public override void Finish({list_summary[selected_index].state_name}UpdateArgData arg)" + " {");
                    writer.WriteLine("    }");

                    writer.WriteLine($"    public {list_summary[selected_index].state_name}{state.name}State(Base{list_summary[selected_index].state_name}State other):base(other)" + "{}");

                    writer.WriteLine($"    public {list_summary[selected_index].state_name}{state.name}State():base()" + "{}");

                    writer.WriteLine("}");
                }
            }
        }
    }

    public void GenerateManegerState(string path)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;

        string initArgName = $"{list_summary[selected_index].state_name}InitArgData";
        string updateArgName = $"{list_summary[selected_index].state_name}UpdateArgData";
        string baseStateName = $"Base{list_summary[selected_index].state_name}State";

        string fileBasePath = path + $"/Base{list_summary[selected_index].state_name}StateManager.cs";
        string filePath = path + $"/{list_summary[selected_index].state_name}StateManager.cs";

        using (StreamWriter writer = new StreamWriter(fileBasePath))
        {
            List<string> usingNames = new List<string>();
            {
                usingNames.Add("System.Collections.Generic");
                foreach (var data in list_summary[selected_index].manager_column.list_column)
                {
                    // 正規表現にマッチするかを確認
                    GetUsingType(data.type_name, ref usingNames);
                }
                foreach (var name in usingNames)
                {
                    if (name == "" || name == null || name == "null") continue;
                    writer.WriteLine($"using {name};");
                }
            }

            writer.WriteLine($"public class Base{list_summary[selected_index].state_name}StateManager : BaseStateManager<{initArgName},{updateArgName},{baseStateName}> " + "{");
            if (list_summary[selected_index].list_state.Count > 0)
            {
                writer.WriteLine($"    protected {list_summary[selected_index].state_name}_State_ID state_id = {list_summary[selected_index].state_name}_State_ID.{list_summary[selected_index].list_state[0].name};");
                writer.WriteLine($"    public {list_summary[selected_index].state_name}_State_ID getStateID" + " {" + "get{ return state_id; } }");
            }
            foreach (var data in list_summary[selected_index].manager_column.list_column)
            {
                if (data.type_name == "" || data.value_name == "")
                {
                    continue;
                }

                if (data.length < 0)
                {
                    writer.WriteLine($"    public List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                }
                else if (data.length > 0)
                {
                    writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new {data.type_name}[{data.length}]; //{data.explanation_text}");
                }
                else
                {
                    writer.WriteLine($"    public {data.type_name} {data.value_name}; //{data.explanation_text}");
                }
            }

            writer.WriteLine($"    public override void NextStateChange({initArgName} arg = null)" + "{");
            writer.WriteLine("        switch (state_id)");
            writer.WriteLine("        {");
            int count = 0;

            foreach (var data in list_summary[selected_index].list_state)
            {
                if (count == 0)
                {
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.Begin:");
                    writer.WriteLine($"                state = new {list_summary[selected_index].state_name}{data.name}State(state);");
                    writer.WriteLine($"                state_id = {list_summary[selected_index].state_name}_State_ID.{list_summary[selected_index].list_state[0].name};");
                    writer.WriteLine($"                state.Start(arg);");
                    writer.WriteLine($"                break;");
                }
                ++count;
                if (count >= list_summary[selected_index].list_state.Count)
                {
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.{data.name}:");
                    writer.WriteLine($"                is_finish = true;");
                    writer.WriteLine($"                state_id = {list_summary[selected_index].state_name}_State_ID.Finish;");
                    writer.WriteLine($"                break;");
                }
                else
                {
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.{data.name}:");
                    writer.WriteLine($"                state = new {list_summary[selected_index].state_name}{list_summary[selected_index].list_state[count].name}State(state);");
                    writer.WriteLine($"                state_id = {list_summary[selected_index].state_name}_State_ID.{list_summary[selected_index].list_state[count].name};");
                    writer.WriteLine($"                state.Start(arg);");
                    writer.WriteLine($"                break;");
                }

                


            }
            writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.Finish:");
            writer.WriteLine($"                is_finish = true;");
            writer.WriteLine($"                break;");
            writer.WriteLine($"            default:");
            writer.WriteLine($"                is_finish = true;");
            writer.WriteLine($"                break;");

            writer.WriteLine("        }");
            writer.WriteLine("    }");

            writer.WriteLine($"    public void NextStateChange({list_summary[selected_index].state_name}_State_ID value_state_id,{initArgName} init_arg = null,{updateArgName} updatet_arg = null)" + "{");
            writer.WriteLine($"        state_id = value_state_id;");

            writer.WriteLine("        switch (state_id)");
            writer.WriteLine("        {");


            writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.Begin:");
            writer.WriteLine($"                is_finish = true;");
            writer.WriteLine($"                break;");
            foreach (var data in list_summary[selected_index].list_state)
            {


                writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.{data.name}:");
                writer.WriteLine($"                if(state != null) state.Finish(updatet_arg);");
                writer.WriteLine($"                state = new {list_summary[selected_index].state_name}{data.name}State(state);");
                writer.WriteLine($"                state.Start(init_arg);");
                writer.WriteLine($"                break;");


            }
            writer.WriteLine($"            case {list_summary[selected_index].state_name}_State_ID.Finish:");
            writer.WriteLine($"                break;");
            writer.WriteLine($"            default:");
            writer.WriteLine($"                break;");

            writer.WriteLine("        }");

            writer.WriteLine("    }");

            writer.WriteLine("");
            writer.WriteLine($"    public override void StartInstance()" + " {");
            if(list_summary[selected_index].list_state.Count > 0)
            {
                writer.WriteLine($"        state = new {list_summary[selected_index].state_name}{list_summary[selected_index].list_state[0].name}State();");
            }
            writer.WriteLine("    }");

            writer.WriteLine("}");



        }

        if (File.Exists(filePath) == false)
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                List<string> usingNames = new List<string>();
                {
                    usingNames.Add("System.Collections.Generic");
                    foreach (var data in list_summary[selected_index].manager_column.list_column)
                    {
                        GetUsingType(data.type_name, ref usingNames);
                    }
                    foreach (var name in usingNames)
                    {
                        if (name == "" || name == null || name == "null") continue;
                        writer.WriteLine($"using {name};");
                    }
                }

                writer.WriteLine($"public class {list_summary[selected_index].state_name}StateManager : Base{list_summary[selected_index].state_name}StateManager " + "{");
                writer.WriteLine("");
                writer.WriteLine($"    public override void ManualStart({initArgName} arg = null)" + " {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine($"    public override void ManualUpdate({updateArgName} arg = null)" + " {");
                writer.WriteLine("    }");
                writer.WriteLine("");

                writer.WriteLine("}");

            }
        }


    }

    public void GenerateBranch()
    {
        if (StateBranchSummaryGenerateEditor.instance == null) return;
        var branch_path = Application.dataPath + $"/Project/Script/State/{list_summary[selected_index].state_name}Branch";
        if (System.IO.Directory.Exists(branch_path) == false) System.IO.Directory.CreateDirectory(branch_path);
        GenerateBranchEnum(branch_path);
        GenerateBranchManager(branch_path);
        GenerateStateBranch(branch_path);
    }

    public void GenerateBranchEnum(string path)
    {
        var enum_path = path + $"/{list_summary[selected_index].state_name}BranchID.cs";
        using (StreamWriter writer = new StreamWriter(enum_path))
        {
            writer.WriteLine("using System;");
            writer.WriteLine($"public enum {list_summary[selected_index].state_name}_Branch_State_ID " + "{");
            writer.WriteLine("Begin = -1,");
            Dictionary<string, int> path_name_count = new Dictionary<string, int>();
            foreach (var item in summary_state_branch.state_branch_data)
            {
                Dictionary<int, string> path_name = new Dictionary<int, string>();
                path_name[item.state_branch_id] = item.name;
                int nest_count = 1;
                GenerateEnumBranchName(ref path_name, item,ref nest_count);
                string value_name = "";

                int nest_sub_count = 0;
                if(path_name_count.ContainsKey($"Nest{nest_count:00}_{item.name}") == false)
                {
                    path_name_count[$"Nest{nest_count:00}_{item.name}"] = 1;
                }
                else
                {
                    path_name_count[$"Nest{nest_count:00}_{item.name}"] += 1;
                    nest_sub_count = path_name_count[$"Nest{nest_count:00}_{item.name}"];
                }
                foreach(var name in path_name)
                {
                    value_name = name.Value + value_name;
                }
                if (nest_sub_count == 0) value_name = $"Nest{nest_count:00}_{item.name}";
                else value_name = $"Nest{nest_count:00}_Index{nest_sub_count:00}_{item.name}";
                item.nest_name = value_name;
                writer.WriteLine($"{value_name},");
            }
            writer.WriteLine("Finish");
            writer.WriteLine("}");
        }
    }

    public void GenerateEnumBranchName(ref Dictionary<int, string> path_name,StateBranchSummaryData.StateBranchData state,ref int nest_count)
    {
        if (state.input_state_branch.Count == 0) return;
        foreach(var input in state.input_state_branch)
        {
            if (path_name.ContainsKey(input.node_id) == true) return;
            nest_count += 1;
            path_name[input.node_id] = input.state_name;
            foreach(var state_sub in summary_state_branch.state_branch_data)
            {
                if (state_sub.state_branch_id == input.node_id) GenerateEnumBranchName(ref path_name, state_sub,ref nest_count);
            }
        }
    }
    public void GenerateBranchManager(string path)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;

        string initArgName = $"{list_summary[selected_index].state_name}InitArgData";
        string updateArgName = $"{list_summary[selected_index].state_name}UpdateArgData";
        string baseStateName = $"Base{list_summary[selected_index].state_name}BranchState";

        string fileBasePath = path + $"/Base{list_summary[selected_index].state_name}StateBranchManager.cs";
        string filePath = path + $"/{list_summary[selected_index].state_name}StateBranchManager.cs";

        using (StreamWriter writer = new StreamWriter(fileBasePath))
        {
            List<string> usingNames = new List<string>();
            {
                usingNames.Add("System.Collections.Generic");
                foreach (var data in list_summary[selected_index].manager_column.list_column)
                {
                    // 正規表現にマッチするかを確認
                    GetUsingType(data.type_name, ref usingNames);
                }
                foreach (var name in usingNames)
                {
                    if (name == "" || name == null || name == "null") continue;
                    writer.WriteLine($"using {name};");
                }
            }

            writer.WriteLine($"public class Base{list_summary[selected_index].state_name}StateBranchManager : BaseStateBranchManager<{initArgName},{updateArgName},{baseStateName},{list_summary[selected_index].state_name}_Branch_State_ID > " + "{");
            writer.WriteLine($"    public Base{list_summary[selected_index].state_name}StateBranchManager({list_summary[selected_index].state_name}_Branch_State_ID value):base(value)" + " {");
            writer.WriteLine("    }");
            foreach (var data in list_summary[selected_index].manager_column.list_column)
            {
                if (data.type_name == "" || data.value_name == "")
                {
                    continue;
                }

                if (data.length < 0)
                {
                    writer.WriteLine($"    public List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                }
                else if (data.length > 0)
                {
                    writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new {data.type_name}[{data.length}]; //{data.explanation_text}");
                }
                else
                {
                    writer.WriteLine($"    public {data.type_name} {data.value_name}; //{data.explanation_text}");
                }
            }

            writer.WriteLine($"    public override void NextStateChange({initArgName} arg = null)" + "{");
            writer.WriteLine("        switch (state_id)");
            writer.WriteLine("        {");

            foreach (var data in summary_state_branch.state_branch_data)
            {

                if(data.output_state_branch.Count == 1)
                {
                    string state_name = data.output_state_branch[0].state_name;
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_Branch_State_ID.{data.nest_name}:");
                    foreach(var data_sub in summary_state_branch.state_branch_data)
                    {
                        if(data_sub.state_branch_id == data.output_state_branch[0].node_id)
                        {
                            writer.WriteLine($"                state_id = {list_summary[selected_index].state_name}_Branch_State_ID.{data_sub.nest_name};");
                            break;
                        }
                    }
                    writer.WriteLine($"                state = new {list_summary[selected_index].state_name}{state_name}BranchState(state_id);");
                    writer.WriteLine($"                state.Start(arg);");
                    writer.WriteLine($"                break;");

                }
                else if(data.output_state_branch.Count == 0)
                {
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_Branch_State_ID.{data.nest_name}:");
                    writer.WriteLine($"                is_finish = true;");
                    writer.WriteLine($"                state_id = {list_summary[selected_index].state_name}_Branch_State_ID.Finish;");
                    writer.WriteLine($"                break;");
                }
                else
                {
                    writer.WriteLine($"            case {list_summary[selected_index].state_name}_Branch_State_ID.{data.nest_name}:");
                    writer.WriteLine("                {");
                    writer.WriteLine($"                {list_summary[selected_index].state_name}_Branch_State_ID next_id = state.GenerateNextState();");
                    writer.WriteLine("                 switch (next_id)");
                    writer.WriteLine("                 {");
                    foreach (var output in data.output_state_branch)
                    {
                        foreach (var data_sub in summary_state_branch.state_branch_data)
                        {
                            if (data_sub.state_branch_id == output.node_id)
                            {
                                writer.WriteLine($"                    case {list_summary[selected_index].state_name}_Branch_State_ID.{data_sub.nest_name}:");
                                writer.WriteLine($"                        state_id = {list_summary[selected_index].state_name}_Branch_State_ID.{data_sub.nest_name};");
                                writer.WriteLine($"                        state = new {list_summary[selected_index].state_name}{data_sub.name}BranchState(state_id);");
                                writer.WriteLine($"                        state.Start(arg);");
                                writer.WriteLine($"                        break;");
                                break;
                            }
                        }
                    }
                    writer.WriteLine("                 }");
                    writer.WriteLine("                }");
                    writer.WriteLine($"                break;");
                }




            }
            writer.WriteLine("             }");
            writer.WriteLine("    }");


            writer.WriteLine("");
            writer.WriteLine($"    public override void StartInstance()" + " {");
            if (list_summary[selected_index].list_state.Count > 0)
            {
                writer.WriteLine($"        state = new {list_summary[selected_index].state_name}{StartBranchState().name}BranchState({list_summary[selected_index].state_name}_Branch_State_ID.{StartBranchState().nest_name});");
            }
            writer.WriteLine("    }");

            writer.WriteLine("}");



        }

        if (File.Exists(filePath) == false)
        {
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                List<string> usingNames = new List<string>();
                {
                    usingNames.Add("System.Collections.Generic");
                    foreach (var data in list_summary[selected_index].manager_column.list_column)
                    {
                        GetUsingType(data.type_name, ref usingNames);
                    }
                    foreach (var name in usingNames)
                    {
                        if (name == "" || name == null || name == "null") continue;
                        writer.WriteLine($"using {name};");
                    }
                }

                writer.WriteLine($"public class {list_summary[selected_index].state_name}StateBranchManager : Base{list_summary[selected_index].state_name}StateBranchManager " + "{");
                writer.WriteLine("");
                writer.WriteLine($"    public {list_summary[selected_index].state_name}StateBranchManager():base({list_summary[selected_index].state_name}_Branch_State_ID.{StartBranchState().nest_name})" + " {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine($"    public override void ManualStart({initArgName} arg = null)" + " {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine($"    public override void ManualUpdate({updateArgName} arg = null)" + " {");
                writer.WriteLine("    }");
                writer.WriteLine("");

                writer.WriteLine("}");

            }
        }
    }

    public void GenerateStateBranch(string path)
    {
        if (selected_index < 0) return;
        if (list_summary[selected_index].list_state.Count <= 0) return;

        //基礎Stateクラス作成
        string fileBaseStatePath = path + $"/Base{list_summary[selected_index].state_name}BranchState.cs";
        using (StreamWriter writer = new StreamWriter(fileBaseStatePath))
        {
            List<string> usingNames = new List<string>();
            usingNames.Add("System.Collections.Generic");
            foreach (var data in list_summary[selected_index].base_state_column.list_column)
            {
                // 正規表現にマッチするかを確認
                GetUsingType(data.type_name, ref usingNames);
            }
            foreach (var name in usingNames)
            {
                if (name == "" || name == null || name == "null") continue;
                writer.WriteLine($"using {name};");
            }
            writer.WriteLine($"public class Base{list_summary[selected_index].state_name}BranchState : BaseBranchState<{list_summary[selected_index].state_name}InitArgData,{list_summary[selected_index].state_name}UpdateArgData,{list_summary[selected_index].state_name}_Branch_State_ID>" + "{");
            foreach (var data in list_summary[selected_index].base_state_column.list_column)
            {
                if (data.type_name == "" || data.value_name == "")
                {
                    continue;
                }

                if (data.length < 0)
                {
                    writer.WriteLine($"    public List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                }
                else if (data.length > 0)
                {
                    writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new  {data.type_name} [  {data.length}]; //{data.explanation_text}");
                }
                else
                {
                    writer.WriteLine($"    public {data.type_name} {data.value_name}; //{data.explanation_text}");
                }
            }
            writer.WriteLine($"    public Base{list_summary[selected_index].state_name}BranchState({list_summary[selected_index].state_name}_Branch_State_ID other) : base(other)" + "{}");

            writer.WriteLine("    }");
        }

        foreach (var state in list_summary[selected_index].list_state)
        {
            List<StateBranchSummaryData.StateBranchData> list_branch_data = new List<StateBranchSummaryData.StateBranchData>();
            ///同じステートを取得
            List<string> state_list = new List<string>();
            foreach (var data_sub in summary_state_branch.state_branch_data)
            {

                if(data_sub.name == state.name && data_sub.output_state_branch.Count > 1)
                {
                    state_list.Add(data_sub.nest_name);
                    list_branch_data.Add(data_sub);
                }
            }
            string fileBasePath = path + $"/Base{list_summary[selected_index].state_name}{state.name}BranchState.cs";
            string filePath = path + $"/{list_summary[selected_index].state_name}{state.name}BranchState.cs";

            using (StreamWriter writer = new StreamWriter(fileBasePath))
            {


                List<string> usingNames = new List<string>();
                usingNames.Add("System.Collections.Generic");
                foreach (var data in state.list_column)
                {
                    // 正規表現にマッチするかを確認
                    GetUsingType(data.type_name, ref usingNames);
                }
                foreach (var name in usingNames)
                {
                    if (name == "" || name == null || name == "null") continue;
                    writer.WriteLine($"using {name};");
                }


                writer.WriteLine($"public class Base{list_summary[selected_index].state_name}{state.name}BranchState : Base{list_summary[selected_index].state_name}BranchState" + "{");

                foreach (var data in state.list_column)
                {
                    if (data.type_name == "" || data.value_name == "")
                    {
                        continue;
                    }

                    if (data.length < 0)
                    {
                        writer.WriteLine($"    protected List<{data.type_name}> {data.value_name} = new List<{data.type_name}>(); //{data.explanation_text}");
                    }
                    else if (data.length > 0)
                    {
                        writer.WriteLine($"    public {data.type_name}[] {data.value_name} = new {data.type_name}[{data.length}]; //{data.explanation_text}");
                    }
                    else
                    {
                        writer.WriteLine($"    protected {data.type_name} {data.value_name}; //{data.explanation_text}");
                    }
                }
                writer.WriteLine($"    public Base{list_summary[selected_index].state_name}{state.name}BranchState({list_summary[selected_index].state_name}_Branch_State_ID other):base(other)" + "{}");


                List<string> func_list = new List<string>();
                string first_id = "";
                foreach (var output in list_branch_data)
                {
                    writer.WriteLine($"    public virtual {list_summary[selected_index].state_name}_Branch_State_ID Branch{output.nest_name}State()" + "{");
                    bool start_flag = true;
                    foreach (var output_sub in output.output_state_branch)
                    {
                        foreach (var parent in summary_state_branch.state_branch_data)
                        {
                            if (parent.state_branch_id == output_sub.node_id)
                            {
                                func_list.Add($"Branch{output.nest_name}To{output_sub.state_name}()");
                                if (start_flag == true)
                                {
                                    first_id = $"{list_summary[selected_index].state_name}_Branch_State_ID.{parent.nest_name}";
                                    writer.WriteLine($"        if(Branch{output.nest_name}To{output_sub.state_name}())" + $" return {list_summary[selected_index].state_name}_Branch_State_ID.{parent.nest_name};");
                                    start_flag = false;
                                }
                                else
                                {
                                    writer.WriteLine($"        else if(Branch{output.nest_name}To{output_sub.state_name}())" + $" return {list_summary[selected_index].state_name}_Branch_State_ID.{parent.nest_name};");
                                }
                                
                            }
                        }
                    }
                    writer.WriteLine($"     return {first_id};");
                    writer.WriteLine("     }");
                }

                //関数作成
                foreach(var func_name in func_list)
                {
                    writer.WriteLine($"    public virtual bool {func_name}" + "{return true;}");
                }
                if (state_list.Count > 0)
                {
                    //分岐関数を作る
                    writer.WriteLine($"    public override {list_summary[selected_index].state_name}_Branch_State_ID GenerateNextState()" + "{");
                    writer.WriteLine("        switch (state_id)");
                    writer.WriteLine("        {");

                    int count = 0;
                    foreach (var id in state_list)
                    {
                        writer.WriteLine($"            case {list_summary[selected_index].state_name}_Branch_State_ID.{id}:");
                        writer.WriteLine($"                return Branch{list_branch_data[count].nest_name}State();");
                        ++count;
                    }
                    writer.WriteLine("        }");
                    writer.WriteLine($"     return {first_id};");
                    writer.WriteLine("     }");


                }
                writer.WriteLine("}");
            }

            if (File.Exists(filePath) == false)
            {
                using (StreamWriter writer = new StreamWriter(filePath))
                {

                    List<string> usingNames = new List<string>();
                    usingNames.Add("System.Collections.Generic");
                    foreach (var data in state.list_column)
                    {
                        // 正規表現にマッチするかを確認
                        GetUsingType(data.type_name, ref usingNames);
                    }
                    foreach (var name in usingNames)
                    {
                        if (name == "" || name == null || name == "null") continue;
                        writer.WriteLine($"using {name};");
                    }
                    writer.WriteLine($"public class {list_summary[selected_index].state_name}{state.name}BranchState : Base{list_summary[selected_index].state_name}{state.name}BranchState " + "{");


                    writer.WriteLine($"    public override void Start({list_summary[selected_index].state_name}InitArgData arg)" + " {");
                    writer.WriteLine("    }");
                    writer.WriteLine($"    public override void Update({list_summary[selected_index].state_name}UpdateArgData arg)" + " {");
                    writer.WriteLine("    }");
                    writer.WriteLine($"    public override void Finish({list_summary[selected_index].state_name}UpdateArgData arg)" + " {");
                    writer.WriteLine("    }");

                    writer.WriteLine($"    public {list_summary[selected_index].state_name}{state.name}BranchState({list_summary[selected_index].state_name}_Branch_State_ID other):base(other)" + "{}");

                    writer.WriteLine("}");
                }
            }
        }
    }

    public StateBranchSummaryData.StateBranchData StartBranchState()
    {
        foreach(var data_sub in summary_state_branch.state_branch_data)
        {
            if (data_sub.input_state_branch.Count == 0 && data_sub.output_state_branch.Count > 0) return data_sub;
        }

        return null;
    }



    public void Generate()
    {
        string path = Application.dataPath + $"/Project/Script/State/{list_summary[selected_index].state_name}";
        string branch_file = Application.dataPath + "/Project/Assets/Editor/Data";
        if (Directory.Exists(path) == false)
        {
            Directory.CreateDirectory(path);
        }
        if(Directory.Exists(branch_file + $"/Branch") == false)
        {
            Directory.CreateDirectory(branch_file + $"/Branch");
        }

        //ブランチデータ
        string branch_path = branch_file + $"/Branch/{list_summary[selected_index].state_name}_Branch.json";
        string jsonstr = JsonUtility.ToJson(summary_state_branch);
        StreamWriter writer = new StreamWriter(branch_path, false);//初めに指定したデータの保存先を開く
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();//バッファをクリアする
        writer.Close();//ファイルをクローズする

        GenerateEnumFile(path);
        GenerateArgFile(path, "Init", list_summary[selected_index].initialize_arg);
        GenerateArgFile(path, "Update", list_summary[selected_index].update_arg);
        GenerateState(path);
        GenerateManegerState(path);

        GenerateBranch();
        UnityEditor.AssetDatabase.Refresh();

    }

    private void OnDestroy()
    {
        if(StateBranchSummaryGenerateEditor.instance != null) StateBranchSummaryGenerateEditor.instance.Close();
        // ウィンドウを作成または表示します
        StateBranchSummaryGenerateEditor.instance = null;
    }
    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal(GUILayout.Height(280.0f));

        {
            // 左側のGUIを表示
            EditorGUILayout.BeginVertical(GUILayout.Width(position.width * 0.2f));

            GUILayout.Label("Left Pane", EditorStyles.boldLabel);

            if (GUILayout.Button("Save"))
            {
                if(selected_index > -1)
                {
                    SaveState();
                }
            }


            if (GUILayout.Button("Generate"))
            {
                if (selected_index > -1)
                {
                    Generate();
                }
            }

            if(GUILayout.Button("BranchFragMake"))
            {
                if (selected_index >= 0)
                {
                    InitBranchCreate();
                    // ウィンドウを作成または表示します
                    StateBranchSummaryGenerateEditor.instance = EditorWindow.GetWindow(typeof(StateBranchSummaryGenerateEditor)) as StateBranchSummaryGenerateEditor;
                }
            }

            // ここに左側のGUI要素を追加します
            text_state_generate_name = EditorGUILayout.TextField("State Name", text_state_generate_name);

            if (GUILayout.Button("Add State"))
            {
                if (!string.IsNullOrEmpty(text_state_generate_name))
                {
                    selected_module_index = -1;
                    AddData(text_state_generate_name);
                }
            }

            left_scroll = EditorGUILayout.BeginScrollView(left_scroll);
            for (int i = 0; i < list_summary.Count; i++)
            {
                GUILayout.BeginHorizontal();
                // リストアイテムをクリックできるラベルに変更
                if (GUILayout.Button(list_summary[i].state_name,GUILayout.Width(position.width * 0.15f)))
                {
                    selected_index = i;
                }
                //削除
                if (GUILayout.Button("×", EditorStyles.miniButtonRight))
                {
                    DeleteData(i);
                }
                GUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();



            EditorGUILayout.EndVertical();
        }
        //初期化、更新関数の引数
        {
            if (selected_index > -1)
            {
                // 左側のGUIを表示
                EditorGUILayout.BeginVertical(GUILayout.Width(position.width * 0.35f));
                

                {
                    EditorGUILayout.BeginHorizontal(GUILayout.Height(120.0f));
                    ColumnDataGUI(ref list_summary[selected_index].initialize_arg, "初期",ref left_init_scroll);
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.BeginHorizontal(GUILayout.Height(120.0f));
                    ColumnDataGUI(ref list_summary[selected_index].update_arg, "更新", ref left_update_scroll);
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndVertical();
            }
        }
        //管理引数
        {
            if (selected_index > -1)
            {
                // 左側のGUIを表示
                EditorGUILayout.BeginVertical(GUILayout.Width(position.width * 0.35f));


                {
                    ColumnDataGUI(ref list_summary[selected_index].manager_column, "管理", ref left_manager_scroll);
                }
                EditorGUILayout.EndVertical();
            }
        }
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        {
            if(selected_index > -1)
            {
                // 左側のGUIを表示
                EditorGUILayout.BeginVertical(GUILayout.Width(position.width * 0.25f));

                ColumnDataGUI(ref list_summary[selected_index].base_state_column, "基礎", ref left_base_state_scroll);

                EditorGUILayout.EndVertical();
            }
        }
        {
            if (selected_index > -1)
            {
                // 左側のGUIを表示
                EditorGUILayout.BeginVertical(GUILayout.Width(position.width * 0.35f));

                GUILayout.Label("State Module", EditorStyles.boldLabel);

                // ここに左側のGUI要素を追加します
                text_state_module_generate_name = EditorGUILayout.TextField("State Name", text_state_module_generate_name);

                if (GUILayout.Button("Add State Module"))
                {
                    if (!string.IsNullOrEmpty(text_state_module_generate_name))
                    {
                        AddModuleData(text_state_module_generate_name);
                    }
                }

                left_module_scroll = EditorGUILayout.BeginScrollView(left_module_scroll);
                for (int i = 0; i < list_summary[selected_index].list_state.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    // リストアイテムをクリックできるラベルに変更
                    if (GUILayout.Button("↑"))
                    {
                        MoveModuleData(i, -1);
                    }
                    // リストアイテムをクリックできるラベルに変更
                    if (GUILayout.Button(list_summary[selected_index].list_state[i].name, GUILayout.Width(position.width * 0.25f)))
                    {
                        selected_module_index = i;
                    }

                    // リストアイテムをクリックできるラベルに変更
                    if (GUILayout.Button("↓"))
                    {
                        MoveModuleData(i, 1);
                    }

                    //削除
                    if (GUILayout.Button("×", EditorStyles.miniButtonRight))
                    {
                        DeleteModuleData(i);
                    }
                    GUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();





                EditorGUILayout.EndVertical();
            }
        }

        {

            if (selected_module_index > -1)
            {
                // 左側のGUIを表示
                EditorGUILayout.BeginVertical();

                GUILayout.BeginHorizontal();
                GUILayout.Label("変数の型", GUILayout.Width(100));
                GUILayout.Label("変数名", GUILayout.Width(150));
                GUILayout.Label("配列数", GUILayout.Width(50));
                GUILayout.Label("説明", GUILayout.Width(250));
                GUILayout.EndHorizontal();


                left_module_column_scroll = EditorGUILayout.BeginScrollView(left_module_column_scroll);
                for (int i = 0; i < list_summary[selected_index].list_state[selected_module_index].list_column.Count; i++)
                {
                    GUILayout.BeginHorizontal();

                    list_summary[selected_index].list_state[selected_module_index].list_column[i].type_name = EditorGUILayout.TextField("", list_summary[selected_index].list_state[selected_module_index].list_column[i].type_name, GUILayout.Width(100));

                    list_summary[selected_index].list_state[selected_module_index].list_column[i].value_name = EditorGUILayout.TextField("", list_summary[selected_index].list_state[selected_module_index].list_column[i].value_name, GUILayout.Width(150));

                    list_summary[selected_index].list_state[selected_module_index].list_column[i].length = EditorGUILayout.IntField("", list_summary[selected_index].list_state[selected_module_index].list_column[i].length, GUILayout.Width(50));

                    list_summary[selected_index].list_state[selected_module_index].list_column[i].explanation_text = EditorGUILayout.TextField("", list_summary[selected_index].list_state[selected_module_index].list_column[i].explanation_text, GUILayout.Width(250));

                    if (GUILayout.Button("×", GUILayout.Width(100)))
                    {
                        DeleteModuleColumnData(i);
                    }

                    GUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Add", GUILayout.Width(100)))
                {
                    AddModuleColumn();
                }
                if (GUILayout.Button("Save", GUILayout.Width(100)))
                {
                    SaveState();
                }
                GUILayout.EndHorizontal();



                EditorGUILayout.EndVertical();
            }
        }
        GUILayout.EndHorizontal();


    }


}
