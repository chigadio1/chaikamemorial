using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using UnityEngine;

public class TestJson : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string json = "{\"page\": 1,\"per_page\": 6,\"total\": 12,\"total_pages\":14,\"data\":[{\"id\": 1,\"name\": \"cerulean\",\"year\": 2000,\"color\": \"#98B2D1\",\"pantone_value\": \"15-4020\"},{\"id\": 2,\"name\": \"fuchsia rose\",\"year\": 2001,\"color\": \"#C74375\",\"pantone_value\": \"17-2031\"},{\"id\": 3,\"name\": \"true red\",\"year\": 2002,\"color\": \"#BF1932\",\"pantone_value\": \"19-1664\"},{\"id\": 4,\"name\": \"aqua sky\",\"year\": 2003,\"color\": \"#7BC4C4\",\"pantone_value\": \"14-4811\"},{\"id\": 5,\"year\": 2004,\"color\": \"#E2583E\",\"pantone_value\": \"17-1456\"},{\"id\": 6,\"name\": \"blue turquoise\",\"year\": 2005,\"color\": \"#53B0AE\",\"pantone_value\": \"15-5217\"}]}";

        // JSONデータをパース
        JsonDocument doc = JsonDocument.Parse(json);

        // キーと値を抽出して表示
        PrintKeyValuePairs(doc.RootElement, "");

        doc.Dispose();
    }

    // Update is called once per frame
    void Update()
    {
        Dictionary<int, List<List<Dictionary<string, object>>>> var = new Dictionary<int, List<List<Dictionary<string, object>>>>();
    }

    static void PrintKeyValuePairs(JsonElement element, string prefix)
    {
        if (element.ValueKind == JsonValueKind.Object)
        {
            foreach (var property in element.EnumerateObject())
            {
                string newPrefix = prefix == "" ? property.Name : $"{prefix}.{property.Name}";
                PrintKeyValuePairs(property.Value, newPrefix);
            }
        }
        else if (element.ValueKind == JsonValueKind.Array)
        {
            for (int i = 0; i < element.GetArrayLength(); i++)
            {
                string newPrefix = $"{prefix}[{i}]";
                PrintKeyValuePairs(element[i], newPrefix);
            }
        }
    }
}
