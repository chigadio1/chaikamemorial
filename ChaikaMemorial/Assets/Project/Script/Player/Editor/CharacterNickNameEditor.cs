using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CharacterNickNameEditor : EditorWindow
{
    public static CharacterNickNameEditor instance = null;

    int selectIndex = -1;
    CharacterNickNameStorage storage = new CharacterNickNameStorage();


    [MenuItem("Custom/キャラニックネームD")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            return;
        }
        // ウィンドウを作成または表示します
        CharacterNickNameEditor.instance = EditorWindow.GetWindow(typeof(CharacterNickNameEditor)) as CharacterNickNameEditor;



    }
    public void OnGUI()
    {
        using (new GUILayout.HorizontalScope(GUILayout.Width(1000)))
        {
            using (new GUILayout.VerticalScope(GUILayout.Width(200)))
            {
                for (int count = 0; count < (int)Character_ID.MAX;)
                {
                    Character_ID id = (Character_ID)count;

                    GUI.backgroundColor = selectIndex == count ? Color.cyan : Color.white;
                    if (GUILayout.Button($"{Enum.GetName(typeof(Character_ID), id)}"))
                    {
                        selectIndex = count;
                    }
                    GUI.backgroundColor = Color.white;

                    ++count;
                }
            }
            using (new GUILayout.VerticalScope(GUILayout.Width(500)))
            {
                if (selectIndex >= 0)
                {
                    storage.OnGUI((Character_ID)selectIndex);
                }
            }
        }
        using (new GUILayout.HorizontalScope(GUILayout.Width(100)))
        {
            if (GUILayout.Button("Save"))
            {
                storage.Save();
            }
            if (GUILayout.Button("Load"))
            {
                storage.Load();
            }
            if (GUILayout.Button("Init"))
            {
                storage.Init();
            }
        }
    }
}
