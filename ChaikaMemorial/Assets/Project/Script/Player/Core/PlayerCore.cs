using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using UnityEngine;
using System.Text.RegularExpressions;
using static EventManagerData;

public class PlayerCore : MonoBehaviour
{

    /// <summary>
    /// シングルトン
    /// </summary>
    private static PlayerCore instace;
    public static PlayerCore Instance
    {
        get
        {
            if (instace == null)
            {
                var obj = GameObject.Find("PlayerCore");
                if (obj != null)
                {
                    instace = obj.GetComponent<PlayerCore>();
                }
                else
                {
                    GameObject game = new GameObject();
                    game.name = "PlayerCore";
                    instace = game.AddComponent<PlayerCore>();
                }
            }
            return instace;
        }
    }

    /// <summary>
    /// コアデータ
    /// </summary>
    [SerializeField]
    private PlayerCoreData player_core_data;
    public PlayerCoreData GetPlayerCoreData { get { return player_core_data; } }

    private EventCalenderCoreData calender_core_data;
    public EventCalenderCoreData EventCalenderCoreData { get { return calender_core_data; } }

    private EventManagerData event_manager_data = new EventManagerData();

    /// <summary>
    /// ニックネームデータ
    /// </summary>
    private CharacterNickNameStorage character_nick_name_storage;
    public CharacterNickNameStorage CharacterNickNameStorage { get { return character_nick_name_storage;} }

    /// <summary>
    /// デートイベントストレージ
    /// </summary>
    private DateEventStorage date_event_storage = new DateEventStorage();
    public DateEventStorage DateEventStorage { get { return date_event_storage;} }

    //汎用イベントストレージ
    private GenericEventStorage generic_event_storage = new GenericEventStorage(); 
    public GenericEventStorage GenericEventStorage { get { return generic_event_storage;} }

    [SerializeField]
    private bool is_date = false;
    public bool IsDate { get { return is_date;} }
    public void SetIsDate(bool valu) { is_date = valu; }

    /// <summary>
    /// 一時保存イベントID
    /// </summary>
    public int event_id = -1;
    /// <summary>
    /// 次回のイベントも設定されているか
    /// </summary>
    public bool is_next_event = false;

    public FindEventData FindID()
    {
        return event_manager_data.FindID(player_core_data);
    }

    private bool is_load_finish = false;
    public bool GetIsLoadFinish { get { return is_load_finish; } }

    public int player_save_id = 1;

    public bool is_button_push = false;
    public bool is_button_cancel_push = false;


    /// <summary>
    /// 計算結果
    /// </summary>
    private StatusCalculatorResult status_calculator_result = new StatusCalculatorResult();
    public StatusCalculatorResult StatusCalculatorResult { get { return status_calculator_result; } }
    public Status_ID select_status_id = Status_ID.NONE;
    // Start is called before the first frame update
    void Start()
    {
        player_core_data.GetPlayerStatusData.AllClearSelectAction();

        calender_core_data = new EventCalenderCoreData();
        calender_core_data.Load();
        character_nick_name_storage = new CharacterNickNameStorage();
        character_nick_name_storage.Load();

        event_manager_data.LoadBinaryAsync();
        date_event_storage.LoadStorage();
        generic_event_storage.LoadStorage();

        if (player_core_data.GetDictCharacterNickName.Count < (int)Character_ID.MAX)
        {
            for (int count = 0; count < (int)Character_ID.MAX;)
            {
                player_core_data.AddValueCharacterNickName((Character_ID)count, Character_NickName_ID.Nick_Name_01);
                ++count;
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
       if(event_manager_data.GetIsLoadEnd)
        {
            is_load_finish = true;
        }
    }

    private void OnDestroy()
    {

    }

    public PlayerCoreData GetLoadData(int select_id)
    {
        int id = select_id;
        string path = Application.dataPath + "/PlayerData";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        path += $"/playerCore{id:00}.bin";
        if (System.IO.File.Exists(path) == false)
        {
            event_manager_data.LoadBinaryAsync();
            return null;
        }
        //binaryファイルからJsonに変換
        byte[] jsonBytes = File.ReadAllBytes(path);
        string jsonData = Encoding.UTF8.GetString(jsonBytes);

        var return_player_core_data = JsonUtility.FromJson<PlayerCoreData>(jsonData);
        return return_player_core_data;
    }

    public void LoadData()
    {
        int id = player_save_id;
        string path = Application.dataPath + "/PlayerData";
        if(System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        path += $"/playerCore{id:00}.bin";
        if(System.IO.File.Exists(path) == false)
        {
            event_manager_data.LoadBinaryAsync();
            return;
        }

        //binaryファイルからJsonに変換
        byte[] jsonBytes = File.ReadAllBytes(path);
        string jsonData = Encoding.UTF8.GetString(jsonBytes);

        player_core_data = JsonUtility.FromJson<PlayerCoreData>(jsonData);
        player_core_data.JsonReader();
        //ロードしたら、チャイカだけのアンロックを解除
        GetPlayerCoreData.GetCharacterUnlockData.On(Character_ID.Chaika);
    }
    public void DeleteData()
    {

        player_core_data.ResetAll();

        SaveData();
    }

    public bool IsSaveData()
    {
        int id = player_save_id;
        string path = Application.dataPath + "/PlayerData";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        path += $"/playerCore{id:00}.bin";
        if (System.IO.File.Exists(path) == false)
        {
            return false;
        }

        return true;
    }

    public bool IsSaveData(int id)
    {

        string path = Application.dataPath + "/PlayerData";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }

        path += $"/playerCore{id:00}.bin";
        if (System.IO.File.Exists(path) == false)
        {
            return false;
        }

        return true;
    }


    public void SaveData()
    {
        int id = player_save_id;
        string path = Application.dataPath + "/PlayerData";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }
        path += $"/playerCore{id:00}.bin";

        player_core_data.JsonWriter();
        if (player_core_data.GetDictCharacterNickName.Count < (int)Character_ID.MAX)
        {
            for(int count = 0; count < (int)Character_ID.MAX;)
            {
                player_core_data.AddValueCharacterNickName((Character_ID)count, Character_NickName_ID.Nick_Name_01);
                ++count;
            }
        }

        // PlayerCoreDataをJSONに変換
        string jsonData = JsonUtility.ToJson(player_core_data);

        // JSONデータをバイト配列に変換
        byte[] jsonBytes = Encoding.UTF8.GetBytes(jsonData);

        // バイト配列をファイルに書き込み
        File.WriteAllBytes(path, jsonBytes);
    }

    /// <summary>
    /// 名前変更
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public string ConvertName(string name)
    {
        string res = name;
        string pattern = @"\{主人公\}";

        bool isMatch = Regex.IsMatch(name, pattern);
        if (isMatch) return player_core_data.GetPlayerStatusData?.PlayerInputData?.SurnName + " " + player_core_data.GetPlayerStatusData?.PlayerInputData?.Name;

        return res;
    }

    public string ConvertText(string text)
    {
        string res = text;
        string pattern = @"\{Chara(\d+)\}";

        string replacedText = Regex.Replace(res, pattern, match =>
        {
            int charaNumber = int.Parse(match.Groups[1].Value);
            Character_ID character_id = (Character_ID)charaNumber;
            return ConvertCharacterName(character_id);

        });

        pattern = @"\{ToChara(\d+)\}";

        replacedText = Regex.Replace(replacedText, pattern, match =>
        {
            int charaNumber = int.Parse(match.Groups[1].Value);
            Character_ID character_id = (Character_ID)charaNumber;
            return PlayerCore.Instance.GetPlayerToCharacterNickName(character_id);

        });

        pattern = @"\{DateSpot\}";
        replacedText = Regex.Replace(replacedText, pattern, DateSpotIDConvert.GetJapaneseName(gameUiCore.Instance.SelectDateSpotID));

        pattern = @"\{一人称\}";
        replacedText = Regex.Replace(replacedText, pattern, PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.PlayerInputData.CurentUserText);

        return replacedText;
    }

    private string ConvertCharacterName(Character_ID character_id)
    {
        int favo = player_core_data.GetCharacterFavorabilityData.GetFavorability(character_id);

        if(favo < 50)
        {
            return player_core_data.GetPlayerStatusData.PlayerInputData.SurnName;
        }
        else if(favo >= 50 && favo <= 100)
        {
            return player_core_data.GetPlayerStatusData.PlayerInputData.Name;
        }
        else
        {
            return player_core_data.GetPlayerStatusData.PlayerInputData.NickName;
        }
    }

    public string GetPlayerToCharacterNickName(Character_ID id)
    {
        if (GetPlayerCoreData.GetDictCharacterNickName.ContainsKey(id) == false) return "";
        var data =  GetPlayerCoreData.GetDictCharacterNickName[id];
        var stock = character_nick_name_storage.GetNickNameDataStock(id);
        return stock.GetNickName(data);
    }


}
