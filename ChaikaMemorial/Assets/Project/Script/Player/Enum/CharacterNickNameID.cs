using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーが呼ぶキャラクターのあだ名
/// </summary>
public enum Character_NickName_ID
{
    NONE = -1,
    Nick_Name_01,
    Nick_Name_02,
    Nick_Name_03,
    Nick_Name_04,
    Nick_Name_05,
    Nick_Name_06,
    Nick_Name_07,
    Nick_Name_08,
    Nick_Name_09,

    MAX
}
