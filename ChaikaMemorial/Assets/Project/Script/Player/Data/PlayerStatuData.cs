using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤー入力データ(名前、あだ名、誕生日など)
/// </summary>
[System.Serializable]
public class PlayerInputData
{
    [SerializeField]
    private string surn_name = "";//姓名
    public string SurnName { get { return surn_name; } }

    [SerializeField]
    private string name = "";//名前
    public string Name { get { return name; } }

    [SerializeField]
    private string nick_name = "";//あだ名
    public string NickName { get { return nick_name; } }

    [SerializeField]
    private int birt_month = 0;//誕生月
    public int BirtMonth { get { return birt_month; } }

    [SerializeField]
    private int birt_day = 0;//誕生日
    public int BirtDay { get { return birt_day; } }

    [SerializeField] //一人称
    private string current_user_text = "私";
    public string CurentUserText { get { return current_user_text; } }

    public void SetValue(string value_surn_name,string value_name,string value_nick_name,int value_birt_month,int value_birt_day,string value_current_user_text)
    {
        surn_name = value_surn_name;
        name = value_name;
        nick_name = value_nick_name;
        birt_month = value_birt_month;
        birt_day = value_birt_day;
        current_user_text =value_current_user_text;
    }
}

/// <summary>
/// プレイヤーステータスデータ
/// </summary>
[System.Serializable]
public class PlayerStatuData
{
    /// <summary>
    /// ターン数
    /// </summary>
    [SerializeField]
    private int num_turn = 0;
    public int GetTurn { get { return num_turn; } }

    [SerializeField]
    private int fix_turn_add = 0;

    public int GetFixTurn { get { return fix_turn_add; } }

    public int GetNumFixTurn { get { return fix_turn_add + num_turn; } }

    /// <summary>
    /// 最大ターン数
    /// </summary>
    private const int MAX_NUM_TURN = 999;

    /// <summary>
    /// 最大ターン数
    /// </summary>
    private const int MIN_NUM_TURN = 0;

    /// <summary>
    /// 最大ステータス
    /// </summary>
    private const int MAX_STATUS_PARAM = 9999;
    /// <summary>
    /// 最小ステータス
    /// </summary>
    private const int MIN_STATUS_PARAM = 0;
    /// <summary>
    /// ステータス配列
    /// </summary>
    [SerializeField]
    private string status_array_text = "";
    private Dictionary<Status_ID,int> status_array = new Dictionary<Status_ID, int>();
    public Dictionary<Status_ID,int> StatusArray { get { return status_array; } }
    /// <summary>
    /// 行動選択
    /// </summary>
    [SerializeField]
    private Status_ID[] select_action_array = new Status_ID[5];

    [SerializeField]
    private PlayerInputData input_data = new PlayerInputData();
    public PlayerInputData PlayerInputData { get { return input_data; } }

    public void JsonWriter()
    {
        if (status_array == null) status_array = new Dictionary<Status_ID, int>();
        status_array_text = JsonConvert.SerializeObject(status_array);
    }

    public void JsonReader()
    {
        if (status_array == null) status_array = new Dictionary<Status_ID, int>();
        status_array  = JsonConvert.DeserializeObject<Dictionary<Status_ID, int>>(status_array_text);
    }



    public void SetSelectAction(int index,Status_ID id)
    {
        if (index >= 5) return;
        select_action_array[index] = id;
    }

    public void AllClearSelectAction()
    {
        for(int count =0; count < (int)Status_ID.MAX;)
        {
            select_action_array[count] = Status_ID.NONE;

            ++count;
        }
    }

    public Status_ID GetSelectionAction(int index)
    {
        if (index >= 5) return Status_ID.NONE;
        return select_action_array[index];
    }

    /// <summary>
    /// ステータス取得
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int GetStatus(Status_ID id)
    {
        if (id == Status_ID.MAX) return MIN_STATUS_PARAM;
        if(status_array.ContainsKey(id) == false)
        {
            status_array[id] = 0;
        }
        return status_array[id];
    }

    /// <summary>
    /// ステータスセット
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    public void SetStatus(Status_ID id,int value)
    {
        if (id == Status_ID.MAX) return;
        if (status_array.ContainsKey(id) == false)
        {
            status_array[id] = 0;
        }
        status_array[id] = value;
        status_array[id] = Mathf.Clamp(status_array[id], MIN_STATUS_PARAM, MAX_STATUS_PARAM);
    }

    /// <summary>
    /// ステータス追加セット
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    public void AddStatus(Status_ID id, int value)
    {
        if (id == Status_ID.MAX) return;
        if (status_array.ContainsKey(id) == false)
        {
            status_array[id] = 0;
        }
        status_array[id] += value;
        status_array[id] = Mathf.Clamp(status_array[id], MIN_STATUS_PARAM, MAX_STATUS_PARAM);
    }

    /// <summary>
    /// 計算ステータス
    /// </summary>
    /// <param name="id"></param>
    /// <param name="calc"></param>
    public void CalcStatus(Status_ID id,int calc)
    {
        if (id == Status_ID.MAX) return;
        if (status_array.ContainsKey(id) == false)
        {
            status_array[id] = 0;
        }
        status_array[id] += calc;
        status_array[id] = Mathf.Clamp(status_array[id],MIN_STATUS_PARAM, MAX_STATUS_PARAM);

    }

    /// <summary>
    /// ターン経過
    /// </summary>
    /// <param name="add"></param>
    public void AddNumTurn(int add = 1)
    {
        fix_turn_add += add;
        fix_turn_add = Mathf.Clamp(fix_turn_add, 0, MAX_NUM_TURN);
    }

    public void FixTurn()
    {
        num_turn += fix_turn_add;
        fix_turn_add = 0;
    }

    public Event_Years_ID GetYearID()
    {
        return (Event_Years_ID)((((num_turn / 4) % 12) / 12));
    }

    public Event_Months_ID GetMonthID()
    {
        return (Event_Months_ID)((num_turn / 4) % 12);
    }

    public Event_Weeks_ID GetWeekID()
    {
        return (Event_Weeks_ID)(num_turn % 4);
    }

    public int GetWeek()
    {
        return num_turn % 4;
    }

    public void SetValuePlayerInput(string value_surn_name, string value_name, string value_nick_name, int value_birt_month, int value_birt_day, string value_current_user_text)
    {
        if (input_data == null) input_data = new PlayerInputData();
        input_data.SetValue(value_surn_name, value_name, value_nick_name, value_birt_month, value_birt_day,value_current_user_text);
    }
}
