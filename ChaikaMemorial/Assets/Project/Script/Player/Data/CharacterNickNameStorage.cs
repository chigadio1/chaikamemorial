using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// キャラニックネーム保存
/// </summary>
public class CharacterNickNameStorage
{

    private CharacterNickName[] character_nick_name_array = new CharacterNickName[(int)Character_ID.MAX];
    public NickName GetNickNameData(Character_ID id,Character_NickName_ID nick_id,int favo)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return null;
        if (character_nick_name_array.Length <= (int)id) return null;
        if (character_nick_name_array[(int)id] == null) return null;

        return character_nick_name_array[(int)id].GetNickNameData(nick_id,favo);
    }

    public string GetNickNameData(Character_ID id, Character_NickName_ID nicl_id)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return null;
        if (character_nick_name_array.Length <= (int)id) return null;
        if (character_nick_name_array[(int)id] == null) return null;

        return character_nick_name_array[(int)id].GetNickName(nicl_id);
    }
    

    public CharacterNickName GetNickNameDataStock(Character_ID id)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return null;
        if (character_nick_name_array.Length <= (int)id) return null;
        if (character_nick_name_array[(int)id] == null) return null;

        return character_nick_name_array[(int)id];
    }

    public bool BinaryReader(BinaryReader reader)
    {
        try
        {
            //カウントを取得
            int count = reader.ReadInt32();
            for (int i = 0; i < count;)
            {
                character_nick_name_array[i] = new CharacterNickName();
                bool res = character_nick_name_array[i].BinaryReader(ref reader);
                if (res == false) return res;
                ++i;
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
            return false;
        }

        return true;
    }

    public void BinaryWriter(BinaryWriter writer)
    {
        writer.Write(character_nick_name_array.Length);

        foreach (var data in character_nick_name_array)
        {
            data.BinaryWriter(ref writer);
        }
    }

    public void Save()
    {
        string path = Application.dataPath + $"/Event/CharacterNickName.bin";


        SaveToBinary(path);
    }



    public void Load()
    {
        string path = Application.dataPath + $"/Event/CharacterNickName.bin";
        if (System.IO.File.Exists(path) == true)
        {
            path = Application.dataPath + $"/Event/CharacterNickName.bin";
            if (System.IO.File.Exists(path) == false)
            {
                Debug.Log("ファイルが存在しません");
                return;
            }
            else
            {
                LoadFromBinary(path);
                return;
            }
        }
    }

    public void SaveToBinary(string filePath)
    {
        using (BinaryWriter writer = new BinaryWriter(File.Create(filePath)))
        {
            BinaryWriter(writer);
        }
    }

    // バイナリファイルからCharacterClothingStorageを読み込む関数
    public bool LoadFromBinary(string filePath)
    {
        bool res = false;
        if (File.Exists(filePath))
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(filePath)))
            {
                res = BinaryReader(reader);
            }
        }
        return res;
    }

#if UNITY_EDITOR
    public void OnGUI(Character_ID select)
    {
        if (character_nick_name_array[(int)select] == null) return;
        character_nick_name_array[(int)select].OnGUI();
    }

    public void Init()
    {
        character_nick_name_array = null;
        character_nick_name_array = new CharacterNickName[(int)Character_ID.MAX];
        for (int i = 0; i < (int)Character_ID.MAX;)
        {
            character_nick_name_array[i] = new CharacterNickName();
            character_nick_name_array[i].Init();
            ++i;
        }
    }
#endif
}

public class NickName
{
    /// <summary>
    /// ニックネーム
    /// </summary>
    private string nick_name = "";
    public string GetNickName { get { return nick_name; }  }

    /// <summary>
    /// 指定好感度
    /// </summary>
    private int favo = 0;
    public int GetFavo { get { return favo;} }
    public bool BinaryReader(ref BinaryReader reader)
    {
        try
        {
            nick_name = reader.ReadString();
            favo = reader.ReadInt32();
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
            return false;
        }

        return true;
    }

    public void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(nick_name);
        writer.Write(favo);
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        nick_name = EditorGUILayout.TextField("あだ名", nick_name);
        favo = EditorGUILayout.IntField("好感度", favo);
    }
#endif

}

public class CharacterNickName
{
    private NickName[] nick_name_array = new NickName[(int)Character_NickName_ID.MAX];

    public bool BinaryReader(ref BinaryReader reader)
    {
        try
        {
            //カウントを取得
            int count = reader.ReadInt32();
            for (int i = 0; i < count;)
            {
                nick_name_array[i] = new NickName();
                bool res = nick_name_array[i].BinaryReader(ref reader);
                if (res == false) return res;
                ++i;
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
            return false;
        }

        return true;
    }

    public void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(nick_name_array.Length);

        foreach(var data in nick_name_array)
        {
            data.BinaryWriter(ref writer);
        }
    }

    public NickName GetNickNameData(Character_NickName_ID nick_id, int favo)
    {
        if (nick_id <= Character_NickName_ID.NONE || nick_id >= Character_NickName_ID.MAX) return null;
        if (nick_name_array.Length <= (int)nick_id) return null;
        if (nick_name_array[(int)nick_id] == null) return null;

        var data = nick_name_array[(int)nick_id];
        if (data.GetFavo <= favo) return data;
        return null;
    }

    public string GetNickName(Character_NickName_ID nick_id)
    {
        if (nick_id <= Character_NickName_ID.NONE || nick_id >= Character_NickName_ID.MAX) return null;
        if (nick_name_array.Length <= (int)nick_id) return null;
        if (nick_name_array[(int)nick_id] == null) return null;

        var data = nick_name_array[(int)nick_id];
        return data.GetNickName;
    }

#if UNITY_EDITOR
    public void Init()
    {
        nick_name_array = null;
        nick_name_array = new NickName[(int)Character_NickName_ID.MAX];
        for (int i = 0; i < (int)Character_NickName_ID.MAX;)
        {
            nick_name_array[i] = new NickName();
            ++i;
        }
    }

    public void OnGUI()
    {
        if(nick_name_array == null) nick_name_array = new NickName[(int)Character_NickName_ID.MAX];

        EditorGUILayout.LabelField("-------------------------------------------------");
        for (int i = 0; i < (int)Character_NickName_ID.MAX;)
        {
            if (nick_name_array[i] == null) continue;
            nick_name_array[i].OnGUI();
            ++i;
        }
        EditorGUILayout.LabelField("-------------------------------------------------");
    }
#endif


}

