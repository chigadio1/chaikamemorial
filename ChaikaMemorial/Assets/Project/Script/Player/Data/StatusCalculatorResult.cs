using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ステータス計算結果
/// </summary>
public class StatusCalculatorResult
{
    /// <summary>
    /// 計算前の数値
    /// </summary>
    private int now_status_num = 0;
    /// <summary>
    /// 計算結果数値
    /// </summary>
    private int calc_status_num = 0;

    /// <summary>
    ///　結果ID
    /// </summary>
    private Result_Action_TypeID calc_result_id = Result_Action_TypeID.NONE;

    /// <summary>
    /// 計算ID
    /// </summary>
    private Status_ID status_id = Status_ID.NONE;

    /// <summary>
    /// 計算前の数値
    /// </summary>
    public int NowStatusNum { get { return now_status_num; } }

    /// <summary>
    /// 計算結果数値
    /// </summary>
    public int CalcStatusNum { get { return calc_status_num; } }

    /// <summary>
    /// 結果ID
    /// </summary>
    public Result_Action_TypeID CalcResultID { get { return calc_result_id; } }

    /// <summary>
    /// 計算ID
    /// </summary>
    public Status_ID StatusID { get { return status_id; } }

    private static readonly int add_num = 20;

    public void CalcStatus(Status_ID id,int value_now_status_num)
    {
        now_status_num = value_now_status_num;
        status_id = id;
        calc_result_id = RandomResult();
        calc_status_num = now_status_num + (int)(add_num * CalculateResultActionValue(calc_result_id));
    }

    private Result_Action_TypeID RandomResult()
    {
        int randomNumber = Random.Range(1, 101); // 1から100までの乱数を生成

        if (randomNumber <= 10) // 10% の確率でGreat_Successを返す
        {
            return Result_Action_TypeID.Great_Success;
        }
        else if (randomNumber <= 50) // 40% の確率でSo_Goodを返す
        {
            return Result_Action_TypeID.So_Good;
        }
        else if (randomNumber <= 80) // 30% の確率でSuccessを返す
        {
            return Result_Action_TypeID.Success;
        }
        else // 20% の確率でBadを返す
        {
            return Result_Action_TypeID.Bad;
        }
    }

    private float CalculateResultActionValue(Result_Action_TypeID resultType)
    {
        switch (resultType)
        {
            case Result_Action_TypeID.Bad:
                return 0.5f;
            case Result_Action_TypeID.So_Good:
                return 0.75f;
            case Result_Action_TypeID.Success:
                return 1.0f;
            case Result_Action_TypeID.Great_Success:
                return 1.25f;
            default:
                return 0.0f; // 未定義の結果タイプに対するデフォルトの値
        }
    }
}
