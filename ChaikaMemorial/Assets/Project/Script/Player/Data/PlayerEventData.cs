using Newtonsoft.Json;
using System.Collections;
using System.Text;
using UnityEngine;
[System.Serializable]
public class PlayerEventData {


    private BitArray event_bits = new BitArray((int)Event_ID.MAX);

    public PlayerEventData()
    {
        event_bits = new BitArray((int)Event_ID.MAX);

    }

    public BitArray EventBits { get { return event_bits; } }

    public void On(Event_ID id)
    {
        if (id <= Event_ID.NONE || id >= Event_ID.MAX) return;
        event_bits[(int)id] = true;
    }

    public bool GetFlag(Event_ID id)
    {
        if (id <= Event_ID.NONE || id >= Event_ID.MAX) return false;
        return event_bits[(int)id];
    }

    public void AllOff()
    {
        for (int count = 0; count < (int)Event_ID.MAX; count++) event_bits[count] = false;
    }

    public string ToJson()
    {
        return BitExc.BitArrayToString(event_bits);
    }

    // 手動でデシリアライズ
    public void FromJson(string json)
    {
        event_bits = BitExc.StringToBitArray(json);
    }

}

[System.Serializable]
public class CharacterUnlockData
{


    private BitArray unlock_character_bits = new BitArray((int)Character_ID.MAX);

    public CharacterUnlockData()
    {
        unlock_character_bits = new BitArray((int)Character_ID.MAX);
    }

    public BitArray UnlockCharacterBits { get { return unlock_character_bits; } }

    public void On(Character_ID id)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return;
        unlock_character_bits[(int)id] = true;
    }

    public bool GetFlag(Character_ID id)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return false;
        return unlock_character_bits[(int)id];
    }

    public void AllOff()
    {
        for (int count = 0; count < (int)Character_ID.MAX; count++) unlock_character_bits[count] = false;
    }

    // 手動でシリアライズ
    public string ToJson()
    {
        return BitExc.BitArrayToString(unlock_character_bits);
    }

    // 手動でデシリアライズ
    public void FromJson(string json)
    {
        unlock_character_bits = BitExc.StringToBitArray(json);
    }

}


public class BitExc
{
    public static string BitArrayToString(BitArray bitArray)
    {
        StringBuilder sb = new StringBuilder(bitArray.Length);

        for (int i = 0; i < bitArray.Length; i++)
        {
            sb.Append(bitArray[i] ? '1' : '0');
        }

        return sb.ToString();
    }

    public static BitArray StringToBitArray(string bitArrayString)
    {
        BitArray bitArray = new BitArray(bitArrayString.Length);

        for (int i = 0; i < bitArrayString.Length; i++)
        {
            bitArray[i] = (bitArrayString[i] == '1');
        }

        return bitArray;
    }
}
