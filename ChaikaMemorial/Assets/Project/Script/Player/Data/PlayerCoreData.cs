using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// プレイヤーコアデータ
/// </summary>
[System.Serializable]
public class PlayerCoreData
{
    /// <summary>
    /// プレイヤーステータスデータ
    /// </summary>
    [SerializeField]
    private PlayerStatuData status_data;

    public PlayerStatuData GetPlayerStatusData { get { return status_data; } }

    [SerializeField]
    string character_favo_data_text = "";

    /// <summary>
    /// キャラクター好感度
    /// </summary>
    private CharacterFavorabilityData character_favorability_data;

    public CharacterFavorabilityData GetCharacterFavorabilityData { get { return character_favorability_data; } }

    [SerializeField]
    string player_event_data_text = "";
    private PlayerEventData player_event_data;


    [SerializeField]
    string character_unlock_data_text = "";
    private CharacterUnlockData character_unlock_data;

    public PlayerEventData GetPlayerEventData { get { return player_event_data; } }

    public CharacterUnlockData GetCharacterUnlockData { get { return character_unlock_data; } }

    [SerializeField]
    private Dictionary<Character_ID, Character_NickName_ID> dict_character_nick_name = new Dictionary<Character_ID, Character_NickName_ID>();
    public Dictionary<Character_ID, Character_NickName_ID> GetDictCharacterNickName { get { return dict_character_nick_name; } }
    public void AddValueCharacterNickName(Character_ID id,Character_NickName_ID nick_id)
    {
        dict_character_nick_name[id] = nick_id;
    }

    public void JsonWriter()
    {
        if (status_data == null) status_data = new PlayerStatuData();
        status_data.JsonWriter();
        if (character_favorability_data == null) character_favorability_data = new CharacterFavorabilityData();
        character_favo_data_text = JsonConvert.SerializeObject(GetCharacterFavorabilityData.FavorabilityArray);
        if (player_event_data == null) player_event_data = new PlayerEventData();
        player_event_data_text = player_event_data.ToJson();
        if(character_unlock_data == null) character_unlock_data = new CharacterUnlockData();
        character_unlock_data_text = character_unlock_data.ToJson();
    }

    public void JsonReader()
    {
        if (status_data == null) status_data = new PlayerStatuData();
        status_data.JsonReader();
        if (character_favorability_data == null) character_favorability_data = new CharacterFavorabilityData();
        character_favorability_data.JsonReader(JsonConvert.DeserializeObject<Dictionary<Character_ID, int>>(character_favo_data_text));
        if (player_event_data == null) player_event_data = new PlayerEventData();
        player_event_data.FromJson(player_event_data_text);
        if (character_unlock_data == null) character_unlock_data = new CharacterUnlockData();
        character_unlock_data.FromJson(character_unlock_data_text);
    }


    public void ResetAll()
    {
        status_data = null;
        status_data = new PlayerStatuData();
        character_favorability_data = null;
        character_favorability_data = new CharacterFavorabilityData();
        player_event_data = null;
        player_event_data = new PlayerEventData();
        player_event_data.AllOff();
        character_unlock_data = null;
        character_unlock_data = new CharacterUnlockData();
        character_unlock_data.AllOff();
        character_unlock_data.On(Character_ID.Chaika);
    }
}
