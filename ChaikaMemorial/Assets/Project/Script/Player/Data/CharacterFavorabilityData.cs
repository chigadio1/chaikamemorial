using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// キャラクタ0好感度データ
/// </summary>
[System.Serializable]
public class CharacterFavorabilityData
{
    [SerializeField]
    private Dictionary<Character_ID, int> favorability_array = new Dictionary<Character_ID, int>();
    public Dictionary<Character_ID,int> FavorabilityArray { get { return favorability_array; } }

    public void JsonReader(Dictionary<Character_ID, int> value)
    {
        favorability_array = value;
    }
    /// <summary>
    /// 最大ステータス
    /// </summary>
    private const int MAX_FAVORABILITY_PARAM = 9999;
    /// <summary>
    /// 最小ステータス
    /// </summary>
    private const int MIN_FAVORABILITY_PARAM = 0;

    public CharacterFavorabilityData()
    {
        for(int i = 0; i < (int)Character_ID.MAX;)
        {
            Character_ID character_ID = (Character_ID)i;

            if(favorability_array.ContainsKey(character_ID) == false)
            {
                favorability_array[character_ID] = 0;
            }

            ++i;
        }
    }


    /// <summary>
    /// ステータス取得
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public int GetFavorability(Character_ID id)
    {
        if (id == Character_ID.MAX) return MAX_FAVORABILITY_PARAM;
        if (favorability_array.ContainsKey(id) == false)
        {
            favorability_array[id] = 0;
            return 0;
        }
        return favorability_array[id];
    }

    /// <summary>
    /// ステータスセット
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    public void SetFavorability(Character_ID id, int value)
    {
        if (id == Character_ID.MAX) return;
        if (favorability_array.ContainsKey(id) == false)
        {
            favorability_array[id] = 0;
            return;
        }
        favorability_array[id] = value;
        favorability_array[id] = Mathf.Clamp(favorability_array[id], MIN_FAVORABILITY_PARAM, MAX_FAVORABILITY_PARAM);
    }

    /// <summary>
    /// 計算ステータス
    /// </summary>
    /// <param name="id"></param>
    /// <param name="calc"></param>
    public void CalcFavorability(Character_ID id, int calc)
    {
        if (id == Character_ID.MAX) return;
        if (favorability_array.ContainsKey(id) == false)
        {
            favorability_array[id] = 0;
        }
        favorability_array[id] += calc;
        favorability_array[id] = Mathf.Clamp(favorability_array[id], MIN_FAVORABILITY_PARAM, MAX_FAVORABILITY_PARAM);

    }
}
