using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GameManagerCore : BaseCoreSingleton<GameManagerCore>
{
    public enum Core_Obj_ID
    {
        TITLE,
        GAME,
        NAMEINPUT,
        MAX
    }
    public static readonly string[] obj_addressable_names = new string[(int)Core_Obj_ID.MAX]
    {
        "Assets/Project/Assets/Object/Core/TitleUiCore.prefab",
        "Assets/Project/Assets/Object/Core/gameUiCore.prefab",
        "Assets/Project/Assets/Object/Core/NameUICore.prefab"
    };

    public static readonly string[] obj_names = new string[(int)Core_Obj_ID.MAX]
{
        "TitleUiCore",
        "gameUiCore",
        "NameUICore"
};

#if UNITY_EDITOR
    public enum Debug_State_ID
    {
        None = -1,
        Title,
        Name,
        Game,
        MAX
    }


    public Debug_State_ID state = Debug_State_ID.None;
#endif

    /// <summary>
    /// ゲームの遷移クラス
    /// </summary>
    MainGameStateBranchManager main_game_manager = new MainGameStateBranchManager();

    /// <summary>
    /// ニューゲームかどうか
    /// </summary>
    private bool is_new_game = false;
    public bool IsNewGame { get { return is_new_game; } }
    public void OnNewGame()
    {
        is_new_game = true;
    }
    public void OffNewGame()
    {
        is_new_game = false;
    }

    /// <summary>
    /// 名前入力から元に戻るフラグ
    /// </summary>
    private bool is_back_name_title = false;
    public bool IsBackNameTitle { get { return is_back_name_title; } }
    public void OnBackNameTitle()
    {
        is_back_name_title = true;
    }
    public void OffBackNameTitle()
    {
        is_back_name_title = false;
    }

    public bool IsLoadEnd()
    {
        if (core_object == null) return true;
        bool res = true;
        res &= core_object.GetFlagSetUpLoading();
        res &= SoundGameCore.Instance.IsLoadEnd();
        return res;
    }
    AddressableData<GameObject> core_object = null;

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_EDITOR
        if (state == Debug_State_ID.None) return;

        else
        {
            main_game_manager.ChangeDebug(state);
        }
#endif
    }

    private void Awake()
    {
        //FPS設定
        Application.targetFrameRate = 60;
    }

    public void ReleaseObj()
    {
        if (core_object != null)
        {
            core_object.OnAutoRelease();
        }
    }

    public void LoadCoreObj(Core_Obj_ID id)
    {
        core_object = new AddressableData<GameObject>();
        core_object.LoadStart(obj_addressable_names[(int)id]);
    }
    public void InstanceCoreObj(Core_Obj_ID id)
    {
        if (core_object != null) core_object.InstanceGameObjectReturnNone(obj_names[(int)id]);
    }

    // Update is called once per frame
    void Update()
    {
        main_game_manager.Update();
    }
}
