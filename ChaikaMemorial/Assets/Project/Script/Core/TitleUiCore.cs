using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleUiCore : BaseCoreSingleton<TitleUiCore>
{
    /// <summary>
    /// タイトルのエフェクトキャンバス
    /// </summary>
    [SerializeField]
    private TitleEffectCanvas title_effect_canvas;
    public TitleEffectCanvas TitleEffectCanvas { get { return title_effect_canvas; } }

    /// <summary>
    /// タイトルの背景画像
    /// </summary>
    [SerializeField]
    private TitleBsckGroundCanvas title_back_ground_canvas;
    public TitleBsckGroundCanvas TitleBackGroundCanvas { get { return title_back_ground_canvas;} }

    /// <summary>
    /// タイトルの選択キャンバス
    /// </summary>
    [SerializeField]
    private TitleSelectChoiceCanvas title_select_choice_canvas;
    public TitleSelectChoiceCanvas TitleSelectChoiceCanvas { get { return title_select_choice_canvas;} }

    /// <summary>
    /// タイトルの選択
    /// </summary>
    [SerializeField]
    private TitleSelectCanvas title_select_canvas;
    public  TitleSelectCanvas TitleSelectCanvas { get { return title_select_canvas;} }

    [SerializeField]
    private FadeCanvas fade_canvas;
    public FadeCanvas FadeCanvas { get { return fade_canvas; } }

    [SerializeField]
    private FadeCanvas select_fade_canvas;
    public FadeCanvas SelectFadeCanvas { get { return select_fade_canvas; } }

    [SerializeField]
    private TitleSelectSubCanvas title_select_sub_canvas;
    public TitleSelectSubCanvas TitleSelectSubCanvas { get { return title_select_sub_canvas;} }

    [SerializeField]
    private SystemPlayerDataObject system_player_data;
    public SystemPlayerDataObject SystemPlayerDataObject { get { return system_player_data;} }



    // Start is called before the first frame update
    void Start()
    {
        AllStartInit();
        title_effect_canvas.BeginInit();
        title_back_ground_canvas.BeginInit();
        title_select_choice_canvas.BeginInit();
        title_select_canvas.BeginInit();
        fade_canvas.BeginInit();
        select_fade_canvas.BeginInit();
        title_select_sub_canvas.BeginInit();
        system_player_data.BaseInit();

    }

    public void AllStartInit()
    {
        GetLoad<TitleEffectCanvas>(ref title_effect_canvas);
        GetLoad<TitleBsckGroundCanvas>(ref title_back_ground_canvas);
        GetLoad<TitleSelectChoiceCanvas>(ref title_select_choice_canvas);
        GetLoad<TitleSelectCanvas>(ref title_select_canvas);
        GetLoad<TitleSelectSubCanvas>(ref title_select_sub_canvas);
        GetLoad<SystemPlayerDataObject>(ref system_player_data);
        GetFadeLoad(ref fade_canvas, ref select_fade_canvas);
    }

    public void SoundInit()
    {
        SoundGameCore.instance.EventSummaryData.LoadTitleSE((int)Default_Title_Sound_ID.Select);
        SoundGameCore.instance.EventSummaryData.LoadTitleSE((int)Default_Title_Sound_ID.Sub_Select);
        SoundGameCore.instance.EventSummaryData.LoadTitleSE((int)Default_Title_Sound_ID.Sub_Select_Space);
        SoundGameCore.instance.EventSummaryData.LoadTitleBGM((int)Default_Title_Sound_ID.Title_BGM);
    }

    public void GetLoad<T>(ref T set) where T : MonoBehaviour
    {
        set = gameObject.GetComponentInChildren<T>();
    }

    public void GetFadeLoad(ref FadeCanvas fade,ref FadeCanvas select_fade)
    {
        var childs = gameObject.GetComponentsInChildren<FadeCanvas>();
        foreach(var child in childs)
        {
            if(child.gameObject.name == "FadeCanvas")
            {
                fade = child;
            }
            else if(child.gameObject.name == "SelectFadeCanvas")
            {
                select_fade_canvas = child;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnDestroy()
    {
       SoundGameCore.instance.AllEventRelease(Sound_Event_Type_ID.Title);
       Resources.UnloadUnusedAssets();
    }
}
