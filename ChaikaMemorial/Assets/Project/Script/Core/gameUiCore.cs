using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class gameUiCore : BaseCoreSingleton<gameUiCore>
{
    /// <summary>
    /// ゲーム中にロードしたとき
    /// </summary>
    public bool is_load_game_retry = false;

    /// <summary>
    /// キャラクターの衣装ID
    /// </summary>
    private CharacterClothingStorage character_clothing_storage = null;
    public CharacterClothingStorage CharacterClothingStorage { get { return character_clothing_storage;} }

    public int GetCharacterSptite(Character_ID chara_id, CharacterClothingTypeID clothing_id,FacialExpressionID facial_id)
    {
        if (character_clothing_storage == null) return 0;
        if (chara_id <= Character_ID.NONE || chara_id >= Character_ID.MAX) return 0;
        if (clothing_id <= CharacterClothingTypeID.NONE || clothing_id >= CharacterClothingTypeID.MAX) return 0;
        if (facial_id <= FacialExpressionID.NONE || facial_id >= FacialExpressionID.MAX) return 0;

        var data = character_clothing_storage.GetCharacterCloting(chara_id)?.GetCharacterClothing(clothing_id)?.GetCharacterClothing(facial_id);
        if(data == null) return 0;

        return data.GetSpriteID;
    }

    public enum Default_UI_Object_ID
    {
        TalkFrame,
        ResultIcon,
        MAX
    }

    public enum UI_Object_ID
    {
        NOVEL,
        SCHOOL,
        BEHAVIOR,
        MAX
    }

    public string GetUIObjectName(UI_Object_ID id)
    {
        string res = "";

        switch (id)
        {
            case UI_Object_ID.NOVEL:
                res = "Novel";
                break;
            case UI_Object_ID.SCHOOL:
                res = "Action";
                break;
            case UI_Object_ID.BEHAVIOR:
                res = "Behavior";
                break;
        }


        return res;
    }

    private Dictionary<UI_Object_ID, AddressableData<GameObject>> game_ui_object = new Dictionary<UI_Object_ID, AddressableData<GameObject>>();


    private static readonly string[] default_ui_object_name = new string[(int)Default_UI_Object_ID.MAX]
    {
        "TalkFrame",
        "ResultIcon"
    };

    public static readonly int[] result_action_names = new int[(int)Result_Action_TypeID.MAX]
    {
        1210,
        1209,
        1208,
        1207,
    };

    [SerializeField]
    private GameObject novel_object;

    [SerializeField]
    private GameObject school_object;

    [SerializeField]
    private GameObject behavior_object;

    public void SetActive(UI_Object_ID id,bool value)
    {
        if(id == UI_Object_ID.NOVEL)
        {
            if (novel_object == null) return;
            novel_object.SetActive(value);
        }
        else if (id == UI_Object_ID.SCHOOL)
        {
            if (novel_object == null) return;
            school_object.SetActive(value);
        }
        else if (id == UI_Object_ID.BEHAVIOR)
        {
            if (novel_object == null) return;
            behavior_object.SetActive(value);
        }
    }

    public bool LoadGameUIObject(UI_Object_ID id)
    {
        if (game_ui_object == null) game_ui_object = new Dictionary<UI_Object_ID, AddressableData<GameObject>>();
        if(game_ui_object.ContainsKey(id) == false)
        {
            game_ui_object[id] = new AddressableData<GameObject>();
        }
        else
        {
            game_ui_object[id].OnAutoRelease();
            game_ui_object[id] = null;
            game_ui_object[id] = new AddressableData<GameObject>();
            ReleaseGameUIObject(id);
        }

        string res = GetUIObjectName(id);
        if (res == null) return false ;
        game_ui_object[id].LoadStart($"Assets/Project/Assets/Object/UI/{res}.prefab");
        return true;
    }

    public bool IsLoadFinishGameUIObject(UI_Object_ID id)
    {
        if (game_ui_object.ContainsKey(id) == false)
        {
            return false;
        }

        return game_ui_object[id].GetFlagSetUpLoading();
    }

    public void ReleaseGameUIObject(UI_Object_ID id)
    {
        if (game_ui_object.ContainsKey(id) == true)
        {
            game_ui_object[id].OnAutoRelease();
            game_ui_object[id] = null;
            game_ui_object.Remove(id);
        }
        switch (id)
        {
            case UI_Object_ID.NOVEL:
                character_canva = null;
                setting_canvas  = null;
                novel_back_ground_canvas = null;
                if (novel_object != null)
                {
                    GameObject.Destroy(novel_object);
                    novel_object = null;
                }
                break;
            case UI_Object_ID.SCHOOL:
                action_canvas        = null;
                player_select_canvas = null;
                if (school_object != null)
                {
                    GameObject.Destroy(school_object);
                    school_object = null;
                }
                break;
            case UI_Object_ID.BEHAVIOR:
                status_canvas = null;
                if (behavior_object != null)
                {
                    GameObject.Destroy(behavior_object);
                    behavior_object = null;
                }
                break;
        }



    }

    public GameObject GetGameUIObject(UI_Object_ID id)
    {
        if (id == UI_Object_ID.NOVEL)
        {
            return novel_object;
        }
        else if (id == UI_Object_ID.SCHOOL)
        {
            return school_object;
        }
        else if (id == UI_Object_ID.BEHAVIOR)
        {
            return behavior_object;
        }
        return null;
    }

    public void SetGameUIObject(UI_Object_ID id,GameObject set)
    {
        if (id == UI_Object_ID.NOVEL)
        {
            novel_object = set;
        }
        else if (id == UI_Object_ID.SCHOOL)
        {
            school_object = set;
        }
        else if (id == UI_Object_ID.BEHAVIOR)
        {
            behavior_object = set;
        }

    }

    public void SetGameUIObjectParent(UI_Object_ID id, GameObject parent)
    {
        if (id == UI_Object_ID.NOVEL)
        {
            if (novel_object == null) return;
            novel_object.transform.SetParent(parent.transform);
        }
        else if (id == UI_Object_ID.SCHOOL)
        {
            if (school_object == null) return;
            school_object.transform.SetParent(parent.transform);
        }
        else if (id == UI_Object_ID.BEHAVIOR)
        {
            if (behavior_object == null) return;
            behavior_object.transform.SetParent(parent.transform);
        }

    }

    public bool InstanceGameUIObject(UI_Object_ID id)
    {
        GameObject ui_object_instance = GetGameUIObject(id);
        if(ui_object_instance != null)
        {
            Debug.Log("すでに存在しています");
            return true;
        }

        if (game_ui_object.ContainsKey(id) == false) return false;
        if (game_ui_object[id].GetFlagSetUpLoading() == false) return false;
        SetGameUIObject(id,GameObject.Instantiate(game_ui_object[id].GetAddressableData()));
        SetGameUIObjectParent(id,gameObject);
        BeginInitGameUI(id);
        return true;
    }

    private void BeginInitGameUI(UI_Object_ID id)
    {
        GameObject ui_object_instance = GetGameUIObject(id);
        if (ui_object_instance == null) return;

        if (id == UI_Object_ID.NOVEL)
        {
            character_canva = ui_object_instance.GetComponentInChildren<CharacterCanvas>();
            setting_canvas = ui_object_instance.GetComponentInChildren<SettingCanvas>();
            novel_back_ground_canvas = ui_object_instance.GetComponentInChildren<BackGroundCanvas>();
            if (character_canva != null) character_canva.BeginInit();
            if (setting_canvas != null) setting_canvas.BeginInit();
            if(novel_back_ground_canvas != null) novel_back_ground_canvas.BeginInit();

        }
        else if(id == UI_Object_ID.SCHOOL)
        {
            action_canvas = ui_object_instance.GetComponentInChildren<ActionCanvas>();
            player_select_canvas = ui_object_instance.GetComponentInChildren<PlayerSelectCanvas>();
            if (action_canvas != null) action_canvas.BeginInit();
            if (player_select_canvas != null) player_select_canvas.BeginInit();
        }
        else if(id == UI_Object_ID.BEHAVIOR)
        {
            status_canvas = ui_object_instance.GetComponentInChildren<StatusCanvas>();
            if (status_canvas != null) status_canvas.BeginInit();
        }


        



    }

    /// <summary>
    /// キャラクターキャンバス
    /// </summary>
    [SerializeField]
    private CharacterCanvas character_canva;
    public CharacterCanvas CharacterCanvas { get { return character_canva; } }
    /// <summary>
    /// 画像ロードデータ
    /// </summary>

    private Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>> directory_sprit;

    private NovelExecGroupManager novel_manager = new NovelExecGroupManager();
    public bool IsNovelEnd { get { return novel_manager.IsFinish && IsLoadingEnd(); } }

    //デフォルト画像
    private Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>> directory_default_sprit;

    //デフォルト画像
    private Dictionary<string, AddressableData<GameObject>> directory_default_ui_obj;


    /// <summary>
    /// 会話画面キャンバス
    /// </summary>
    [SerializeField]
    private SettingCanvas setting_canvas;

    public SettingCanvas SettingCanvas { get { return setting_canvas; } }

    /// <summary>
    /// フェードキャンバス
    /// </summary>
    [SerializeField]
    private FadeCanvas fade_canvas;
    public FadeCanvas FadeCanvas { get { return fade_canvas; } }

    /// <summary>
    /// ステータスキャンバス
    /// </summary>
    [SerializeField]
    private StatusCanvas status_canvas;
    public StatusCanvas StatusCanvas { get { return status_canvas; } }



    [SerializeField]
    private ActionCanvas action_canvas;
    public ActionCanvas ActionCanvas { get { return action_canvas; } }



    [SerializeField]
    private PlayerSelectCanvas player_select_canvas;
    public PlayerSelectCanvas PlayerSelectCanvas { get { return player_select_canvas; } }

    /// <summary>
    /// ノベルの背景画像
    /// </summary>
    [SerializeField]
    private BackGroundCanvas novel_back_ground_canvas;
    public BackGroundCanvas NovelBackGroundCanvas { get { return novel_back_ground_canvas;} }

    /// <summary>
    /// ゲーム中のセレクトタイプ
    /// </summary>
    private Parent_Action_Select_Type_ID parent_action_select_type_id = Parent_Action_Select_Type_ID.ParameterUP;
    public Parent_Action_Select_Type_ID ParentActionSelectTypeID { get { return parent_action_select_type_id; } set { parent_action_select_type_id = value; } }
    private bool is_move_select = false; //falseなら左　trueなら右
    public bool IsMoveSelect { get { return is_move_select; } set { is_move_select = value;} }



    /// <summary>
    /// 現在選択しているもの
    /// </summary>
    private Player_Select_ID select_plater_id = Player_Select_ID.NONE;
    public void SetPlayerSelectID(Player_Select_ID value)
    {
        select_plater_id = value;
    }
    public Player_Select_ID SelectPlayerID { get { return select_plater_id; } }

    private Character_NickName_ID select_nick_name_id = Character_NickName_ID.NONE;

    /// <summary>
    /// 現在選択しているニックネーム
    /// </summary>
    /// <param name="id"></param>
    public void SetCharacterSelectNickNameID(Character_NickName_ID id)
    {
        select_nick_name_id=id;
    }
    public Character_NickName_ID SelectNickNameID { get { return select_nick_name_id;} }

    /// <summary>
    /// 現在選択しているニックネーム
    /// </summary>
    /// <param name="id"></param>
    private Character_ID select_character_id = Character_ID.NONE;
    public void SetSelectCharacterID(Character_ID id)
    {
        select_character_id = id;
    }
    public Character_ID SelectCharacterID { get { return select_character_id; } }

    /// <summary>
    /// 現在選択しているニックネーム
    /// </summary>
    /// <param name="id"></param>
    private Date_Spot_ID select_date_spot_id = Date_Spot_ID.NONE;
    public void SetSelectDateSpotID(Date_Spot_ID id)
    {
        select_date_spot_id = id;
    }
    public Date_Spot_ID SelectDateSpotID { get { return select_date_spot_id; } }



    /// <summary>
    /// ボタン押したときのフラグ
    /// </summary>
    public bool is_button_push = false;
    /// <summary>
    /// キャンセルボタンを押したときのフラグ
    /// </summary>
    public bool is_button_cancel_push = false;
    /// <summary>
    /// 汎用インデックス
    /// </summary>
    public int GeneralPurposeIndex = 0;

    /// <summary>
    /// ロードが全て完了しているか
    /// </summary>
    /// <returns></returns>
    public bool IsLoadingEnd()
    {
        if (directory_sprit == null) return false;
        bool res = true;

        
        foreach (var data in directory_sprit.Values)
        {
            foreach (var dataID in data.Values)
            {
                res &= dataID.GetFlagSetUpLoading();
            }
        }
        foreach (var data in directory_default_sprit.Values)
        {
            foreach (var dataID in data.Values)
            {
                res &= dataID.GetFlagSetUpLoading();
            }
        }
        foreach(var data in directory_default_ui_obj)
        {
            res &= data.Value.GetFlagSetUpLoading();
        }
        return res;
    }
    /// <summary>
    ///全ての画像をリリース
    /// </summary>
    public void AllRelease()
    {
        foreach (var data in directory_sprit.Values)
        {
            foreach (var dataID in data.Values)
            {
                dataID.OnAutoRelease();
            }
            data.Clear();
        }
        directory_sprit.Clear();
    }

    public void AllDefaultRelease()
    {
        foreach (var data in directory_default_sprit.Values)
        {
            foreach (var dataID in data.Values)
            {
                dataID.OnAutoRelease();
            }
            data.Clear();
        }
        directory_default_sprit.Clear();
        foreach (var data in directory_default_ui_obj.Values)
        {
            data.OnAutoRelease();
        }
        directory_default_ui_obj.Clear();
    }

    // Start is called before the first frame update
    void Awake()
    {


        directory_default_ui_obj = new Dictionary<string, AddressableData<GameObject>>();
        directory_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
        directory_default_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
        fade_canvas.BeginInit();
        InitDefaultSprine();
        InitDefaultObject();


    }

    private void InitDefaultSprine()
    {
        LoadDefaultSprite(1204, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1205, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1206, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1207, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1208, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1209, Novel_Sprite_Type_ID.ICON);
        LoadDefaultSprite(1210, Novel_Sprite_Type_ID.ICON);
    }

    private void InitDefaultObject()
    {
        LoadDefaultUIObject(default_ui_object_name[0]);
        LoadDefaultUIObject(default_ui_object_name[1]);
    }

    public void PlayEvent(int id)
    {
        PlayEnd();
        if (System.IO.Directory.Exists(Application.dataPath + "/Scenario") == false)
        {
            System.IO.Directory.CreateDirectory(Application.dataPath + "/Scenario");
        }
        character_clothing_storage = new CharacterClothingStorage();
        character_clothing_storage.Load();
        using (FileStream fileStream = new FileStream(Application.dataPath + $"/Scenario/{id}.bin", FileMode.Open))
        {
            BinaryReader reader = new BinaryReader(fileStream);

            novel_manager.BinaryReaderData(ref reader);
            reader.Close();

        }


    }

    public void PlayEnd()
    {
        if (novel_manager != null)
        {
            novel_manager.Reset();
            novel_manager = null;
            novel_manager = new NovelExecGroupManager();
        }
        character_clothing_storage = null;

        gameUiCore.Instance.AllRelease();
        SoundGameCore.Instance.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
        Resources.UnloadUnusedAssets();
    }

    // Update is called once per frame
    void Update()
    {
        //status_canvas.UpdateExec();
        //actionSelect.Update();
        //NovelUpdate();
    }

    public void NovelUpdate()
    {
        if (IsLoadingEnd() && PlayerCore.Instance.GetIsLoadFinish)
        {
            novel_manager.Update();
        }
    }

    private void LoadSprite(string path, int id, Novel_Sprite_Type_ID type_id)
    {
#if UNITY_EDITOR
        if (EditorApplication.isPlaying == false) return;
#endif
        if (directory_sprit == null) directory_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
        if (directory_sprit.ContainsKey(type_id) == false)
        {
            directory_sprit[type_id] = new Dictionary<int, AddressableData<Sprite>>();
        }
        ///すでに存在していたら破棄
        if(directory_sprit[type_id].ContainsKey(id))
        {
            return;
        }

        var data = new AddressableData<Sprite>();
        data.LoadStart(path);
        directory_sprit[type_id][id] = data;
    }

    private void LoadDefaultSprite(string path, int id, Novel_Sprite_Type_ID type_id)
    {
        if (directory_default_sprit == null) directory_default_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
        if (directory_default_sprit.ContainsKey(type_id) == false)
        {
            directory_default_sprit[type_id] = new Dictionary<int, AddressableData<Sprite>>();
        }

        var data = new AddressableData<Sprite>();
        data.LoadStart(path);
        directory_default_sprit[type_id][id] = data;
    }

    public void LoadSprite(int id, Novel_Sprite_Type_ID type_id)
    {
        string path = "Assets/Project/image/";
        switch (type_id)
        {
            case Novel_Sprite_Type_ID.CHARACTER:
                path += "Character/";
                break;
            case Novel_Sprite_Type_ID.BACKGROUND:
                path += "BackGround/";
                break;
            case Novel_Sprite_Type_ID.FADE:
                path += "Rule/";
                break;
            case Novel_Sprite_Type_ID.Still:
                path += "Still/";
                break;
            case Novel_Sprite_Type_ID.ICON:
                path += "Icon/";
                break;
        }
        path += $"{id}.png";
        LoadSprite(path, id, type_id);

    }

    public void LoadDefaultSprite(int id, Novel_Sprite_Type_ID type_id)
    {
        string path = "Assets/Project/image/";
        switch (type_id)
        {
            case Novel_Sprite_Type_ID.CHARACTER:
                path += "Character/";
                break;
            case Novel_Sprite_Type_ID.BACKGROUND:
                path += "BackGround/";
                break;
            case Novel_Sprite_Type_ID.FADE:
                path += "Rule/";
                break;
            case Novel_Sprite_Type_ID.Still:
                path += "Still/";
                break;
            case Novel_Sprite_Type_ID.ICON:
                path += "Icon/";
                break;
        }
        path += $"{id}.png";
        LoadDefaultSprite(path, id, type_id);

    }

    public void LoadDefaultUIObject(string name)
    {
        string path = "Assets/Project/Assets/Object/UI/";
        path += name + ".prefab";

        if(directory_default_ui_obj == null)
        {
            directory_default_ui_obj = new Dictionary<string, AddressableData<GameObject>>();
        }
        if (directory_default_ui_obj.ContainsKey(name)) return;

        AddressableData<GameObject> add = new AddressableData<GameObject>();
        add.LoadStart(path);
        directory_default_ui_obj[name] = add;

    }

    public GameObject GetGameObjectUI(Default_UI_Object_ID id)
    {
        if (directory_default_ui_obj == null) return null;
        if(directory_default_ui_obj.ContainsKey(default_ui_object_name[(int)id]))
        {
            string name = default_ui_object_name[(int)id];
            return directory_default_ui_obj[name].GetAddressableData();
        }
        return null;
    }

    private void OnDestroy()
    {
        AllRelease();
        AllDefaultRelease();
    }

    public Sprite GetSprite(Novel_Sprite_Type_ID type_id, int id)
    {
        Sprite sprite = null;



        if (directory_sprit == null)
        {
            directory_sprit = directory_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
            return sprite;
        }
        if (directory_sprit.ContainsKey(type_id) == false)
        {
            return sprite;
        }
        if (directory_sprit[type_id].ContainsKey(id) == false)
        {
            return sprite;
        }

        if (directory_sprit[type_id][id].GetFlagSetUpLoading() == false)
        {
            return sprite;
        }

        return directory_sprit[type_id][id].GetAddressableData();
    }

    public Sprite GetDefaultSprite(Novel_Sprite_Type_ID type_id, int id)
    {
        Sprite sprite = null;



        if (directory_default_sprit == null)
        {
            directory_default_sprit = directory_default_sprit = new Dictionary<Novel_Sprite_Type_ID, Dictionary<int, AddressableData<Sprite>>>();
            return sprite;
        }
        if (directory_default_sprit.ContainsKey(type_id) == false)
        {
            return sprite;
        }
        if (directory_default_sprit[type_id].ContainsKey(id) == false)
        {
            return sprite;
        }

        if (directory_default_sprit[type_id][id].GetFlagSetUpLoading() == false)
        {
            return sprite;
        }

        return directory_default_sprit[type_id][id].GetAddressableData();
    }

    public void NovelJump(int jump_exec_id, int jump_order_id)
    {
        if (jump_exec_id < 0 || jump_order_id < 0) return;
        novel_manager.JumpExecOrderID(jump_exec_id, jump_order_id);
    }
}
