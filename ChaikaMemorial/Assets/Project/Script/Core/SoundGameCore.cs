using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGameCore : BaseCoreSingleton<SoundGameCore>
{
    public static readonly int max_audio_source_num = 2;
    /// <summary>
    /// 再生サウンドコンポーネント配列
    /// </summary>
    private AudioSource[] audio_source_array = new AudioSource[max_audio_source_num];

    /// <summary>
    /// サウンド管理クラス
    /// </summary>
    private SoundEventSummaryData event_summary_data = new SoundEventSummaryData();

    public SoundEventSummaryData EventSummaryData { get { return event_summary_data; } }

    public bool IsLoadEnd() { return event_summary_data.IsLoadEnd(); }
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Init()
    {
        int count_BGM = 0;
        //保持する音コンポーネントが足りないため追加
        for (int count = 0; count < max_audio_source_num;)
        {
            var audio = gameObject.AddComponent<AudioSource>();
            audio.playOnAwake = false;
            if (count_BGM == 0)
            {
                audio.loop = true;
                ++count_BGM;
            }
            audio_source_array[count] = audio;
            ++count;
        }
    }

    // Update is called once per frame
    void Update()
    {
        NoneAudioUpdate();
    }

    public void PlaySound(Sound_Event_Type_ID event_type,Sound_Type_ID type_id,int sound_id)
    {
        if (IsLoadEnd() == false) return;
        if (event_summary_data.GetAudio(event_type, type_id, sound_id) == null) return;


        if(type_id == Sound_Type_ID.BGM)
        {
            audio_source_array[0].Stop();
            audio_source_array[0].clip = event_summary_data.GetAudio(event_type, type_id, sound_id);
            audio_source_array[0].loop = true;
            audio_source_array[0].Play();
            audio_source_array[0].playOnAwake = false;
            return;
        }

        else
        {
            audio_source_array[1].Stop();
            audio_source_array[1].clip = event_summary_data.GetAudio(event_type, type_id, sound_id);
            audio_source_array[1].loop = false;
            audio_source_array[1].Play();
            audio_source_array[1].playOnAwake = false;
            return;
        }
        


    }

    public void SetVolume(Sound_Event_Type_ID event_type, Sound_Type_ID type_id, float value)
    {
        if (type_id == Sound_Type_ID.BGM)
        {
            audio_source_array[0].volume = value;
            return;
        }

        else
        {
            audio_source_array[1].volume = value;
            return;
        }
    }

    public float GetVolume(Sound_Event_Type_ID event_type, Sound_Type_ID type_id)
    {
        if (type_id == Sound_Type_ID.BGM)
        {
            return audio_source_array[0].volume;
        }

        else
        {
            return audio_source_array[1].volume;
        }
    }

    public void StopBGM()
    {
        audio_source_array[0].Stop();
    }

    private void NoneAudioUpdate()
    {
        foreach(var audio in audio_source_array)
        {
            if (audio.loop == true) continue;
            if (audio.isPlaying == false)
            {
                audio.clip = null;
            }
        }

    }

    public void Release(Sound_Event_Type_ID event_type, Sound_Type_ID type_id, int sound_id)
    {
        event_summary_data.Release(event_type,type_id,sound_id);
    }

    public void AllEventRelease(Sound_Event_Type_ID event_id)
    {
        event_summary_data.AllEventRelease(event_id);
    }

    public void OnDestroy()
    {
        event_summary_data.AllEventRelease(Sound_Event_Type_ID.NovelEvent);
        event_summary_data.AllEventRelease(Sound_Event_Type_ID.ScheduleAction);
        event_summary_data.AllEventRelease(Sound_Event_Type_ID.SelectStartGame);
        event_summary_data.AllEventRelease(Sound_Event_Type_ID.Title);
    }

    public void SoundStartCoroutine(IEnumerator func)
    {
        StartCoroutine(func);
    }

}
