using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NameUICore : BaseCoreSingleton<NameUICore>
{
    /// <summary>
    /// 名前入力キャンバス
    /// </summary>
    [SerializeField]
    NameInputCanvas name_input_canvas = null;


    NameInputStateManager name_input_state_manager = new NameInputStateManager();

    [SerializeField]
    FadeCanvas fade_canvas = null;
    public FadeCanvas FadeCanvas { get { return fade_canvas;} }
    

    /// <summary>
    /// 入学ボタン
    /// </summary>
    bool is_enter = false;
    public bool IsEnter { get { return is_enter; } }
    public void OffEnter()
    {
        is_enter = false;
    }

    bool is_end = false;
    public bool IsEnd { get { return is_end; } }

    public NameInputCanvas NameInputCanvas { get { return name_input_canvas; } }
    // Start is called before the first frame update
    void Start()
    {
        name_input_canvas?.BeginInit();
        fade_canvas?.BeginInit();
    }

    // Update is called once per frame
    void Update()
    {
        if(is_end == true) return;
        if (name_input_state_manager == null) return;
        name_input_state_manager.Update();
        is_end = name_input_state_manager.IsFinish;
    }

    private void LateUpdate()
    {
    }

    public void OnDestroy()
    {
        name_input_state_manager = null;
        Resources.UnloadUnusedAssets();
    }

    public void OnEnter()
    {
        is_enter = true;
    }
}
