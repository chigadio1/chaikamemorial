
#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(GameManagerCore))]
public class GameManagerCoreEditor : Editor
{


    public override void OnInspectorGUI()
    {
        var core = target as GameManagerCore;
        core.state = (GameManagerCore.Debug_State_ID)EditorGUILayout.EnumPopup("�����J�n", core.state);

        
    }

   
}

#endif
