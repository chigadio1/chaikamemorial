using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 基礎シングルトン
/// </summary>
public class BaseCoreSingleton<T> : MonoBehaviour where T  : MonoBehaviour
{
    static protected T instance; //インスタンス


   
    static public T Instance
    {
        get
        {
            if(instance == null)
            {
                instance = GenerateInstance();
                return instance;
            }
            return instance;
        }
    }


    static protected T GenerateInstance()
    {
        T instance;
        var obj = GameObject.FindAnyObjectByType<T>();
        if(obj != null)
        {
            return obj.GetComponent<T>();
        }

        GameObject gameObject = new GameObject();
        gameObject.name = typeof(T).Name;
        instance = gameObject.AddComponent<T>();



        return instance;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
