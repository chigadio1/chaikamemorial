using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CharacterClothingEditor : EditorWindow
{
    public static CharacterClothingEditor instance = null;

    int selectIndex = -1;
    CharacterClothingStorage storage = new CharacterClothingStorage();


    [MenuItem("Custom/キャラ衣装ID")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            return;
        }
        // ウィンドウを作成または表示します
        CharacterClothingEditor.instance = EditorWindow.GetWindow(typeof(CharacterClothingEditor)) as CharacterClothingEditor;



    }
    public void OnGUI()
    {
        using (new GUILayout.HorizontalScope(GUILayout.Width(1000)))
        {
            using (new GUILayout.VerticalScope(GUILayout.Width(200)))
            {
                for (int count = 0; count < (int)Character_ID.MAX;)
                {
                    Character_ID id = (Character_ID)count;

                    if (GUILayout.Button($"{Enum.GetName(typeof(Character_ID), id)}"))
                    {
                        selectIndex = count;
                    }

                    ++count;
                }
            }
            using (new GUILayout.VerticalScope(GUILayout.Width(500)))
            {
                storage.OnGUI((Character_ID)selectIndex);
            }
        }
        using (new GUILayout.HorizontalScope(GUILayout.Width(100)))
        {
            if(GUILayout.Button("Save"))
            {
                storage.Save();
            }
            if(GUILayout.Button("Load"))
            {
                storage.Load();
            }
            if (GUILayout.Button("Init"))
            {
                storage.EditorInit();
            }
        }
    }
}
