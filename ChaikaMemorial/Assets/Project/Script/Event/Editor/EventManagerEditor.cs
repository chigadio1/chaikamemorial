using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EventManagerEditor : EditorWindow
{
    public static EventManagerEditor instance = null;

    private EventManagerData manager_data = new EventManagerData();

    [MenuItem("Custom/イベント作成")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            Debug.Log("すでにウィンドウは存在しています");
            return;
        }
        // ウィンドウを作成または表示します
        EventManagerEditor.instance = EditorWindow.GetWindow(typeof(EventManagerEditor)) as EventManagerEditor;

      

    }

    private void OnGUI()
    {
        manager_data.OnGUI();
    }
}
