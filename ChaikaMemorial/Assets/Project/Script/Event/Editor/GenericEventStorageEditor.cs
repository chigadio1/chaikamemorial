using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class GenericEventStorageEditor : EditorWindow
{
    public static GenericEventStorageEditor instance = null;

    public GenericEventStorage storage = new GenericEventStorage();
    public int add_event_id;
    public int select;
    [MenuItem("Custom/汎用いべんと管理")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            return;
        }
        // ウィンドウを作成または表示します
        GenericEventStorageEditor.instance = EditorWindow.GetWindow(typeof(GenericEventStorageEditor)) as GenericEventStorageEditor;

    }

    public void OnGUI()
    {
        using (new EditorGUILayout.VerticalScope())
        {
            using (new EditorGUILayout.HorizontalScope())
            {
                using (new EditorGUILayout.VerticalScope((GUILayout.Width(200))))
                {
                    add_event_id = EditorGUILayout.IntField("Add_Event_ID", add_event_id);
                    if (GUILayout.Button("Add"))
                    {
                        if (add_event_id > Event_ID.NONE.GetInt() + 9001 && add_event_id < Event_ID.MAX.GetInt() + 9001)
                        {
                            storage.OnGUIAdd(add_event_id);
                        }
                    }
                    if (GUILayout.Button("Remove"))
                    {
                        if (add_event_id > Event_ID.NONE.GetInt() + 9001 && add_event_id < Event_ID.MAX.GetInt() + 9001)
                        {
                            storage.OnGUIRemove(add_event_id);
                        }
                    }
                    storage.OnGUIButtonList(ref select);
                }
                storage.OnGUI(select);
            }
            if (GUILayout.Button("Save"))
                storage.SaveStorage();
            if (GUILayout.Button("Load"))
                storage.LoadStorage();
        }
    }
}
