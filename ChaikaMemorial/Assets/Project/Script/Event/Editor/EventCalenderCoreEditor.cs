using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class EventCalenderCoreEditor : EditorWindow
{

    public static EventCalenderCoreEditor instance = null;

    EventCalenderCoreData core_data = null;
    Event_Years_ID year_id;
    Event_Months_ID month_id;
    int select_day = 1;
    private Vector2 _buttonPosition;
    private Vector2 _detailsPosition;
    private Vector2 _eventTextPosition;
    private Vector2 _titlePosition;
    private Vector2 _cullenderPosition;

    private bool is_dbg = false;
    private     int start_x = 100;
    private     int add_x   = 200;
    private     int start_y = 50;
    private     int add_y   = 120;
    private   float width = 100.0f;
    private   float height = 100.0f;
    private Vector2 _dbgPosition;

    [MenuItem("Custom/カレンダーイベント作成")]
    public static void ShowWindow()
    {
        if(instance != null)
        {
            return;
        }
        // ウィンドウを作成または表示します
        EventCalenderCoreEditor.instance = EditorWindow.GetWindow(typeof(EventCalenderCoreEditor)) as EventCalenderCoreEditor;

        instance.BeginInit();

    }

    public void BeginInit()
    {
        if (core_data != null) return;
        core_data = new EventCalenderCoreData();
        year_id = Event_Years_ID.ONE;
        month_id = Event_Months_ID.April;
        select_day = 1;
    }

    public void OnGUI()
    {
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            using (new GUILayout.VerticalScope(GUILayout.Width(300)))
            {

                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_detailsPosition, EditorStyles.helpBox, GUILayout.Height(150)))
                {
                    //その日の詳細
                    using (new GUILayout.VerticalScope(GUILayout.Width(250)))
                    {
                        core_data.SelectGUI(year_id, month_id, select_day);
                    }
                }
                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_eventTextPosition, EditorStyles.helpBox, GUILayout.Height(150)))
                {
                    //その月にあるイベント
                    using (new GUILayout.VerticalScope(GUILayout.Width(250)))
                    {
                        var details_text = core_data.MonthEventDetailsText(year_id, month_id);
                        if (details_text != null)
                        {
                            foreach (var text in details_text)
                            {
                                EditorGUILayout.LabelField(text);
                            }
                        }
                        else
                        {
                            EditorGUILayout.LabelField("イベントが存在しません");
                        }
                    }
                }

                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_buttonPosition, EditorStyles.helpBox))
                {
                    //ボタン
                    using (new GUILayout.VerticalScope(GUILayout.Width(250)))
                    {
                        if (GUILayout.Button("Save"))
                        {
                            core_data.Save();
                        }
                        if (GUILayout.Button("Load"))
                        {
                            core_data.Load();
                        }
                        if (GUILayout.Button("初期化"))
                        {
                            core_data.InitEditor();
                        }
                        if (GUILayout.Button("FIX"))
                        {
                            core_data.FixGUI();
                        }
                    }
                }
                is_dbg = GUILayout.Toggle(is_dbg,"DebugLayout:");
                if (is_dbg)
                {
                    using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_dbgPosition, EditorStyles.helpBox))
                    {
                        start_x = EditorGUILayout.IntField("StartX:", start_x);
                        start_y = EditorGUILayout.IntField("StartX:", start_y);
                        add_x = EditorGUILayout.IntField("AddX:", add_x);
                        add_y = EditorGUILayout.IntField("AddY:", add_y);
                        width = EditorGUILayout.FloatField("Width;", width);
                        height = EditorGUILayout.FloatField("Height:", height);
                    }
                }
            }
        
            //カレンダーボタン
            using (new GUILayout.VerticalScope(GUILayout.Width(position.width)))
            {
                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_titlePosition, EditorStyles.helpBox, GUILayout.Height(100)))
                {
                    using (new GUILayout.HorizontalScope())
                    {
                        if(GUILayout.Button("BackMonth", GUILayout.Height(100) ,GUILayout.Width(100)))
                        {
                            if(month_id == Event_Months_ID.January)
                            {
                                --year_id;
                                if (year_id <= Event_Years_ID.NONE) year_id = Event_Years_ID.ONE;
                                month_id = Event_Months_ID.December;
                            }
                            else if(month_id == Event_Months_ID.April && year_id == Event_Years_ID.ONE)
                            {
                                month_id = Event_Months_ID.April;
                            }
                            else {
                                --month_id;
                            }
                            select_day = 1;
                        }
                        GUILayout.Label($"{(int)year_id + EventYearData.start_year:0000}年/{(int)month_id + 1:00}月", GUILayout.Height(100), GUILayout.Width(100));
                        if (GUILayout.Button("NextMonth", GUILayout.Height(100), GUILayout.Width(100)))
                        {
                            if (month_id == Event_Months_ID.December)
                            {
                                ++year_id;
                                if (year_id >= Event_Years_ID.MAX) year_id = Event_Years_ID. FOURTH;
                                month_id = Event_Months_ID.January;
                            }
                            else if (month_id == Event_Months_ID.March && year_id == Event_Years_ID.FOURTH)
                            {
                                month_id = Event_Months_ID.March;
                            }
                            else {
                                ++month_id;
                            }
                            select_day = 1;
                        }
                    }
                }
                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_titlePosition, EditorStyles.helpBox, GUILayout.Height(50)))
                {

                }
                using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_cullenderPosition, EditorStyles.helpBox))
                {
                    var month_data = core_data.GetMonthData(year_id, month_id);
                    if (month_data == null) return;

                    int start_offset = core_data.MonthOffsetStar(year_id, month_id);
                    for (int count = 0; count < start_offset;)
                    {
                        GUI.backgroundColor = Color.gray;
                        if (GUI.Button(new Rect(start_x + count * add_x, start_y, width, height), ""))
                        {
                        }
                        GUI.backgroundColor = Color.white;
                        ++count;
                    }


                    int week = 0;
                    if (start_offset == 0) week = -1;
                    for(int count = 0; count < month_data.EventDayData.Count;)
                    {
                        

                        int offset_x = (int)month_data.EventDayData[count].GetDayWeekID;
                        if(month_data.EventDayData[count].GetDayWeekID == Day_Week_ID.Sunday)
                        {
                            week += 1;
                        }
                        GUI.backgroundColor = month_data.EventDayData[count].IsStop == true || month_data.EventDayData[count].GetListDayDetails.Count > 0 ? Color.red : Color.white;
                        if(select_day == month_data.EventDayData[count].GetDay)
                        {
                            GUI.backgroundColor = Color.cyan;
                        }
                        if (GUI.Button(new Rect(start_x + offset_x * add_x, start_y + (add_y * (week)), width, height), $"{month_data.EventDayData[count].GetDay:00}"))
                        {
                            select_day = month_data.EventDayData[count].GetDay;
                        }
                        GUI.backgroundColor = Color.white;
                        ++count;
                    }

                    int end_offset = core_data.MonthOffsetFinish(year_id, month_id);


                }
            }
        }
    }
}
