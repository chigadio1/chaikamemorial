using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class DayDataDetails
{

    /// <summary>
    /// イベントフラグ
    /// </summary>
    public bool is_event = false;
    /// <summary>
    /// イベントID
    /// </summary>
    public int event_id = -1;
    /// <summary>
    /// 祝日ID
    /// </summary>
    public Day_Holiday_ID day_holiday_id = Day_Holiday_ID.NONE;


    /// <summary>
    /// 学校イベントID
    /// </summary>
    public Day_School_Event_ID day_school_event_id = Day_School_Event_ID.NONE;


    /// <summary>
    /// 誕生日キャラ
    /// </summary>
    public Character_ID birth_character_id = Character_ID.NONE;


    public Event_Occurrence_ID occurrence_id = Event_Occurrence_ID.NONE;

    public bool BinaryReader(ref BinaryReader reader)
    {
        try
        {

            is_event = reader.ReadBoolean();
            event_id = reader.ReadInt32();
            day_holiday_id = (Day_Holiday_ID)reader.ReadInt32();
            day_school_event_id = (Day_School_Event_ID)reader.ReadInt32();
            birth_character_id = (Character_ID)reader.ReadInt32();
            occurrence_id = (Event_Occurrence_ID)reader.ReadInt32();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }
    public bool BinaryWriter(ref BinaryWriter writer)
    {
        try
        {
            writer.Write(is_event);
            writer.Write(event_id);
            writer.Write((int)day_holiday_id);
            writer.Write((int)day_school_event_id);
            writer.Write((int)birth_character_id);
            writer.Write((int)occurrence_id);
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

#if UNITY_EDITOR
    public void OnGUI(ref bool is_stop)
    {
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            is_event = EditorGUILayout.Foldout(is_event, "イベント:");
            if (is_event)
            {
                EditorGUI.indentLevel++;
                event_id = EditorGUILayout.IntField("イベントID:", event_id);
                EditorGUI.indentLevel--;
            }
        }
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            birth_character_id = (Character_ID)EditorGUILayout.EnumPopup("誕生日キャラID:", birth_character_id);
            if (birth_character_id > Character_ID.NONE)
            {
                is_event = true;
            }
            if (birth_character_id == Character_ID.MAX)
            {
                birth_character_id = Character_ID.NONE;
            }
        }
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            day_holiday_id = (Day_Holiday_ID)EditorGUILayout.EnumPopup("祝日ID:", day_holiday_id);
            if (day_holiday_id > Day_Holiday_ID.NONE && day_holiday_id < Day_Holiday_ID.MAX)
            {
                is_stop = true;
            }
            if (day_holiday_id == Day_Holiday_ID.MAX)
            {
                is_stop = false;
                day_holiday_id = Day_Holiday_ID.NONE;
            }
        }
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            day_school_event_id = (Day_School_Event_ID)EditorGUILayout.EnumPopup("学校ID", day_school_event_id);

            if (day_school_event_id == Day_School_Event_ID.MAX)
            {
                day_school_event_id = Day_School_Event_ID.NONE;
            }
        }
        if (day_school_event_id != Day_School_Event_ID.NONE)
        {
            using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
            {
                occurrence_id = (Event_Occurrence_ID)EditorGUILayout.EnumPopup("発動場所ID", occurrence_id);
            }
        }
    }
#endif

}


/// <summary>
/// イベント日データ
/// </summary>
public class EventDayData
{

    /// <summary>
    /// ストップ
    /// </summary>
    bool is_stop = false;
    public bool IsStop { get { return is_stop; } }
    /// <summary>
    /// 日付
    /// </summary>
    int day_num = 0;
    public int GetDay { get { return day_num; } }


    /// <summary>
    /// 何曜日か
    /// </summary>
    Day_Week_ID day_week_id = Day_Week_ID.NONE;
    public Day_Week_ID GetDayWeekID { get { return day_week_id; } }

    private List<DayDataDetails> list_day_details = new List<DayDataDetails>();
    public List<DayDataDetails> GetListDayDetails { get { return list_day_details; } }


    public bool BinaryReader(ref BinaryReader reader)
    {
        try
        {
            is_stop = reader.ReadBoolean();
            day_num = reader.ReadInt32();
            day_week_id = (Day_Week_ID)reader.ReadInt32();
            
            int size_count = reader.ReadInt32();
            for(int i = 0; i < size_count;)
            {
                DayDataDetails add = new DayDataDetails();
                bool res = add.BinaryReader(ref reader);
                if (res == false) return false;
                list_day_details.Add(add);
                ++i;
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public bool BinaryWriter(ref BinaryWriter writer)
    {
        try
        {
            writer.Write(is_stop);
            writer.Write(day_num);

            writer.Write((int)day_week_id);

            //サイズを書き込む
            writer.Write(list_day_details.Count);
            foreach(var data in list_day_details)
            {
                bool res = data.BinaryWriter(ref writer);
                if(res == false) return false;
            }

        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public void JsonReader(Dictionary<string, object> reader)
    {

    }

    public Dictionary<string, object> JsonWriter()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        return data;
    }

    public Color DayColor()
    {

        if (day_week_id == Day_Week_ID.Sunday) return Color.red;
        if (day_week_id == Day_Week_ID.Saturday) return Color.blue;

        return Color.white;
    }

    public string DayText(bool now_day = false,bool birthday = false)
    {
        string res = day_num.ToString() + "\n";

        string add = "";

        foreach (var data in list_day_details)
        {
            if ((data.day_holiday_id > Day_Holiday_ID.NONE && data.day_holiday_id < Day_Holiday_ID.MAX) ||
                (data.day_school_event_id > Day_School_Event_ID.NONE && data.day_school_event_id < Day_School_Event_ID.MAX))
            {
                add = "●";
            }
            if (birthday)
            {
                add = "◎";
                break;
            }
        }

        if (now_day == true)
        {
            add = "★";

        }



        return res + add;

    }

    public List<string> DayDetailsText()
    {
        List<string> res = new List<string>();

        foreach (var data in list_day_details)
        {
            if (data.day_school_event_id > Day_School_Event_ID.NONE)
            {
                string add = EventDayConvert.GetSchoolEventComment(data.day_school_event_id);
                add += $" {day_num:00}日";
                res.Add(add);
            }
            if (data.day_holiday_id > Day_Holiday_ID.NONE)
            {
                string add = EventDayConvert.GetDayHolidayComment(data.day_holiday_id);
                add += $" {day_num:00}日";
                res.Add(add);
            }
            if (data.birth_character_id > Character_ID.NONE)
            {
                string add = "誕生日:" + CharacterConvert.GetCharacterComment(data.birth_character_id);
                add += $" {day_num:00}日";
                res.Add(add);
            }
        }

        return res;
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        using (new GUILayout.HorizontalScope(GUILayout.Width(250)))
        {
            is_stop = EditorGUILayout.Toggle("停止:", is_stop);
        }
        using (new GUILayout.VerticalScope(GUILayout.Width(350)))
        {
            foreach (var data in list_day_details)
            {
                data.OnGUI(ref is_stop);
            }
        }
        using (new GUILayout.VerticalScope(GUILayout.Width(350)))
        {
            if(GUILayout.Button("追加"))
            {
                list_day_details.Add(new DayDataDetails());
            }
        }
    }

    public void InitEditor(int value_day,Day_Week_ID value_day_week_iD)
    {
        day_num = value_day;
        day_week_id = value_day_week_iD;
    }

    public void FixGUI()
    {
        if(day_week_id == Day_Week_ID.Sunday || day_week_id == Day_Week_ID.Saturday || day_week_id == Day_Week_ID.Monday)
        {
            is_stop = true;
        }
    }


#endif
}
