using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class EventYearData
{
    /// <summary>
    /// 月情報
    /// </summary>
    Dictionary<Event_Months_ID, EventMonthData> dic_month = new Dictionary<Event_Months_ID, EventMonthData>();
    public Dictionary<Event_Months_ID, EventMonthData> GetMonthsData { get { return dic_month; } }
    public EventMonthData GetMonthData(Event_Months_ID month_ID)
    {
        if (dic_month.ContainsKey(month_ID) == false) return null;
        return dic_month[month_ID];
    }
    Event_Years_ID years_ID = Event_Years_ID.ONE;
    public Event_Years_ID GetYearID { get { return years_ID; } }

    public static readonly int start_year = 2020;
#if UNITY_EDITOR

    public void InitEditor(Event_Years_ID value_year_id)
    {
        years_ID = value_year_id;

        int year = (int)years_ID + start_year;
        DateTime startDate = new DateTime();
        DateTime endDate = new DateTime();
        if (years_ID == Event_Years_ID.ONE)
        {
            startDate = new DateTime(year, 4, 1); // 4月の初日
            endDate = new DateTime(year, 12, 31); // 同年の最終日
        }
        else if (years_ID == Event_Years_ID.SECOND || years_ID == Event_Years_ID.THIRD)
        {
            startDate = new DateTime(year, 1, 1); // 4月の初日
            endDate = new DateTime(year, 12, 31); // 同年の最終日
        }
        else
        {
            startDate = new DateTime(year, 1, 1); // 4月の初日
            endDate = new DateTime(year, 3, 1); // 同年の最終日
            DateTime date = endDate;

            int weekCount = 0;

            while (weekCount < 3)
            {
                if (date.DayOfWeek == DayOfWeek.Friday)
                {
                    weekCount++;
                    if (weekCount == 3)
                    {
                        // 3月の第三週目の金曜日を見つけた
                        Console.WriteLine(date.ToShortDateString());
                        break;
                    }
                }
                date = date.AddDays(1);
            }
            endDate = date;
        }

        for (DateTime monthDate = startDate; monthDate <= endDate; monthDate = monthDate.AddMonths(1))
        {
            year = monthDate.Year;
            int month = monthDate.Month;

            Event_Months_ID month_id = (Event_Months_ID)month - 1;
            // Calculate the last day of the current month
            int lastDay = DateTime.DaysInMonth(year, month);

            for (int day = 1; day <= lastDay; day++)
            {
                dic_month[month_id] = new EventMonthData();
                dic_month[month_id].InitEditor(years_ID, month_id);
            }
        }

    }

    public void SelectGUI(Event_Months_ID month_id, int day)
    {
        if (dic_month.ContainsKey(month_id) == false) return;
        dic_month[month_id].OnGUI(day);
    }

    public void FixGUI()
    {
        foreach (var data in dic_month)
        {
            data.Value.FixGUI();
        }
    }

#endif
    public bool BinaryReader(Event_Years_ID value_year_id, ref BinaryReader reader)
    {
        try
        {
            dic_month.Clear(); // ディクショナリをクリア

            // 年IDを読み込む
            years_ID = value_year_id;

            // 月データの数を読み込む
            int monthDataCount = reader.ReadInt32();

            for (int i = 0; i < monthDataCount; i++)
            {
                Event_Months_ID month_id = (Event_Months_ID)reader.ReadInt32();
                EventMonthData monthData = new EventMonthData();
                if (!monthData.BinaryReader(month_id, ref reader))
                {
                    return false; // 読み込みエラーが発生した場合、falseを返す
                }
                dic_month[month_id] = monthData;
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public bool BinaryWriter(ref BinaryWriter writer)
    {
        try
        {
            // 年IDを書き込む
            writer.Write((int)years_ID);

            // 月データの数を書き込む
            writer.Write(dic_month.Count);

            foreach (var monthData in dic_month.Values)
            {
                if (!monthData.BinaryWriter(ref writer))
                {
                    return false; // 書き込みエラーが発生した場合、falseを返す
                }
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public void JsonReader(Dictionary<string, object> reader)
    {
        if (reader.ContainsKey("years_ID"))
            years_ID = (Event_Years_ID)Enum.Parse(typeof(Event_Years_ID), reader["years_ID"].ToString());

        if (reader.ContainsKey("dic_month") && reader["dic_month"] is Dictionary<string, object>)
        {
            Dictionary<string, object> monthDataDict = (Dictionary<string, object>)reader["dic_month"];
            dic_month.Clear();
            foreach (Event_Months_ID monthID in Enum.GetValues(typeof(Event_Months_ID)))
            {
                if (monthDataDict.ContainsKey(monthID.ToString()))
                {
                    if (monthDataDict[monthID.ToString()] is Dictionary<string, object>)
                    {
                        EventMonthData monthData = new EventMonthData();
                        ;
                        monthData.JsonReader(monthID, (Dictionary<string, object>)monthDataDict[monthID.ToString()]);
                        dic_month[monthID] = monthData;
                    }
                }
            }
        }
    }

    public Dictionary<string, object> JsonWriter()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        data["years_ID"] = years_ID.ToString();

        Dictionary<string, object> monthDataDict = new Dictionary<string, object>();
        foreach (var monthData in dic_month.Values)
        {
            monthDataDict[monthData.MonthID.ToString()] = monthData.JsonWriter();
        }
        data["dic_month"] = monthDataDict;

        return data;
    }

    public List<string> MonthEventDetailsText(Event_Months_ID month_id)
    {
        if (dic_month.ContainsKey(month_id) == false) return null;
        return dic_month[month_id].MonthEventDetailsText();
    }

    public EventDayData GetNowTurnToDayData(Event_Months_ID id, int turn)
    {
        if (dic_month.ContainsKey(id) == false) return null;
        return dic_month[id].GetNowTurnToDayData(turn);
    }

    public DayDataDetails[] GetNowTurnToDayData(Event_Months_ID id, int turn,Event_Occurrence_ID event_occurrence_id)
    {
        if (dic_month.ContainsKey(id) == false) return null;
        return dic_month[id].GetNowTurnToDayData(turn,event_occurrence_id);
    }
    
}