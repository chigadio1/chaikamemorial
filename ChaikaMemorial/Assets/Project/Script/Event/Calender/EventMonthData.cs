using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

public class EventMonthData
{

    Event_Months_ID month_id = Event_Months_ID.NONE;
    public Event_Months_ID MonthID { get { return month_id; } }

    List<EventDayData> list_event_day_data = new List<EventDayData>();
    public List<EventDayData> EventDayData { get { return list_event_day_data; } }




#if UNITY_EDITOR
    public void InitEditor(Event_Years_ID year_id, Event_Months_ID value_mont_id)
    {
        month_id = value_mont_id;
        DateTime firstDayOfMonth = new DateTime((int)year_id + EventYearData.start_year, (int)month_id + 1, 1);
        DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);

        for (DateTime date = firstDayOfMonth; date <= lastDayOfMonth; date = date.AddDays(1))
        {
            EventDayData add_day = new EventDayData();

            var week_id = (Day_Week_ID)date.DayOfWeek;
            add_day.InitEditor(date.Day, week_id);
            list_event_day_data.Add(add_day);
        }



    }

    public void OnGUI(int select_day)
    {
        var day_data = list_event_day_data.Find(a => a.GetDay == select_day);
        if (day_data == null) return;
        day_data.OnGUI();
    }

    public void FixGUI()
    {
        foreach (var data in list_event_day_data)
        {
            data.FixGUI();
        }
    }
#endif

    public bool BinaryReader(Event_Months_ID value_mont_id, ref BinaryReader reader)
    {
        try
        {
            list_event_day_data.Clear(); // リストをクリア


            month_id = value_mont_id;

            // イベント日データの数を読み込む
            int eventDayCount = reader.ReadInt32();

            for (int i = 0; i < eventDayCount; i++)
            {
                EventDayData eventDay = new EventDayData();
                if (!eventDay.BinaryReader(ref reader))
                {
                    return false; // 読み込みエラーが発生した場合、falseを返す
                }
                list_event_day_data.Add(eventDay);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public bool BinaryWriter(ref BinaryWriter writer)
    {
        try
        {
            // 年IDを書き込む
            writer.Write((int)month_id);

            // イベント日データの数を書き込む
            writer.Write(list_event_day_data.Count);

            foreach (EventDayData eventDay in list_event_day_data)
            {
                if (!eventDay.BinaryWriter(ref writer))
                {
                    return false; // 書き込みエラーが発生した場合、falseを返す
                }
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public void JsonReader(Event_Months_ID id, Dictionary<string, object> reader)
    {
        month_id = id;

        if (reader.ContainsKey("list_event_day_data") && reader["list_event_day_data"] is List<object>)
        {
            List<object> eventDayList = (List<object>)reader["list_event_day_data"];
            list_event_day_data.Clear();
            foreach (object eventDayObj in eventDayList)
            {
                if (eventDayObj is Dictionary<string, object>)
                {
                    EventDayData eventDay = new EventDayData();
                    eventDay.JsonReader((Dictionary<string, object>)eventDayObj);
                    list_event_day_data.Add(eventDay);
                }
            }
        }
    }

    public Dictionary<string, object> JsonWriter()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();
        data["month_id"] = month_id.ToString();

        List<object> eventDayList = new List<object>();
        foreach (EventDayData eventDay in list_event_day_data)
        {
            eventDayList.Add(eventDay.JsonWriter());
        }
        data["list_event_day_data"] = eventDayList;

        return data;
    }

    public List<string> MonthEventDetailsText()
    {
        List<List<string>> lists = new List<List<string>>();

        foreach (var day in list_event_day_data)
        {
            var push_data = day.DayDetailsText();
            if (push_data == null) continue;
            if (push_data.Count <= 0) continue;
            lists.Add(push_data);
        }

        return CombineLists(lists);
    }

    static List<string> CombineLists(List<List<string>> inputLists)
    {
        Dictionary<string, List<int>> prefixToDays = new Dictionary<string, List<int>>();

        foreach (var inputList in inputLists)
        {
            foreach (string item in inputList)
            {
                string[] parts = item.Split(' ');
                if (parts.Length == 2 && parts[1].EndsWith("日"))
                {
                    string prefix = parts[0];
                    int day = int.Parse(parts[1].Replace("日", ""));
                    if (!prefixToDays.ContainsKey(prefix))
                    {
                        prefixToDays[prefix] = new List<int>();
                    }
                    prefixToDays[prefix].Add(day);
                }
            }
        }

        List<string> combinedList = new List<string>();

        foreach (var prefixDaysPair in prefixToDays)
        {
            string prefix = prefixDaysPair.Key;
            List<int> days = prefixDaysPair.Value;
            days.Sort();

            List<string> ranges = new List<string>();
            int startDay = days[0];
            int endDay = days[0];

            for (int i = 1; i < days.Count; i++)
            {
                if (days[i] == endDay + 1)
                {
                    endDay = days[i];
                }
                else
                {
                    ranges.Add(GetDayRange(startDay, endDay));
                    startDay = days[i];
                    endDay = days[i];
                }
            }

            ranges.Add(GetDayRange(startDay, endDay));

            foreach (string range in ranges)
            {
                combinedList.Add($"{prefix} {range}");
            }
        }

        return combinedList;
    }

    static string GetDayRange(int start, int end)
    {
        if (start == end)
        {
            return $"{start}日";
        }
        else
        {
            return $"{start}~{end}日";
        }
    }

    public EventDayData GetNowTurnToDayData(int turn)
    {
        return list_event_day_data.Find(a => a.GetDay == turn);
    }

    public  DayDataDetails[] GetNowTurnToDayData(int turn,Event_Occurrence_ID id)
    {

        var data = list_event_day_data.Find(a => a.GetDay == turn);

        return data.GetListDayDetails.FindAll(a => a.occurrence_id == id && a.is_event == true).ToArray();
    }
    
}
