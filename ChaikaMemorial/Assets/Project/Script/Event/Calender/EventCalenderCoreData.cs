using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
///Jsonデータ
[System.Serializable]
public class RootCalender
{
    [SerializeField]
    public Dictionary<string, object> root = new Dictionary<string, object>();
}

public class EventCalenderCoreData
{
    Dictionary<Event_Years_ID, EventYearData> dict_event_year_data = new Dictionary<Event_Years_ID, EventYearData>();
    public EventYearData GetYearData(Event_Years_ID years_ID)
    {
        if (dict_event_year_data.ContainsKey(years_ID) == false) return null;
        return dict_event_year_data[years_ID];
    }

    public EventMonthData GetMonthData(Event_Years_ID years_ID,Event_Months_ID month_ID)
    {
        return GetYearData(years_ID)?.GetMonthData(month_ID);
    }

    public int MonthOffsetStar(Event_Years_ID years_ID, Event_Months_ID month_ID)
    {
        var data = GetMonthData(years_ID, month_ID);
        if (data == null) return 0;


        DateTime firstDayOfMonth = new DateTime((int)years_ID + EventYearData.start_year, (int)month_ID + 1, 1);
        int offset = (int)firstDayOfMonth.DayOfWeek;
        return Mathf.Clamp(offset, (int)DayOfWeek.Sunday, (int)DayOfWeek.Saturday);

    }

    public int MonthOffsetFinish(Event_Years_ID years_ID, Event_Months_ID month_ID)
    {
        var data = GetMonthData(years_ID, month_ID);
        if (data == null) return 0;


        DateTime lastDayOfMonth = new DateTime((int)years_ID + EventYearData.start_year,(int)month_ID + 1, DateTime.DaysInMonth((int)years_ID + EventYearData.start_year, (int)month_ID + 1));
        return (int)lastDayOfMonth.DayOfWeek < 0 ? (int)lastDayOfMonth.DayOfWeek : 0;
    }



#if UNITY_EDITOR
    public void InitEditor()
    {
        dict_event_year_data[Event_Years_ID.ONE] = new EventYearData();
        dict_event_year_data[Event_Years_ID.ONE].InitEditor(Event_Years_ID.ONE);

        dict_event_year_data[Event_Years_ID.SECOND] = new EventYearData();
        dict_event_year_data[Event_Years_ID.SECOND].InitEditor(Event_Years_ID.SECOND);

        dict_event_year_data[Event_Years_ID.THIRD] = new EventYearData();
        dict_event_year_data[Event_Years_ID.THIRD].InitEditor(Event_Years_ID.THIRD);

        dict_event_year_data[Event_Years_ID.FOURTH] = new EventYearData();
        dict_event_year_data[Event_Years_ID.FOURTH].InitEditor(Event_Years_ID.FOURTH);
    }

    public void SelectGUI(Event_Years_ID year_id,Event_Months_ID months_ID,int day)
    {
        if (dict_event_year_data.ContainsKey(year_id) == false) return;
        dict_event_year_data[year_id].SelectGUI(months_ID,day);
    }

    public void FixGUI()
    {
        foreach(var data in dict_event_year_data)
        {
            data.Value.FixGUI();
        }
    }
#endif

    public bool BinaryReader(ref BinaryReader reader)
    {
        try
        {
            dict_event_year_data.Clear(); // ディクショナリをクリア

            // イベント年データの数を読み込む
            int eventYearDataCount = reader.ReadInt32();

            for (int i = 0; i < eventYearDataCount; i++)
            {
                Event_Years_ID event_Years_ID = (Event_Years_ID)reader.ReadInt32();
                EventYearData yearData = new EventYearData();
                if (!yearData.BinaryReader(event_Years_ID,ref reader))
                {
                    return false; // 読み込みエラーが発生した場合、falseを返す
                }
                dict_event_year_data[event_Years_ID] = yearData;
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public bool BinaryWriter(ref BinaryWriter writer)
    {
        try
        {
            // イベント年データの数を書き込む
            writer.Write(dict_event_year_data.Count);

            foreach (var yearData in dict_event_year_data.Values)
            {
                if (!yearData.BinaryWriter(ref writer))
                {
                    return false; // 書き込みエラーが発生した場合、falseを返す
                }
            }
        }
        catch (Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }
        return true;
    }

    public void JsonReader(Dictionary<string, object> reader)
    {
        if (reader.ContainsKey("dict_event_year_data"))
        {
            var yearDataDict = reader["dict_event_year_data"];
            dict_event_year_data.Clear();
            foreach (Event_Years_ID yearID in Enum.GetValues(typeof(Event_Years_ID)))
            {
                //if (yearDataDict.ContainsKey(yearID.ToString()))
                //{
                //    if (yearDataDict[yearID.ToString()] is Dictionary<string, object>)
                //    {
                //        EventYearData yearData = new EventYearData();
                //        yearData.JsonReader((Dictionary<string, object>)yearDataDict[yearID.ToString()]);
                //        dict_event_year_data[yearID] = yearData;
                //    }
                //}
            }
        }
    }

    public Dictionary<string, object> JsonWriter()
    {
        Dictionary<string, object> data = new Dictionary<string, object>();

        Dictionary<string, object> yearDataDict = new Dictionary<string, object>();
        foreach (var yearData in dict_event_year_data.Values)
        {
            yearDataDict[yearData.GetYearID.ToString()] = yearData.JsonWriter();
        }
        data["dict_event_year_data"] = yearDataDict;

        return data;
    }

    public void Save()
    {
        string path = Application.dataPath + $"/Event/Calender.bin";

        

        using (FileStream fileStream = new FileStream(path, FileMode.Create))
        {
            BinaryWriter writer = new BinaryWriter(fileStream);

            BinaryWriter(ref writer);
            writer.Flush();
            writer.Close();

        }
    }

    public void SaveEditor()
    {
        Save();
        var json_data = JsonWriter();
        RootCalender root = new RootCalender();
        root.root = json_data;
        string path = Application.dataPath + $"/Project/Assets/Editor/Data/Event/Calender.json";
        using (FileStream fileStream = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter fileStreamWriter = new StreamWriter(fileStream))
            {
                string data = JsonConvert.SerializeObject(root);
                fileStreamWriter.Write(data);
            }

        }
    }

    public void Load()
    {
        string path = Application.dataPath + $"/Event/Calender.bin";
        if (System.IO.File.Exists(path) == true)
        {
            path = Application.dataPath + $"/Event/Calender.bin";
            if (System.IO.File.Exists(path) == false)
            {
                Debug.Log("ファイルが存在しません");
                return;
            }
            else
            {


                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    BinaryReader reader = new BinaryReader(fileStream);

                    BinaryReader(ref reader);
                    reader.Close();
                }
                return;
            }
        }
    }

    public void LoadEditor()
    {
        string path = Application.dataPath + $"/Project/Assets/Editor/Data/Event/Calender.json";
        if (System.IO.File.Exists(path) == false)
        {
            Debug.Log("ファイルが存在しません");
            return;
        }
        using (FileStream fileStream = new FileStream(path, FileMode.Open))
        {
            using (StreamReader fileStreamReader = new StreamReader(fileStream))
            {
                string jsonStr = fileStreamReader.ReadToEnd();
                var data = JsonConvert.DeserializeObject<RootCalender>(jsonStr);
                JsonReader(data.root);
                data = null;
                fileStreamReader.Close();
            }
        
        }

        
    }

    public List<string> MonthEventDetailsText(Event_Years_ID year_id,Event_Months_ID month_id)
    {
        if (dict_event_year_data.ContainsKey(year_id) == false) return null;
        return dict_event_year_data[year_id].MonthEventDetailsText(month_id);
    }

    public (Event_Years_ID,Event_Months_ID) GetNouTurnYearMonthID(int turn)
    {
        var startDate = new DateTime((int)Event_Years_ID.ONE + EventYearData.start_year, 4, 1); // 4月の初日
        startDate = startDate.AddDays(turn);

        Event_Years_ID res_year_id =  (Event_Years_ID)(startDate.Year - EventYearData.start_year);
        Event_Months_ID res_month_id = (Event_Months_ID)(startDate.Month - 1);
        return (res_year_id, res_month_id);
    }


    public EventDayData GetNowTurnToDayData(int turn)
    {
        EventDayData dayData = null;
        var startDate = new DateTime((int)Event_Years_ID.ONE + EventYearData.start_year, 4, 1); // 4月の初日
        startDate  = startDate.AddDays(turn);

        var year_id = GetConvertYearID(startDate);
        var month_id = GetConverMonthID(startDate);
        if (dict_event_year_data.ContainsKey(year_id) == false) return null;

        dayData =  dict_event_year_data[year_id].GetNowTurnToDayData(month_id, startDate.Day);

        return dayData;
    }

    public DayDataDetails[] GetNowTurnToDayData(int turn,Event_Occurrence_ID id)
    {
        DayDataDetails[] dayData = null;
        var startDate = new DateTime((int)Event_Years_ID.ONE + EventYearData.start_year, 4, 1); // 4月の初日
        startDate = startDate.AddDays(turn);

        var year_id = GetConvertYearID(startDate);
        var month_id = GetConverMonthID(startDate);
        if (dict_event_year_data.ContainsKey(year_id) == false) return null;

        dayData = dict_event_year_data[year_id].GetNowTurnToDayData(month_id, startDate.Day, id);
        dayData = dayData.Where(a => a.event_id > 0).ToArray();
        dayData = dayData.Where(a => PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.GetFlag(EventIDExtensions.EventIDConvert(a.event_id)) == false).ToArray();
        return dayData;
    }

    private Event_Years_ID GetConvertYearID(DateTime dateTime)
    {
        int year = dateTime.Year;

        year = year - EventYearData.start_year;

        return (Event_Years_ID)year;
    }

    private Event_Months_ID GetConverMonthID(DateTime dateTime)
    {
        int month = dateTime.Month;


        return (Event_Months_ID)month - 1;
    }


}
