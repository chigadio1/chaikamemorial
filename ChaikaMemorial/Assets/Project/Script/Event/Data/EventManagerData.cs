using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Threading.Tasks;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// イベント管理データ
/// </summary>
public class EventManagerData
{
    /// <summary>
    /// イベントリスト
    /// </summary>
    private List<EventData> event_list = new List<EventData>();

#if UNITY_EDITOR
    public List<EventData> EventList { get { return event_list; } set { event_list = value; } }

    private Vector2 scroll = Vector2.zero;

    public void OnGUI()
    {
        EditorGUILayout.BeginVertical();

        scroll = EditorGUILayout.BeginScrollView(scroll);
        for(int count = 0; count < event_list.Count;)
        {
            event_list[count].OnGUI();
            if (GUILayout.Button("×"))
            {
                event_list.RemoveAt(count);
            }
            ++count;
        }

        EditorGUILayout.EndScrollView();

        EditorGUILayout.EndVertical();

        if(GUILayout.Button("Add"))
        {
            event_list.Add(new EventData());
        }
        if (GUILayout.Button("Save"))
        {
            SaveBinary();
            GenerateEnum();
            GeneratePlayerEventSet();
        }
        if (GUILayout.Button("Load"))
        {
            event_list.Clear();
            LoadBinary();
        }
    }

    public void GenerateEnum()
    {
        string path = Application.dataPath;
        path += "/Project/Script/Event/Enum";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }
        path += "/Event_ID.cs";


        using (StreamWriter writer = new StreamWriter(path))
        {
            writer.WriteLine($"public enum Event_ID " + "{");
            writer.WriteLine($"    NONE = -1,");

            foreach(var data in event_list)
            {
                writer.WriteLine($"    ID_{data.GetEventID},");
            }

            writer.WriteLine($"    END,");

            writer.WriteLine("}");
        }
    }

    public void GeneratePlayerEventSet()
    {
        string path = Application.dataPath;
        path += "/Project/Script/Player/Data";
        if (System.IO.Directory.Exists(path) == false)
        {
            System.IO.Directory.CreateDirectory(path);
        }
        path += "/PlayerEventData.cs";



        using (StreamWriter writer = new StreamWriter(path))
        {
            writer.WriteLine("using System.Collections;");
            writer.WriteLine("using UnityEngine;");
            writer.WriteLine("[System.Serializable]");
            writer.WriteLine($"public class PlayerEventData " + "{");

            writer.WriteLine("    [SerializeField]");
            writer.WriteLine("    BitArray event_bits = new BitArray((int)Event_ID.END);");

            writer.WriteLine("");

            writer.WriteLine("    public void On(Event_ID id) {");
            writer.WriteLine("        if(id <= Event_ID.NONE || id >= Event_ID.END) return;");
            writer.WriteLine("        event_bits[(int)id] = true;");
            writer.WriteLine("    }");

            writer.WriteLine("    public bool GetFlag(Event_ID id) {");
            writer.WriteLine("        if(id <= Event_ID.NONE || id >= Event_ID.END) return false;");
            writer.WriteLine("        return event_bits[(int)id];");
            writer.WriteLine("    }");

            writer.WriteLine("    public void AllOff(Event_ID id) {");
            writer.WriteLine("        if(id <= Event_ID.NONE || id >= Event_ID.END) return;");
            writer.WriteLine("        for(int count = 0; count < (int)Event_ID.END;count++) event_bits[count]=false;");
            writer.WriteLine("    }");


            writer.WriteLine("}");
        }
    }
#endif

    /// <summary>
    /// ロードが終了しているかどうか
    /// </summary>
    private bool is_load_end = false;
    public bool GetIsLoadEnd { get { return is_load_end; } }

    /// <summary>
    /// ロードパス
    /// </summary>
    private string load_path = "";

    public void LoadBinaryAsync()
    {
        Task.Run(() => LoadBinary());
    }

    public void SaveBinaryAsync()
    {
        Task.Run(() => SaveBinary());
    }

    public  void LoadBinary()
    {


        load_path = Application.dataPath + "/Event";
        if (!Directory.Exists(load_path))
        {
            Directory.CreateDirectory(load_path);
        }
        load_path += "/all_event.bin";
        if (!File.Exists(load_path))
        {
            return;
        }

        using (FileStream fileStream = new FileStream(load_path, FileMode.Open))
        using (BinaryReader reader = new BinaryReader(fileStream))
        {
            // ファイルの終端に達するまで読み込みを続けるループ
            while (fileStream.Position < fileStream.Length)
            {
                EventData add_data = new EventData();
                bool result =  add_data.LoadBinary(reader);
                if(!result)
                {
                    return;
                }
                event_list.Add(add_data);
            }
        }

        is_load_end=true;
    }


    public void SaveBinary()
    {


        load_path = Application.dataPath + "/Event";
        if (!Directory.Exists(load_path))
        {
            Directory.CreateDirectory(load_path);
        }
        load_path += "/all_event.bin";


        using (FileStream fileStream = new FileStream(load_path, FileMode.Create))
        {
            using (BinaryWriter writer = new BinaryWriter(fileStream))
            {
                int index_count = 0;
                foreach(var data in event_list)
                {
                    data.SaveBinary(writer,index_count);
                    ++index_count;
                }
                writer.Flush();
                writer.Close();
            }
        }
#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh();
#endif
    }
    public struct FindEventData
    {
        public int event_id;
        public EventData event_data;
    }
    public FindEventData FindID(PlayerCoreData core_data)
    {
        FindEventData res = new FindEventData();
        res.event_data = null;
        res.event_id = 0;


        List<EventData> list_data = new List<EventData>();

        foreach (var data in event_list)
        {
            bool check = data.PlayerCoreDataConditions(core_data);
            if (check)
            {
                list_data.Add(data);
            }
        }

        list_data.Sort((a, b) => a.GetPriorityID.CompareTo(b.GetPriorityID));

        foreach (var data in list_data)
        {
            float random = Random.Range(0, 101);
            if (data.GetRandomNum >= random)
            {
                res.event_id = data.GetEventID;
                res.event_data = data;
                return res;
            }
        }


        return res;
    }



}
