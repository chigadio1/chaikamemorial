using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// イベントデータ
/// </summary>
public class EventData
{
    /// <summary>
    /// イベントID
    /// </summary>
    private int event_id;
    public int GetEventID { get { return event_id; } }

    /// <summary>
    /// 優先度ID
    /// </summary>  
    private int priority_id;
    public int GetPriorityID { get { return priority_id;} }

    /// <summary>
    /// インデックスID
    /// </summary>
    private int index_id;
    public int GetIndexID { get { return index_id; } }
    /// <summary>
    /// イベントアクションID
    /// </summary>
    private Event_Actions_ID event_action_id = Event_Actions_ID.NONE;
    public Event_Actions_ID GetEventActionID { get { return event_action_id;} }

    /// <summary>
    /// イベント年ID(最小)
    /// </summary>
    private Event_Years_ID event_min_years_id = Event_Years_ID.NONE;
    public Event_Years_ID GetEventMinYearsID { get { return event_min_years_id;} }

    /// <summary>
    /// イベント年ID(最大)
    /// </summary>
    private Event_Years_ID event_max_years_id = Event_Years_ID.NONE;
    public Event_Years_ID GetEventmaxYearsID { get { return event_max_years_id; } }


    /// <summary>
    /// イベント月ID(最小)
    /// </summary>
    private Event_Months_ID event_min_months_id = Event_Months_ID.NONE;
    public Event_Months_ID GetEventMinMonthsID { get { return event_min_months_id; } }

    /// <summary>
    /// イベント月ID(最大)
    /// </summary>
    private Event_Months_ID event_max_months_id = Event_Months_ID.NONE;
    public Event_Months_ID GetEventmaxMonthsID { get { return event_max_months_id; } }

    /// <summary>
    /// イベント週ID(最小)
    /// </summary>
    private Event_Weeks_ID event_min_weeks_id = Event_Weeks_ID.NONE;
    public Event_Weeks_ID GetEventMinweeksID { get { return event_min_weeks_id; } }

    /// <summary>
    /// イベント週ID(最大)
    /// </summary>
    private Event_Weeks_ID event_max_weeks_id = Event_Weeks_ID.NONE;
    public Event_Weeks_ID GetEventmaxweeksID { get { return event_max_weeks_id; } }

    /// <summary>
    /// プレイヤーステータス
    /// </summary>
    private int[] player_status_param_array = new int[(int)Status_ID.MAX];

    public int[] GetPlayerStatusParamArray { get { return player_status_param_array; } }

    private int[] favo_character_param_array = new int[(int)Character_ID.MAX];

    public int[] GetFavoCharacterParamArray { get { return favo_character_param_array; } }

    /// <summary>
    /// 一回限りイベント
    /// </summary>
    private bool one_only_flag = false;
    public bool GetOnlyFlag { get { return one_only_flag; } }
    /// <summary>
    /// ランダム数値
    /// </summary>
    private int random_num = 100;
    public int GetRandomNum { get { return random_num; } }
    public bool LoadBinary(BinaryReader reader)
    {
        try
        {
            event_id = reader.ReadInt32();
            priority_id = reader.ReadInt32();
            index_id = reader.ReadInt32();
            event_action_id = (Event_Actions_ID)reader.ReadInt32();
            event_min_years_id = (Event_Years_ID)reader.ReadInt32();
            event_max_years_id = (Event_Years_ID)reader.ReadInt32();
            event_min_months_id = (Event_Months_ID)reader.ReadInt32();
            event_max_months_id = (Event_Months_ID)reader.ReadInt32();
            event_min_weeks_id = (Event_Weeks_ID)reader.ReadInt32();
            event_max_weeks_id = (Event_Weeks_ID)reader.ReadInt32();
            one_only_flag = reader.ReadBoolean();
            random_num = reader.ReadInt32();
            for (int count = 0; count < (int)Status_ID.MAX;)
            {
                player_status_param_array[count] = reader.ReadInt32();
                ++count;
            }

            for (int count = 0; count < (int)Character_ID.MAX;)
            {
                favo_character_param_array[count] = reader.ReadInt32();
                ++count;
            }
        }
        catch(Exception ex)
        {
            Debug.LogError(ex.Message);
            return false;
        }

        return true;
    }

    public bool SaveBinary(BinaryWriter writer,int index)
    {
        try
        {
            writer.Write(event_id);
            writer.Write(priority_id);
            writer.Write(index);
            writer.Write((int)event_action_id);
            writer.Write((int)event_min_years_id);
            writer.Write((int)event_max_years_id);
            writer.Write((int)event_min_months_id);
            writer.Write((int)event_max_months_id);
            writer.Write((int)event_min_weeks_id);
            writer.Write((int)event_max_weeks_id);
            writer.Write(one_only_flag);
            writer.Write(random_num);
            for (int count = 0; count < (int)Status_ID.MAX;)
            {
                writer.Write(player_status_param_array[count]);
                ++count;
            }

            for (int count = 0; count < (int)Character_ID.MAX;)
            {
                writer.Write(favo_character_param_array[count]);
                ++count;
            }

        }
        catch (Exception ex)
        {
#if UNITY_EDITOR
            Debug.LogError(ex.Message);
            return false;
#endif
        }

        return true;
    }

    public int GetBinarySize()
    {
        int size = 0;
        size += sizeof(int);
        size += sizeof(int);
        size += sizeof(int);
        size += sizeof(int);
        size += sizeof(int);
        size += sizeof(int);
        size += sizeof(int);
        return size;
    }

    public bool PlayerCoreDataConditions(PlayerCoreData player)
    {
        bool res = true;

        res &= player.GetPlayerStatusData?.GetStatus(Status_ID.STUDY) >= player_status_param_array[(int)Status_ID.STUDY];
        res &= player.GetPlayerStatusData?.GetStatus(Status_ID.EXERCISE) >= player_status_param_array[(int)Status_ID.EXERCISE];
        res &= player.GetPlayerStatusData?.GetStatus(Status_ID.CHARM) >= player_status_param_array[(int)Status_ID.CHARM];
        res &= player.GetPlayerStatusData?.GetYearID() >= event_min_years_id && player.GetPlayerStatusData?.GetYearID() <= event_max_years_id;
        res &= player.GetPlayerStatusData?.GetMonthID() >= event_min_months_id && player.GetPlayerStatusData?.GetMonthID() <= event_max_months_id;
        res &= player.GetPlayerStatusData?.GetWeekID() >= event_min_weeks_id && player.GetPlayerStatusData?.GetWeekID() <= event_max_weeks_id;
        for (int count = 0; count < (int)Character_ID.MAX;)
        {
            Character_ID character = (Character_ID)count;

            res &= player.GetCharacterFavorabilityData?.GetFavorability(character) >= favo_character_param_array[(int)character];

            ++count;
        }

        return res;
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("イベントID:", GUILayout.Width(100));
        event_id = EditorGUILayout.IntField("", event_id, GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("優先度ID  :", GUILayout.Width(100));
        priority_id = EditorGUILayout.IntField("", priority_id, GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("ランダム:", GUILayout.Width(100));
        random_num = EditorGUILayout.IntSlider(random_num, 0,100,GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("一回限りイベント  :", GUILayout.Width(100));
        one_only_flag = EditorGUILayout.Toggle("",one_only_flag, GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();


        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("優先度アクション:", GUILayout.Width(100));
        event_action_id = (Event_Actions_ID)EditorGUILayout.EnumPopup("", event_action_id);
        EditorGUILayout.EndHorizontal();


        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("発生年ID:", GUILayout.Width(100));
        EditorGUILayout.LabelField("発生年(最小)ID:", GUILayout.Width(100));
        event_min_years_id = (Event_Years_ID)EditorGUILayout.EnumPopup("", event_min_years_id);
        EditorGUILayout.LabelField("~", GUILayout.Width(120));
        EditorGUILayout.LabelField("発生年(最大)ID:", GUILayout.Width(100));
        event_max_years_id = (Event_Years_ID)EditorGUILayout.EnumPopup("", event_max_years_id);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("発生月ID:", GUILayout.Width(100));
        EditorGUILayout.LabelField("発生月(最小)ID:", GUILayout.Width(100));
        event_min_months_id = (Event_Months_ID)EditorGUILayout.EnumPopup("", event_min_months_id);
        EditorGUILayout.LabelField("~", GUILayout.Width(120));
        EditorGUILayout.LabelField("発生月(最大)ID:", GUILayout.Width(100));
        event_max_months_id = (Event_Months_ID)EditorGUILayout.EnumPopup("", event_max_months_id);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("発生週ID:", GUILayout.Width(100));
        EditorGUILayout.LabelField("発生週(最小)ID:", GUILayout.Width(100));
        event_min_weeks_id = (Event_Weeks_ID)EditorGUILayout.EnumPopup("", event_min_weeks_id);
        EditorGUILayout.LabelField("~", GUILayout.Width(120));
        EditorGUILayout.LabelField("発生週(最大)ID:", GUILayout.Width(100));
        event_max_weeks_id = (Event_Weeks_ID)EditorGUILayout.EnumPopup("", event_max_weeks_id);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("勉学:", GUILayout.Width(100));
        player_status_param_array[(int)Status_ID.STUDY] = EditorGUILayout.IntField("", player_status_param_array[(int)Status_ID.STUDY], GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("運動:", GUILayout.Width(100));
        player_status_param_array[(int)Status_ID.EXERCISE] = EditorGUILayout.IntField("", player_status_param_array[(int)Status_ID.EXERCISE], GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("魅力:", GUILayout.Width(100));
        player_status_param_array[(int)Status_ID.CHARM] = EditorGUILayout.IntField("", player_status_param_array[(int)Status_ID.CHARM], GUILayout.Width(150));
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndVertical();

        for (int count = 0; count < (int)Character_ID.MAX;)
        {
            if(count % 3 == 0)
            {
                EditorGUILayout.BeginVertical();
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField($"{Enum.GetName(typeof(Character_ID),(Character_ID)count)}:", GUILayout.Width(100));
            favo_character_param_array[count] = EditorGUILayout.IntField("", favo_character_param_array[count], GUILayout.Width(150));
            EditorGUILayout.EndHorizontal();

            if (count % 3 == 2)
            {
                EditorGUILayout.EndVertical();
            }

            ++count;
        }



        EditorGUILayout.EndHorizontal();
    }
#endif




}
