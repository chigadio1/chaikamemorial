using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json;
#if UNITY_EDITOR
using UnityEditor;
#endif



/// <summary>
/// ベースイベントデータ
/// </summary>
public class BaseEventData
{
    public BaseEventData()
    {

    }
    public BaseEventData(int value_event_id)
    {
        event_id = value_event_id;
    }
    /// <summary>
    /// イベントID
    /// </summary>
    protected int event_id = 0;
    public int GetEventID { get { return event_id; } }

#if UNITY_EDITOR
    public void SetEventIDEditor(int id)
    {
        event_id = id;
    }
#endif
    /// <summary>
    /// イベントが一回だけか
    /// </summary>
    protected bool is_one_event = false;
    public bool IsOneEvent { get { return is_one_event; } }

    /// <summary>
    /// 発生範囲(最大年)
    /// </summary>
    protected Event_Years_ID max_event_year_id = Event_Years_ID.FOURTH;

    /// <summary>
    /// 発生範囲(最小年)
    /// </summary>
    protected Event_Years_ID min_event_year_id = Event_Years_ID.ONE;

    /// <summary>
    /// 発生範囲(最大月)
    /// </summary>
    protected Event_Months_ID max_event_month_id = Event_Months_ID.December;

    /// <summary>
    /// 発生範囲(最小月)
    /// </summary>
    protected Event_Months_ID min_event_month_id = Event_Months_ID.January;

    /// <summary>
    /// ステータス以上
    /// </summary>
    protected Dictionary<Status_ID, int> status_more_dict = new Dictionary<Status_ID, int>();

    /// <summary>
    /// 好感度以上
    /// </summary>
    protected Dictionary<Character_ID, int> character_favo_more_dict = new Dictionary<Character_ID, int>();

    /// <summary>
    /// 対応キャラクター
    /// </summary>
    protected BitArray character_select = new BitArray((int)Character_ID.MAX);

    /// <summary>
    /// 発生済み
    /// </summary>
    protected List<int> list_already_occurred_event_id = new List<int>();

    /// <summary>
    /// 発生確率
    /// </summary>
    private int probability = 100;

    /// <summary>
    /// 優先度
    /// </summary>
    private int priority = 1;
    public int GetPriority { get { return priority; } }

    /// <summary>
    /// 一致するか
    /// </summary>
    /// <param name="year_id"></param>
    /// <param name="month_id"></param>
    /// <param name="status_dict"></param>
    /// <param name="character_favo_dict"></param>
    /// <returns></returns>
    public bool ConditionMatching(Event_Years_ID year_id,Event_Months_ID month_id,Dictionary<Status_ID,int> status_dict,Dictionary<Character_ID,int> character_favo_dict, BitArray character_unlock,
        BitArray occurred_event)
    {
        bool res = true;
        res &= min_event_year_id <= year_id && max_event_year_id >= year_id;
        res &= min_event_month_id <= month_id && max_event_month_id >= month_id;

        foreach(var status in status_dict)
        {
            if(status_more_dict.ContainsKey(status.Key))
            {
                res &= status_more_dict[status.Key] <= status.Value;
            }
        }


        foreach (var favo in character_favo_dict)
        {
            if (character_favo_more_dict.ContainsKey(favo.Key))
            {
                res &= character_favo_more_dict[favo.Key] <= favo.Value;
            }
        }

        if (character_unlock == null) return false;
        // character_select と character_unlock をビット毎に比較
        for (int i = 0; i < (int)Character_ID.MAX; i++)
        {
            bool selectBit = character_select[i];
            // selectBit が0の場合は比較しない
            if (!selectBit)
                continue;
            bool unlockBit = character_unlock[i];
            res &= (selectBit == unlockBit); // 両方が1の場合に条件を満たすと仮定しています
        }

        if (occurred_event == null) return false;

        //すでに発生したかみる
        Event_ID find_id = EventIDExtensions.EventIDConvert(event_id);
        if (find_id <= Event_ID.NONE || find_id >= Event_ID.MAX) return false;
        if (occurred_event.Length <= find_id.GetInt()) return false;
        if (occurred_event[find_id.GetInt()] == true) return false;

        //事前に発生しているのを確認
        foreach(var data in list_already_occurred_event_id)
        {
            find_id = EventIDExtensions.EventIDConvert(data);
            if (find_id <= Event_ID.NONE || find_id >= Event_ID.MAX) return false;
            if (occurred_event.Length <= find_id.GetInt()) return false;
            if (occurred_event[find_id.GetInt()] == false) return false;
        }

        int value = UnityEngine.Random.Range(1, 101);
        res &= value <= probability;


        return res;
    }

    public virtual void BinaryReader(ref BinaryReader reader)
    {
        event_id = reader.ReadInt32();
        is_one_event = reader.ReadBoolean();
        probability = reader.ReadInt32();
        priority = reader.ReadInt32();
        max_event_year_id = (Event_Years_ID)reader.ReadInt32();
        min_event_year_id = (Event_Years_ID)reader.ReadInt32();
        max_event_month_id = (Event_Months_ID)reader.ReadInt32();
        min_event_month_id = (Event_Months_ID)reader.ReadInt32();

        int size = reader.ReadInt32();
        status_more_dict.Clear();
        for(int count = 0; count < size;)
        {
            Status_ID id = (Status_ID)reader.ReadInt32();
            int num = reader.ReadInt32();
            if(status_more_dict.ContainsKey(id) == false)
            {
                status_more_dict.Add(id, num);
            }
            ++count;
        }

        size = reader.ReadInt32();
        character_favo_more_dict.Clear();
        for (int count = 0; count < size;)
        {
            Character_ID id = (Character_ID)reader.ReadInt32();
            int num = reader.ReadInt32();
            if (character_favo_more_dict.ContainsKey(id) == false)
            {
                character_favo_more_dict.Add(id, num);
            }
            ++count;
        }

        size = reader.ReadInt32();
        character_select = new BitArray(size);
        for(int count = 0; count < size;)
        {
            character_select[count] = reader.ReadBoolean();
            ++count;
        }

        size = reader.ReadInt32();
        list_already_occurred_event_id.Clear();
        list_already_occurred_event_id = new List<int>();
        for(int count = 0; count < size;)
        {
            list_already_occurred_event_id.Add(reader.ReadInt32());
            ++count;
        }
    }

    public virtual void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(event_id);
        writer.Write(is_one_event);
        writer.Write(probability);
        writer.Write(priority);
        writer.Write((int)max_event_year_id);
        writer.Write((int)min_event_year_id);
        writer.Write((int)max_event_month_id);
        writer.Write((int)min_event_month_id);

        writer.Write(status_more_dict.Count);
        foreach(var data in status_more_dict)
        {
            writer.Write((int)data.Key);
            writer.Write(data.Value);
        }

        writer.Write(character_favo_more_dict.Count);
        foreach (var data in character_favo_more_dict)
        {
            writer.Write((int)data.Key);
            writer.Write(data.Value);
        }

        writer.Write(character_select.Length);

        // ビットセットの内容をバイナリに書き込む
        foreach (bool bit in character_select)
        {
            writer.Write(bit);
        }

        writer.Write(list_already_occurred_event_id.Count);
        foreach(var data in list_already_occurred_event_id)
        {
            writer.Write(data);
        }
    }

    public virtual void JsonReader(ref Dictionary<string,object> reader)
    {
        if (reader.ContainsKey("event_id"))
            event_id = Convert.ToInt32(reader["event_id"]);

        if (reader.ContainsKey("is_one_event"))
            is_one_event = Convert.ToBoolean(reader["is_one_event"]);

        if (reader.ContainsKey("probability"))
            probability = Convert.ToInt32(reader["probability"]);

        if (reader.ContainsKey("priority"))
            priority = Convert.ToInt32(reader["priority"]);

        if (reader.ContainsKey("max_event_year_id"))
            max_event_year_id = (Event_Years_ID)Convert.ToInt32(reader["max_event_year_id"]);

        if (reader.ContainsKey("min_event_year_id"))
            min_event_year_id = (Event_Years_ID)Convert.ToInt32(reader["min_event_year_id"]);

        if (reader.ContainsKey("max_event_month_id"))
            max_event_month_id = (Event_Months_ID)Convert.ToInt32(reader["max_event_month_id"]);

        if (reader.ContainsKey("min_event_month_id"))
            min_event_month_id = (Event_Months_ID)Convert.ToInt32(reader["min_event_month_id"]);



        status_more_dict.Clear();
        if (reader.ContainsKey("status_dict_size"))
        {
            string parent = "status_dict_size";
            int size = Convert.ToInt32(reader[parent]);
            for (int count = 0; count < size;)
            {
                Status_ID id = (Status_ID)Enum.Parse(typeof(Status_ID), count.ToString());
    

                string name = Enum.GetName(typeof(Status_ID), id);

                Status_ID value_id = Status_ID.NONE;
                int value = 0;
                if (reader.ContainsKey(parent + "_" + name + "_ID"))
                {
                    value_id = (Status_ID)Convert.ToInt32(reader[parent + "_" + name + "_ID"]);
                }
                if (reader.ContainsKey(parent + "_" + name + "_NUM"))
                {
                    value = Convert.ToInt32(reader[parent + "_" + name + "_NUM"]);
                }
                if (value_id > Status_ID.NONE)
                {
                    status_more_dict.Add(value_id, value);
                }

                ++count;
            }
        }

        character_favo_more_dict.Clear();
        if (reader.ContainsKey("character_favo_more_dict_size"))
        {
            string parent = "character_favo_more_dict_size";
            int size = Convert.ToInt32(reader[parent]);
            for (int count = 0; count < size;)
            {
                Character_ID id = (Character_ID)Enum.Parse(typeof(Character_ID), count.ToString());


                string name = Enum.GetName(typeof(Character_ID), id);

                Character_ID value_id = Character_ID.NONE;
                int value = 0;
                if (reader.ContainsKey(parent + "_" + name + "_ID"))
                {
                    value_id = (Character_ID)Convert.ToInt32(reader[parent + "_" + name + "_ID"]);
                }
                if (reader.ContainsKey(parent + "_" + name + "_NUM"))
                {
                    value = Convert.ToInt32(reader[parent + "_" + name + "_NUM"]);
                }
                if (value_id > Character_ID.NONE)
                {
                    character_favo_more_dict.Add(value_id, value);
                }

                ++count;
            }
        }

        if (reader.ContainsKey("character_select_size"))
        {
            string parent = "character_select_size";
            int size = Convert.ToInt32(reader[parent]);

            character_select = new BitArray(size);
            for (int count = 0; count < size;)
            {
                Character_ID id = (Character_ID)Enum.Parse(typeof(Character_ID), count.ToString());


                string name = Enum.GetName(typeof(Character_ID), id);

                 bool value = false;
                if (reader.ContainsKey(parent + "_" + name + "_ID"))
                {
                    value = Convert.ToBoolean(reader[parent + "_" + name + "_ID"]);
                }
                character_select[count] = value;
                ++count;
            }
        }

        if(reader.ContainsKey("list_already_occurred_event_id_size"))
        {
            string parent = "list_already_occurred_event_id_size";
            int size = (Convert.ToInt32(reader[parent]));
            list_already_occurred_event_id.Clear();
            for(int count = 0;count < size;)
            {
                string id = parent + $"_{count}";
                if(reader.ContainsKey(id))
                {
                    list_already_occurred_event_id.Add(Convert.ToInt32(reader[id]));
                }
                ++count;
            }
        }

    }

    public virtual Dictionary<string,object> JsonWriter()
    {
        Dictionary<string, object> res = new Dictionary<string, object>();

        res.Add("event_id", event_id);
        res.Add("is_one_event", is_one_event);
        res.Add("probability", probability);
        res.Add("priority", priority);
        res.Add("max_event_year_id", (int)max_event_year_id);
        res.Add("min_event_year_id", (int)min_event_year_id);
        res.Add("max_event_month_id", (int)max_event_month_id);
        res.Add("min_event_month_id", (int)min_event_month_id);

        // Writing status_more_dict
        res.Add("status_dict_size", status_more_dict.Count);
        int count = 0;
        foreach (var kvp in status_more_dict)
        {
            string name = Enum.GetName(typeof(Status_ID), kvp.Key);
            res.Add($"status_dict_size_{name}_ID", (int)kvp.Key);
            res.Add($"status_dict_size_{name}_NUM", kvp.Value);
            ++count;
        }

        // Writing character_favo_more_dict
        res.Add("character_favo_more_dict_size", character_favo_more_dict.Count);
        count = 0;
        foreach (var kvp in character_favo_more_dict)
        {
            string name = Enum.GetName(typeof(Character_ID), kvp.Key);
            res.Add($"character_favo_more_dict_size_{name}_ID", (int)kvp.Key);
            res.Add($"character_favo_more_dict_size_{name}_NUM", kvp.Value);
            ++count;
        }

        res.Add("character_select_size", character_select.Count);
        count = 0;
        foreach (Character_ID id in Enum.GetValues(typeof(Character_ID)))
        {
            if (id == Character_ID.NONE || id == Character_ID.MAX) continue;
            string name = Enum.GetName(typeof(Character_ID), id);
            res.Add($"character_select_size_{name}_ID", character_select[count]);
            ++count;
        }

        res.Add("list_already_occurred_event_id_size", list_already_occurred_event_id.Count);
        count = 0;
        foreach(var data in list_already_occurred_event_id)
        {
            res.Add($"list_already_occurred_event_id_size_{count}", data);
            ++count;
        }
        return res;
    }

#if UNITY_EDITOR
    protected bool is_status_on_flag;
    protected bool is_favo_on_flag;
    protected bool is_occurred_on_flag;
    public virtual void OnGUI()
    {
        event_id = EditorGUILayout.IntField("イベントID", event_id);
        is_one_event = EditorGUILayout.Toggle("イベントが一回だけか", is_one_event);
        probability = EditorGUILayout.IntField("発生確率", probability);
        priority = EditorGUILayout.IntField("優先度(高いほど発生しやすい)", priority);
        using (new EditorGUILayout.HorizontalScope())
        {
            max_event_year_id = (Event_Years_ID)EditorGUILayout.EnumPopup("発生範囲(最大年)", max_event_year_id);
            min_event_year_id = (Event_Years_ID)EditorGUILayout.EnumPopup("発生範囲(最小年)", min_event_year_id);
        }
        using (new EditorGUILayout.HorizontalScope())
        {
            max_event_month_id = (Event_Months_ID)EditorGUILayout.EnumPopup("発生範囲(最大月)", max_event_month_id);
            min_event_month_id = (Event_Months_ID)EditorGUILayout.EnumPopup("発生範囲(最小月)", min_event_month_id);
        }

        // ステータス以上の描画
        is_status_on_flag =  EditorGUILayout.Toggle("ステータス以上",is_status_on_flag);
        if (is_status_on_flag)
        {
            if(status_more_dict.Count <= 0)
            {
                for(int i = 0; i < (int)Status_ID.MAX;)
                {
                    Status_ID id = (Status_ID)Enum.Parse(typeof(Status_ID), i.ToString());
                    status_more_dict.Add(id, 0);
                    ++i;
                }
            }
            EditorGUI.indentLevel++;
            for (int i = 0; i < (int)Status_ID.MAX;)
            {
                Status_ID id = (Status_ID)Enum.Parse(typeof(Status_ID), i.ToString());

                status_more_dict[id] = EditorGUILayout.IntField(StatusStringConverter.GetStatusString(id), status_more_dict[id]);
                ++i;
            }
            EditorGUI.indentLevel--;
        }

        // 好感度以上の描画
        is_favo_on_flag = EditorGUILayout.Toggle("好感度以上", is_favo_on_flag);
        if (is_favo_on_flag)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < (int)Character_ID.MAX;)
            {
                Character_ID id = (Character_ID)Enum.Parse(typeof(Character_ID), i.ToString());
                if (character_favo_more_dict.ContainsKey(id))
                {
                    character_favo_more_dict[id] = EditorGUILayout.IntField(CharacterConvert.GetCharacterComment(id), character_favo_more_dict[id]);
                }
                ++i;
            }
            EditorGUI.indentLevel--;
        }

        //発生済みイベント指定の描画
        is_occurred_on_flag = EditorGUILayout.Toggle("発生済みイベントID", is_occurred_on_flag);
        if (is_occurred_on_flag)
        {
            EditorGUI.indentLevel++;
            for (int count = 0; count < list_already_occurred_event_id.Count;)
            {
                //発生済みのイベント指定
                using (new EditorGUILayout.HorizontalScope())
                {
                    list_already_occurred_event_id[count] = EditorGUILayout.IntField("イベントID:", list_already_occurred_event_id[count]);
                    if(GUILayout.Button("×"))
                    {
                        list_already_occurred_event_id.RemoveAt(count);
                        --count;
                        continue;
                    }

                }
                ++count;
            }
            EditorGUI.indentLevel--;
            if(GUILayout.Button("Add"))
            {
                list_already_occurred_event_id.Add(0);
            }
        }

        // 対応キャラクターの描画
        EditorGUILayout.LabelField("対応キャラクター");
        int selectedIndices = BitArrayToInt(character_select);
        selectedIndices = EditorGUILayout.MaskField("選択", selectedIndices, GetBitLabelsCharacter());
        character_select = IntToBitArray<Character_ID>(selectedIndices);
        //選択したキャラクターを好感度のdictに追加
        {
            for(int i = 0; i < (int)Character_ID.MAX;)
            {
                Character_ID id = (Character_ID)Enum.Parse(typeof(Character_ID), i.ToString());

                if(character_favo_more_dict.ContainsKey(id) == true)
                {
                    //選択しなくなったら削除
                    if(character_select[i] == false)
                    {
                        character_favo_more_dict.Remove(id);
                    }
                }
                else
                {
                    if(character_select[i])
                    {
                        character_favo_more_dict.Add(id, 0);
                    }
                }

                ++i;
            }

        }
    }

    private string[] GetBitLabelsCharacter()
    {
        // ビットごとのラベルを作成
        string[] labels = new string[(int)Character_ID.MAX]; // 

        for (int i = 0; i < labels.Length; i++)
        {
            Character_ID id = (Character_ID)Enum.Parse(typeof(Character_ID), i.ToString());
            labels[i] = new string($"{CharacterConvert.GetCharacterComment(id)}");
        }

        return labels;
    }

    public static int BitArrayToInt(BitArray bitArray)
    {
        int[] array = new int[1];
        bitArray.CopyTo(array, 0);
        return array[0];
    }

    public static BitArray IntToBitArray<T>(int intValue) where T : Enum
    {

        // バイト配列をBitArrayに変換します。
        BitArray bitArray = new BitArray(Enum.GetValues(typeof(T)).Length - 1);

        for (int i = 0; i < (int)Enum.GetValues(typeof(T)).Length - 1; i++)
        {
            int bit = (intValue >> i) & 1;
            bitArray[i] = bit == 1 ? true : false;
        }



        return bitArray;
    }
#endif
}

/// <summary>
/// イベントデータまとめ
/// </summary>
/// <typeparam name="T"></typeparam>
public class BaseEventStorage<T> where T : BaseEventData, new()
{
    /// <summary>
    /// イベントデータ
    /// </summary>
    protected Dictionary<int, T> event_storage_dict = new Dictionary<int, T>();

    public virtual void BinaryReader(ref BinaryReader reader)
    {
        int size = reader.ReadInt32();
        for (int i = 0; i < size;)
        {
            int event_id = reader.ReadInt32();
            if (event_storage_dict.ContainsKey(event_id) == false)
            {
                event_storage_dict[event_id] = new T();
                event_storage_dict[event_id].BinaryReader(ref reader);
            }
            ++i;
        }
    }

    public virtual void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(event_storage_dict.Count);
        foreach (var data in event_storage_dict)
        {
            writer.Write(data.Key);
            data.Value.BinaryWriter(ref writer);
        }
    }

    public virtual void JsonReader(ref Dictionary<string, object> reader)
    {
        event_storage_dict.Clear();
        string parent = "event_storage_dict_size";
        if (reader.ContainsKey(parent))
        {
            for(int i = 0; i < (int)Event_ID.MAX;)
            {
                string id_text = parent + $"_{9001 + i}";
                int id = 0;
                if(reader.ContainsKey(id_text))
                {
                    id = Convert.ToInt32(reader[id_text]);
                }
                string id_data = id_text + "_data";
                if(reader.ContainsKey(id_data))
                {
                    string json = Convert.ToString(reader[id_data]);
                    JObject jObject = JObject.Parse(json);
                    Dictionary<string, object> dictionary = jObject.ToObject<Dictionary<string, object>>();
                    if(event_storage_dict.ContainsKey(id) == false)
                    {
                        event_storage_dict[id] = new T();
                        event_storage_dict[id].JsonReader(ref dictionary);
                    }
                }
                ++i;
            }
        }
    }

    public virtual Dictionary<string, object> JsonWriter()
    {
        Dictionary<string, object> writer = new Dictionary<string , object>();


        writer.Add("event_storage_dict_size", event_storage_dict.Count);

        string parent = "event_storage_dict_size";
        foreach (var data in event_storage_dict)
        {
            string id_text = parent + $"_{data.Key}";
            writer.Add(id_text, data.Key);
            string id_data = id_text + "_data";
            writer.Add(id_data, data.Value.JsonWriter());
        }

        return writer;
    }

#if UNITY_EDITOR
    protected Vector2 _parameterScrollPosition;
    protected Vector2 _buttonPosition;
    public void OnGUI(int id)
    {
        using(new EditorGUILayout.VerticalScope(GUILayout.Width(700)))
        {
            using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_parameterScrollPosition, EditorStyles.helpBox))
            {
                _parameterScrollPosition = scroll.scrollPosition;
                if (event_storage_dict.ContainsKey(id))
                {
                    event_storage_dict[id].OnGUI();
                }
            }
        }
    }

    public void OnGUIButtonList(ref int id)
    {
        using (new EditorGUILayout.VerticalScope(GUILayout.Width(200)))
        {
            using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_buttonPosition, EditorStyles.helpBox))
            {
                _buttonPosition = scroll.scrollPosition;
                foreach (var data in event_storage_dict)
                {
                    GUI.backgroundColor = data.Key == id ? Color.cyan : Color.white;
                    if (GUILayout.Button($"{data.Key}"))
                    {
                        id = data.Key;
                    }
                    GUI.backgroundColor = Color.white;
                }
            }
        }
    }

    public void OnGUIAdd(int value_id)
    {
        if (event_storage_dict.ContainsKey(value_id)) return;
        event_storage_dict[value_id] = new T();
        event_storage_dict[value_id].SetEventIDEditor(value_id);
    }

    public void OnGUIRemove(int remove_id)
    {
        if (event_storage_dict.ContainsKey(remove_id) == false) return;
        event_storage_dict.Remove(remove_id);
    }

#endif
    protected void Load(string file_name)
    {
        string path = Application.dataPath + $"/Project/Assets/Editor/Data/Event/{file_name}.json";
        if (System.IO.File.Exists(path) == false)
        {
            path = Application.dataPath + $"/Event/{file_name}.bin";
            if (System.IO.File.Exists(path) == false)
            {
                Debug.Log("ファイルが存在しません");
                return;
            }
            else
            {


                using (FileStream fileStream = new FileStream(path, FileMode.Open))
                {
                    BinaryReader reader = new BinaryReader(fileStream);

                    BinaryReader(ref reader);
                    reader.Close();
                }
                return;
            }
        }
#if UNITY_EDITOR
        else
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Open))
            {
                using (StreamReader fileStreamReader = new StreamReader(fileStream))
                {
                    string jsonStr = fileStreamReader.ReadToEnd();
                    var data = JObject.Parse(Convert.ToString(jsonStr));
                    var dict = data.ToObject<Dictionary<string, object>>();
                    JsonReader(ref dict);
                    data = null;
                    fileStreamReader.Close();
                }

            }
        }
#endif
    }

    protected void Save(string file_name)
    {
        string path = Application.dataPath + $"/Event/{file_name}.bin";


        using (FileStream fileStream = new FileStream(path, FileMode.Create))
        {
            BinaryWriter writer = new BinaryWriter(fileStream);

            BinaryWriter(ref writer);
            writer.Flush();
            writer.Close();

        }

#if UNITY_EDITOR
        var json_data = JsonWriter();
        path = Application.dataPath + $"/Project/Assets/Editor/Data/Event/{file_name}.json";
        using (FileStream fileStream = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter fileStreamWriter = new StreamWriter(fileStream))
            {
                string data = JsonConvert.SerializeObject(json_data);
                fileStreamWriter.Write(data);
            }

        }
#endif
    }



}

