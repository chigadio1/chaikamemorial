using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public enum Character_Favo_ID
{
    NONE = -1,
    Low,
    Medium,
    High,
    MAX
}



/// <summary>
/// デート用の関数
/// </summary>
public class DateUnit
{
    public static readonly int LowFavorabilityThreshold = 20;
    public static readonly int MediumFavorabilityThreshold = 50;
    public static readonly int LowFavorabilityFailureRate = 30;
    public static readonly int MediumFavorabilityFailureRate = 25;
    public static readonly int HighFavorabilityFailureRate = 15;

    public static readonly int[] CancelCharacterDateEventIDArray = new int[(int)Character_ID.MAX]
    {
        9801,
        9802,
        9803,
        9804,
        9805,
        9806,
        9807,
        9808,
        9809,
        9810,
        9811,
        9812,
        9813,
        9814,
        9815,
        9816,
        9817,
        9818,
        9819,
        9820,

    };

    public static readonly int[] SuccessCharacterDateEventIDArray = new int[(int)Character_ID.MAX]
{
        9831,
        9832,
        9833,
        9834,
        9835,
        9836,
        9837,
        9838,
        9839,
        9830,
        9831,
        9832,
        9833,
        9834,
        9835,
        9836,
        9837,
        9838,
        9839,
        9830,

};

    public static bool CanInviteForDate(Character_ID chara_id)
    {
        bool res;
        if (chara_id <= Character_ID.NONE || chara_id >= Character_ID.MAX) return false;
        int favo = PlayerCore.Instance.GetPlayerCoreData.GetCharacterFavorabilityData.GetFavorability(chara_id);
        res = IsDateSuccessful(favo);
        return res;
    }

    public static CharacterFavoDetails JumpCharacterFavoSelect(Character_ID chara_id,CharacterFavoJump favo_jump)
    {
        if(favo_jump == null) return null;
        if (chara_id <= Character_ID.NONE || chara_id >= Character_ID.MAX) return null;
        int favo = PlayerCore.Instance.GetPlayerCoreData.GetCharacterFavorabilityData.GetFavorability(chara_id);

        if (favo < LowFavorabilityThreshold)
        {
            return favo_jump.low_favo_jump;
        }
        else if (favo < MediumFavorabilityThreshold)
        {
            return favo_jump.medium_favo_jump;
        }
        else
        {
            return favo_jump.high_favo_jump;
        }
    }

    public static int DateMeetingLocationEventID(Character_ID select_character_id,Date_Spot_ID date_Spot_ID)
    {
        return DateEventID(select_character_id, date_Spot_ID, Date_Occurrence_ID.MeetingLocation);
    }

    public static int DatePlayEventID(Character_ID select_character_id, Date_Spot_ID date_Spot_ID)
    {
        return DateEventID(select_character_id, date_Spot_ID, Date_Occurrence_ID.DatePlay);
    }


    public static int DateEventID(Character_ID select_character_id, Date_Spot_ID date_Spot_ID,Date_Occurrence_ID occ_id)
    {
        var data_array = PlayerCore.Instance.DateEventStorage.ConditionMatching(select_character_id, date_Spot_ID, occ_id);
        if (data_array == null) return 9801;

        var special_event = data_array.Where(a => a.IsOneEvent == true).ToArray();
        if (special_event.Length > 0 )
        {
            special_event.OrderByDescending(a => a.GetPriority).ToArray();
            //ここでフラグをON
            PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.On(EventIDExtensions.EventIDConvert(special_event.First().GetEventID));
            return special_event.First().GetEventID;
        }
        data_array = data_array.Where(a => a.IsOneEvent == false).ToArray();

        int value = 0;
        int max_value = 0;
        foreach(var data in data_array)
        {
            max_value += data.GetPriority;
        }
        int random_num = UnityEngine.Random.Range(0, max_value + 1);
        foreach(var data in data_array)
        {
            value += data.GetPriority;
            if (value <= random_num) return data.GetEventID;
        }

        return data_array[0].GetEventID;
    }

    static bool IsDateSuccessful(int favorability)
    {
        int successProbability;

        if (favorability < LowFavorabilityThreshold)
        {
            successProbability = LowFavorabilityFailureRate;
        }
        else if (favorability < MediumFavorabilityThreshold)
        {
            successProbability = MediumFavorabilityFailureRate;
        }
        else
        {
            successProbability = HighFavorabilityFailureRate;
        }

        int randomNumber = Random.Range(1, 101);

        return randomNumber > successProbability;
    }
}
