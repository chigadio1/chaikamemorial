using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
public enum Date_Occurrence_ID
{
    NONE = -1,
    DateCancel,　//デートのキャンセル
    DateSuccess, //デートの成功
    MeetingLocation, //待ち合わせ
    DatePlay, //デート中
    AfterPickUp, //デート後の送り向かい
    MAX
}

public class DateEventData : BaseEventData
{
    public DateEventData()
    {

    }
    public DateEventData(int value_event_id) : base(value_event_id)
    {
    }
    /// <summary>
    /// デートキャラクター
    /// </summary>
    private Character_ID date_character_id = Character_ID.NONE;

    /// <summary>
    /// デート発生条件
    /// </summary>
    private Date_Occurrence_ID date_occurrence_id = Date_Occurrence_ID.NONE;

    /// <summary>
    /// デートスポット
    /// </summary>
    protected BitArray date_spot_bits = new BitArray((int)Date_Spot_ID.MAX);

    public override void BinaryReader(ref BinaryReader reader)
    {
        base.BinaryReader(ref reader);
        date_character_id = (Character_ID)reader.ReadInt32();
        date_occurrence_id = (Date_Occurrence_ID)reader.ReadInt32();

        int size = reader.ReadInt32();
        date_spot_bits = new BitArray(size);
        for (int count = 0; count < size;)
        {
            date_spot_bits[count] = reader.ReadBoolean();
            ++count;
        }
    }

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        base.BinaryWriter(ref writer);
        writer.Write(Convert.ToInt32((int)date_character_id));
        writer.Write(Convert.ToInt32((int)date_occurrence_id));

        writer.Write(date_spot_bits.Length);

        // ビットセットの内容をバイナリに書き込む
        foreach (bool bit in date_spot_bits)
        {
            writer.Write(bit);
        }
    }

    public override void JsonReader(ref Dictionary<string, object> reader)
    {
        base.JsonReader(ref reader);

        if (reader.ContainsKey("date_character_id"))
            date_character_id = (Character_ID)Convert.ToInt32(reader["date_character_id"]);

        if (reader.ContainsKey("date_occurrence_id"))
            date_occurrence_id = (Date_Occurrence_ID)Convert.ToInt32(reader["date_occurrence_id"]);

        if (reader.ContainsKey("date_spot_bits_size"))
        {
            string parent = "date_spot_bits_size";
            int size = Convert.ToInt32(reader[parent]);

            date_spot_bits = new BitArray(size);
            for (int count = 0; count < size;)
            {
                Date_Spot_ID id = (Date_Spot_ID)Enum.Parse(typeof(Date_Spot_ID), count.ToString());


                string name = Enum.GetName(typeof(Date_Spot_ID), id);

                bool value = false;
                if (reader.ContainsKey(parent + "_" + name + "_ID"))
                {
                    value = Convert.ToBoolean(reader[parent + "_" + name + "_ID"]);
                }
                date_spot_bits[count] = value;
                ++count;
            }
        }
    }

    public override Dictionary<string, object> JsonWriter()
    {
        var res =  base.JsonWriter();
        res.Add("date_character_id", (int)date_character_id);
        res.Add("date_occurrence_id", (int)date_occurrence_id);

        res.Add("date_spot_bits_size", date_spot_bits.Count);
        int count = 0;
        foreach (Date_Spot_ID id in Enum.GetValues(typeof(Date_Spot_ID)))
        {
            if (id == Date_Spot_ID.NONE || id == Date_Spot_ID.MAX) continue;
            string name = Enum.GetName(typeof(Date_Spot_ID), id);
            res.Add($"date_spot_bits_size_{name}_ID", date_spot_bits[count]);
            ++count;
        }

        return res;
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        base.OnGUI();
        date_character_id = (Character_ID)EditorGUILayout.EnumPopup("デートキャラID", date_character_id);
        date_occurrence_id = (Date_Occurrence_ID)EditorGUILayout.EnumPopup("デート発生ID", date_occurrence_id);

        int selectedIndices = BitArrayToInt(date_spot_bits);
        selectedIndices = EditorGUILayout.MaskField("選択", selectedIndices, GetBitLabelsDateSpot());
        date_spot_bits = IntToBitArray<Date_Spot_ID>(selectedIndices);
    }

    private string[] GetBitLabelsDateSpot()
    {
        // ビットごとのラベルを作成
        string[] labels = new string[(int)Date_Spot_ID.MAX]; // 

        for (int i = 0; i < labels.Length; i++)
        {
            Date_Spot_ID id = (Date_Spot_ID)Enum.Parse(typeof(Character_ID), i.ToString());
            labels[i] = new string($"{DateSpotIDConvert.GetJapaneseName(id)}");
        }

        return labels;
    }
#endif

    public bool ConditionMatchingDate(Event_Years_ID year_id, Event_Months_ID month_id, Dictionary<Status_ID, int> status_dict, Dictionary<Character_ID, int> character_favo_dict, BitArray character_unlock,
        BitArray occurred_event, Character_ID select_character_id,Date_Spot_ID select_date_spot_id,Date_Occurrence_ID select_date_occurrence_id)
    {
        bool res = true;
        res &= ConditionMatching(year_id, month_id, status_dict, character_favo_dict, character_unlock, occurred_event);

        res &= date_character_id == select_character_id;
        res &= date_occurrence_id == select_date_occurrence_id;
        res &= date_spot_bits[(int)select_date_spot_id];

        return res;
    }
}

public class DateEventStorage : BaseEventStorage<DateEventData>
{

    public void SaveStorage()
    {
        Save("DateEvent");
    }

    public void LoadStorage()
    {
        Load("DateEvent");
    }

    public DateEventData[] ConditionMatching(Character_ID select_date_character_id,Date_Spot_ID select_date_spot_id,Date_Occurrence_ID date_occurrence_id)
    {
        List<DateEventData> conditions = new List<DateEventData>();

        var dat_day = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);
        var res_id = PlayerCore.Instance.EventCalenderCoreData.GetNouTurnYearMonthID(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);

        if (dat_day == null) return null;
        if (res_id.Item1 <= Event_Years_ID.NONE || res_id.Item1 >= Event_Years_ID.MAX) return null;
        if (res_id.Item2 <= Event_Months_ID.NONE || res_id.Item2 >= Event_Months_ID.MAX) return null;

        foreach(var data in event_storage_dict)
        {
            bool comp = data.Value.ConditionMatchingDate(res_id.Item1,res_id.Item2,
                                          PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.StatusArray,
                                          PlayerCore.Instance.GetPlayerCoreData.GetCharacterFavorabilityData.FavorabilityArray,
                                          PlayerCore.Instance.GetPlayerCoreData.GetCharacterUnlockData.UnlockCharacterBits,
                                          PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.EventBits,
                                          select_date_character_id,
                                          select_date_spot_id,
                                          date_occurrence_id);

            if (comp)
                conditions.Add(data.Value);
        }




        return conditions.ToArray();
    }
}
