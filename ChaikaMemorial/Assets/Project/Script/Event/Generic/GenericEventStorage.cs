using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GenericEventData : BaseEventData
{
    public GenericEventData()
    {

    }
    public GenericEventData(int value_event_id) : base(value_event_id)
    {
    }
    /// <summary>
    /// 汎用イベントタイプ
    /// </summary>
    private Generic_Event_Type_ID generic_event_type_id = Generic_Event_Type_ID.NONE;

    /// <summary>
    /// 学校イベントタイプID
    /// </summary>
    private Generic_Event_School_Type_ID generic_event_scool_type_id = Generic_Event_School_Type_ID.NONE;




    public override void BinaryReader(ref BinaryReader reader)
    {
        base.BinaryReader(ref reader);
        generic_event_type_id = (Generic_Event_Type_ID)reader.ReadInt32();
        generic_event_scool_type_id = (Generic_Event_School_Type_ID)reader.ReadInt32();


    }

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        base.BinaryWriter(ref writer);
        writer.Write(Convert.ToInt32((int)generic_event_type_id));
        writer.Write(Convert.ToInt32((int)generic_event_scool_type_id));


    }

    public override void JsonReader(ref Dictionary<string, object> reader)
    {
        base.JsonReader(ref reader);

        if (reader.ContainsKey("generic_event_type_id"))
            generic_event_type_id = (Generic_Event_Type_ID)Convert.ToInt32(reader["generic_event_type_id"]);

        if (reader.ContainsKey("generic_event_scool_type_id"))
            generic_event_scool_type_id = (Generic_Event_School_Type_ID)Convert.ToInt32(reader["generic_event_scool_type_id"]);
    }

    public override Dictionary<string, object> JsonWriter()
    {
        var res = base.JsonWriter();
        res.Add("generic_event_type_id", (int)generic_event_type_id);
        res.Add("generic_event_scool_type_id", (int)generic_event_scool_type_id);



        return res;
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        base.OnGUI();
        generic_event_type_id = (Generic_Event_Type_ID)EditorGUILayout.EnumPopup("汎用イベントタイプID", generic_event_type_id);
        generic_event_scool_type_id = (Generic_Event_School_Type_ID)EditorGUILayout.EnumPopup("学校イベントタイプID", generic_event_scool_type_id);


        if(generic_event_scool_type_id > Generic_Event_School_Type_ID.NONE || generic_event_scool_type_id < Generic_Event_School_Type_ID.MAX)
        {
            generic_event_type_id = Generic_Event_Type_ID.School;
        }
        if(generic_event_type_id == Generic_Event_Type_ID.Holiday)
        {
            generic_event_scool_type_id = Generic_Event_School_Type_ID.NONE;
        }
    }


#endif

    public bool ConditionMatchingGeneric(Event_Years_ID year_id, Event_Months_ID month_id, Dictionary<Status_ID, int> status_dict, Dictionary<Character_ID, int> character_favo_dict, BitArray character_unlock,
        BitArray occurred_event, Generic_Event_Type_ID select_generic_event_type_id, Generic_Event_School_Type_ID select_generic_event_scool_type_id)
    {
        bool res = true;
        res &= ConditionMatching(year_id, month_id, status_dict, character_favo_dict, character_unlock, occurred_event);

        res &= generic_event_type_id == select_generic_event_type_id;
        res &= generic_event_scool_type_id == select_generic_event_scool_type_id;

        return res;
    }
}

public class GenericEventStorage : BaseEventStorage<GenericEventData>
{

    public void SaveStorage()
    {
        Save("GenericEvent");
    }

    public void LoadStorage()
    {
        Load("GenericEvent");
    }

    public GenericEventData[] ConditionMatching(Generic_Event_Type_ID select_generic_event_type_id, Generic_Event_School_Type_ID select_generic_event_scool_type_id)
    {
        List<GenericEventData> conditions = new List<GenericEventData>();

        var dat_day = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);
        var res_id = PlayerCore.Instance.EventCalenderCoreData.GetNouTurnYearMonthID(PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.GetTurn);

        if (dat_day == null) return null;
        if (res_id.Item1 <= Event_Years_ID.NONE || res_id.Item1 >= Event_Years_ID.MAX) return null;
        if (res_id.Item2 <= Event_Months_ID.NONE || res_id.Item2 >= Event_Months_ID.MAX) return null;

        foreach (var data in event_storage_dict)
        {
            bool comp = data.Value.ConditionMatchingGeneric(res_id.Item1, res_id.Item2,
                                          PlayerCore.Instance.GetPlayerCoreData.GetPlayerStatusData.StatusArray,
                                          PlayerCore.Instance.GetPlayerCoreData.GetCharacterFavorabilityData.FavorabilityArray,
                                          PlayerCore.Instance.GetPlayerCoreData.GetCharacterUnlockData.UnlockCharacterBits,
                                          PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.EventBits,
                                          select_generic_event_type_id,
                                          select_generic_event_scool_type_id);

            if (comp)
                conditions.Add(data.Value);
        }




        return conditions.ToArray();
    }
}
