using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// 汎用イベントUnit
/// </summary>
public class GenericEventUnit
{
    public static readonly int lesson_probability = 30;
    public static readonly int lunch_break_probabillty = 30;
    public static readonly int after_school_probabillity = 30;

    public static bool OccurrenceSchool(Generic_Event_School_Type_ID type)
    {
        int value = UnityEngine.Random.Range(1, 101);

        if(type == Generic_Event_School_Type_ID.Lesson)
        {
            return value <= lesson_probability;
        }
        else if(type == Generic_Event_School_Type_ID.Lunch_Break)
        {
            return value <= lunch_break_probabillty;

        }
        else if(type == Generic_Event_School_Type_ID.After_School)
        {
            return value <= after_school_probabillity;
        }
        return false;
    }
    private static int GenericEventSchoolID(Generic_Event_School_Type_ID select_generic_event_scool_type_id)
    {
        return GenericEventEventID(Generic_Event_Type_ID.School, select_generic_event_scool_type_id);
    }

    public static int GenericEventSchoolLessonID()
    {
        return GenericEventSchoolID( Generic_Event_School_Type_ID.Lesson);
    }

    public static int GenericEventSchoolLunch_BreakID()
    {
        return GenericEventSchoolID( Generic_Event_School_Type_ID.Lunch_Break);
    }

    public static int GenericEventSchoolAfterID()
    {
        return GenericEventSchoolID( Generic_Event_School_Type_ID.After_School);
    }


    public static int GenericEventHolidayID()
    {
        return GenericEventEventID(Generic_Event_Type_ID.Holiday,Generic_Event_School_Type_ID.NONE);
    }


    public static int GenericEventEventID(Generic_Event_Type_ID select_generic_event_type_id, Generic_Event_School_Type_ID select_generic_event_scool_type_id)
    {
        var data_array = PlayerCore.Instance.GenericEventStorage.ConditionMatching(select_generic_event_type_id, select_generic_event_scool_type_id);
        if (data_array == null || data_array.Length <= 0) return -1;

        var special_event = data_array.Where(a => a.IsOneEvent == true).ToArray();
        if (special_event.Length > 0)
        {
            special_event.OrderByDescending(a => a.GetPriority).ToArray();
            //ここでフラグをON
            PlayerCore.Instance.GetPlayerCoreData.GetPlayerEventData.On(EventIDExtensions.EventIDConvert(special_event.First().GetEventID));
            return special_event.First().GetEventID;
        }
        data_array = data_array.Where(a => a.IsOneEvent == false).ToArray();

        int value = 0;
        int max_value = 0;
        foreach (var data in data_array)
        {
            max_value += data.GetPriority;
        }
        int random_num = UnityEngine.Random.Range(0, max_value + 1);
        foreach (var data in data_array)
        {
            value += data.GetPriority;
            if (value <= random_num) return data.GetEventID;
        }

        return data_array[0].GetEventID;
    }
}
