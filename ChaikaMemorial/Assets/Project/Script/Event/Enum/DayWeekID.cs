using System;

public enum Day_Week_ID
{
    NONE = -1,
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    MAX
}

public class DayWeekConvert
{
    public static Day_Week_ID GetDayWeekID(string dayOfWeek)
    {
        switch (dayOfWeek)
        {
            case "Sunday":
                return Day_Week_ID.Sunday;
            case "Monday":
                return Day_Week_ID.Monday;
            case "Tuesday":
                return Day_Week_ID.Tuesday;
            case "Wednesday":
                return Day_Week_ID.Wednesday;
            case "Thursday":
                return Day_Week_ID.Thursday;
            case "Friday":
                return Day_Week_ID.Friday;
            case "Saturday":
                return Day_Week_ID.Saturday;
            default:
                return Day_Week_ID.NONE;
        }
    }

    public static string GetDayWeekJPID(Day_Week_ID dayOfWeek)
    {
        switch (dayOfWeek)
        {
            case Day_Week_ID.Sunday:
                return "日";
            case Day_Week_ID.Monday:
                return "月";
            case Day_Week_ID.Tuesday:
                return "火";
            case Day_Week_ID.Wednesday:
                return "水";
            case Day_Week_ID.Thursday:
                return "木";
            case Day_Week_ID.Friday:
                return "金";
            case Day_Week_ID.Saturday:
                return "土";
            default:
                return "NONE";
        }
    }


}

public enum Day_Holiday_ID
{
    NONE = -1,
    // 固定の日付
    NewYear,               // 元日
    ComingOfAge,           // 成人の日
    NationalFoundation,    // 建国記念の日
    EmperorBirthday,       // 天皇誕生日
    ChildrensDay,          // こどもの日
    MarineDay,             // 海の日
    MountainDay,           // 山の日
    RespectForTheAged,     // 敬老の日
    AutumnalEquinox,       // 秋分の日
    HealthAndSports,       // 体育の日
    CultureDay,            // 文化の日
    LaborThanksgiving,     // 勤労感謝の日

    // 移動する日付
    VernalEquinox,         // 春分の日
    ShowaDay,              // 昭和の日
    GreeneryDay,           // みどりの日
    ConstitutionMemorial, // 憲法記念日
    NationalHoliday,       // 国民の休日
    EmperorEnthronement,   // 天皇即位の礼
    SubstituteHoliday,     // 振替休日
    EmperorReign,           // 天皇の即位日
    MAX
}

public enum Day_School_Event_ID
{
    NONE = -1,
    EntranceCeremony,     // 入学式
    MidtermExam,          // 中間試験
    FinalExam,            // 期末試験
    SportsDay,            // 運動会
    CulturalFestival,     // 文化祭
    SchoolTrip,           // 修学旅行
    GraduationCeremony,   // 卒業式
    MAX
}
public class EventDayConvert
{
    public static string GetDayHolidayComment(Day_Holiday_ID holidayID)
    {
        switch (holidayID)
        {
            case Day_Holiday_ID.NONE:
                return "不明な祝日";
            case Day_Holiday_ID.NewYear:
                return "元日";
            case Day_Holiday_ID.ComingOfAge:
                return "成人の日";
            case Day_Holiday_ID.NationalFoundation:
                return "建国記念の日";
            case Day_Holiday_ID.EmperorBirthday:
                return "天皇誕生日";
            case Day_Holiday_ID.ChildrensDay:
                return "こどもの日";
            case Day_Holiday_ID.MarineDay:
                return "海の日";
            case Day_Holiday_ID.MountainDay:
                return "山の日";
            case Day_Holiday_ID.RespectForTheAged:
                return "敬老の日";
            case Day_Holiday_ID.AutumnalEquinox:
                return "秋分の日";
            case Day_Holiday_ID.HealthAndSports:
                return "体育の日";
            case Day_Holiday_ID.CultureDay:
                return "文化の日";
            case Day_Holiday_ID.LaborThanksgiving:
                return "勤労感謝の日";
            case Day_Holiday_ID.VernalEquinox:
                return "春分の日";
            case Day_Holiday_ID.ShowaDay:
                return "昭和の日";
            case Day_Holiday_ID.GreeneryDay:
                return "みどりの日";
            case Day_Holiday_ID.ConstitutionMemorial:
                return "憲法記念日";
            case Day_Holiday_ID.NationalHoliday:
                return "国民の休日";
            case Day_Holiday_ID.EmperorEnthronement:
                return "天皇即位の礼";
            case Day_Holiday_ID.SubstituteHoliday:
                return "振替休日";
            case Day_Holiday_ID.EmperorReign:
                return "天皇の即位日";
            default:
                return "不明な祝日";
        }
    }
    public static string GetSchoolEventComment(Day_School_Event_ID schoolEvent)
    {
        switch (schoolEvent)
        {
            case Day_School_Event_ID.EntranceCeremony:
                return "入学式";
            case Day_School_Event_ID.MidtermExam:
                return "中間試験";
            case Day_School_Event_ID.FinalExam:
                return "期末試験";
            case Day_School_Event_ID.SportsDay:
                return "運動会";
            case Day_School_Event_ID.CulturalFestival:
                return "文化祭";
            case Day_School_Event_ID.SchoolTrip:
                return "修学旅行";
            case Day_School_Event_ID.GraduationCeremony:
                return "卒業式";
            default:
                return "不明な行事";
        }
    }
}

/// <summary>
/// 発生ID
/// </summary>
public enum Event_Occurrence_ID
{
    NONE = -1,
    StartOfTheDay, //一日の始まり
    StartOfAction, //実行時の1日目の初め
    FinishOfAction,//実行時の1日目の終わり
    MAX
}