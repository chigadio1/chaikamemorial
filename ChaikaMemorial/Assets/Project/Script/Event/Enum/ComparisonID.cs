using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 比較ID
/// </summary>
public enum Comparison_ID
{
    NONE = -1, //なし
    MORE,//以上
    BELOW, //以下
    MAX
}
