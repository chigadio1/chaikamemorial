using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound_Type_ID
{
    NONE = -1,
    SE,
    BGM,
    NAX
}
