using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sound_Event_Type_ID
{
    NONE = -1,
    Title,
    SelectStartGame,
    ScheduleAction,
    NovelEvent,
    MAX
}
