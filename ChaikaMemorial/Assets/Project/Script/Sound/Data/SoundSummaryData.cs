using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// サウンドデータ
/// </summary>
public class SoundData
{
    private AddressableData<AudioClip> audio_data = null;

    public static readonly string audio_path = "Assets/Project/Assets/Sound/";

    public void LoadSound(int sound_id)
    {
        audio_data = new AddressableData<AudioClip>();
        audio_data.LoadStart(audio_path + $"{sound_id:0000}.mp3");
    }

    public bool IsLoadEnd()
    {
        if (audio_data == null) return true;
        return audio_data.GetFlagSetUpLoading();
    }

    public void Release()
    {
        if (audio_data == null) return;
        audio_data.OnAutoRelease();
        audio_data = null;
    }

    public AudioClip GetAudio()
    {
        if (IsLoadEnd() == false) return null;
        return audio_data.GetAddressableData();
    }
}

/// <summary>
/// サウンド纏データ
/// </summary>
public class SoundSummaryData
{
    /// <summary>
    /// サウンド辞書
    /// </summary>
    private Dictionary<int, SoundData> audio_dict = new Dictionary<int, SoundData>();

    public void LoadSound(int sound_id)
    {
        if (audio_dict == null) audio_dict = new Dictionary<int, SoundData>();
        if (audio_dict.ContainsKey(sound_id)) return;

        audio_dict[sound_id] = new SoundData();
        audio_dict[sound_id].LoadSound(sound_id);
    }

    public AudioClip GetAudioClip(int sound_id)
    {
        if (audio_dict == null) audio_dict = new Dictionary<int, SoundData>();
        if (audio_dict.ContainsKey(sound_id) == false) return null;
        if (audio_dict[sound_id].IsLoadEnd() == false) return null;
        return audio_dict[sound_id].GetAudio();
    }

    public void Release(int sound_id)
    {
        if (audio_dict == null) audio_dict = new Dictionary<int, SoundData>();
        if (audio_dict.ContainsKey(sound_id) == false) return;
        audio_dict[sound_id].Release();
    }

    public void AllRelease()
    {
        foreach(var audio in audio_dict)
        {
            audio.Value.Release();
        }
        audio_dict = null;
    }

    public bool IsLoadEnd()
    {
        foreach(var audio in audio_dict)
        {
            if (audio.Value.IsLoadEnd() == false) return false;
        }
        return true;
    }
}

/// <summary>
/// SE,BGM分けサウンドデータ
/// </summary>
public class SoundTypeSummaryData
{
    /// <summary>
    /// サウンド纏データ
    /// </summary>
    private Dictionary<Sound_Type_ID, SoundSummaryData> sound_summary_dict = new Dictionary<Sound_Type_ID, SoundSummaryData>();

    public void Load(Sound_Type_ID type_id,int sound_id)
    {
        if (sound_summary_dict == null) sound_summary_dict = new Dictionary<Sound_Type_ID, SoundSummaryData>();
        if (sound_summary_dict.ContainsKey(type_id) == false) sound_summary_dict[type_id] = new SoundSummaryData();
        sound_summary_dict[type_id].LoadSound(sound_id);
    }

    public void LoadSE(int sound_id)
    {
        Load(Sound_Type_ID.SE, sound_id);
    }
    public void LoadBGM(int sound_id)
    {
        Load(Sound_Type_ID.BGM, sound_id);
    }


    public AudioClip GetAudio(Sound_Type_ID type_id,int sound_id)
    {
        if (sound_summary_dict == null) sound_summary_dict = new Dictionary<Sound_Type_ID, SoundSummaryData>();
        if (sound_summary_dict.ContainsKey(type_id) == false)
        {
            sound_summary_dict[type_id] = new SoundSummaryData();
            return null;
        }

        return sound_summary_dict[type_id].GetAudioClip(sound_id);
    }

    public AudioClip GetAudioSE(int sound_id)
    {
        return GetAudio(Sound_Type_ID.SE, sound_id);
    }
    public AudioClip GetAudioBGM(int sound_id)
    {
        return GetAudio(Sound_Type_ID.BGM, sound_id);
    }


    public void Release(Sound_Type_ID type_id,int sound_id)
    {
        if (sound_summary_dict.ContainsKey(type_id) == false)
        {
            sound_summary_dict[type_id] = new SoundSummaryData();
            return;
        }

        sound_summary_dict[type_id].Release(sound_id);
    }
    public void ReleaseSE(int sound_id)
    {
        Release(Sound_Type_ID.SE, sound_id);
    }

    public void ReleaseBGM(int sound_id)
    {
        Release(Sound_Type_ID.BGM, sound_id);
    }

    public void AllRelease()
    {
        if (sound_summary_dict == null) return;
        foreach(var audio in sound_summary_dict)
        {
            audio.Value.AllRelease();
            
        }

        sound_summary_dict = null;
    }

    public bool IsLoadEnd()
    {
        if (sound_summary_dict == null) sound_summary_dict = new Dictionary<Sound_Type_ID, SoundSummaryData>();
        foreach(var audio in sound_summary_dict)
        {
            if (audio.Value.IsLoadEnd() == false) return false;
        }

        return true;
    }
}

/// <summary>
/// サウンドのイベント事のデータ
/// </summary>
public class SoundEventSummaryData
{
    /// <summary>
    /// タイトル、ゲームごとのサウンドデータ
    /// </summary>
    private Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData> sound_event_summary_dict = new Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData>();

    public void Load(Sound_Event_Type_ID event_id, Sound_Type_ID type_id,int sound_id)
    {
        if (sound_event_summary_dict == null) sound_event_summary_dict = new Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData>();
        if (sound_event_summary_dict.ContainsKey(event_id) == false) sound_event_summary_dict[event_id] = new SoundTypeSummaryData();
        sound_event_summary_dict[event_id].Load(type_id,sound_id);
    }

    public void LoadTitleSE(int sound_id)
    {
        Load(Sound_Event_Type_ID.Title,Sound_Type_ID.SE, sound_id);
    }
    public void LoadTitleBGM(int sound_id)
    {
        Load(Sound_Event_Type_ID.Title,Sound_Type_ID.BGM, sound_id);
    }

    public void LoadSelectStartGameSE(int sound_id)
    {
        Load(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, sound_id);
    }
    public void LoadSelectStartGameBGM(int sound_id)
    {
        Load(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.BGM, sound_id);
    }

    public void LoadNovelSE(int sound_id)
    {
        Load(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, sound_id);
    }
    public void LoadNovelBGM(int sound_id)
    {
        Load(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, sound_id);
    }


    public AudioClip GetAudio(Sound_Event_Type_ID event_id, Sound_Type_ID type_id, int sound_id)
    {
        if (sound_event_summary_dict == null) sound_event_summary_dict = new Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData>();
        if (sound_event_summary_dict.ContainsKey(event_id) == false)
        {
            sound_event_summary_dict[event_id] = new SoundTypeSummaryData();
            return null;
        }

        return sound_event_summary_dict[event_id].GetAudio(type_id, sound_id);
    }


    public void GetAudioTitleSE(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, sound_id);
    }
    public void GetAudioTitleBGM(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.Title, Sound_Type_ID.BGM, sound_id);
    }

    public void GetAudioSelectStartGameSE(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, sound_id);
    }
    public void GetAudioSelectStartGameBGM(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.BGM, sound_id);
    }

    public void GetAudioNovelSE(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, sound_id);
    }
    public void GetAudioNovelBGM(int sound_id)
    {
        GetAudio(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, sound_id);
    }


    public void Release(Sound_Event_Type_ID event_id, Sound_Type_ID type_id, int sound_id)
    {
        if (sound_event_summary_dict == null) sound_event_summary_dict = new Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData>(); 
        if (sound_event_summary_dict.ContainsKey(event_id) == false)
        {
            sound_event_summary_dict[event_id] = new SoundTypeSummaryData();
            return;
        }

        sound_event_summary_dict[event_id].Release(type_id,sound_id);
    }

    public void AllEventRelease(Sound_Event_Type_ID event_id)
    {
        if (sound_event_summary_dict == null) sound_event_summary_dict = new Dictionary<Sound_Event_Type_ID, SoundTypeSummaryData>();
        if (sound_event_summary_dict.ContainsKey(event_id) == false)
        {
            sound_event_summary_dict[event_id] = new SoundTypeSummaryData();
            return;
        }

        sound_event_summary_dict[event_id].AllRelease();

    }

    public void ReleaseTitleSE(int sound_id)
    {
        Release(Sound_Event_Type_ID.Title, Sound_Type_ID.SE, sound_id);
    }
    public void ReleaseTitleBGM(int sound_id)
    {
        Release(Sound_Event_Type_ID.Title, Sound_Type_ID.BGM, sound_id);
    }

    public void ReleaseSelectStartGameSE(int sound_id)
    {
        Release(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.SE, sound_id);
    }
    public void ReleaseSelectStartGameBGM(int sound_id)
    {
        Release(Sound_Event_Type_ID.SelectStartGame, Sound_Type_ID.BGM, sound_id);
    }

    public void ReleaseNovelSE(int sound_id)
    {
        Release(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.SE, sound_id);
    }
    public void ReleaseNovelBGM(int sound_id)
    {
        Release(Sound_Event_Type_ID.NovelEvent, Sound_Type_ID.BGM, sound_id);
    }


    public void AllRelease()
    {
        foreach (var audio in sound_event_summary_dict)
        {
            audio.Value.AllRelease();

        }

        sound_event_summary_dict = null;
    }

    public bool IsLoadEnd()
    {
        foreach(var audio in  sound_event_summary_dict)
        {
            if (audio.Value.IsLoadEnd() == false) return false;
        }
        return true;
    }


}