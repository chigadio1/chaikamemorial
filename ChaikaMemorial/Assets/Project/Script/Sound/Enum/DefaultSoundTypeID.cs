
/// <summary>
/// タイトルのデフォルトID
/// </summary>
public enum Default_Title_Sound_ID
{
    NONE = -1,
    Sub_Select = 8001,
    Sub_Select_Space,
    Select,
    Title_BGM = 8501,
    MAX
}