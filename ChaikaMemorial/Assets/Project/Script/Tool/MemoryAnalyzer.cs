using UnityEngine;
using System.Collections.Generic;

public class MemoryAnalyzer : MonoBehaviour
{

    public bool check = false;
    void Start()
    {
    }

    private void Update()
    {
        if (check) return;

        // すべてのテクスチャを取得
        Texture2D[] allTextures = Resources.FindObjectsOfTypeAll<Texture2D>();

        // テクスチャのリストを表示
        foreach (Texture2D texture in allTextures)
        {
            if (texture.name == "2101")
            {
                Debug.Log("Texture: " + texture.name);
            }
            
        }


        check = true;
    }
}