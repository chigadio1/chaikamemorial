using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.Json;
using Newtonsoft.Json;
#if UNITY_EDITOR
using UnityEditor;
#endif

///Jsonデータ
[System.Serializable]
public class RootNovelExecOrderObject
{
    [SerializeField]
    public Dictionary<int, Dictionary<int, List<Dictionary<string, object>>>> Data = new Dictionary<int, Dictionary<int, List<Dictionary<string, object>>>>();
}

/// <summary>
///　ノベル管理
/// </summary>
public class NovelExecGroupManager
{
    private List<NovelExecOrderGroup> list_novel_order = new List<NovelExecOrderGroup>();


    /// <summary>
    /// 終了フラグ
    /// </summary>
    private bool is_finish = false;
    public bool IsFinish { get { return is_finish; } }
    /// <summary>
    /// 実行カウント
    /// </summary>
    private int exec_count = 0;
    /// <summary>
    /// オーダーカウント
    /// </summary>
    private int order_count = 0;

    /// <summary>
    /// 初期フラグ
    /// </summary>
    private bool is_begin = false;



    public void JumpExecOrderID(int jump_exec_id,int jump_order_id)
    {
        if (jump_exec_id >= list_novel_order.Count) return;

        exec_count = jump_exec_id;
        order_count = jump_order_id;

        foreach(var role in list_novel_order[exec_count].list_novel_role)
        {
            if(order_count == role.OrderID)
            {
                role.BeginInit();
            }
        }
       
    }

    
#if UNITY_EDITOR
    public List<NovelExecOrderGroup> ListNovelOrder { get { return list_novel_order;}  set { list_novel_order = value;} }
    bool changeRoutineFlag;
#endif

    /// <summary>
    /// 初期化
    /// </summary>
    public void Init()
    {
        exec_count = order_count = 0;
    }
    /// <summary>
    /// 読み込み
    /// </summary>
    /// <param name="reader"></param>
    /// <returns></returns>
    public bool BinaryReaderData(ref BinaryReader reader)
    {
        list_novel_order.Clear();
        list_novel_order = new List<NovelExecOrderGroup>();
        if (reader == null) return false;
        //実行回数取得
        int exec_count = reader.ReadInt32();

        for (int count = 0; count < exec_count;)
        {
            NovelExecOrderGroup add_group = new NovelExecOrderGroup();
            if (add_group.BinaryReaderData(ref reader) == false) return false;
            list_novel_order.Add(add_group);
            ++count;
        }
        return true;
    }
    /// <summary>
    /// 書き込み
    /// </summary>
    /// <param name="writer"></param>
    /// <returns></returns>
    public bool BinaryWriterData(ref BinaryWriter writer)
    {
        if (writer == null) return false;

        writer.Write(list_novel_order.Count);

        foreach(var data in list_novel_order)
        {
            data.BinaryWriterData(ref writer);
        }

        return true;
    }

    /// <summary>
    /// Json読み込み
    /// </summary>
    /// <param name="reader"></param>
    public void JsonReaderData(ref Dictionary<int, Dictionary<int, List<Dictionary<string, object>>>> reader)
    {
        foreach(var exec_data in reader)
        {
            foreach(var exec_data_details in exec_data.Value)
            {
                
                var add_data = new NovelExecOrderGroup();
                var data = exec_data_details.Value;
                add_data.JsonReaderData(ref data, exec_data.Key);
                list_novel_order.Add(add_data);
            }
            
        }
    }

    /// <summary>
    /// Json書き込み
    /// </summary>
    /// <param name="directory"></param>
    public Dictionary<int,Dictionary<int, List<Dictionary<string, object>>>> JsonWriterData()
    {
        var res = new Dictionary<int, Dictionary<int, List<Dictionary<string, object>>>>();

        foreach(var exec_data in list_novel_order)
        {
            int exec_key = exec_data.ExecID;
            res[exec_key] = new Dictionary<int, List<Dictionary<string, object>>>();
            var add_data = exec_data.JsonWriterData();
            res[exec_key] = add_data;
        }

        return res;
    }

    public void Update()
    {
        if (is_finish == true) return;
        if (list_novel_order.Count <= 0)
        {
            is_finish = true;
            return;
        }
        if(is_begin == false)
        {
            is_begin = true;
            list_novel_order[exec_count].StartBeginInit();
            return;
        }

        bool finish = list_novel_order[exec_count].Update(ref order_count);
        bool is_comp = list_novel_order[exec_count].IsCompulsion;
        if (finish == true)
        {
            order_count = 0;
            ++exec_count;
        }
        if(PlayerCore.Instance.is_next_event)
        {
            is_finish = false;
            order_count = exec_count = 0;
            gameUiCore.Instance.PlayEvent(PlayerCore.Instance.event_id);
            PlayerCore.Instance.is_next_event = false;
            PlayerCore.Instance.event_id = 0;
        }
        if(is_comp)
        {
            is_finish = true;
            return;
        }
        if (exec_count >= list_novel_order.Count)
        {
            is_finish = true;
            return;
        }

    }

    public void FixExecID()
    {
        int exec_count = 1;
        list_novel_order.Sort((a, b) => a.ExecID.CompareTo(b.ExecID));
        foreach(var data in list_novel_order)
        {
            data.ExecID = exec_count;
            ++exec_count;
        }

    }

    public void Reset()
    {
        foreach (var exec_data in list_novel_order)
        {
            exec_data.Reset();
        }
        is_begin = false;
        gameUiCore.Instance?.CharacterCanvas?.ResetAll();
        gameUiCore.Instance?.SettingCanvas?.ResetAll();
    }

#if UNITY_EDITOR
    Vector2 _dataScrollPosition;
    Vector2 _dataOrderScrollPosition;
    Vector2 _parameterScrollPosition;
    Vector2 _buttonPosition;
    int _select_exec = -1;
    int _select_order = -1;
    int _load_event_id = -1;
    int _start_load_event_id = -1;
    private void UpdateLayoutData()
    {
        using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_dataScrollPosition, EditorStyles.helpBox, GUILayout.Width(150)))
        {
            _dataScrollPosition = scroll.scrollPosition;
            GenericMenu menu = new GenericMenu();
            if (Event.current.type == EventType.ContextClick && Event.current.button == 1)
            {
                menu.AddItem(new GUIContent("実行追加"), false, () =>
                {
                    var add = new NovelExecOrderGroup();
                    add.ExecID = list_novel_order.Count == 0 ? 0 : list_novel_order[list_novel_order.Count - 1].ExecID + 1;
                    list_novel_order.Add(add);
                    return;
                });
            }
            GUILayout.Label("Exec");


            for (int i = 0; i < list_novel_order.Count; i++)
            {
                GUI.backgroundColor = i == _select_exec ? Color.cyan : Color.white;
                if (GUILayout.Button($"Exec:{i}"))
                {
                    _select_exec = i;
                    _select_order = -1;
                }
                GUI.backgroundColor = Color.white;
            }

            // メニュー表示
            if (menu.GetItemCount() > 0)
            {
                menu.ShowAsContext();
                Event.current.Use();
            }

        }
    }

    private void UpdateLayoutOrderData(ref Novel_Role_ID role_id)
    {
        if (_select_exec < 0) return;
        using (new GUILayout.VerticalScope(GUILayout.Width(250)))
        {
            using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_dataOrderScrollPosition, EditorStyles.helpBox))
            {

                GenericMenu menu = new GenericMenu();
                if (Event.current.type == EventType.ContextClick && Event.current.button == 1)
                {
                    menu.AddItem(new GUIContent("削除"), false, () =>
                    {
                        if (_select_order < 0) return;
                        if (_select_exec < 0) return;

                        int remove_oreder_id = list_novel_order[_select_exec].list_novel_role[_select_order].OrderID;
                        list_novel_order[_select_exec].list_novel_role.RemoveAt(_select_order);
                        for(int count = 0; count < list_novel_order[_select_exec].list_novel_role.Count;)
                        {
                            if(list_novel_order[_select_exec].list_novel_role[count].OrderID > remove_oreder_id)
                            {
                                list_novel_order[_select_exec].list_novel_role[count].OrderID--;
                            }
                            ++count;
                        }
                        list_novel_order[_select_exec].list_novel_role.Sort((a, b) => a.OrderID.CompareTo(b.OrderID));
                        return;
                    });
                }
                _dataOrderScrollPosition = scroll.scrollPosition;
                GUILayout.Label("Order");

                for (int i = 0; i < list_novel_order[_select_exec].list_novel_role.Count; i++)
                {
                    GUI.backgroundColor = i == _select_order ? Color.cyan : Color.white;
                    if (GUILayout.Button($"Order[{list_novel_order[_select_exec].list_novel_role[i].OrderID}]:{list_novel_order[_select_exec].list_novel_role[i].NovelRoleID.ToString()}"))
                    {
                        _select_order = i;
                    }
                    GUI.backgroundColor = Color.white;
                }

                // メニュー表示
                if (menu.GetItemCount() > 0)
                {
                    menu.ShowAsContext();
                    Event.current.Use();
                }


            }
            role_id = (Novel_Role_ID)EditorGUILayout.EnumPopup("", role_id);
            if (GUILayout.Button("ノベル役割追加"))
            {
                var add_order = NovelRoleGenerate.Create(role_id);
                add_order.OrderID = list_novel_order[_select_exec].list_novel_role.Count == 0 ? 0 : list_novel_order[_select_exec].list_novel_role[list_novel_order[_select_exec].list_novel_role.Count - 1].OrderID + 1;
                list_novel_order[_select_exec].list_novel_role.Add(add_order);
            }
            if (GUILayout.Button("Order修正"))
            {
                list_novel_order[_select_exec].list_novel_role.Sort((a,b) => a.OrderID.CompareTo(b.OrderID));
            }
        }


    }
    private void UpdateLayoutParameter()
    {
        using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_parameterScrollPosition, EditorStyles.helpBox))
        {
            _parameterScrollPosition = scroll.scrollPosition;

            GUILayout.Label("parameter");

            if (_select_exec < 0) return;
            if (_select_order < 0) return;

            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("OrderID");
                list_novel_order[_select_exec].list_novel_role[_select_order].OrderID = EditorGUILayout.IntField(list_novel_order[_select_exec].list_novel_role[_select_order].OrderID);
            }


            list_novel_order[_select_exec].list_novel_role[_select_order].OnGUI();

        }
    }

    private void UpdateOnGUIButton()
    {
        using (GUILayout.ScrollViewScope scroll = new GUILayout.ScrollViewScope(_buttonPosition, EditorStyles.helpBox, GUILayout.Height(150)))
        {
            _buttonPosition = scroll.scrollPosition;

            GUILayout.Label("test");
            using (new GUILayout.HorizontalScope())
            {
                GUILayout.Label("読み込みイベントID");
                _load_event_id = EditorGUILayout.IntField(_load_event_id);
            }
            if (GUILayout.Button("LoadEvent"))
            {
                _start_load_event_id = _load_event_id;
                string path = Application.dataPath + $"/Project/Assets/Editor/Data/Scenario/{_load_event_id}.json";
                if (System.IO.File.Exists(path) == false)
                {
                    path = Application.dataPath + $"/Scenario/{_load_event_id}.bin";
                    if (System.IO.File.Exists(path) == false)
                    {
                        Debug.Log("ファイルが存在しません");
                        return;
                    }
                    else
                    {


                        using (FileStream fileStream = new FileStream(path, FileMode.Open))
                        {
                            BinaryReader reader = new BinaryReader(fileStream);

                            BinaryReaderData(ref reader);
                            reader.Close();
                        }
                        return;
                    }
                }
                else
                {
                    using (FileStream fileStream = new FileStream(path, FileMode.Open))
                    {
                        using (StreamReader fileStreamReader = new StreamReader(fileStream))
                        {
                            string jsonStr = fileStreamReader.ReadToEnd();
                            var data = JsonConvert.DeserializeObject<RootNovelExecOrderObject>(jsonStr);
                            list_novel_order = new List<NovelExecOrderGroup>();
                            _select_exec = _select_order = -1;
                            JsonReaderData(ref data.Data);
                            data = null;
                            fileStreamReader.Close();
                        }

                    }

                }
            }
            if (GUILayout.Button("SaveEvent"))
            {
                string path = Application.dataPath + $"/Scenario/{_load_event_id}.bin";

                var json_data = JsonWriterData();

                using (FileStream fileStream = new FileStream(path, FileMode.Create))
                {
                    BinaryWriter writer = new BinaryWriter(fileStream);

                    BinaryWriterData(ref writer);
                    writer.Flush();
                    writer.Close();

                }
                RootNovelExecOrderObject root = new RootNovelExecOrderObject();
                root.Data = json_data;
                path = Application.dataPath + $"/Project/Assets/Editor/Data/Scenario/{_load_event_id}.json";
                using (FileStream fileStream = new FileStream(path, FileMode.Create))
                {
                    using (StreamWriter fileStreamWriter = new StreamWriter(fileStream))
                    {
                        string data = JsonConvert.SerializeObject(root);
                        fileStreamWriter.Write(data);
                    }

                }
            }

        }
    }
    public void OnGUI(ref Vector2 scroll,ref Novel_Role_ID role_id,EditorWindow window)
    {
        using (new GUILayout.HorizontalScope())
        {
            UpdateLayoutData();
            UpdateLayoutOrderData(ref  role_id);
            UpdateLayoutParameter();
        }

        UpdateOnGUIButton();

    }
#endif
}
