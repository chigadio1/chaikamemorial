using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// ノベルの実行グループクラス
/// </summary>
public class NovelExecOrderGroup
{
    /// <summary>
    /// 実行ID
    /// </summary>
    protected int exec_id = 0;
    public int ExecID { get { return exec_id; }  set { exec_id = value; } }




    public List<BaseNovelRole> list_novel_role = new List<BaseNovelRole>(); //役割リスト

    /// <summary>
    /// 強制終了フラグS
    /// </summary>
    private bool is_compulsion = false;
    public bool IsCompulsion { get { return is_compulsion; }  }
    public void Reset()
    {
        is_compulsion = false;
    }

    /// <summary>
    /// 読み込み
    /// </summary>
    /// <param name="reader"></param>
    public bool BinaryReaderData(ref BinaryReader reader)
    {
        if (reader == null) return false;

        try
        {
            exec_id = reader.ReadInt32();
            
            //役割の数を取得
            int role_count = reader.ReadInt32();
            for (int count = 0; count < role_count;)
            {
                Novel_Role_ID role_id;
                role_id = (Novel_Role_ID)reader.ReadInt32();
                BaseNovelRole role = NovelRoleGenerate.Create(role_id);
                if (role == null)
                {
                    return false;
                }
                role.BinaryReaderData(ref reader);
                list_novel_role.Add(role);
                role.BeginSetting();
                ++count;
            }
            
        }
        catch (IOException e)
        {
            // その他の入出力エラーが発生した場合の処理
            Debug.LogError("IO Exception: " + e.Message);
            return false;
        }

        return true;
    }
    /// <summary>
    /// 書き込み
    /// </summary>
    /// <param name="writer"></param>
    /// <returns></returns>
    public  bool BinaryWriterData(ref BinaryWriter writer)
    {
        if (writer == null) return false ;

        try
        {
            writer.Write(exec_id);

            writer.Write(list_novel_role.Count);

            foreach(var data in list_novel_role)
            {
                data.BinaryWriterData(ref writer);
            }
        }
        catch (IOException e)
        {
            // その他の入出力エラーが発生した場合の処理
            Debug.LogError("IO Exception: " + e.Message);
            return false;
        }

        return true;
    }

    public int BeginOrderID()
    {
        if (list_novel_role.Count <= 0) return -1;

        return list_novel_role[list_novel_role.Count - 1].OrderID;
    }


    /// <summary>
    /// Json読み込み
    /// </summary>
    /// <param name="reader"></param>
    public void JsonReaderData(ref List<Dictionary<string, object>> reader,int exec_key)
    {
        exec_id = exec_key;
        foreach (var order_role in reader)
        {
            Novel_Role_ID role_id;
            role_id = (Novel_Role_ID)Convert.ToInt32(order_role["role_id"]);

            var role = NovelRoleGenerate.Create(role_id);
            if (role == null) continue;

            role.JsonReaderData(order_role);
            list_novel_role.Add(role);
        }
    }

    /// <summary>
    /// Json書き込み
    /// </summary>
    /// <param name="directory"></param>
    public Dictionary<int,List<Dictionary<string,object>>> JsonWriterData()
    {
        var res = new Dictionary<int,List<Dictionary<string, object>>>();
        res[exec_id] = new List<Dictionary<string, object>>();

        foreach(var data in list_novel_role)
        {
            var column = new Dictionary<string, object>();

            data.JsonWriterData(ref column);

            res[exec_id].Add(column);
        }

        return res;
    }
    public void StartBeginInit()
    {
        if (list_novel_role.Count <= 0) return;
        if (is_compulsion) return;

        int order_id = list_novel_role[0].OrderID;
        foreach(var data in list_novel_role)
        {
            if(data.OrderID == order_id)
            {
                data.BeginInit();
            }
            else
            {
                return;
            }
        }
    }

    public bool Update(ref int order_count,bool auto_skip = false)
    {
        if (list_novel_role.Count <= 0) return true;
        if (is_compulsion) return true;

        bool finish = true;
        foreach(var data in list_novel_role)
        {
            if(order_count == data.OrderID)
            {
                data.UpdateExec();
                finish &= data.IsFinish;
                if (data.IsFinish || auto_skip)
                {
                    data.FinishUninit();
                    //強制ジャンプ系は終了
                    if(data.NovelRoleID == Novel_Role_ID.JumpScript || data.NovelRoleID == Novel_Role_ID.ChoicesSelect || data.NovelRoleID == Novel_Role_ID.JumpEvent || data.NovelRoleID == Novel_Role_ID.ProbabilityJumpScript ||
                       data.NovelRoleID == Novel_Role_ID.CompulsoryGameFinish || data.NovelRoleID == Novel_Role_ID.NickNameChoice || data.NovelRoleID == Novel_Role_ID.CharacterFavoJumpScript)
                    {
                        //ランダムジャンプで、確率に入らなければ終わる
                        if(data.NovelRoleID == Novel_Role_ID.ProbabilityJumpScript || data.NovelRoleID == Novel_Role_ID.CharacterFavoJumpScript)
                        {
                            if (data.IsSubFlag == false) continue;
                        }
                        finish = true;
                        if (data.NovelRoleID == Novel_Role_ID.CompulsoryGameFinish)
                        {
                            is_compulsion = true;
                            if (data.IsSubFlag == false)
                            {
                                continue;
                            }
                            return true;
                        }
                        return false;
                    }
                    
                }
            }
        }
        if (finish)
        {
            bool check = false;
            if (order_count == list_novel_role[list_novel_role.Count - 1].OrderID)
            {
                return true;
            }
            foreach (var data in list_novel_role)
            {
                if (order_count < data.OrderID && check == false)
                {
                    order_count = data.OrderID;
                    data.BeginInit();
                    check = true;
                }
                else if(check)
                {
                    if(order_count == data.OrderID)
                    {
                        data.BeginInit();
                    }
                }
            }
        }

        return false;
    }

#if UNITY_EDITOR
    private void DrawHorizontalLine(float width, Color color)
    {
        Rect lineRect = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(width));
        EditorGUI.DrawRect(lineRect, color);
    }
    private void DrawDashedLine(float width, Color color, float dash, float gap)
    {
        Rect lineRect = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(width));
        Handles.BeginGUI();
        Handles.color = color;

        Vector2 startPoint = new Vector2(lineRect.x, lineRect.y + lineRect.height / 2);
        Vector2 endPoint = new Vector2(lineRect.x + lineRect.width, lineRect.y + lineRect.height / 2);

        float currentDash = 0;

        while (currentDash < lineRect.width)
        {
            float dashDrawn = Mathf.Min(dash, lineRect.width - currentDash);
            Handles.DrawLine(new Vector2(startPoint.x + currentDash, startPoint.y), new Vector2(startPoint.x + currentDash + dashDrawn, startPoint.y));
            currentDash += dash + gap;
        }

        Handles.EndGUI();
    }
    public int OnGUI()
    {


        list_novel_role.Sort((a, b) => a.OrderID.CompareTo(b.OrderID));
        int index = 0;
        //DrawHorizontalLine(10.0f, Color.black);
        for (index = 0; index < list_novel_role.Count;)
        {
            //EditorGUILayout.BeginVertical();
        //    EditorGUILayout.LabelField($"===={list_novel_role[index].NovelRoleID.ToString()}===", GUILayout.Width(200));
        //    EditorGUILayout.BeginHorizontal(GUILayout.Width(100));
        //    EditorGUILayout.LabelField("ExecID:", GUILayout.Width(100));
        //    GUILayout.Space(-50);
            //int exeec_change_id = EditorGUILayout.IntField("", exec_id, GUILayout.Width(50));
            //EditorGUILayout.EndHorizontal();
            //if (exec_id != exeec_change_id)
            //{
            //    EditorGUILayout.EndVertical();
            //    return index;
            //}
            //EditorGUILayout.BeginHorizontal(GUILayout.Width(100));
            //EditorGUILayout.LabelField("OrderID:", GUILayout.Width(100));;
            //GUILayout.Space(-50);
            //int order_id = EditorGUILayout.IntField("", list_novel_role[index].OrderID, GUILayout.Width(50));
            //EditorGUILayout.EndHorizontal();
            //if (order_id != list_novel_role[index].OrderID)
            //{
            //    list_novel_role[index].OrderID = order_id;
            //    list_novel_role.Sort((a, b) => a.OrderID.CompareTo(b.OrderID));
            //}
            //EditorGUILayout.BeginHorizontal(GUILayout.Width(100));
            list_novel_role[index].OnGUI();
            //EditorGUILayout.EndHorizontal();
            //if (GUILayout.Button("×",GUILayout.Width(150)))
            //{
            //    list_novel_role.RemoveAt(index);
            //}
            
            ++index;
            //EditorGUILayout.EndVertical();

            //DrawDashedLine(10.0f, Color.white, 5, 5);


        }
        //DrawHorizontalLine(10.0f, Color.black);


        return 0;
    }


#endif

}
