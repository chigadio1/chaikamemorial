using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BasePlayAudioSENovelRole : BaseNovelRole {
    protected int sound_id = 0; //サウンドID
    protected float delay_time = 0.0f; //待ち時間
    protected float volume = 0.0f; //ボリューム
    public BasePlayAudioSENovelRole(BasePlayAudioSENovelRole other):base(other) {
        role_id = Novel_Role_ID.PlayAudioSE;
        sound_id = other.sound_id;
        delay_time = other.delay_time;
        volume = other.volume;
    }
    public  BasePlayAudioSENovelRole():base() {
        role_id = Novel_Role_ID.PlayAudioSE;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        sound_id = reader.ReadInt32();
        delay_time = reader.ReadSingle();
        volume = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(sound_id);
        writer.Write(delay_time);
        writer.Write(volume);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("sound_id")) sound_id = Convert.ToInt32(directory["sound_id"]);
        if (directory.ContainsKey("delay_time")) delay_time = Convert.ToSingle(directory["delay_time"]);
        if (directory.ContainsKey("volume")) volume = Convert.ToSingle(directory["volume"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["sound_id"] = (int)sound_id;
        directory["delay_time"] = (float)delay_time;
        directory["volume"] = (float)volume;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("サウンドID(INT):", GUILayout.Width(150));
        sound_id = EditorGUILayout.IntField("",sound_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("待ち時間(FLOAT):", GUILayout.Width(150));
        delay_time = EditorGUILayout.FloatField("",delay_time, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("ボリューム(FLOAT):", GUILayout.Width(150));
        volume = EditorGUILayout.FloatField("",volume, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
