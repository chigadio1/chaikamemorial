using System.Collections;

public class LoadSpriteNovelRole : BaseLoadSpriteNovelRole {
    public LoadSpriteNovelRole(LoadSpriteNovelRole other):base(other) {
    }

    public LoadSpriteNovelRole():base() {
    }

    public override void BeginSetting()  {
        //ロード開始
        gameUiCore.Instance?.LoadSprite(this.sprite_id, this.sprite_type_id);
    }

    public override void BeginInit()  {
       
    }

    public override void UpdateExec()  {
        is_finish = true;
    }

    public override void FinishUninit()  {
    }
}
