using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseLoadSpriteNovelRole : BaseNovelRole {
    protected int sprite_id = 0; //
    protected Novel_Sprite_Type_ID sprite_type_id = Novel_Sprite_Type_ID.NONE; //)
    public BaseLoadSpriteNovelRole(BaseLoadSpriteNovelRole other):base(other) {
        role_id = Novel_Role_ID.LoadSprite;
        sprite_id = other.sprite_id;
        sprite_type_id = other.sprite_type_id;
    }
    public  BaseLoadSpriteNovelRole():base() {
        role_id = Novel_Role_ID.LoadSprite;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        sprite_id = reader.ReadInt32();
        sprite_type_id = (Novel_Sprite_Type_ID)reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(sprite_id);
        writer.Write((int)sprite_type_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("sprite_id")) sprite_id = Convert.ToInt32(directory["sprite_id"]);
        if (directory.ContainsKey("sprite_type_id")) sprite_type_id = (Novel_Sprite_Type_ID)Convert.ToInt32(directory["sprite_type_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["sprite_id"] = (int)sprite_id;
        directory["sprite_type_id"] = (int)sprite_type_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("(INT):", GUILayout.Width(150));
        sprite_id = EditorGUILayout.IntField("",sprite_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("(SPRITETYPE):", GUILayout.Width(150));
        sprite_type_id = (Novel_Sprite_Type_ID)EditorGUILayout.EnumPopup("", sprite_type_id);
        GUILayout.EndHorizontal();
    }
#endif
}
