using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseStillRootAnimationNovelRole : BaseNovelRole {
    protected NovelAnimationData animation = new NovelAnimationData(); //アニメーションデータ)
    protected int still_id = 0; //スチルID
    public BaseStillRootAnimationNovelRole(BaseStillRootAnimationNovelRole other):base(other) {
        role_id = Novel_Role_ID.StillRootAnimation;
        animation = other.animation;
        still_id = other.still_id;
    }
    public  BaseStillRootAnimationNovelRole():base() {
        role_id = Novel_Role_ID.StillRootAnimation;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        animation.BinaryReader(ref reader);
        still_id = reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        animation.BinaryWriter(ref writer);
        writer.Write(still_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        animation.JsonReaderData(directory);
        if (directory.ContainsKey("still_id")) still_id = Convert.ToInt32(directory["still_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        animation.JsonWriterData(ref directory);
        directory["still_id"] = (int)still_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        animation.OnGUI();
        GUILayout.BeginHorizontal();
        GUILayout.Label("スチルID(INT):", GUILayout.Width(150));
        still_id = EditorGUILayout.IntField("",still_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
