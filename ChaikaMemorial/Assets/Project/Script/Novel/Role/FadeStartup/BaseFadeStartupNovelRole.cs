using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseFadeStartupNovelRole : BaseNovelRole {
    protected Fade_Type_ID fade_type_id = Fade_Type_ID.NONE; //フェードタイプ)
    protected int fade_sprite_id = 0; //フェード画像ID
    protected float end_time = 0.0f; //終了時間
    public BaseFadeStartupNovelRole(BaseFadeStartupNovelRole other):base(other) {
        role_id = Novel_Role_ID.FadeStartup;
        fade_type_id = other.fade_type_id;
        fade_sprite_id = other.fade_sprite_id;
        end_time = other.end_time;
    }
    public  BaseFadeStartupNovelRole():base() {
        role_id = Novel_Role_ID.FadeStartup;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        fade_type_id = (Fade_Type_ID)reader.ReadInt32();
        fade_sprite_id = reader.ReadInt32();
        end_time = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write((int)fade_type_id);
        writer.Write(fade_sprite_id);
        writer.Write(end_time);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("fade_type_id")) fade_type_id = (Fade_Type_ID)Convert.ToInt32(directory["fade_type_id"]);
        if (directory.ContainsKey("fade_sprite_id")) fade_sprite_id = Convert.ToInt32(directory["fade_sprite_id"]);
        if (directory.ContainsKey("end_time")) end_time = Convert.ToSingle(directory["end_time"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["fade_type_id"] = (int)fade_type_id;
        directory["fade_sprite_id"] = (int)fade_sprite_id;
        directory["end_time"] = (float)end_time;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェードタイプ(FADETYPEID):", GUILayout.Width(150));
        fade_type_id = (Fade_Type_ID)EditorGUILayout.EnumPopup("", fade_type_id);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェード画像ID(INT):", GUILayout.Width(150));
        fade_sprite_id = EditorGUILayout.IntField("",fade_sprite_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("終了時間(FLOAT):", GUILayout.Width(150));
        end_time = EditorGUILayout.FloatField("",end_time, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
