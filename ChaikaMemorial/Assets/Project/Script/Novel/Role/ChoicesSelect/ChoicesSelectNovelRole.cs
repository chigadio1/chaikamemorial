using System.Collections;
using UnityEngine;

public class ChoicesSelectNovelRole : BaseChoicesSelectNovelRole {

    NovelChoiceStateUpdateArgData update_arg;
    NovelChoiceStateInitArgData init_arg;
    NovelChoiceStateStateManager manager;

    int jump_exec_id = 0;
    int jump_order_id = 0;
    public ChoicesSelectNovelRole(ChoicesSelectNovelRole other):base(other) {
    }

    public ChoicesSelectNovelRole():base() {
    }

    public override void BeginSetting()  {

    }

    public override void BeginInit() 
    {
        is_finish = false;
        manager = new NovelChoiceStateStateManager();
        update_arg = new NovelChoiceStateUpdateArgData();
        init_arg = new NovelChoiceStateInitArgData();

        update_arg.choice_summary = choice_summary_data;
        init_arg.choice_summary = choice_summary_data;
    }

    public override void UpdateExec()  {
        if (is_finish) return;

        manager.Update(init_arg, update_arg);
        if(manager.IsFinish)
        {
            jump_exec_id = update_arg.jump_exec_id;
            jump_order_id = update_arg.jump_order_id;
            is_finish = true;
        }

    }

    public override void FinishUninit()  {
       

        gameUiCore.Instance?.NovelJump(jump_exec_id,jump_order_id);

        manager = null;
        update_arg = null;
        init_arg = null;
    }
}
