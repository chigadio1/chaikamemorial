using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseChoicesSelectNovelRole : BaseNovelRole {
    protected ChoiceSummaryData choice_summary_data = new ChoiceSummaryData(); //選択肢データ)
    public BaseChoicesSelectNovelRole(BaseChoicesSelectNovelRole other):base(other) {
        role_id = Novel_Role_ID.ChoicesSelect;
        choice_summary_data = other.choice_summary_data;
    }
    public  BaseChoicesSelectNovelRole():base() {
        role_id = Novel_Role_ID.ChoicesSelect;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        choice_summary_data.BinaryReader(ref reader);
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        choice_summary_data.BinaryWriter(ref writer);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        choice_summary_data.JsonReaderData(directory);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        choice_summary_data.JsonWriterData(ref directory);
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        choice_summary_data.OnGUI();
    }
#endif
}
