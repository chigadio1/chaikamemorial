using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseJumpEventNovelRole : BaseNovelRole {
    protected int event_id = 0; //再生するイベントID
    public BaseJumpEventNovelRole(BaseJumpEventNovelRole other):base(other) {
        role_id = Novel_Role_ID.JumpEvent;
        event_id = other.event_id;
    }
    public  BaseJumpEventNovelRole():base() {
        role_id = Novel_Role_ID.JumpEvent;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        event_id = reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(event_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("event_id")) event_id = Convert.ToInt32(directory["event_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["event_id"] = (int)event_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("再生するイベントID(INT):", GUILayout.Width(150));
        event_id = EditorGUILayout.IntField("",event_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
