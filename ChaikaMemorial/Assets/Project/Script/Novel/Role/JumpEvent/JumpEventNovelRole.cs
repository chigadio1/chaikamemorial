using System.Collections;

public class JumpEventNovelRole : BaseJumpEventNovelRole {
    public JumpEventNovelRole(JumpEventNovelRole other):base(other) {
    }

    public JumpEventNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
    }

    public override void UpdateExec()  {
        is_finish = true;
        PlayerCore.Instance.event_id = event_id;
        PlayerCore.Instance.is_next_event = true;
    }

    public override void FinishUninit()  {
    }
}
