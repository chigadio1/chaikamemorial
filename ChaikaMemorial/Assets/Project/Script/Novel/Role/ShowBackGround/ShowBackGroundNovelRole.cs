using System.Collections;
using UnityEngine;

public class ShowBackGroundNovelRole : BaseShowBackGroundNovelRole {
    /// <summary>
    /// フレーム
    /// </summary>
    float time = 0.0f; //フレーム数


    public ShowBackGroundNovelRole(ShowBackGroundNovelRole other):base(other) {
    }

    public ShowBackGroundNovelRole():base() {
    }

    public override void BeginSetting()  {
        if (this.sprite_id <= 0) return;
        gameUiCore.Instance.LoadSprite(this.sprite_id, Novel_Sprite_Type_ID.BACKGROUND);
    }

    public override void BeginInit()  {
        //gameUiCore.Instance.Back
        is_finish = false;
        time = 0.0f;
        gameUiCore.Instance.NovelBackGroundCanvas.SetBackGroundImage(this.sprite_id);
    }

    public override void UpdateExec()  {
        if (is_finish) return;
        if (this.alpha_time <= 0)
        {
            gameUiCore.Instance.NovelBackGroundCanvas.SetAlpha(this.alpha);
            is_finish = true;
            return;
        }
        time += Time.deltaTime;
        time = Mathf.Clamp(time, 0.0f, this.alpha_time);
        float value_alpha = Mathf.Lerp(0.0f, this.alpha, time / this.alpha_time);


        gameUiCore.Instance.NovelBackGroundCanvas.SetAlpha(value_alpha);

        if(time >= this.alpha_time)
        {
            is_finish = true;
        }

    }

    public override void FinishUninit()  {
        gameUiCore.Instance.NovelBackGroundCanvas.SetAlpha(this.alpha);
    }
}
