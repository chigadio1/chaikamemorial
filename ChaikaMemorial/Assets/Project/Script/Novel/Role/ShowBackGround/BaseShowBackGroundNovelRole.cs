using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseShowBackGroundNovelRole : BaseNovelRole {
    protected int sprite_id = 0; //画像ID
    protected float alpha = 1.0f; //アルファ値
    protected float alpha_time = 1.0f; //アルファ値遷移
    public BaseShowBackGroundNovelRole(BaseShowBackGroundNovelRole other):base(other) {
        role_id = Novel_Role_ID.ShowBackGround;
        sprite_id = other.sprite_id;
        alpha = other.alpha;
        alpha_time = other.alpha_time;
    }
    public  BaseShowBackGroundNovelRole():base() {
        role_id = Novel_Role_ID.ShowBackGround;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        sprite_id = reader.ReadInt32();
        alpha = reader.ReadSingle();
        alpha_time = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(sprite_id);
        writer.Write(alpha);
        writer.Write(alpha_time);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("sprite_id")) sprite_id = Convert.ToInt32(directory["sprite_id"]);
        if (directory.ContainsKey("alpha")) alpha = Convert.ToSingle(directory["alpha"]);
        if (directory.ContainsKey("alpha_time")) alpha_time = Convert.ToSingle(directory["alpha_time"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["sprite_id"] = (int)sprite_id;
        directory["alpha"] = (float)alpha;
        directory["alpha_time"] = (float)alpha_time;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("画像ID(INT):", GUILayout.Width(150));
        sprite_id = EditorGUILayout.IntField("",sprite_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("アルファ値(FLOAT):", GUILayout.Width(150));
        alpha = EditorGUILayout.FloatField("",alpha, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("アルファ値遷移(FLOAT):", GUILayout.Width(150));
        alpha_time = EditorGUILayout.FloatField("",alpha_time, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
