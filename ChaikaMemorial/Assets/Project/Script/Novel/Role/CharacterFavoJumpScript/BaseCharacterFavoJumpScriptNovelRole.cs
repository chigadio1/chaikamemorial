using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseCharacterFavoJumpScriptNovelRole : BaseNovelRole {
    protected Character_ID game_character_id = Character_ID.NONE; //キャラID)
    protected CharacterFavoJump favo_jump = new CharacterFavoJump(); //友好度ジャンプ)
    public BaseCharacterFavoJumpScriptNovelRole(BaseCharacterFavoJumpScriptNovelRole other):base(other) {
        role_id = Novel_Role_ID.CharacterFavoJumpScript;
        game_character_id = other.game_character_id;
        favo_jump = other.favo_jump;
    }
    public  BaseCharacterFavoJumpScriptNovelRole():base() {
        role_id = Novel_Role_ID.CharacterFavoJumpScript;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        game_character_id = (Character_ID)reader.ReadInt32();
        favo_jump.BinaryReader(ref reader);
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write((int)game_character_id);
        favo_jump.BinaryWriter(ref writer);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("game_character_id")) game_character_id = (Character_ID)Convert.ToInt32(directory["game_character_id"]);
        favo_jump.JsonReaderData(directory);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["game_character_id"] = (int)game_character_id;
        favo_jump.JsonWriterData(ref directory);
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("キャラID(GameCharacterID):", GUILayout.Width(150));
        game_character_id = (Character_ID)EditorGUILayout.EnumPopup("", game_character_id);
        GUILayout.EndHorizontal();
        favo_jump.OnGUI();
    }
#endif
}
