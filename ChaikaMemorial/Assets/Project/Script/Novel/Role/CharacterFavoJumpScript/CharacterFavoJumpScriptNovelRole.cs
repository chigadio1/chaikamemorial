using System.Collections;

public class CharacterFavoJumpScriptNovelRole : BaseCharacterFavoJumpScriptNovelRole {

    bool is_jump = false;
    public CharacterFavoJumpScriptNovelRole(CharacterFavoJumpScriptNovelRole other):base(other) {
    }

    public CharacterFavoJumpScriptNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_jump = false;
        is_sub_flag = false;
    }

    public override void UpdateExec()  {
        is_finish = true;
    }

    public override void FinishUninit()  {
        if (is_jump == true) return;
        var data = DateUnit.JumpCharacterFavoSelect(this.game_character_id, this.favo_jump);
        if (data == null) return;

        gameUiCore.Instance.NovelJump(data.jump_exec_id,data.jump_order_id);

        is_sub_flag = true;
        
    }
}
