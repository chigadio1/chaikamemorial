using System.Collections;
using UnityEngine;

public class DelayNovelRole : BaseDelayNovelRole {

    float calc_time = 0.0f;
    public DelayNovelRole(DelayNovelRole other):base(other) {
    }

    public DelayNovelRole():base() {
    }

    public override void BeginSetting()  {
        calc_time = 0.0f;
        is_finish = false;
    }

    public override void BeginInit()  {
        calc_time = 0.0f;
        is_finish = false;
    }

    public override void UpdateExec()  {
        calc_time += Time.deltaTime;
        calc_time = Mathf.Clamp(calc_time, 0.0f, delay_time);
        if (calc_time >= delay_time)
        {
            is_finish = true;
        }
    }

    public override void FinishUninit()  {
    }
}
