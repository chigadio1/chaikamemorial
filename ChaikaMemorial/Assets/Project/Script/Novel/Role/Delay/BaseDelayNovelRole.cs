using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseDelayNovelRole : BaseNovelRole {
    protected float delay_time = 0.0f; //待ち時間
    public BaseDelayNovelRole(BaseDelayNovelRole other):base(other) {
        role_id = Novel_Role_ID.Delay;
        delay_time = other.delay_time;
    }
    public  BaseDelayNovelRole():base() {
        role_id = Novel_Role_ID.Delay;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        delay_time = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(delay_time);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("delay_time")) delay_time = Convert.ToSingle(directory["delay_time"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["delay_time"] = (float)delay_time;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("待ち時間(FLOAT):", GUILayout.Width(150));
        delay_time = EditorGUILayout.FloatField("",delay_time, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
