using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseCharacterFavoUINovelRole : BaseNovelRole {
    public BaseCharacterFavoUINovelRole(BaseCharacterFavoUINovelRole other):base(other) {
        role_id = Novel_Role_ID.CharacterFavoUI;
    }
    public  BaseCharacterFavoUINovelRole():base() {
        role_id = Novel_Role_ID.CharacterFavoUI;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
    }
#endif
}
