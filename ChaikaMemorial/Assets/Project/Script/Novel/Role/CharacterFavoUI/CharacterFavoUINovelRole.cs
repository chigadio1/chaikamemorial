using System.Collections;

public class CharacterFavoUINovelRole : BaseCharacterFavoUINovelRole {

    CharacterFavoSystemNovelObjectStateBranchManager manager = new CharacterFavoSystemNovelObjectStateBranchManager();
    public CharacterFavoUINovelRole(CharacterFavoUINovelRole other):base(other) {
    }

    public CharacterFavoUINovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
        manager = new CharacterFavoSystemNovelObjectStateBranchManager();
    }

    public override void UpdateExec()  {
        if (is_finish == true) return;
        manager.Update();
        if(manager.IsFinish)
        {
            is_finish = true;
        }
    }

    public override void FinishUninit()  {
    }
}
