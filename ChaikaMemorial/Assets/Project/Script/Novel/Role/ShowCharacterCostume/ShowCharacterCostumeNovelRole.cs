using System.Collections;
using UnityEngine;

public class ShowCharacterCostumeNovelRole : BaseShowCharacterCostumeNovelRole {
    /// <summary>
    /// フレーム
    /// </summary>
    float time = 0.0f; //フレーム数

    /// <summary>
    /// アルファ値
    /// </summary>
    float alpha = 0.0f;

    int sprite_id = 0;
    public ShowCharacterCostumeNovelRole(ShowCharacterCostumeNovelRole other):base(other) {
    }

    public ShowCharacterCostumeNovelRole():base() {
    }

    public override void BeginSetting()  {
        sprite_id = gameUiCore.Instance.GetCharacterSptite(game_chara_id, clothing_id, facial_id);
        if(sprite_id > 0)
        {
            gameUiCore.Instance.LoadSprite(sprite_id, Novel_Sprite_Type_ID.CHARACTER);
        }
    }

    public override void BeginInit()  {
        is_finish = false;
        time = 0.0f;
        if (sprite_id > 0)
        {
            gameUiCore.Instance.CharacterCanvas.SetCharacter(character_id, sprite_id);
        }
        if(fade_out_frame > 0)gameUiCore.Instance.CharacterCanvas.SetAlpha(character_id, 0.0f);
        else gameUiCore.Instance.CharacterCanvas.SetAlpha(character_id, 1.0f);
        gameUiCore.Instance.CharacterCanvas.SetCharacterPosition(character_id, show_position);
    }

    public override void UpdateExec()  {
        if (is_finish) return;
        if(fade_out_frame <= 0)
        {
            is_finish = true;
            return;
        }
        time += Time.deltaTime;
        time = Mathf.Clamp(time, 0.0f, this.fade_out_frame);
        if (this.fade_out_frame == 0.0f)
        {
            alpha = 1.0f;
        }
        else alpha = Mathf.Lerp(0.0f, 1.0f, time / this.fade_out_frame);


        gameUiCore.Instance.CharacterCanvas.SetAlpha(character_id, alpha);
        if (alpha >= 1.0f)
        {
            is_finish = true;

        }
    }

    public override void FinishUninit()  {
    }
}
