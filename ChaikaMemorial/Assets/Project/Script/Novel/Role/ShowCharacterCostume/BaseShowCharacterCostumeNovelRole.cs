using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseShowCharacterCostumeNovelRole : BaseNovelRole {
    protected Novel_Character_ID character_id = Novel_Character_ID.NONE; //ノベルのキャラID)
    protected CharacterClothingTypeID clothing_id = CharacterClothingTypeID.NONE; //服ID)
    protected FacialExpressionID facial_id = FacialExpressionID.NONE; //表情ID)
    protected float fade_out_frame = 0.0f; //フェードアウトフレーム
    protected Vector2 show_position = Vector2.zero; //表示位置)
    protected Character_ID game_chara_id = Character_ID.NONE; //)
    public BaseShowCharacterCostumeNovelRole(BaseShowCharacterCostumeNovelRole other):base(other) {
        role_id = Novel_Role_ID.ShowCharacterCostume;
        character_id = other.character_id;
        clothing_id = other.clothing_id;
        facial_id = other.facial_id;
        fade_out_frame = other.fade_out_frame;
        show_position = other.show_position;
        game_chara_id = other.game_chara_id;
    }
    public  BaseShowCharacterCostumeNovelRole():base() {
        role_id = Novel_Role_ID.ShowCharacterCostume;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        character_id = (Novel_Character_ID)reader.ReadInt32();
        clothing_id = (CharacterClothingTypeID)reader.ReadInt32();
        facial_id = (FacialExpressionID)reader.ReadInt32();
        fade_out_frame = reader.ReadSingle();
        show_position = new Vector2(reader.ReadSingle(),reader.ReadSingle());
        game_chara_id = (Character_ID)reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write((int)character_id);
        writer.Write((int)clothing_id);
        writer.Write((int)facial_id);
        writer.Write(fade_out_frame);
        writer.Write(show_position.x);
        writer.Write(show_position.y);
        writer.Write((int)game_chara_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("character_id")) character_id = (Novel_Character_ID)Convert.ToInt32((directory["character_id"]));
        if (directory.ContainsKey("clothing_id")) clothing_id = (CharacterClothingTypeID)Convert.ToInt32(directory["clothing_id"]);
        if (directory.ContainsKey("facial_id")) facial_id = (FacialExpressionID)Convert.ToInt32(directory["facial_id"]);
        if (directory.ContainsKey("fade_out_frame")) fade_out_frame = Convert.ToSingle(directory["fade_out_frame"]);
        if (directory.ContainsKey("show_position_x")) show_position.x = Convert.ToSingle(directory["show_position_x"]);
        if (directory.ContainsKey("show_position_y")) show_position.y = Convert.ToSingle(directory["show_position_y"]);
        if (directory.ContainsKey("game_chara_id")) game_chara_id = (Character_ID)Convert.ToInt32(directory["game_chara_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["character_id"] = (int)character_id;
        directory["clothing_id"] = (int)clothing_id;
        directory["facial_id"] = (int)facial_id;
        directory["fade_out_frame"] = (float)fade_out_frame;
        directory["show_position_x"] = (float)show_position.x;
        directory["show_position_y"] = (float)show_position.y;
        directory["game_chara_id"] = (int)game_chara_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("ノベルのキャラID(CHARACTERID):", GUILayout.Width(150));
        character_id = (Novel_Character_ID)EditorGUILayout.EnumPopup("", character_id);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("服ID(CharacterClothingTypeID):", GUILayout.Width(150));
        clothing_id = (CharacterClothingTypeID)EditorGUILayout.EnumPopup("", clothing_id);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("表情ID(FacialExpressionID):", GUILayout.Width(150));
        facial_id = (FacialExpressionID)EditorGUILayout.EnumPopup("", facial_id);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェードアウトフレーム(FLOAT):", GUILayout.Width(150));
        fade_out_frame = EditorGUILayout.FloatField("",fade_out_frame, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginVertical();
        float show_position_X,show_position_Y = 0.0f;
        GUILayout.BeginHorizontal();
        GUILayout.Label("表示位置.X(VECTOR2.X):", GUILayout.Width(150));
        show_position_X = EditorGUILayout.FloatField("",show_position.x, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("表示位置.Y(VECTOR2.Y):", GUILayout.Width(150));
        show_position_Y = EditorGUILayout.FloatField("",show_position.y, GUILayout.Width(250));
        show_position = new Vector2(show_position_X,show_position_Y);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("(GameCharacterID):", GUILayout.Width(150));
        game_chara_id = (Character_ID)EditorGUILayout.EnumPopup("", game_chara_id);
        GUILayout.EndHorizontal();
    }
#endif
}
