using System.Collections;
using UnityEngine;

public class CutFadeStarUpNovelRole : BaseCutFadeStarUpNovelRole {

    /// <summary>
    /// �v���^�C��
    /// </summary>
    private float add_calc_time = 0.0f;
    public CutFadeStarUpNovelRole(CutFadeStarUpNovelRole other):base(other) {
    }

    public CutFadeStarUpNovelRole():base() {
    }

    public override void BeginSetting()  {

    }

    public override void BeginInit()  {
        is_finish = false;
        if (this.fade_type_id == Fade_Type_ID.Fade_OUT)
        {
            gameUiCore.Instance?.SettingCanvas?.SetCutFadeAlpha(1.0f);
        }
        else if (this.fade_type_id != Fade_Type_ID.Fade_IN)
        {
            gameUiCore.Instance?.SettingCanvas?.SetCutFadeAlpha(0.0f);
        }
    }

    public override void UpdateExec()  {
        add_calc_time += Time.deltaTime;
        add_calc_time = Mathf.Clamp(add_calc_time, 0.0f, this.end_time);
        float alpha = add_calc_time / this.end_time;
        if (add_calc_time >= this.end_time)
        {
            is_finish = true;
        }

        if (this.fade_type_id == Fade_Type_ID.Fade_OUT)
        {
            alpha = 1.0f - add_calc_time / this.end_time;
        }

        alpha = Mathf.Abs(alpha);
        alpha = Mathf.Clamp(alpha, 0.0f, 1.0f);



        gameUiCore.Instance?.SettingCanvas?.SetCutFadeAlpha(alpha);
    }

    public override void FinishUninit()  {
        add_calc_time = 0.0f;
        if (this.fade_type_id == Fade_Type_ID.Fade_OUT)
        {
            gameUiCore.Instance?.SettingCanvas?.SetCutFadeAlpha(0.0f);
        }
        else if (this.fade_type_id != Fade_Type_ID.Fade_IN)
        {
            gameUiCore.Instance?.SettingCanvas?.SetCutFadeAlpha(1.0f);
        }
    }
}
