using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif


/// <summary>
/// 基礎ノベル実行
/// </summary>
public class BaseNovelRole
{

    /// <summary>
    /// 順番ID
    /// </summary>
    protected int order_id = 0;
    public int OrderID { get { return order_id; } set { order_id = value; } }

    /// <summary>
    /// ノベルID
    /// </summary>
    protected Novel_Role_ID role_id;
    public Novel_Role_ID NovelRoleID { get { return role_id; }  }

    /// <summary>
    /// 終了フラグ
    /// </summary>
    protected bool is_finish = false;
    public bool IsFinish { get { return is_finish;} }

    /// <summary>
    ///　サブラグ
    /// </summary>
    protected bool is_sub_flag = false;
    public bool IsSubFlag { get { return is_sub_flag;} }

    public BaseNovelRole() { }
    public BaseNovelRole(BaseNovelRole other)
    {
        order_id = other.order_id;
    }



    /// <summary>
    /// 読み込み
    /// </summary>
    /// <param name="reader"></param>
    public virtual void BinaryReaderData(ref BinaryReader reader) 
    {
        if (reader == null) return;

        order_id = reader.ReadInt32();
    }

    /// <summary>
    /// 書き込み
    /// </summary>
    /// <param name="reader"></param>
    public virtual void BinaryWriterData(ref BinaryWriter writer)
    {
        if (writer == null) return;

        writer.Write(((int)role_id));
        writer.Write(order_id);
    }

    /// <summary>
    /// Json読み込み
    /// </summary>
    /// <param name="directory"></param>
    public virtual void JsonReaderData(Dictionary<string,object> directory)
    {
        if (directory.ContainsKey("role_id")) role_id = (Novel_Role_ID)Convert.ToInt32(directory["role_id"]);
        if (directory.ContainsKey("order_id")) order_id = Convert.ToInt32(directory["order_id"]);
    }

    /// <summary>
    /// Json書き込み
    /// </summary>
    /// <param name="directory"></param>
    public virtual void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["role_id"] = (int)role_id;
        directory["order_id"] = (int)order_id;
    }

    public virtual int GetSize()
    {
        int size =  sizeof(float) * 2; //role_id と order_id
        return size;
    }

    /// <summary>
    /// 初期設定
    /// </summary>
    public virtual void BeginSetting() { }
    /// <summary>
    /// 初期化
    /// </summary>
    public virtual void BeginInit() { }

    /// <summary>
    /// 更新実行
    /// </summary>
    public virtual void UpdateExec() { }

    /// <summary>
    /// 最終実行
    /// </summary>
    public virtual void FinishUninit() { }

#if UNITY_EDITOR
    public virtual void OnGUI() { }
#endif
}

