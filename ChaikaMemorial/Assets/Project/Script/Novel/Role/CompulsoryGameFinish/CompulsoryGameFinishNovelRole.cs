using System.Collections;

public class CompulsoryGameFinishNovelRole : BaseCompulsoryGameFinishNovelRole {
    public CompulsoryGameFinishNovelRole(CompulsoryGameFinishNovelRole other):base(other) {
    }

    public CompulsoryGameFinishNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
    }

    public override void UpdateExec()  {
        is_finish = true;
    }

    public override void FinishUninit()  {
    }
}
