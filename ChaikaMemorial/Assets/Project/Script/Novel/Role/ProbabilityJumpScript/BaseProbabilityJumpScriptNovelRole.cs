using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseProbabilityJumpScriptNovelRole : BaseNovelRole {
    protected float probability = 0.0f; //確率
    protected int jump_exec_id = 0; //ジャンプ実行ID
    protected int jump_order_id = 0; //ジャンプ命令ID
    public BaseProbabilityJumpScriptNovelRole(BaseProbabilityJumpScriptNovelRole other):base(other) {
        role_id = Novel_Role_ID.ProbabilityJumpScript;
        probability = other.probability;
        jump_exec_id = other.jump_exec_id;
        jump_order_id = other.jump_order_id;
    }
    public  BaseProbabilityJumpScriptNovelRole():base() {
        role_id = Novel_Role_ID.ProbabilityJumpScript;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        probability = reader.ReadSingle();
        jump_exec_id = reader.ReadInt32();
        jump_order_id = reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(probability);
        writer.Write(jump_exec_id);
        writer.Write(jump_order_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("probability")) probability = Convert.ToSingle(directory["probability"]);
        if (directory.ContainsKey("jump_exec_id")) jump_exec_id = Convert.ToInt32(directory["jump_exec_id"]);
        if (directory.ContainsKey("jump_order_id")) jump_order_id = Convert.ToInt32(directory["jump_order_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["probability"] = (float)probability;
        directory["jump_exec_id"] = (int)jump_exec_id;
        directory["jump_order_id"] = (int)jump_order_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("確率(FLOAT):", GUILayout.Width(150));
        probability = EditorGUILayout.FloatField("",probability, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("ジャンプ実行ID(INT):", GUILayout.Width(150));
        jump_exec_id = EditorGUILayout.IntField("",jump_exec_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("ジャンプ命令ID(INT):", GUILayout.Width(150));
        jump_order_id = EditorGUILayout.IntField("",jump_order_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
