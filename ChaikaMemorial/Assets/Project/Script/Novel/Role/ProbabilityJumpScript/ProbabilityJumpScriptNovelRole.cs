using System.Collections;
using UnityEngine;

public class ProbabilityJumpScriptNovelRole : BaseProbabilityJumpScriptNovelRole {

    bool is_random = false;
    public ProbabilityJumpScriptNovelRole(ProbabilityJumpScriptNovelRole other):base(other) {
    }

    public ProbabilityJumpScriptNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
        is_random = false;
        is_sub_flag = false;
    }

    public override void UpdateExec()  {
        is_finish = true;
    }

    public override void FinishUninit()  {
        if (is_random) return;
        float value = Random.Range(0, 101);
        value = Mathf.Clamp(value, 0, 100);

        if (value <= this.probability)
        {

            gameUiCore.Instance?.NovelJump(this.jump_exec_id, this.jump_order_id);
            is_sub_flag = true;
            return;
        }
        is_random = true;

    }
}
