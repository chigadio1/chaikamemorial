using System.Collections;
using UnityEngine;
public class ShowCharacterNovelRole : BaseShowCharacterNovelRole {

    /// <summary>
    /// フレーム
    /// </summary>
    float time = 0.0f; //フレーム数

    /// <summary>
    /// アルファ値
    /// </summary>
    float alpha = 0.0f;
    public ShowCharacterNovelRole(ShowCharacterNovelRole other):base(other) {
    }

    public ShowCharacterNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
        time = 0.0f;
        gameUiCore.Instance.CharacterCanvas.SetCharacter(character_id, character_sprite_id);
        gameUiCore.Instance.CharacterCanvas.SetAlpha(character_id, 0.0f);
        gameUiCore.Instance.CharacterCanvas.SetCharacterPosition(character_id, show_position);
    }

    public override void UpdateExec()  {
        if (is_finish) return;
        time += Time.deltaTime;
        time = Mathf.Clamp(time, 0.0f, this.fade_out_frame);
        if(this.fade_out_frame == 0.0f)
        {
            alpha = 1.0f;
        }
        else alpha = Mathf.Lerp(0.0f, 1.0f, time / this.fade_out_frame);


        gameUiCore.Instance.CharacterCanvas.SetAlpha(character_id, alpha);
        if (alpha >= 1.0f)
        {
            is_finish = true;
            
        }
    }

    public override void FinishUninit()  {
        time = 0.0f;
    }
}
