using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseShowCharacterNovelRole : BaseNovelRole {
    protected int character_sprite_id = 0; //キャラ画像ID
    protected float fade_out_frame = 0.0f; //フェードアウトタイム
    protected Vector2 show_position = Vector2.zero; //表示位置)
    protected Novel_Character_ID character_id = Novel_Character_ID.NONE; //)
    public BaseShowCharacterNovelRole(BaseShowCharacterNovelRole other):base(other) {
        role_id = Novel_Role_ID.ShowCharacter;
        character_sprite_id = other.character_sprite_id;
        fade_out_frame = other.fade_out_frame;
        show_position = other.show_position;
        character_id = other.character_id;
    }
    public  BaseShowCharacterNovelRole():base() {
        role_id = Novel_Role_ID.ShowCharacter;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        character_sprite_id = reader.ReadInt32();
        fade_out_frame = reader.ReadSingle();
        show_position = new Vector2(reader.ReadSingle(),reader.ReadSingle());
        character_id = (Novel_Character_ID)reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(character_sprite_id);
        writer.Write(fade_out_frame);
        writer.Write(show_position.x);
        writer.Write(show_position.y);
        writer.Write((int)character_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("character_sprite_id")) character_sprite_id = Convert.ToInt32(directory["character_sprite_id"]);
        if (directory.ContainsKey("fade_out_frame")) fade_out_frame = Convert.ToSingle(directory["fade_out_frame"]);
        if (directory.ContainsKey("show_position_x")) show_position.x = Convert.ToSingle(directory["show_position_x"]);
        if (directory.ContainsKey("show_position_y")) show_position.y = Convert.ToSingle(directory["show_position_y"]);
        if (directory.ContainsKey("character_id")) character_id = (Novel_Character_ID)Convert.ToInt32(directory["character_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["character_sprite_id"] = (int)character_sprite_id;
        directory["fade_out_frame"] = (float)fade_out_frame;
        directory["show_position_x"] = (float)show_position.x;
        directory["show_position_y"] = (float)show_position.y;
        directory["character_id"] = (int)character_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("キャラ画像ID(INT):", GUILayout.Width(150));
        character_sprite_id = EditorGUILayout.IntField("",character_sprite_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェードアウトタイム(FLOAT):", GUILayout.Width(150));
        fade_out_frame = EditorGUILayout.FloatField("",fade_out_frame, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginVertical();
        float show_position_X,show_position_Y = 0.0f;
        GUILayout.BeginHorizontal();
        GUILayout.Label("表示位置.X(VECTOR2.X):", GUILayout.Width(150));
        show_position_X = EditorGUILayout.FloatField("",show_position.x, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("表示位置.Y(VECTOR2.Y):", GUILayout.Width(150));
        show_position_Y = EditorGUILayout.FloatField("",show_position.y, GUILayout.Width(250));
        show_position = new Vector2(show_position_X,show_position_Y);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("(CHARACTERID):", GUILayout.Width(150));
        character_id = (Novel_Character_ID)EditorGUILayout.EnumPopup("", character_id);
        GUILayout.EndHorizontal();
    }
#endif
}
