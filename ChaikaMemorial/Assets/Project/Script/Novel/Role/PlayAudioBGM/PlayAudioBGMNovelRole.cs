using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

public class PlayAudioBGMNovelRole : BasePlayAudioBGMNovelRole {

    NovelAudioStateManager novel_audio_manager = new NovelAudioStateManager();
    NovelAudioInitArgData novel_audio_init = new NovelAudioInitArgData();
    public PlayAudioBGMNovelRole(PlayAudioBGMNovelRole other):base(other) {
    }

    public PlayAudioBGMNovelRole():base() {
    }

    public override void BeginSetting()  {
        SoundGameCore.Instance.EventSummaryData.LoadNovelBGM(sound_id);
    }

    public override void BeginInit()  {
        novel_audio_init = new NovelAudioInitArgData();
        novel_audio_manager = new NovelAudioStateManager();
        is_finish = false;
        novel_audio_init.delay_time = delay_time;
        novel_audio_init.fade_time = fade_out_time;
        novel_audio_init.is_bgm = true;
        novel_audio_init.is_play = true;
        novel_audio_init.sound_id = sound_id;
        novel_audio_init.volume = volume;
    }

    public override void UpdateExec()  {
        if (novel_audio_init == null) return;
        if (novel_audio_manager == null) return;
        if (is_finish) return;
        novel_audio_init.delay_time = delay_time;
        novel_audio_init.fade_time = fade_out_time;
        novel_audio_init.is_bgm = true;
        novel_audio_init.is_play = true;
        novel_audio_init.sound_id = sound_id;
        novel_audio_init.volume = volume;
        SoundGameCore.Instance.SoundStartCoroutine(UpdateNovelAudioCoroutine());
        is_finish = true;

    }

    public override void FinishUninit()  {
    }



    IEnumerator UpdateNovelAudioCoroutine()
    {
        if(novel_audio_manager == null) yield break;
        if(novel_audio_init == null) yield break;
        do
        {
            // novel_audio_manager.Update() を呼び出す
            novel_audio_manager.Update(novel_audio_init, null);

            // 1フレーム待機
            yield return null;
        } while ((!novel_audio_manager.IsFinish));

        novel_audio_manager = null;
        novel_audio_init = null;

        yield return null;

    }
}
