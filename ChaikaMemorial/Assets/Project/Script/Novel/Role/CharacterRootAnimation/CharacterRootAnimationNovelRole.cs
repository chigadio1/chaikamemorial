using System.Collections;
using UnityEngine;

public class CharacterRootAnimationNovelRole : BaseCharacterRootAnimationNovelRole {
    float calc_time = 0.0f;
    RectTransform rect_transform = null;
    public CharacterRootAnimationNovelRole(CharacterRootAnimationNovelRole other):base(other) {
    }

    public CharacterRootAnimationNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        rect_transform = gameUiCore.Instance.CharacterCanvas.GetCharacterPosition(character_id);
        animation.Init(rect_transform);
        calc_time = 0.0f;
        is_finish = false;
    }

    public override void UpdateExec()  {
        calc_time += Time.deltaTime;
        animation.Update(ref rect_transform, calc_time);
        if(animation.IsEndAnimation)
        {
            is_finish = true;
        }
    }

    public override void FinishUninit()  {
    }
}
