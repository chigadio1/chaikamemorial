using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseCharacterRootAnimationNovelRole : BaseNovelRole {
    protected NovelAnimationData animation = new NovelAnimationData(); //アニメーションデータ)
    protected Novel_Character_ID character_id = Novel_Character_ID.NONE; //キャラID)
    public BaseCharacterRootAnimationNovelRole(BaseCharacterRootAnimationNovelRole other):base(other) {
        role_id = Novel_Role_ID.CharacterRootAnimation;
        animation = other.animation;
        character_id = other.character_id;
    }
    public  BaseCharacterRootAnimationNovelRole():base() {
        role_id = Novel_Role_ID.CharacterRootAnimation;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        animation.BinaryReader(ref reader);
        character_id = (Novel_Character_ID)reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        animation.BinaryWriter(ref writer);
        writer.Write((int)character_id);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        animation.JsonReaderData(directory);
        if (directory.ContainsKey("character_id")) character_id = (Novel_Character_ID)Convert.ToInt32(directory["character_id"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        animation.JsonWriterData(ref directory);
        directory["character_id"] = (int)character_id;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        animation.OnGUI();
        GUILayout.BeginHorizontal();
        GUILayout.Label("キャラID(CHARACTERID):", GUILayout.Width(150));
        character_id = (Novel_Character_ID)EditorGUILayout.EnumPopup("", character_id);
        GUILayout.EndHorizontal();
    }
#endif
}
