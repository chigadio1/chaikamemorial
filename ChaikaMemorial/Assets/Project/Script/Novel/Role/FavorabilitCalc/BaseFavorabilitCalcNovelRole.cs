using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseFavorabilitCalcNovelRole : BaseNovelRole {
    protected Character_ID character_favo_Id = Character_ID.NONE; //ゲーム上のキャラID)
    protected int calc_num = 0; //
    public BaseFavorabilitCalcNovelRole(BaseFavorabilitCalcNovelRole other):base(other) {
        role_id = Novel_Role_ID.FavorabilitCalc;
        character_favo_Id = other.character_favo_Id;
        calc_num = other.calc_num;
    }
    public  BaseFavorabilitCalcNovelRole():base() {
        role_id = Novel_Role_ID.FavorabilitCalc;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        character_favo_Id = (Character_ID)reader.ReadInt32();
        calc_num = reader.ReadInt32();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write((int)character_favo_Id);
        writer.Write(calc_num);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("character_favo_Id")) character_favo_Id = (Character_ID)Convert.ToInt32(directory["character_favo_Id"]);
        if (directory.ContainsKey("calc_num")) calc_num = Convert.ToInt32(directory["calc_num"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["character_favo_Id"] = (int)character_favo_Id;
        directory["calc_num"] = (int)calc_num;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("ゲーム上のキャラID(CHARACTERFAVOID):", GUILayout.Width(150));
        character_favo_Id = (Character_ID)EditorGUILayout.EnumPopup("", character_favo_Id);
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("(INT):", GUILayout.Width(150));
        calc_num = EditorGUILayout.IntField("",calc_num, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
