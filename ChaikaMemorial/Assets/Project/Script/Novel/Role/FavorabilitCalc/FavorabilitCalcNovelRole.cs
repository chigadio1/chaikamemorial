using System.Collections;

public class FavorabilitCalcNovelRole : BaseFavorabilitCalcNovelRole {
    public FavorabilitCalcNovelRole(FavorabilitCalcNovelRole other):base(other) {
    }

    public FavorabilitCalcNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        PlayerCore.Instance?.GetPlayerCoreData?.GetCharacterFavorabilityData?.CalcFavorability(character_favo_Id, calc_num);
    }

    public override void UpdateExec()  {
        
        is_finish = true;
    }

    public override void FinishUninit()  {
    }
}
