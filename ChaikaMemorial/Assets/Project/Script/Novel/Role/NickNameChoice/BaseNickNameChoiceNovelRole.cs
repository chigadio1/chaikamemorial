using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseNickNameChoiceNovelRole : BaseNovelRole {
    protected Character_ID chara_id = Character_ID.NONE; //キャラクターID)
    protected ListNickNameChoiceJumpData nick_name_jump = new ListNickNameChoiceJumpData(); //ニックネームごとのジャンプ)
    public BaseNickNameChoiceNovelRole(BaseNickNameChoiceNovelRole other):base(other) {
        role_id = Novel_Role_ID.NickNameChoice;
        chara_id = other.chara_id;
        nick_name_jump = other.nick_name_jump;
    }
    public  BaseNickNameChoiceNovelRole():base() {
        role_id = Novel_Role_ID.NickNameChoice;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        chara_id = (Character_ID)reader.ReadInt32();
        nick_name_jump.BinaryReader(ref reader);
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write((int)chara_id);
        nick_name_jump.BinaryWriter(ref writer);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("chara_id")) chara_id = (Character_ID)Convert.ToInt32(directory["chara_id"]);
        nick_name_jump.JsonReaderData(directory);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["chara_id"] = (int)chara_id;
        nick_name_jump.JsonWriterData(ref directory);
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("キャラクターID(GameCharacterID):", GUILayout.Width(150));
        chara_id = (Character_ID)EditorGUILayout.EnumPopup("", chara_id);
        GUILayout.EndHorizontal();
        nick_name_jump.OnGUI();
    }
#endif
}
