using System.Collections;

public class NickNameChoiceNovelRole : BaseNickNameChoiceNovelRole {

    NickNameChoiceStateManager manager = new NickNameChoiceStateManager();
    NickNameChoiceUpdateArgData update = new NickNameChoiceUpdateArgData();
    public NickNameChoiceNovelRole(NickNameChoiceNovelRole other):base(other) {
    }

    public NickNameChoiceNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
        update.chara_id = this.chara_id;
     
    }

    public override void UpdateExec()  {
        manager.Update(null,update);
        if(manager.IsFinish)
        {
            var data = this.nick_name_jump.dict_nick_name_data[gameUiCore.Instance.SelectNickNameID];
            if (data == null) return;
            if(update.is_success)
            {

                gameUiCore.Instance.NovelJump(data.jump_success_exec_id,data.jump_success_order_id);
            }
            else
            {
                gameUiCore.Instance.NovelJump(data.jump_failure_exec_id, data.jump_failure_order_id);
            }
            is_finish = true;
        }
    }

    public override void FinishUninit()  {
    }
}
