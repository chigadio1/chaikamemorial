using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseShowStillNovelRole : BaseNovelRole {
    protected int still_sprine_id = 0; //スチル画像ID
    protected float fade_out_frame = 0.0f; //フェードアウト終了時間
    protected Vector2 position = Vector2.zero; //座標)
    protected float scale = 0.0f; //スケール
    protected float angle = 0.0f; //回転
    public BaseShowStillNovelRole(BaseShowStillNovelRole other):base(other) {
        role_id = Novel_Role_ID.ShowStill;
        still_sprine_id = other.still_sprine_id;
        fade_out_frame = other.fade_out_frame;
        position = other.position;
        scale = other.scale;
        angle = other.angle;
    }
    public  BaseShowStillNovelRole():base() {
        role_id = Novel_Role_ID.ShowStill;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        still_sprine_id = reader.ReadInt32();
        fade_out_frame = reader.ReadSingle();
        position = new Vector2(reader.ReadSingle(),reader.ReadSingle());
        scale = reader.ReadSingle();
        angle = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(still_sprine_id);
        writer.Write(fade_out_frame);
        writer.Write(position.x);
        writer.Write(position.y);
        writer.Write(scale);
        writer.Write(angle);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("still_sprine_id")) still_sprine_id = Convert.ToInt32(directory["still_sprine_id"]);
        if (directory.ContainsKey("fade_out_frame")) fade_out_frame = Convert.ToSingle(directory["fade_out_frame"]);
        if (directory.ContainsKey("position_x")) position.x = Convert.ToSingle(directory["position_x"]);
        if (directory.ContainsKey("position_y")) position.y = Convert.ToSingle(directory["position_y"]);
        if (directory.ContainsKey("scale")) scale = Convert.ToSingle(directory["scale"]);
        if (directory.ContainsKey("angle")) angle = Convert.ToSingle(directory["angle"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["still_sprine_id"] = (int)still_sprine_id;
        directory["fade_out_frame"] = (float)fade_out_frame;
        directory["position_x"] = (float)position.x;
        directory["position_y"] = (float)position.y;
        directory["scale"] = (float)scale;
        directory["angle"] = (float)angle;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("スチル画像ID(INT):", GUILayout.Width(150));
        still_sprine_id = EditorGUILayout.IntField("",still_sprine_id, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("フェードアウト終了時間(FLOAT):", GUILayout.Width(150));
        fade_out_frame = EditorGUILayout.FloatField("",fade_out_frame, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginVertical();
        float position_X,position_Y = 0.0f;
        GUILayout.BeginHorizontal();
        GUILayout.Label("座標.X(VECTOR2.X):", GUILayout.Width(150));
        position_X = EditorGUILayout.FloatField("",position.x, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("座標.Y(VECTOR2.Y):", GUILayout.Width(150));
        position_Y = EditorGUILayout.FloatField("",position.y, GUILayout.Width(250));
        position = new Vector2(position_X,position_Y);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("スケール(FLOAT):", GUILayout.Width(150));
        scale = EditorGUILayout.FloatField("",scale, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("回転(FLOAT):", GUILayout.Width(150));
        angle = EditorGUILayout.FloatField("",angle, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
