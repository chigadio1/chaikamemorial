using System.Collections;
using UnityEngine;

public class ShowStillNovelRole : BaseShowStillNovelRole {
    /// <summary>
    /// �v���^�C��
    /// </summary>
    private float add_calc_time = 0.0f;
    public ShowStillNovelRole(ShowStillNovelRole other):base(other) {
    }

    public ShowStillNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        add_calc_time = 0.0f;
        is_finish = false;
        gameUiCore.Instance?.SettingCanvas?.SetStillAlpha(0.0f);
        gameUiCore.Instance?.SettingCanvas?.SetStillImage(gameUiCore.Instance?.GetSprite(Novel_Sprite_Type_ID.Still, this.still_sprine_id));
        gameUiCore.Instance?.SettingCanvas?.SetStillPosition(this.position);
        gameUiCore.Instance?.SettingCanvas?.SetStillScale(new Vector2(this.scale,this.scale));

        gameUiCore.Instance?.SettingCanvas?.SetStillAngle(this.angle);


    }

    public override void UpdateExec()  {
        add_calc_time += Time.deltaTime;
        add_calc_time = Mathf.Clamp(add_calc_time, 0.0f, this.fade_out_frame);
        float alpha = add_calc_time / this.fade_out_frame;
        alpha = Mathf.Clamp01(alpha);

        if(add_calc_time >= this.fade_out_frame)
        {
            is_finish = true;
        }

        gameUiCore.Instance?.SettingCanvas?.SetStillAlpha(alpha);

    }

    public override void FinishUninit()  {
        add_calc_time = 0.0f;
        gameUiCore.Instance?.SettingCanvas?.SetStillAlpha(1.0f);
    }

}
