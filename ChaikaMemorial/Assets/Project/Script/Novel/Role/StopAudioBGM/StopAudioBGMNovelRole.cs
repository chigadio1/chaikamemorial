using System.Collections;

public class StopAudioBGMNovelRole : BaseStopAudioBGMNovelRole {
    NovelAudioStateManager novel_audio_manager = new NovelAudioStateManager();
    NovelAudioInitArgData novel_audio_init = new NovelAudioInitArgData();
    public StopAudioBGMNovelRole(StopAudioBGMNovelRole other):base(other) {
    }

    public StopAudioBGMNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        novel_audio_init.delay_time = delay_time;
        novel_audio_init.fade_time = 0.0f;
        novel_audio_init.is_bgm = true;
        novel_audio_init.is_play = false;
        novel_audio_init.sound_id = sound_id;
    }

    public override void UpdateExec()  {
        novel_audio_manager.Update(novel_audio_init, null);
        if (novel_audio_manager.IsFinish)
        {
            is_finish = true;
        }
    }

    public override void FinishUninit()  {
        novel_audio_manager = null;
        novel_audio_init = null;
    }
}
