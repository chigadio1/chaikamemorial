using System;
using System.Collections;
using UnityEngine;

public class TextOutputNovelRole : BaseTextOutputNovelRole {

    /// <summary>
    /// 計算タイム
    /// </summary>
    private float calc_time = 0.0f;

    /// <summary>
    /// 表示文字
    /// </summary>
    private int show_text_length = 0;




    public TextOutputNovelRole(TextOutputNovelRole other):base(other) {
    }

    public TextOutputNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
        calc_time = 0.0f;
        show_text_length = 0;
        this.name = PlayerCore.Instance.ConvertName(this.name);
        this.text = PlayerCore.Instance.ConvertText(this.text);
    }

    public override void UpdateExec()  {
        if (is_finish) return;
        calc_time += Time.deltaTime;

        //一文字表示タイム計測
        if (calc_time >= this.single_text_time)
        {
            
            show_text_length += 1;
            calc_time = 0.0f;
            if (show_text_length >= this.text.Length)
            {
                show_text_length = this.text.Length;
               
            }
            else
            {
                try
                {
                    if (this.text[show_text_length].ToString() == "\\")
                    {
                        calc_time = this.single_text_time;
                        show_text_length = show_text_length + 2;
                    };


                }
                catch(IndexOutOfRangeException ex)
                {
                    Debug.LogError(ex.Message + $"=index:{show_text_length} length:{text.Length}");
                }
            }

        }

        if(Input.GetMouseButtonDown(0))
        {
            if(show_text_length >= this.text.Length)
            {
                is_finish = true;
            }
            show_text_length = this.text.Length;
        }

        gameUiCore.Instance?.SettingCanvas.ShowNameText(this.name);
        gameUiCore.Instance?.SettingCanvas.ShowTalkText(this.text.Substring(0, show_text_length));
    }

    public override void FinishUninit()  {
        calc_time = 0.0f;
        show_text_length = 0;
    }
}
