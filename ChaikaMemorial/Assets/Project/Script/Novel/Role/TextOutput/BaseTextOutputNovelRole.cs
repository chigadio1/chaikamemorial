using System;
using System.Collections;
using System.IO;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseTextOutputNovelRole : BaseNovelRole {
    protected string text = ""; //テキスト
    protected string name = ""; //名前表示
    protected float single_text_time = 0.0f; //一文字表示タイム
    public BaseTextOutputNovelRole(BaseTextOutputNovelRole other):base(other) {
        role_id = Novel_Role_ID.TextOutput;
        text = other.text;
        name = other.name;
        single_text_time = other.single_text_time;
    }
    public  BaseTextOutputNovelRole():base() {
        role_id = Novel_Role_ID.TextOutput;
    }

    public override void BinaryReaderData(ref BinaryReader reader) {
        base.BinaryReaderData(ref reader);
        text = reader.ReadString();
        name = reader.ReadString();
        single_text_time = reader.ReadSingle();
    }
    public override void BinaryWriterData(ref BinaryWriter writer)  {
        base.BinaryWriterData(ref writer);
        writer.Write(text);
        writer.Write(name);
        writer.Write(single_text_time);
    }
    public override　void JsonReaderData(Dictionary<string,object> directory)  {
        base.JsonReaderData(directory);
        if (directory.ContainsKey("text")) text = (string)directory["text"];
        if (directory.ContainsKey("name")) name = (string)directory["name"];
        if (directory.ContainsKey("single_text_time")) single_text_time = Convert.ToSingle(directory["single_text_time"]);
    }
    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {
        base.JsonWriterData(ref directory);
        directory["text"] = (string)text;
        directory["name"] = (string)name;
        directory["single_text_time"] = (float)single_text_time;
    }
#if UNITY_EDITOR
    public override void OnGUI() { 
        GUILayout.BeginHorizontal();
        GUILayout.Label("テキスト(STRING):", GUILayout.Width(150));
        text = EditorGUILayout.TextField("",text, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("名前表示(STRING):", GUILayout.Width(150));
        name = EditorGUILayout.TextField("",name, GUILayout.Width(250));
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("一文字表示タイム(FLOAT):", GUILayout.Width(150));
        single_text_time = EditorGUILayout.FloatField("",single_text_time, GUILayout.Width(250));
        GUILayout.EndHorizontal();
    }
#endif
}
