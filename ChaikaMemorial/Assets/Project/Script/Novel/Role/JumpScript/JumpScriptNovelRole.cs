using System.Collections;

public class JumpScriptNovelRole : BaseJumpScriptNovelRole {
    public JumpScriptNovelRole(JumpScriptNovelRole other):base(other) {
    }

    public JumpScriptNovelRole():base() {
    }

    public override void BeginSetting()  {
    }

    public override void BeginInit()  {
        is_finish = false;
    }

    public override void UpdateExec()  {
        is_finish = true;
    }

    public override void FinishUninit()  {
        gameUiCore.Instance?.NovelJump(this.jump_exec_id,this.jump_order_id);
    }
}
