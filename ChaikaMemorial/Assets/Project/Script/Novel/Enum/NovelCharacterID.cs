using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ノベルキャラクターID
/// </summary>
public enum Novel_Character_ID
{
    NONE = -1, //存在しない
    CHARACTER_01,
    CHARACTER_02,
    CHARACTER_03,
    CHARACTER_04,
    CHARACTER_05,
    CHARACTER_06,
    MAX
}
