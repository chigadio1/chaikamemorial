using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Fade_Type_ID
{
    NONE = -1,
    Fade_IN,
    Fade_OUT
}
