using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif


public enum CharacterClothingTypeID
{
    NONE = -1,
    Uniform,
    SpringCasual,
    SummerCasual,
    AutumnCasual,
    WinterCasual,
    MAX
}
public enum FacialExpressionID
{
    NONE = -1,
    Neutral, // ニュートラルな表情
    Happy,   // 嬉しい表情
    Sad,     // 悲しい表情
    Angry,   // 怒った表情
    Surprised, // 驚いた表情
    Disgusted, // 嫌悪の表情
    Confused,  // 混乱した表情
    Excited,    // 興奮した表情
    MAX
}

public class CharacterFacialClothing
{
    int sprite_id = 0;
    public int GetSpriteID { get { return sprite_id; } set { } }

    public void SaveToBinary(BinaryWriter writer)
    {
        writer.Write(sprite_id);
    }

    // バイナリファイルからCharacterFacialClothingを読み込む関数
    public void LoadFromBinary(BinaryReader reader)
    {
        sprite_id = reader.ReadInt32();
    }
#if UNITY_EDITOR
    public void OnGUI(FacialExpressionID id)
    {
        
        sprite_id = EditorGUILayout.IntField($"{Enum.GetName(typeof(FacialExpressionID), id)}", sprite_id);
    }
#endif
    }

public class CharacterSeasonClothing
{
    CharacterFacialClothing[] character_clothings_array = new CharacterFacialClothing[(int)FacialExpressionID.MAX];

    public CharacterFacialClothing GetCharacterClothing(FacialExpressionID id)
    {
        if (id <= FacialExpressionID.NONE || id >= FacialExpressionID.MAX) return null;
        return character_clothings_array[(int)id];
    }

    public void SaveToBinary(BinaryWriter writer)
    {
        foreach (CharacterFacialClothing clothing in character_clothings_array)
        {
            if (clothing != null)
            {
                clothing.SaveToBinary(writer);
            }
        }
    }

    // バイナリファイルからCharacterSeasonClothingを読み込む関数
    public void LoadFromBinary(BinaryReader reader)
    {
        for (FacialExpressionID id = FacialExpressionID.NONE + 1; id < FacialExpressionID.MAX; id++)
        {
            if (id <= FacialExpressionID.NONE || id >= FacialExpressionID.MAX)
                continue;

            if (character_clothings_array[(int)id] == null)
            {
                character_clothings_array[(int)id] = new CharacterFacialClothing();
            }

            character_clothings_array[(int)id].LoadFromBinary(reader);
        }
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        using (new GUILayout.VerticalScope())
        {
            for(int count = 0;  count < character_clothings_array.Length;)
            {
                if (character_clothings_array[count] == null) character_clothings_array[count] = new CharacterFacialClothing();

                character_clothings_array[count].OnGUI((FacialExpressionID)count);
                ++count;
            }
        }
    }

    public void Init()
    {
        for (int count = 0; count < character_clothings_array.Length;)
        {
            character_clothings_array[count] = new CharacterFacialClothing();
            ++count;
        }
    }
#endif
}

public class CharacterClothing
{
    CharacterSeasonClothing[] character_season_clothings_array = new CharacterSeasonClothing[(int)CharacterClothingTypeID.MAX];

    public CharacterSeasonClothing  GetCharacterClothing(CharacterClothingTypeID id)
    {
        if (id <= CharacterClothingTypeID.NONE || id >= CharacterClothingTypeID.MAX) return null;
        return character_season_clothings_array[(int)id];
    }

    public void SaveToBinary(BinaryWriter writer)
    {
        foreach (CharacterSeasonClothing seasonClothing in character_season_clothings_array)
        {
            if (seasonClothing != null)
            {
                seasonClothing.SaveToBinary(writer);
            }
        }
    }

    // バイナリファイルからCharacterClothingを読み込む関数
    public void LoadFromBinary(BinaryReader reader)
    {
        for (CharacterClothingTypeID id = CharacterClothingTypeID.NONE + 1; id < CharacterClothingTypeID.MAX; id++)
        {
            if (id <= CharacterClothingTypeID.NONE || id >= CharacterClothingTypeID.MAX)
                continue;

            if (character_season_clothings_array[(int)id] == null)
            {
                character_season_clothings_array[(int)id] = new CharacterSeasonClothing();
            }

            character_season_clothings_array[(int)id].LoadFromBinary(reader);
        }
    }

#if UNITY_EDITOR
    public void OnGUI()
    {

        using (new GUILayout.VerticalScope())
        {
            for(int count = 0;  count < character_season_clothings_array.Length;)
            {
                if (character_season_clothings_array[count] == null) character_season_clothings_array[count] = new CharacterSeasonClothing();
                CharacterClothingTypeID id = (CharacterClothingTypeID)count;
                EditorGUILayout.LabelField($"{Enum.GetName(typeof(Character_ID), id)}-------------------------------------------------------");
                character_season_clothings_array[count].OnGUI();
                ++count;
            }
        }
    }

    public void Init()
    {
        for (int count = 0; count < character_season_clothings_array.Length;)
        {
            character_season_clothings_array[count] = new CharacterSeasonClothing();
            character_season_clothings_array[count].Init();
            ++count;
        }
    }


#endif
}




public class CharacterClothingStorage
{
    private Dictionary<Character_ID, CharacterClothing> character_clothing = new Dictionary<Character_ID, CharacterClothing>();
    
    public CharacterClothing GetCharacterCloting(Character_ID id)
    {
        if (id <= Character_ID.NONE || id >= Character_ID.MAX) return null;
        if (character_clothing.ContainsKey(id) == false) return null;

        return character_clothing[id];
    }

    public void SaveToBinary(string filePath)
    {
        using (BinaryWriter writer = new BinaryWriter(File.Create(filePath)))
        {
            foreach (var kvp in character_clothing)
            {
                Character_ID characterID = kvp.Key;
                CharacterClothing characterClothing = kvp.Value;

                // キャラクターIDをバイナリに書き込む
                writer.Write((int)characterID);

                // キャラクターの衣装情報をバイナリに書き込む
                characterClothing.SaveToBinary(writer);
            }
        }
    }

    // バイナリファイルからCharacterClothingStorageを読み込む関数
    public void LoadFromBinary(string filePath)
    {
        if (File.Exists(filePath))
        {
            using (BinaryReader reader = new BinaryReader(File.OpenRead(filePath)))
            {
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    Character_ID characterID = (Character_ID)reader.ReadInt32();
                    CharacterClothing characterClothing = new CharacterClothing();

                    // キャラクターの衣装情報をバイナリから読み込む
                    characterClothing.LoadFromBinary(reader);

                    character_clothing[characterID] = characterClothing;
                }
            }
        }
    }

#if UNITY_EDITOR
    public void OnGUI(Character_ID id)
    {
        if (character_clothing.ContainsKey(id) == false) return;
        string name = CharacterConvert.GetCharacterComment(id);
        EditorGUILayout.LabelField($"{name}=>");
        character_clothing[id].OnGUI();
    }
    public void EditorInit()
    {
        character_clothing.Clear();
        for(int count = 0; count < (int)Character_ID.MAX;)
        {
            character_clothing[(Character_ID)count] = new CharacterClothing();
            character_clothing[(Character_ID)count].Init();
            ++count;
        }
    }
#endif

    public void Save()
    {
        string path = Application.dataPath + $"/Event/Clothing.bin";


        SaveToBinary(path);
    }



    public void Load()
    {
        string path = Application.dataPath + $"/Event/Clothing.bin";
        if (System.IO.File.Exists(path) == true)
        {
            path = Application.dataPath + $"/Event/Clothing.bin";
            if (System.IO.File.Exists(path) == false)
            {
                Debug.Log("ファイルが存在しません");
                return;
            }
            else
            {
                LoadFromBinary(path);
                return;
            }
        }
    }
}

