using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Character_ID
{
    NONE = -1,
    Chaika, //チャイカ
    CHARACTER_01,
    CHARACTER_02,
    CHARACTER_03,
    CHARACTER_04,
    CHARACTER_05,
    CHARACTER_06,
    CHARACTER_07,
    CHARACTER_08,
    CHARACTER_09,
    CHARACTER_10,
    CHARACTER_11,
    CHARACTER_12,
    CHARACTER_13,
    CHARACTER_14,
    CHARACTER_15,
    CHARACTER_16,
    CHARACTER_17,
    CHARACTER_18,
    Yashiro,
    MAX
}

public class CharacterConvert
{
    public static string GetCharacterComment(Character_ID characterID)
    {
        switch (characterID)
        {
            case Character_ID.NONE:
                return "不明なキャラクター";
            case Character_ID.Chaika:
                return "花畑チャイカ";
            case Character_ID.CHARACTER_01:
                return "キャラクター 01";
            case Character_ID.CHARACTER_02:
                return "キャラクター 02";
            case Character_ID.CHARACTER_03:
                return "キャラクター 03";
            case Character_ID.CHARACTER_04:
                return "キャラクター 04";
            case Character_ID.CHARACTER_05:
                return "キャラクター 05";
            case Character_ID.CHARACTER_06:
                return "キャラクター 06";
            case Character_ID.CHARACTER_07:
                return "キャラクター 07";
            case Character_ID.CHARACTER_08:
                return "キャラクター 08";
            case Character_ID.CHARACTER_09:
                return "キャラクター 09";
            case Character_ID.CHARACTER_10:
                return "キャラクター 10";
            case Character_ID.CHARACTER_11:
                return "キャラクター 11";
            case Character_ID.CHARACTER_12:
                return "キャラクター 12";
            case Character_ID.CHARACTER_13:
                return "キャラクター 13";
            case Character_ID.CHARACTER_14:
                return "キャラクター 14";
            case Character_ID.CHARACTER_15:
                return "キャラクター 15";
            case Character_ID.CHARACTER_16:
                return "キャラクター 16";
            case Character_ID.CHARACTER_17:
                return "キャラクター 17";
            case Character_ID.CHARACTER_18:
                return "キャラクター18";
            case Character_ID.Yashiro:
                return "社築";
            default:
                return "不明なキャラクター";
        }
    }
}