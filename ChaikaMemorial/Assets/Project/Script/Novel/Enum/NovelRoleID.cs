public enum Novel_Role_ID {
    LoadSprite, //画像ロード
    ShowCharacter, //キャラクター表示
    TextOutput, //テキスト表示
    FadeStartup, //フェード起動
    FavorabilitCalc, //好感度計算
    JumpScript, //指定したところにジャンプ
    ChoicesSelect, //選択肢
    CutFadeStarUp, //カットフェード起動
    ShowStill, //スチル画像表示
    CharacterRootAnimation, //キャラクターアニメーション
    PlayAudioSE, //SE再生
    PlayAudioBGM, //BGM再生
    StopAudioBGM, //BGM停止
    CharacterFavoUI, //キャラクターの好感度表示
    StillRootAnimation, //スチルアニメーション
    Delay, //待ち時間
    JumpEvent, //指定イベント
    ShowBackGround, //背景表示
    ShowCharacterCostume, //キャラクター表示(服指定)
    CompulsoryGameFinish, //強制終了
    ProbabilityJumpScript, //確率でジャンプ
    NickNameChoice, //ニックネーム選択
    CharacterFavoJumpScript //友好度でジャンプ
}
