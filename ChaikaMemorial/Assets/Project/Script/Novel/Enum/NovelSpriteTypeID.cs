/// <summary>
/// ノベル画像タイプ
/// </summary>
public enum Novel_Sprite_Type_ID
{
    NONE,
    CHARACTER, //キャラクター
    BACKGROUND, //背景
    FADE,
    Still,
    ICON
}


