using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Xe[^XID
public enum Status_ID
{
    NONE = -1,
    STUDY = 0, //×w
    EXERCISE, //^®
    CHARM, //£Í
    MAX
}

public class StatusStringConverter
{
    public static string GetStatusString(Status_ID statusID)
    {
        switch (statusID)
        {
            case Status_ID.STUDY:
                return "×w";
            case Status_ID.EXERCISE:
                return "^®";
            case Status_ID.CHARM:
                return "£Í";
            default:
                return "¢mÌXe[^X";
        }
    }
}