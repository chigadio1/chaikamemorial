using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Newtonsoft.Json.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class CharacterFavoDetails
{
    public int jump_exec_id = 0;
    public int jump_order_id = 0;

    public void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(jump_exec_id);
        writer.Write(jump_order_id);
    }

    public void BinaryReader(ref BinaryReader reader)
    {
        jump_exec_id = reader.ReadInt32();
        jump_order_id = reader.ReadInt32();
    }

    public void JsonReaderData(JObject jsonObject)
    {
        // jump_exec_idとjump_order_idをJSONオブジェクトから読み込む
        if (jsonObject.ContainsKey("jump_exec_id"))
        {
            jump_exec_id = jsonObject.Value<int>("jump_exec_id");
        }
        if (jsonObject.ContainsKey("jump_order_id"))
        {
            jump_order_id = jsonObject.Value<int>("jump_order_id");
        }
    }

    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["jump_exec_id"] = Convert.ToInt32(jump_exec_id);
        directory["jump_order_id"] = Convert.ToInt32(jump_order_id);
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        jump_exec_id = EditorGUILayout.IntField("ジャンプ先実行ID", jump_exec_id);
        jump_order_id = EditorGUILayout.IntField("ジャンプ先指定ID", jump_order_id);
    }
#endif
}

/// <summary>
/// キャラの好感度でジャンプするシステム
/// </summary>
public class CharacterFavoJump
{
    public CharacterFavoDetails low_favo_jump = new CharacterFavoDetails();
    public CharacterFavoDetails medium_favo_jump = new CharacterFavoDetails();
    public CharacterFavoDetails high_favo_jump = new CharacterFavoDetails();

    public void BinaryWriter(ref BinaryWriter writer)
    {
        low_favo_jump.BinaryWriter(ref writer);
        medium_favo_jump.BinaryWriter(ref writer);
        high_favo_jump.BinaryWriter(ref writer);

    }

    public void BinaryReader(ref BinaryReader reader)
    {
        low_favo_jump.BinaryReader(ref reader);
        medium_favo_jump.BinaryReader(ref reader);
        high_favo_jump.BinaryReader(ref reader);
    }

    public void JsonReaderData(Dictionary<string,object> directory)
    {
        if (directory.ContainsKey("low_favo_character"))
        {
            string lowFavoCharacterJson = Convert.ToString(directory["low_favo_character"]);
            if (lowFavoCharacterJson != null)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(lowFavoCharacterJson);
                    low_favo_jump.JsonReaderData(jsonObject);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Error parsing JSON: " + ex.Message);
                }
            }
        }

        if (directory.ContainsKey("medium_favo_character"))
        {
            string mediumFavoCharacterJson = Convert.ToString(directory["medium_favo_character"]);
            if (mediumFavoCharacterJson != null)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(mediumFavoCharacterJson);
                    medium_favo_jump.JsonReaderData(jsonObject);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Error parsing JSON: " + ex.Message);
                }
            }
        }

        if (directory.ContainsKey("high_favo_character"))
        {
            string highFavoCharacterJson = Convert.ToString(directory["high_favo_character"]);
            if (highFavoCharacterJson != null)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(highFavoCharacterJson);
                    high_favo_jump.JsonReaderData(jsonObject);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Error parsing JSON: " + ex.Message);
                }
            }
        }

    }


    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        // キャラクターの好感度ジャンプデータを格納する辞書を作成
        var lowFavoCharacterData = new Dictionary<string, object>();
        var mediumFavoCharacterData = new Dictionary<string, object>();
        var highFavoCharacterData = new Dictionary<string, object>();

        // 各好感度ジャンプデータを JsonWriterData メソッドを使用して辞書に書き込む
        low_favo_jump.JsonWriterData(ref lowFavoCharacterData);
        medium_favo_jump.JsonWriterData(ref mediumFavoCharacterData);
        high_favo_jump.JsonWriterData(ref highFavoCharacterData);

        // 作成した辞書を directory に格納
        directory["low_favo_character"] = lowFavoCharacterData;
        directory["medium_favo_character"] = mediumFavoCharacterData;
        directory["high_favo_character"] = highFavoCharacterData;
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        EditorGUILayout.LabelField("低い好感度のとき");
        EditorGUI.indentLevel++;
        low_favo_jump.OnGUI();
        EditorGUI.indentLevel--;
        EditorGUILayout.LabelField("普通好感度のとき");
        EditorGUI.indentLevel++;
        medium_favo_jump.OnGUI();
        EditorGUI.indentLevel--;
        EditorGUILayout.LabelField("高い好感度のとき");
        EditorGUI.indentLevel++;
        high_favo_jump.OnGUI();
        EditorGUI.indentLevel--;
    }
#endif
}
