using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 選択肢ジャンプデータ
/// </summary>
public struct SelectJumpData
{
    public string select_text;
    public int jump_exec_id;
    public int jump_order_id;
}
