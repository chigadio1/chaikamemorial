using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


[Serializable]
public class SerializableNickNameChoiceJumpData
{
    [SerializeField]
    public int jump_success_exec_id;
    [SerializeField]
    public int jump_success_order_id;
    [SerializeField]
    public int jump_failure_exec_id;
    [SerializeField]
    public int jump_failure_order_id;
    [SerializeField]
    public Character_NickName_ID nick_name_id;
    public SerializableNickNameChoiceJumpData(NickNameChoiceJumpData data,Character_NickName_ID id)
    {
        nick_name_id = id;
        jump_success_order_id = data.jump_success_order_id;
        jump_success_exec_id = data.jump_success_exec_id;
        jump_failure_exec_id = data.jump_failure_exec_id;
        jump_failure_order_id = data.jump_failure_order_id;
    }

}

[Serializable]
public class EditorListNickNameChoiceJumpData
{
    [SerializeField]
    public List<SerializableNickNameChoiceJumpData> list_json = new List<SerializableNickNameChoiceJumpData>();
}

public class NickNameChoiceJumpData
{
    public int jump_success_exec_id;
    public int jump_success_order_id;
    public int jump_failure_exec_id;
    public int jump_failure_order_id;
}

public class ListNickNameChoiceJumpData
{
    public Dictionary<Character_NickName_ID, NickNameChoiceJumpData> dict_nick_name_data = new Dictionary<Character_NickName_ID, NickNameChoiceJumpData>();

    public ListNickNameChoiceJumpData()
    {
        dict_nick_name_data.Clear();
        for (int i = 0; i < (int)Character_NickName_ID.MAX;)
        {
            dict_nick_name_data[(Character_NickName_ID)i] = new NickNameChoiceJumpData();
            ++i;
        }
    }

    public void BinaryWriter(ref BinaryWriter writer)
    {
        //サイズを保存
        writer.Write(dict_nick_name_data.Count);

        foreach (var data in dict_nick_name_data)
        {
            writer.Write((int)data.Key);
            writer.Write(data.Value.jump_success_exec_id);
            writer.Write(data.Value.jump_success_order_id);
            writer.Write(data.Value.jump_failure_exec_id);
            writer.Write(data.Value.jump_failure_order_id);
        }
    }

    public void BinaryReader(ref BinaryReader reader)
    {
        //サイズを取得
        int size = reader.ReadInt32();

        for (int i = 0; i < size;)
        {
            Character_NickName_ID id = (Character_NickName_ID)reader.ReadInt32();

            dict_nick_name_data[id] = new NickNameChoiceJumpData();
            dict_nick_name_data[id].jump_success_exec_id = reader.ReadInt32();
            dict_nick_name_data[id].jump_success_order_id = reader.ReadInt32();
            dict_nick_name_data[id].jump_failure_exec_id = reader.ReadInt32();
            dict_nick_name_data[id].jump_failure_order_id = reader.ReadInt32();

            ++i;
        }
    }

    protected string SaveNickNameChoiceJumpDataToJson()
    {
        EditorListNickNameChoiceJumpData keyframes = new EditorListNickNameChoiceJumpData();

        foreach (var data in dict_nick_name_data)
        {
            keyframes.list_json.Add(new SerializableNickNameChoiceJumpData(data.Value, data.Key));
        }


        string json = JsonUtility.ToJson(keyframes);

        return json;
    }

    protected void LoadNickNameChoiceJumpDataFromJson(string json)
    {
        EditorListNickNameChoiceJumpData keyframes = JsonUtility.FromJson<EditorListNickNameChoiceJumpData>(json);
        dict_nick_name_data.Clear();

        foreach (var key in keyframes.list_json)
        {
            if (dict_nick_name_data.ContainsKey(key.nick_name_id) == true) continue;
            NickNameChoiceJumpData add = new NickNameChoiceJumpData();
            add.jump_success_order_id = key.jump_success_order_id;
            add.jump_success_exec_id =  key.jump_success_exec_id;
            add.jump_failure_order_id = key.jump_failure_order_id;
            add.jump_failure_exec_id  = key.jump_failure_exec_id;
            dict_nick_name_data[key.nick_name_id] = add;
        }
    }

    public void JsonReaderData(Dictionary<string, object> directory)
    {
        if (directory.ContainsKey("nick_name_jump_data")) LoadNickNameChoiceJumpDataFromJson(Convert.ToString(directory["nick_name_jump_data"]));
    }
    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["nick_name_jump_data"] = SaveNickNameChoiceJumpDataToJson();
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        foreach(var data in dict_nick_name_data)
        {
            EditorGUILayout.LabelField($"{Enum.GetName(typeof(Character_NickName_ID),data.Key)}", GUILayout.Width(150));
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            data.Value.jump_success_exec_id = EditorGUILayout.IntField("ジャンプ実行(成功) ID", data.Value.jump_success_exec_id, GUILayout.Width(450));
            data.Value.jump_success_order_id = EditorGUILayout.IntField("ジャンプ指定(成功)ID", data.Value.jump_success_order_id, GUILayout.Width(450));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal();
            data.Value.jump_failure_exec_id = EditorGUILayout.IntField("ジャンプ実行(失敗) ID", data.Value.jump_failure_exec_id, GUILayout.Width(450));
            data.Value.jump_failure_order_id = EditorGUILayout.IntField("ジャンプ指定(失敗)ID", data.Value.jump_failure_order_id, GUILayout.Width(450));
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndVertical();
    }
#endif
}

