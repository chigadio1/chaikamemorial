using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ChoiceData : BaseNovelDetails
{
    public string text = "";
    public int jump_exec_id = 0;
    public int jump_order_id = 0;

    public override void BinaryReader(ref BinaryReader reader)
    {
        text = reader.ReadString();
        jump_exec_id = reader.ReadInt32();
        jump_order_id = reader.ReadInt32();
    }

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(text);
        writer.Write(jump_exec_id);
        writer.Write(jump_order_id);
    }

    public override void JsonReaderData(JObject jsonObject)
    {
        // jump_exec_idとjump_order_idをJSONオブジェクトから読み込む
        if (jsonObject.ContainsKey("text"))
        {
            text = jsonObject.Value<string>("text");
        }
        if (jsonObject.ContainsKey("jump_exec_id"))
        {
            jump_exec_id = jsonObject.Value<int>("jump_exec_id");
        }
        if (jsonObject.ContainsKey("jump_order_id"))
        {
            jump_order_id = jsonObject.Value<int>("jump_order_id");
        }
    }

    public override void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["text"] = Convert.ToString(text);
        directory["jump_exec_id"] = Convert.ToInt32(jump_exec_id);
        directory["jump_order_id"] = Convert.ToInt32(jump_order_id);
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        using(new EditorGUILayout.VerticalScope())
        {
            EditorGUI.indentLevel++;
            text = EditorGUILayout.TextField("選択文言:",text);
            jump_exec_id = EditorGUILayout.IntField("ジャンプ実行ID:", jump_exec_id);
            jump_order_id= EditorGUILayout.IntField("ジャンプ命令ID:", jump_order_id);
            EditorGUI.indentLevel--;
        }

    }
#endif
}


public class ChoiceSummaryData : BaseNovelDetails
{
    public List<ChoiceData> list_choice = new List<ChoiceData>();

    public override void BinaryReader(ref BinaryReader reader)
    {
        int size = reader.ReadInt32();
        list_choice.Clear();

        for (int i = 0; i < size; i++)
        {
            ChoiceData data = new ChoiceData();
            data.BinaryReader(ref reader);
            list_choice.Add(data);
        }
    }

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        writer.Write(list_choice.Count);

        foreach(var data in list_choice)
        {
            data.BinaryWriter(ref writer);
        }
    }

    public override void JsonReaderData(Dictionary<string, object> directory)
    {
        list_choice.Clear();
        foreach(var key_data in directory)
        {
            string json_text = Convert.ToString(key_data.Value);
            if(json_text != null)
            {
                try
                {
                    JObject jsonObject = JObject.Parse(json_text);
                    ChoiceData add = new ChoiceData();
                    add.JsonReaderData(jsonObject);
                    list_choice.Add(add);
                }
                catch (Exception ex)
                {
                    Debug.LogWarning("Error parsing JSON: " + ex.Message);
                }
            }
        }
    }

    public override void JsonWriterData(ref Dictionary<string, object> directory)
    {
        for(int i = 0; i < list_choice.Count; i++)
        {
            var json_text = new Dictionary<string, object>();
            list_choice[i].JsonWriterData(ref json_text);
            directory[$"Choice_{i:00}"] = json_text;
        }
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        for(int i = 0; i < list_choice.Count;)
        {
            EditorGUILayout.LabelField($"選択肢{i:00}:");
            list_choice[i].OnGUI();
            ++i;
        }

        using(new EditorGUILayout.HorizontalScope())
        {
            if(GUILayout.Button("追加"))
            {
                if (list_choice.Count >= 3) return;
                list_choice.Add(new ChoiceData());
            }
            if(GUILayout.Button("削除"))
            {
                if (list_choice.Count == 1) return;
                list_choice.RemoveAt(list_choice.Count - 1);
            }
        }
    }
#endif

}
