using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class BaseNovelDetails
{
    public virtual void BinaryWriter(ref BinaryWriter writer)
    {

    }

    public virtual void BinaryReader(ref BinaryReader reader)
    {

    }

    public virtual void JsonReaderData(JObject jsonObject)
    {
    }

    public virtual void JsonReaderData(Dictionary<string, object> directory)
    {
    }

    public virtual void JsonWriterData(ref Dictionary<string, object> directory)
    {

    }

#if UNITY_EDITOR
    public virtual void OnGUI()
    {
    }
#endif
}
