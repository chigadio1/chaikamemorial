using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class SerializableKeyframe
{
    [SerializeField]
    public float time;
    [SerializeField]
    public float value;
    [SerializeField]
    public float inTangent;
    [SerializeField]
    public float outTangent;
    [SerializeField]
    public float inWeight;
    [SerializeField]
    public float outWeight;
    [SerializeField]
    public WeightedMode weightedMode;

    public SerializableKeyframe(Keyframe keyframe)
    {
        time = keyframe.time;
        value = keyframe.value;
        inTangent = keyframe.inTangent;
        outTangent = keyframe.outTangent;

        inWeight = keyframe.inWeight;
        outWeight = keyframe.outWeight;
        weightedMode = keyframe.weightedMode;

    }
}

[Serializable]
public class EditorAnimationCurveJson
{
    [SerializeField]
    public List<SerializableKeyframe> keyframes = new List<SerializableKeyframe>();
}


public class BaseNovelAnimationData
{
    /// <summary>
    /// 時間
    /// </summary>
    protected float end_time = 0.0f;

    public  bool IsEnd(float now_time)
    {
        return now_time >= end_time;
    }

#if UNITY_EDITOR

    public bool is_view = false;
    public virtual void OnGUI()
    {

    }

    protected virtual void OnCurveGUI(ref AnimationCurve curv,string name,Color color)
    {
        EditorGUILayout.BeginHorizontal(GUILayout.Width(150));
        EditorGUILayout.LabelField(name);
        Rect curveRect = GUILayoutUtility.GetRect(0, EditorGUIUtility.singleLineHeight);
        curveRect.height = 100; // カーブの高さを設定
        curv = EditorGUILayout.CurveField(curv, color, curveRect);
        EditorGUILayout.EndHorizontal();
    }

    public void AddKey(ref AnimationCurve curve,float time,float value)
    {
        for(int count = 0; count < curve.keys.Length;count++)
        {
            if(curve.keys[count].time == time)
            {
                curve.keys[count].value = value;
                return;
            }
        }

        curve.AddKey(time, value);
    }
#endif
    protected string SaveAnimationCurveToJson(ref AnimationCurve curve)
    {
        EditorAnimationCurveJson keyframes = new EditorAnimationCurveJson();

        foreach (Keyframe keyframe in curve.keys)
        {
            keyframes.keyframes.Add(new SerializableKeyframe(keyframe));
        }

        string json = JsonUtility.ToJson(keyframes);

        return json;
    }
    protected void LoadAnimationCurveFromJson(ref AnimationCurve curve, string json)
    {
        EditorAnimationCurveJson keyframes = JsonUtility.FromJson<EditorAnimationCurveJson>(json);
        curve.ClearKeys();

        foreach (var key in keyframes.keyframes)
        {
            Keyframe add_key = new Keyframe();
            add_key.inTangent = key.inTangent;
            add_key.inWeight = key.inWeight;
            add_key.outTangent = key.outTangent;
            add_key.outWeight = key.outWeight;
            add_key.time = key.time;
            add_key.value = key.value;
            add_key.weightedMode = key.weightedMode;
            curve.AddKey(add_key);
        }
    }

    protected void GetTimeEnd(ref AnimationCurve curve,ref float time)
    {
        foreach(var key in curve.keys)
        {
            if(time <= key.time)
            {
                time = key.time;
            }
        }
    }

    public float GetCurveUpdate(ref AnimationCurve curve, float time)
    {
        if (curve.keys.Length == 0) return 0.0f;

        return curve.Evaluate(time);
    }

    public virtual void BinaryWriter(ref BinaryWriter writer) { }
    public virtual void BinaryReader(ref BinaryReader reader) { }

    protected virtual void BinaryWriter(ref AnimationCurve curve,ref BinaryWriter writer)
    {
        writer.Write(curve.keys.Length);

        foreach(var key in curve.keys)
        {
            writer.Write(key.inTangent);
            writer.Write(key.inWeight);
            writer.Write(key.outTangent);
            writer.Write(key.outWeight);
            writer.Write(key.time);
            writer.Write(key.value);
            writer.Write((int)key.weightedMode);;
        }
    }

    protected virtual void BinaryReader(ref AnimationCurve curve, ref BinaryReader reader)
    {
        int ket_count = reader.ReadInt32();

        curve.ClearKeys();
        for(int count = 0; count < ket_count; count++)
        {
            Keyframe key = new Keyframe();
            key.inTangent = reader.ReadSingle();
            key.inWeight = reader.ReadSingle();
            key.outTangent = reader.ReadSingle();
            key.outWeight = reader.ReadSingle();
            key.time = reader.ReadSingle();
            key.value = reader.ReadSingle();
            key.weightedMode = (WeightedMode)reader.ReadInt32();
            curve.AddKey(key);
        }




    }


}


/// <summary>
/// 座標アニメーション
/// </summary>
public class NovelAnimationPositionData : BaseNovelAnimationData
{
    private AnimationCurve position_x = new AnimationCurve();
    private AnimationCurve position_y = new AnimationCurve();
    private AnimationCurve position_z = new AnimationCurve();

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        BinaryWriter(ref position_x, ref writer);
        BinaryWriter(ref position_y, ref writer);
        BinaryWriter(ref position_z, ref writer);
    }

    public override void BinaryReader(ref BinaryReader reader)
    {
        BinaryReader(ref position_x, ref reader);
        BinaryReader(ref position_y, ref reader);
        BinaryReader(ref position_z, ref reader);
        GetTimeEnd(ref position_x, ref end_time);
        GetTimeEnd(ref position_y, ref end_time);
        GetTimeEnd(ref position_z, ref end_time);
    }

    public Vector3 AddPosition(float time)
    {
        Vector3 add_position = Vector3.zero;
        add_position.x = GetCurveUpdate(ref position_x, time);
        add_position.y = GetCurveUpdate(ref position_y, time);
        add_position.z = GetCurveUpdate(ref position_z, time);
        return add_position;
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        is_view = EditorGUILayout.Foldout(is_view, "座標");
        if(is_view)
        {
            OnCurveGUI(ref position_x, "X:", Color.green);
            OnCurveGUI(ref position_y, "Y:", Color.red);
            OnCurveGUI(ref position_z, "Z:", Color.blue);
        }
        EditorGUILayout.EndVertical();

    }

#endif
    public void JsonReaderData(Dictionary<string, object> directory)
    {
        if (directory.ContainsKey("anim_position_x")) LoadAnimationCurveFromJson(ref position_x, Convert.ToString(directory["anim_position_x"]));
        if (directory.ContainsKey("anim_position_y")) LoadAnimationCurveFromJson(ref position_y, Convert.ToString(directory["anim_position_y"]));
        if (directory.ContainsKey("anim_position_z")) LoadAnimationCurveFromJson(ref position_z, Convert.ToString(directory["anim_position_z"]));
    }

    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["anim_position_x"] = (string)SaveAnimationCurveToJson(ref position_x);
        directory["anim_position_y"] = (string)SaveAnimationCurveToJson(ref position_y);
        directory["anim_position_z"] = (string)SaveAnimationCurveToJson(ref position_z);
    }

}

/// <summary>
/// 回転アニメーション
/// </summary>
public class NovelAnimationAngleData : BaseNovelAnimationData
{
    private AnimationCurve angle_x = new AnimationCurve();
    private AnimationCurve angle_y = new AnimationCurve();
    private AnimationCurve angle_z = new AnimationCurve();
                                      
    public override void BinaryWriter(ref BinaryWriter writer)
    {
        BinaryWriter(ref angle_x, ref writer);
        BinaryWriter(ref angle_y, ref writer);
        BinaryWriter(ref angle_z, ref writer);
    }

    public override void BinaryReader(ref BinaryReader reader)
    {
        BinaryReader(ref angle_x, ref reader);
        BinaryReader(ref angle_y, ref reader);
        BinaryReader(ref angle_z, ref reader);
        GetTimeEnd(ref angle_x, ref end_time);
        GetTimeEnd(ref angle_y, ref end_time);
        GetTimeEnd(ref angle_z, ref end_time);
    }

    public Vector3 AddAngle(float time)
    {
        Vector3 add_angle = Vector3.zero;
        add_angle.x = GetCurveUpdate(ref angle_x, time);
        add_angle.y = GetCurveUpdate(ref angle_y, time);
        add_angle.z = GetCurveUpdate(ref angle_z, time);
        return add_angle;
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        is_view = EditorGUILayout.Foldout(is_view, "回転");
        if (is_view)
        {
            OnCurveGUI(ref angle_x, "X:", Color.green);
            OnCurveGUI(ref angle_y, "Y:", Color.red);
            OnCurveGUI(ref angle_z, "Z:", Color.blue);
        }
        EditorGUILayout.EndVertical();

    }
#endif
    public void JsonReaderData(Dictionary<string, object> directory)
    {
      
        if (directory.ContainsKey("anim_angle_x")) LoadAnimationCurveFromJson(ref angle_x, Convert.ToString(directory["anim_angle_x"]));
        if (directory.ContainsKey("anim_angle_y")) LoadAnimationCurveFromJson(ref angle_y, Convert.ToString(directory["anim_angle_y"]));
        if (directory.ContainsKey("anim_angle_z")) LoadAnimationCurveFromJson(ref angle_z, Convert.ToString(directory["anim_angle_z"]));
    }

    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["anim_angle_x"] = (string)SaveAnimationCurveToJson(ref angle_x);
        directory["anim_angle_y"] = (string)SaveAnimationCurveToJson(ref angle_y);
        directory["anim_angle_z"] = (string)SaveAnimationCurveToJson(ref angle_z);
    }

}

/// <summary>
/// 拡大縮小アニメーション
/// </summary>
public class NovelAnimationScaleData : BaseNovelAnimationData
{
    private AnimationCurve scale_x = new AnimationCurve();
    private AnimationCurve scale_y = new AnimationCurve();

    public override void BinaryWriter(ref BinaryWriter writer)
    {
        BinaryWriter(ref scale_x, ref writer);
        BinaryWriter(ref scale_y, ref writer);
    }

    public override void BinaryReader(ref BinaryReader reader)
    {
        BinaryReader(ref scale_x, ref reader);
        BinaryReader(ref scale_y, ref reader);
        GetTimeEnd(ref scale_x, ref end_time);
        GetTimeEnd(ref scale_y, ref end_time);
    }

    public Vector2 AddScale(float time)
    {
        Vector2 add_scale = Vector2.zero;
        add_scale.x = GetCurveUpdate(ref scale_x, time);
        add_scale.y = GetCurveUpdate(ref scale_y, time);
        return add_scale;
    }

#if UNITY_EDITOR
    public override void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        is_view = EditorGUILayout.Foldout(is_view, "拡縮");
        if (is_view)
        {
            OnCurveGUI(ref scale_x, "X:", Color.green);
            OnCurveGUI(ref scale_y, "Y:", Color.red);
        }
        EditorGUILayout.EndVertical();

    }

#endif
    public void JsonReaderData(Dictionary<string, object> directory)
    {
        if (directory.ContainsKey("anim_scale_x")) LoadAnimationCurveFromJson(ref scale_x, Convert.ToString(directory["anim_scale_x"]));
        if (directory.ContainsKey("anim_scale_y")) LoadAnimationCurveFromJson(ref scale_y, Convert.ToString(directory["anim_scale_y"]));
    }

    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        directory["anim_scale_x"] = (string)SaveAnimationCurveToJson(ref scale_x);
        directory["anim_scale_y"] = (string)SaveAnimationCurveToJson(ref scale_y);
    }

}

public class NovelAnimationData
{
    private NovelAnimationPositionData position_curve = new NovelAnimationPositionData();
    private NovelAnimationAngleData angle_curve = new NovelAnimationAngleData();
    private NovelAnimationScaleData scale_curve = new NovelAnimationScaleData();

    private Vector3 start_position;
    private Vector3 start_angle;
    private Vector2 start_scale;

    private float calc_time = 0.0f;
    private bool is_loop = false;
    private bool is_end_animation = false;
    public bool IsEndAnimation { get { return is_end_animation; } }

    public void Init(RectTransform rectTransform)
    {
        calc_time = 0.0f;
        if (rectTransform == null) return;
        start_position = rectTransform.anchoredPosition;
        start_angle = rectTransform.rotation.eulerAngles;
        start_scale = rectTransform.localScale;
    }

    public void Update(ref RectTransform rectTransform, float add_calc)
    {
        calc_time = add_calc;
        Vector3 add_position = position_curve.AddPosition(calc_time);
        Vector3 add_angle = angle_curve.AddAngle(calc_time);
        Vector2 add_scale = scale_curve.AddScale(calc_time);

        if (rectTransform != null)
        {
            rectTransform.anchoredPosition = start_position + add_position;
            rectTransform.rotation = Quaternion.Euler(add_angle + start_angle);
            rectTransform.localScale = start_scale + add_scale;
        }

        bool is_end = true;
        is_end &= position_curve.IsEnd(calc_time);
        is_end &= angle_curve.IsEnd(calc_time);
        is_end &= scale_curve.IsEnd(calc_time);
        if(is_end)
        {
            is_end_animation = true;
            if(is_loop)
            {
                calc_time = 0.0f;
            }
        }



    }

    public  void BinaryReader(ref BinaryReader reader)
    {
        position_curve.BinaryReader(ref reader);
        angle_curve.BinaryReader(ref reader);
        scale_curve.BinaryReader(ref reader);
    }

    public void BinaryWriter(ref BinaryWriter writer)
    {
        position_curve.BinaryWriter(ref writer);
        angle_curve.BinaryWriter(ref writer);
        scale_curve.BinaryWriter(ref writer);
    }

#if UNITY_EDITOR
    public void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Loop", GUILayout.Width(150));
        is_loop = EditorGUILayout.Toggle(is_loop);
        EditorGUILayout.EndHorizontal();


        position_curve.OnGUI();


        angle_curve.OnGUI();
        scale_curve.OnGUI();
    }
#endif
    public void JsonReaderData(Dictionary<string, object> directory)
    {
        position_curve.JsonReaderData(directory);
        angle_curve.JsonReaderData(directory);
        scale_curve.JsonReaderData(directory);

    }
    public void JsonWriterData(ref Dictionary<string, object> directory)
    {
        position_curve.JsonWriterData(ref directory);
        angle_curve.JsonWriterData(ref directory);
        scale_curve.JsonWriterData(ref directory);
    }

}
