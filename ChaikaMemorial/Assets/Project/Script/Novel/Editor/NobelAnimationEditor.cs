using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class NobelAnimationEditor : EditorWindow
{
    public static NobelAnimationEditor instance = null;


    private AnimationCurve curve = new AnimationCurve();
    private List<Keyframe> keyframes = new List<Keyframe>();
    private SerializedProperty curveProperty;
    // Start is called before the first frame update
    [MenuItem("Custom/ノベルアニメーション")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            Debug.Log("すでにウィンドウは存在しています");
            return;
        }
        // ウィンドウを作成または表示します
        NobelAnimationEditor.instance = EditorWindow.GetWindow(typeof(NobelAnimationEditor)) as NobelAnimationEditor;
        NobelAnimationEditor.instance.Init();


    }
    
    public void Init()
    {



    }

    private void OnGUI()
    {

        // カーブの表示と編集
        curve = CustomCurveField("X Curve", curve);

    }

    private AnimationCurve CustomCurveField(string label, AnimationCurve curve)
    {
        GUIStyle customStyle = new GUIStyle();
        customStyle.fixedHeight = 300; // 幅を変更

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel(label);
        curve = EditorGUILayout.CurveField(curve, Color.green, new Rect(0, 0, 100, 300));
        EditorGUILayout.EndHorizontal();
        return curve;
    }

}
