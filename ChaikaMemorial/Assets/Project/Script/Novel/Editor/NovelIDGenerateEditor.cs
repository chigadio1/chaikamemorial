using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System;

public class NovelIDGenerateEditor : EditorWindow
{
    public static NovelIDGenerateEditor instance = null;

    [System.Serializable]
    public class NovelIDEditor
    {
        [System.Serializable]
        public class ID
        {
            public enum ParameterID
            {
                INT,
                FLOAT,
                BOOL,
                STRING,
                VECTOR2,
                SPRITETYPE,
                CHARACTERID,
                CHARACTERFAVORABILITYID,
                FADETYPEID,
                JUMPSELECT,
                ANIMATION,
                CharacterClothingTypeID,
                FacialExpressionID,
                GameCharacterID,
                Character_NickName_ID,
                ListNickNameChoiceJumpData,
                CharacterFavoJump,
                ChoiceSummaryData
            }
            [System.Serializable]
            public class IDColmun
            {
                public ParameterID id;
                public string value_name; //変数名
                public string explanation = ""; //説明
            }

            [SerializeField]
            public string name = "";　//名前
            [SerializeField]
            public string explanation = ""; //説明
            [SerializeField]
            public List<IDColmun> colmun_list = new List<IDColmun>();
        }
        [SerializeField]
        public List<ID> id_names = new List<ID>();
    }

    private string file_path = "";
    private NovelIDEditor novel_id = new NovelIDEditor(); //ノベルID
    private string generate_id = ""; //作成ID名
    private Vector2 scroll = Vector2.zero;
    private Vector2 scroll_column = Vector2.zero;
    private int select_nove_role = 0;
    [MenuItem("Custom/ノベルID作成")]
    public static void ShowWindow()
    {
        if (instance != null)
        {
            Debug.Log("すでにウィンドウは存在しています");
            return;
        }
        // ウィンドウを作成または表示します
        NovelIDGenerateEditor.instance = EditorWindow.GetWindow(typeof(NovelIDGenerateEditor)) as NovelIDGenerateEditor;
        NovelIDGenerateEditor.instance.Init();


    }

    public void Init()
    {
        file_path += Application.dataPath + "/Project/Assets/Editor/Data/Novel/ID/";
        select_nove_role = -1;
        if (File.Exists(file_path + "/ID.json") == false)
        {
            Save();
        }
        else
        {
            using (StreamReader reader = new StreamReader(file_path + "/ID.json"))
            {

                string jsonstr = reader.ReadToEnd();
                novel_id = JsonUtility.FromJson<NovelIDEditor>(jsonstr);
            }
        }
    }

    private void Save()
    {
        StreamWriter writer = new StreamWriter(file_path + "/ID.json",false);
        

        string jsonstr = JsonUtility.ToJson(novel_id);
        writer.WriteLine(jsonstr);//JSONデータを書き込み
        writer.Flush();
        writer.Close();
        UnityEditor.AssetDatabase.Refresh();
        
    }

    private void AddColumn()
    {
        if (select_nove_role < 0) return;
        novel_id.id_names[select_nove_role].colmun_list.Add(new NovelIDEditor.ID.IDColmun());
    }

    private void Add(string value)
    {
        bool check = novel_id.id_names.Exists(item => item.name == value);
        if (check) return;
        NovelIDEditor.ID add = new NovelIDEditor.ID();
        add.name = value;
        novel_id.id_names.Add(add);
    }

    private void DeleteIndex(int index)
    {
        if (novel_id.id_names.Count < 0) return;
        if (index < 0) return;
        if (index >= novel_id.id_names.Count) return;

        novel_id.id_names.RemoveAt(index);
    }

    private void DeleteColumnIndex(int index)
    {
        if (novel_id.id_names.Count < 0) return;
        if (index < 0) return;
        if (select_nove_role < 0) return;

        novel_id.id_names[select_nove_role].colmun_list.RemoveAt(index);
    }

    private void GenerateEnum()
    {

        if (novel_id.id_names.Count < 0) return;
        if(Directory.Exists(Application.dataPath + "/Project/Script/Novel/Enum/") == false)
        {
            Directory.CreateDirectory(file_path + "/Project/Script/Novel/Enum/");
        }
        int index = 0;
        using (StreamWriter writer = new StreamWriter(Application.dataPath + "/Project/Script/Novel/Enum/NovelRoleID.cs"))
        {

            writer.WriteLine("public enum Novel_Role_ID {");

            foreach(var data in novel_id.id_names)
            {
                ++index;
                if (index >= novel_id.id_names.Count)
                {
                    writer.WriteLine($"    {data.name} //{data.explanation}");
                }
                else
                {
                    writer.WriteLine($"    {data.name}, //{data.explanation}");
                }
            }

            writer.WriteLine("}");
        }
        UnityEditor.AssetDatabase.Refresh();
    }

    private void GenerateCreate()
    {
        if (novel_id.id_names.Count < 0) return;
        using (StreamWriter writer = new StreamWriter(Application.dataPath + "/Project/Script/Novel/Generate/NovelRoleGenerate.cs"))
        {
            writer.WriteLine("using System.Collections;");
            writer.WriteLine("");

            writer.WriteLine("public class NovelRoleGenerate {");
            writer.WriteLine("    public static BaseNovelRole Create(Novel_Role_ID id) {");
            writer.WriteLine("        BaseNovelRole novelRole = null;");
            writer.WriteLine("        switch(id) {");
            

            foreach (var data in novel_id.id_names)
            {
                writer.WriteLine($"       case Novel_Role_ID.{data.name}:");
                writer.WriteLine($"       novelRole = new {data.name}NovelRole();");
                writer.WriteLine("        break;");
            }
            writer.WriteLine("        default:");
            writer.WriteLine("        break;");
            writer.WriteLine("        }");
            writer.WriteLine("        return novelRole;");
            writer.WriteLine("    }");
            writer.WriteLine("}");
        }


    }

    private void AllGenerateClass()
    {
        for(int count = 0; count < novel_id.id_names.Count;)
        {
            var data = novel_id.id_names[count];
            if (Directory.Exists(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}") == false)
            {
                Directory.CreateDirectory(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}");
            }

            using (StreamWriter writer = new StreamWriter(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/Base{data.name}NovelRole.cs"))
            {
                writer.WriteLine("using System;");
                writer.WriteLine("using System.Collections;");
                writer.WriteLine("using System.IO;");
                writer.WriteLine("using UnityEngine;");
                writer.WriteLine("using System.Collections.Generic;");
                writer.WriteLine("#if UNITY_EDITOR");
                writer.WriteLine("using UnityEditor;");
                writer.WriteLine("#endif");
                writer.WriteLine("");

                writer.WriteLine($"public class Base{data.name}NovelRole : BaseNovelRole" + " {");
                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {
                        case NovelIDEditor.ID.ParameterID.INT:
                            writer.WriteLine($"    protected int {column.value_name} = 0; //{column.explanation}");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"    protected float {column.value_name} = 0.0f; //{column.explanation}");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"    protected bool {column.value_name} = false; //{column.explanation}");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"    protected string {column.value_name} = \"\"; //{column.explanation}");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"    protected Vector2 {column.value_name} = Vector2.zero; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                            writer.WriteLine($"    protected Novel_Sprite_Type_ID {column.value_name} = Novel_Sprite_Type_ID.NONE; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                            writer.WriteLine($"    protected Novel_Character_ID {column.value_name} = Novel_Character_ID.NONE; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                            writer.WriteLine($"    protected Character_ID {column.value_name} = Character_ID.NONE; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"    protected Fade_Type_ID {column.value_name} = Fade_Type_ID.NONE; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"    protected SelectJumpData {column.value_name}; //{column.explanation})");
                            break;
                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"    protected NovelAnimationData {column.value_name} = new NovelAnimationData(); //{column.explanation})");
                            break;
                    }
                }


                writer.WriteLine($"    public Base{data.name}NovelRole(Base{data.name}NovelRole other):base(other) " + "{");
                writer.WriteLine($"        role_id = Novel_Role_ID.{data.name};");
                foreach (var column in data.colmun_list)
                {
                    writer.WriteLine($"        {column.value_name} = other.{column.value_name};");

                }
                writer.WriteLine("    }");
                writer.WriteLine($"    public  Base{data.name}NovelRole():base() " + "{");
                writer.WriteLine($"        role_id = Novel_Role_ID.{data.name};");
                writer.WriteLine("    }");
                writer.WriteLine("");

                writer.WriteLine("    public override void BinaryReaderData(ref BinaryReader reader) {");
                writer.WriteLine("        base.BinaryReaderData(ref reader);");

                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {
                        case NovelIDEditor.ID.ParameterID.INT:
                            writer.WriteLine($"        {column.value_name} = reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"        {column.value_name} = reader.ReadSingle();");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"        {column.value_name} = reader.ReadBoolean();");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"        {column.value_name} = reader.ReadString();");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"        {column.value_name} = new Vector2(reader.ReadSingle(),reader.ReadSingle());");
                            break;
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                            writer.WriteLine($"        {column.value_name} = (Novel_Sprite_Type_ID)reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                            writer.WriteLine($"        {column.value_name} = (Novel_Character_ID)reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                            writer.WriteLine($"        {column.value_name} = (Character_ID)reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"        {column.value_name} = (Fade_Type_ID)reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"        {column.value_name}.select_text = reader.ReadString();");
                            writer.WriteLine($"        {column.value_name}.jump_exec_id = reader.ReadInt32();");
                            writer.WriteLine($"        {column.value_name}.jump_order_id = reader.ReadInt32();");
                            break;
                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"        {column.value_name}.BinaryReader(ref reader);");
                            break;
                    }

                }

                writer.WriteLine("    }");

                writer.WriteLine("    public override void BinaryWriterData(ref BinaryWriter writer)  {");
                writer.WriteLine("        base.BinaryWriterData(ref writer);");

                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {
                        case NovelIDEditor.ID.ParameterID.INT:
                            writer.WriteLine($"        writer.Write({column.value_name});");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"        writer.Write({column.value_name});");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"        writer.Write({column.value_name});");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"        writer.Write({column.value_name});");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"        writer.Write({column.value_name}.x);");
                            writer.WriteLine($"        writer.Write({column.value_name}.y);");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"        writer.Write({column.value_name}.select_text);");
                            writer.WriteLine($"        writer.Write({column.value_name}.jump_exec_id);");
                            writer.WriteLine($"        writer.Write({column.value_name}.jump_order_id);");
                            break;
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"        writer.Write((int){column.value_name});");
                            break;
                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"        {column.value_name}.BinaryWriter(ref writer);");
                            break;
                    }


                }

                writer.WriteLine("    }");

                //Json読み込み
                writer.WriteLine("    public override　void JsonReaderData(Dictionary<string,object> directory)  {");
                writer.WriteLine("        base.JsonReaderData(directory);");
                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {

                        case NovelIDEditor.ID.ParameterID.INT:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToInt32(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToSingle(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToBoolean(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToSingle(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_x\")) {column.value_name}.x = Convert.ToSingle(directory[\"{column.value_name}_x\"]);");
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_y\")) {column.value_name}.y = Convert.ToSingle(directory[\"{column.value_name}_y\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Novel_Sprite_Type_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Novel_Character_ID)Convert.ToInt32((directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Character_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Fade_Type_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_select_text\"))   {column.value_name}.select_text = Convert.ToString(directory[\"{column.value_name}_select_text\"]);");
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_jump_exec_id\"))  {column.value_name}.jump_exec_id  = Convert.ToInt32(directory[\"{column.value_name}_jump_exec_id\"]);");
                            writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_jump_order_id\")) {column.value_name}.jump_order_id = Convert.ToInt32(directory[\"{column.value_name}_jump_order_id\"]);");
                            break;
                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"        {column.value_name}.JsonReaderData(directory);");
                            break;
                    }
                }


                writer.WriteLine("    }");

                //Json書き込み

                writer.WriteLine("    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {");
                writer.WriteLine("        base.JsonWriterData(ref directory);");
                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {
                        case NovelIDEditor.ID.ParameterID.INT:
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"        directory[\"{column.value_name}\"] = (int){column.value_name};");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"        directory[\"{column.value_name}\"] = (float){column.value_name};");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"        directory[\"{column.value_name}\"] = (bool){column.value_name};");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"        directory[\"{column.value_name}\"] = (string){column.value_name};");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"        directory[\"{column.value_name}_x\"] = (float){column.value_name}.x;");
                            writer.WriteLine($"        directory[\"{column.value_name}_y\"] = (float){column.value_name}.y;");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"        directory[\"{column.value_name}_select_text\"] = (string){column.value_name}.select_text;");
                            writer.WriteLine($"        directory[\"{column.value_name}_jump_exec_id\"] = (int){column.value_name}.jump_exec_id;");
                            writer.WriteLine($"        directory[\"{column.value_name}_jump_order_id\"] = (int){column.value_name}.jump_order_id;");
                            break;
                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"        {column.value_name}.JsonWriterData(ref directory);");
                            break;
                    }
                }
                writer.WriteLine("    }");


                //Size取得

                //writer.WriteLine("    public override　int GetSize()  {");
                //writer.WriteLine("        int size = base.JsonWriterData(ref directory);");
                //foreach (var column in data.colmun_list)
                //{
                //    switch (column.id)
                //    {
                //        case NovelIDEditor.ID.ParameterID.INT:
                //        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                //        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                //        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                //        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                //            writer.WriteLine($"        size += sizeof(int);");
                //            break;
                //        case NovelIDEditor.ID.ParameterID.FLOAT:
                //            writer.WriteLine($"        size += sizeof(float);");
                //            break;
                //        case NovelIDEditor.ID.ParameterID.BOOL:
                //            writer.WriteLine($"        size += sizeof(bool);");
                //            break;
                //        case NovelIDEditor.ID.ParameterID.STRING:
                //            writer.WriteLine($"        size += System.Text.Encoding.UTF8.GetByteCount({column.value_name});");
                //            break;
                //        case NovelIDEditor.ID.ParameterID.VECTOR2:
                //            writer.WriteLine($"        size += sizeof(float);");
                //            writer.WriteLine($"        size += sizeof(float);");
                //            break;
                //    }
                //}




                writer.WriteLine("#if UNITY_EDITOR");
                writer.WriteLine("    public override void OnGUI() { ");
                foreach (var column in data.colmun_list)
                {
                    switch (column.id)
                    {
                        case NovelIDEditor.ID.ParameterID.INT:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(INT):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = EditorGUILayout.IntField(\"\",{column.value_name}, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.FLOAT:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(FLOAT):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = EditorGUILayout.FloatField(\"\",{column.value_name}, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.BOOL:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(BOOL):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = EditorGUILayout.Toggle(\"\", {column.value_name}, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.STRING:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(STRING):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = EditorGUILayout.TextField(\"\",{column.value_name}, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.VECTOR2:
                            writer.WriteLine($"        GUILayout.BeginVertical();");
                            writer.WriteLine($"        float {column.value_name}_X,{column.value_name}_Y = 0.0f;");
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}.X(VECTOR2.X):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name}_X = EditorGUILayout.FloatField(\"\",{column.value_name}.x, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}.Y(VECTOR2.Y):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name}_Y = EditorGUILayout.FloatField(\"\",{column.value_name}.y, GUILayout.Width(250));");
                            writer.WriteLine($"        {column.value_name} = new Vector2({column.value_name}_X,{column.value_name}_Y);");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            writer.WriteLine($"        GUILayout.EndVertical();");
                            break;
                        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(SPRITETYPE):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = (Novel_Sprite_Type_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERID:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(CHARACTERID):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = (Novel_Character_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(CHARACTERFAVOID):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = (Character_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.FADETYPEID:
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(FADETYPEID):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name} = (Fade_Type_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            break;
                        case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                            writer.WriteLine($"        GUILayout.BeginVertical();");
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(文言):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name}.select_text = EditorGUILayout.TextField(\"\",{column.value_name}.select_text, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(実行ID):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name}.jump_exec_id = EditorGUILayout.IntField(\"\",{column.value_name}.jump_exec_id, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            writer.WriteLine($"        GUILayout.BeginHorizontal();");
                            writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(順序ID):\", GUILayout.Width(150));");
                            writer.WriteLine($"        {column.value_name}.jump_order_id = EditorGUILayout.IntField(\"\",{column.value_name}.jump_order_id, GUILayout.Width(250));");
                            writer.WriteLine($"        GUILayout.EndHorizontal();");
                            writer.WriteLine($"        GUILayout.EndVertical();");
                            break;

                        case NovelIDEditor.ID.ParameterID.ANIMATION:
                            writer.WriteLine($"        {column.value_name}.OnGUI();");
                            break;
                    }

                }
                writer.WriteLine("    }");
                writer.WriteLine("#endif");

                writer.WriteLine("}");
            }

            if (File.Exists(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/{data.name}NovelRole.cs") == true)
            {
                return;
            }

            using (StreamWriter writer = new StreamWriter(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/{data.name}NovelRole.cs"))
            {
                writer.WriteLine("using System.Collections;");
                writer.WriteLine("");

                writer.WriteLine($"public class {data.name}NovelRole : Base{data.name}NovelRole" + " {");
                writer.WriteLine($"    public {data.name}NovelRole({data.name}NovelRole other):base(other) " + "{");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine($"    public {data.name}NovelRole():base() " + "{");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine("    public override void BeginSetting()  {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine("    public override void BeginInit()  {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine("    public override void UpdateExec()  {");
                writer.WriteLine("    }");
                writer.WriteLine("");
                writer.WriteLine("    public override void FinishUninit()  {");
                writer.WriteLine("    }");
                writer.WriteLine("}");
            }
            ++count;
        }
        UnityEditor.AssetDatabase.Refresh();
    }

    private void GenerateClass()
    {
        if (novel_id.id_names.Count < 0) return;
        if (select_nove_role < 0) return;

        var data = novel_id.id_names[select_nove_role];
        if (Directory.Exists(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}") == false)
        {
            Directory.CreateDirectory(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}");
        }

        using (StreamWriter writer = new StreamWriter(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/Base{data.name}NovelRole.cs"))
        {
            writer.WriteLine("using System;");
            writer.WriteLine("using System.Collections;");
            writer.WriteLine("using System.IO;");
            writer.WriteLine("using UnityEngine;");
            writer.WriteLine("using System.Collections.Generic;");
            writer.WriteLine("#if UNITY_EDITOR");
            writer.WriteLine("using UnityEditor;");
            writer.WriteLine("#endif");
            writer.WriteLine("");

            writer.WriteLine($"public class Base{data.name}NovelRole : BaseNovelRole" + " {");
            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    case NovelIDEditor.ID.ParameterID.INT:
                        writer.WriteLine($"    protected int {column.value_name} = 0; //{column.explanation}");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"    protected float {column.value_name} = 0.0f; //{column.explanation}");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"    protected bool {column.value_name} = false; //{column.explanation}");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"    protected string {column.value_name} = \"\"; //{column.explanation}") ;
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"    protected Vector2 {column.value_name} = Vector2.zero; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        writer.WriteLine($"    protected Novel_Sprite_Type_ID {column.value_name} = Novel_Sprite_Type_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        writer.WriteLine($"    protected Novel_Character_ID {column.value_name} = Novel_Character_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        writer.WriteLine($"    protected Character_ID {column.value_name} = Character_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                        writer.WriteLine($"    protected Fade_Type_ID {column.value_name} = Fade_Type_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"    protected SelectJumpData {column.value_name}; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"    protected NovelAnimationData {column.value_name} = new NovelAnimationData(); //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                        writer.WriteLine($"    protected CharacterClothingTypeID {column.value_name} = CharacterClothingTypeID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                        writer.WriteLine($"    protected FacialExpressionID {column.value_name} = FacialExpressionID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                        writer.WriteLine($"    protected Character_ID {column.value_name} = Character_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"    protected Character_NickName_ID {column.value_name} = Character_NickName_ID.NONE; //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                        writer.WriteLine($"    protected ListNickNameChoiceJumpData {column.value_name} = new ListNickNameChoiceJumpData(); //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                        writer.WriteLine($"    protected CharacterFavoJump {column.value_name} = new CharacterFavoJump(); //{column.explanation})");
                        break;
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"    protected ChoiceSummaryData {column.value_name} = new ChoiceSummaryData(); //{column.explanation})");
                        break;
                }
            }


            writer.WriteLine($"    public Base{data.name}NovelRole(Base{data.name}NovelRole other):base(other) " + "{");
            writer.WriteLine($"        role_id = Novel_Role_ID.{data.name};");
            foreach (var column in data.colmun_list)
            { 
                writer.WriteLine($"        {column.value_name} = other.{column.value_name};");
            
            }
            writer.WriteLine("    }");
            writer.WriteLine($"    public  Base{data.name}NovelRole():base() " + "{");
            writer.WriteLine($"        role_id = Novel_Role_ID.{data.name};");
            writer.WriteLine("    }");
            writer.WriteLine("");

            writer.WriteLine("    public override void BinaryReaderData(ref BinaryReader reader) {");
            writer.WriteLine("        base.BinaryReaderData(ref reader);");

            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    case NovelIDEditor.ID.ParameterID.INT:
                        writer.WriteLine($"        {column.value_name} = reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"        {column.value_name} = reader.ReadSingle();");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"        {column.value_name} = reader.ReadBoolean();");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"        {column.value_name} = reader.ReadString();");
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"        {column.value_name} = new Vector2(reader.ReadSingle(),reader.ReadSingle());");
                        break;
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        writer.WriteLine($"        {column.value_name} = (Novel_Sprite_Type_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        writer.WriteLine($"        {column.value_name} = (Novel_Character_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        writer.WriteLine($"        {column.value_name} = (Character_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                        writer.WriteLine($"        {column.value_name} = (Fade_Type_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"        {column.value_name}.select_text = reader.ReadString();");
                        writer.WriteLine($"        {column.value_name}.jump_exec_id = reader.ReadInt32();");
                        writer.WriteLine($"        {column.value_name}.jump_order_id = reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"        {column.value_name}.BinaryReader(ref reader);");
                        break;
                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                        writer.WriteLine($"        {column.value_name} = (CharacterClothingTypeID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                        writer.WriteLine($"        {column.value_name} = (FacialExpressionID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                        writer.WriteLine($"        {column.value_name} = (Character_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"        {column.value_name} = (Character_NickName_ID)reader.ReadInt32();");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                        writer.WriteLine($"        {column.value_name}.BinaryReader(ref reader);");
                        break;
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"        {column.value_name}.BinaryReader(ref reader);");
                        break;
                }

            }

            writer.WriteLine("    }");

            writer.WriteLine("    public override void BinaryWriterData(ref BinaryWriter writer)  {");
            writer.WriteLine("        base.BinaryWriterData(ref writer);");

            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    case NovelIDEditor.ID.ParameterID.INT:
                        writer.WriteLine($"        writer.Write({column.value_name});");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"        writer.Write({column.value_name});");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"        writer.Write({column.value_name});");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"        writer.Write({column.value_name});");
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"        writer.Write({column.value_name}.x);");
                        writer.WriteLine($"        writer.Write({column.value_name}.y);");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"        writer.Write({column.value_name}.select_text);");
                        writer.WriteLine($"        writer.Write({column.value_name}.jump_exec_id);");
                        writer.WriteLine($"        writer.Write({column.value_name}.jump_order_id);");
                        break;
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"        writer.Write((int){column.value_name});");
                        break;
                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"        {column.value_name}.BinaryWriter(ref writer);");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"        {column.value_name}.BinaryWriter(ref writer);");
                        break;
                }
                

            }

            writer.WriteLine("    }");

            //Json読み込み
            writer.WriteLine("    public override　void JsonReaderData(Dictionary<string,object> directory)  {");
            writer.WriteLine("        base.JsonReaderData(directory);");
            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    
                    case NovelIDEditor.ID.ParameterID.INT:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToSingle(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToBoolean(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = Convert.ToSingle(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_x\")) {column.value_name}.x = Convert.ToSingle(directory[\"{column.value_name}_x\"]);");
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_y\")) {column.value_name}.y = Convert.ToSingle(directory[\"{column.value_name}_y\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Novel_Sprite_Type_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Novel_Character_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Character_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Fade_Type_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_select_text\"))   {column.value_name}.select_text = Convert.ToString(directory[\"{column.value_name}_select_text\"]);");
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_jump_exec_id\"))  {column.value_name}.jump_exec_id  = Convert.ToInt32(directory[\"{column.value_name}_jump_exec_id\"]);");
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}_jump_order_id\")) {column.value_name}.jump_order_id = Convert.ToInt32(directory[\"{column.value_name}_jump_order_id\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"        {column.value_name}.JsonReaderData(directory);");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"        {column.value_name}.JsonReaderData(directory);");
                        break;
                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (CharacterClothingTypeID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (FacialExpressionID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Character_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"        if (directory.ContainsKey(\"{column.value_name}\")) {column.value_name} = (Character_NickName_ID)Convert.ToInt32(directory[\"{column.value_name}\"]);");
                        break;
                }
            }


            writer.WriteLine("    }");

            //Json書き込み

            writer.WriteLine("    public override　void JsonWriterData(ref Dictionary<string, object> directory)  {");
            writer.WriteLine("        base.JsonWriterData(ref directory);");
            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    case NovelIDEditor.ID.ParameterID.INT:
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"        directory[\"{column.value_name}\"] = (int){column.value_name};");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"        directory[\"{column.value_name}\"] = (float){column.value_name};");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"        directory[\"{column.value_name}\"] = (bool){column.value_name};");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"        directory[\"{column.value_name}\"] = (string){column.value_name};");
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"        directory[\"{column.value_name}_x\"] = (float){column.value_name}.x;");
                        writer.WriteLine($"        directory[\"{column.value_name}_y\"] = (float){column.value_name}.y;");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"        directory[\"{column.value_name}_select_text\"] = (string){column.value_name}.select_text;");
                        writer.WriteLine($"        directory[\"{column.value_name}_jump_exec_id\"] = (int){column.value_name}.jump_exec_id;");
                        writer.WriteLine($"        directory[\"{column.value_name}_jump_order_id\"] = (int){column.value_name}.jump_order_id;");
                        break;
                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"        {column.value_name}.JsonWriterData(ref directory);");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"        {column.value_name}.JsonWriterData(ref directory);");
                        break;
                }
            }
            writer.WriteLine("    }");


            //Size取得

            //writer.WriteLine("    public override　int GetSize()  {");
            //writer.WriteLine("        int size = base.JsonWriterData(ref directory);");
            //foreach (var column in data.colmun_list)
            //{
            //    switch (column.id)
            //    {
            //        case NovelIDEditor.ID.ParameterID.INT:
            //        case NovelIDEditor.ID.ParameterID.SPRITETYPE:
            //        case NovelIDEditor.ID.ParameterID.CHARACTERID:
            //        case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
            //        case NovelIDEditor.ID.ParameterID.FADETYPEID:
            //            writer.WriteLine($"        size += sizeof(int);");
            //            break;
            //        case NovelIDEditor.ID.ParameterID.FLOAT:
            //            writer.WriteLine($"        size += sizeof(float);");
            //            break;
            //        case NovelIDEditor.ID.ParameterID.BOOL:
            //            writer.WriteLine($"        size += sizeof(bool);");
            //            break;
            //        case NovelIDEditor.ID.ParameterID.STRING:
            //            writer.WriteLine($"        size += System.Text.Encoding.UTF8.GetByteCount({column.value_name});");
            //            break;
            //        case NovelIDEditor.ID.ParameterID.VECTOR2:
            //            writer.WriteLine($"        size += sizeof(float);");
            //            writer.WriteLine($"        size += sizeof(float);");
            //            break;
            //    }
            //}




            writer.WriteLine("#if UNITY_EDITOR");
            writer.WriteLine("    public override void OnGUI() { ");
            foreach (var column in data.colmun_list)
            {
                switch (column.id)
                {
                    case NovelIDEditor.ID.ParameterID.INT:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(INT):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = EditorGUILayout.IntField(\"\",{column.value_name}, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.FLOAT:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(FLOAT):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = EditorGUILayout.FloatField(\"\",{column.value_name}, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.BOOL:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(BOOL):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = EditorGUILayout.Toggle(\"\", {column.value_name}, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.STRING:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(STRING):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = EditorGUILayout.TextField(\"\",{column.value_name}, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.VECTOR2:
                        writer.WriteLine($"        GUILayout.BeginVertical();");
                        writer.WriteLine($"        float {column.value_name}_X,{column.value_name}_Y = 0.0f;");
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}.X(VECTOR2.X):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name}_X = EditorGUILayout.FloatField(\"\",{column.value_name}.x, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}.Y(VECTOR2.Y):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name}_Y = EditorGUILayout.FloatField(\"\",{column.value_name}.y, GUILayout.Width(250));");
                        writer.WriteLine($"        {column.value_name} = new Vector2({column.value_name}_X,{column.value_name}_Y);");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        writer.WriteLine($"        GUILayout.EndVertical();");
                        break;
                    case NovelIDEditor.ID.ParameterID.SPRITETYPE:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(SPRITETYPE):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Novel_Sprite_Type_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(CHARACTERID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Novel_Character_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.CHARACTERFAVORABILITYID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(CHARACTERFAVOID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Character_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.FADETYPEID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(FADETYPEID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Fade_Type_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                    case NovelIDEditor.ID.ParameterID.JUMPSELECT:
                        writer.WriteLine($"        GUILayout.BeginVertical();");
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(文言):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name}.select_text = EditorGUILayout.TextField(\"\",{column.value_name}.select_text, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(実行ID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name}.jump_exec_id = EditorGUILayout.IntField(\"\",{column.value_name}.jump_exec_id, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(順序ID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name}.jump_order_id = EditorGUILayout.IntField(\"\",{column.value_name}.jump_order_id, GUILayout.Width(250));");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        writer.WriteLine($"        GUILayout.EndVertical();");
                        break;

                    case NovelIDEditor.ID.ParameterID.ANIMATION:
                        writer.WriteLine($"        {column.value_name}.OnGUI();");
                        break;
                    case NovelIDEditor.ID.ParameterID.ListNickNameChoiceJumpData:
                    case NovelIDEditor.ID.ParameterID.CharacterFavoJump:
                    case NovelIDEditor.ID.ParameterID.ChoiceSummaryData:
                        writer.WriteLine($"        {column.value_name}.OnGUI();");
                        break;

                    case NovelIDEditor.ID.ParameterID.CharacterClothingTypeID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(CharacterClothingTypeID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (CharacterClothingTypeID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;

                    case NovelIDEditor.ID.ParameterID.FacialExpressionID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(FacialExpressionID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (FacialExpressionID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;

                    case NovelIDEditor.ID.ParameterID.GameCharacterID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(GameCharacterID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Character_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;

                    case NovelIDEditor.ID.ParameterID.Character_NickName_ID:
                        writer.WriteLine($"        GUILayout.BeginHorizontal();");
                        writer.WriteLine($"        GUILayout.Label(\"{column.explanation}(Character_NickName_ID):\", GUILayout.Width(150));");
                        writer.WriteLine($"        {column.value_name} = (Character_NickName_ID)EditorGUILayout.EnumPopup(\"\", {column.value_name});");
                        writer.WriteLine($"        GUILayout.EndHorizontal();");
                        break;
                }

            }
            writer.WriteLine("    }");
            writer.WriteLine("#endif");

            writer.WriteLine("}");
        }

        if (File.Exists(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/{data.name}NovelRole.cs") == true)
        {
            return;
        }

        using (StreamWriter writer = new StreamWriter(Application.dataPath + $"/Project/Script/Novel/Role/{data.name}/{data.name}NovelRole.cs"))
        {
            writer.WriteLine("using System.Collections;");
            writer.WriteLine("");

            writer.WriteLine($"public class {data.name}NovelRole : Base{data.name}NovelRole" + " {");
            writer.WriteLine($"    public {data.name}NovelRole({data.name}NovelRole other):base(other) " + "{");
            writer.WriteLine("    }");
            writer.WriteLine("");
            writer.WriteLine($"    public {data.name}NovelRole():base() " + "{");
            writer.WriteLine("    }");
            writer.WriteLine("");
            writer.WriteLine("    public override void BeginSetting()  {");
            writer.WriteLine("    }");
            writer.WriteLine("");
            writer.WriteLine("    public override void BeginInit()  {");
            writer.WriteLine("    }");
            writer.WriteLine("");
            writer.WriteLine("    public override void UpdateExec()  {");
            writer.WriteLine("    }");
            writer.WriteLine("");
            writer.WriteLine("    public override void FinishUninit()  {");
            writer.WriteLine("    }");
            writer.WriteLine("}");
        }
        UnityEditor.AssetDatabase.Refresh();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        // 左側のGUIを表示
        EditorGUILayout.BeginVertical(GUILayout.Width(400));

        {
            if (GUILayout.Button("Save"))
            {
                Save();
            }
            if (GUILayout.Button("GenerateEnum"))
            {
                GenerateEnum();
                GenerateCreate();
            }
            if (GUILayout.Button("AllGenerateEnum"))
            {
                GenerateEnum();
                AllGenerateClass();
            }
            EditorGUILayout.BeginHorizontal();
            generate_id = EditorGUILayout.TextField("",generate_id);
            if (GUILayout.Button("Add"))
            {
                Add(generate_id);
            }
            EditorGUILayout.EndHorizontal();

            scroll = EditorGUILayout.BeginScrollView(scroll);
            int index = 0;
            if (novel_id.id_names.Count > 0)
            {
                for(index = 0; index < novel_id.id_names.Count;)
                {
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(400));
                    if(GUILayout.Button(novel_id.id_names[index].name, GUILayout.Width(100)))
                    {
                        select_nove_role = index;
                    }

                    novel_id.id_names[index].explanation = EditorGUILayout.TextField("", novel_id.id_names[index].explanation, GUILayout.Width(150));
                    if (GUILayout.Button("×", GUILayout.Width(100)))
                    {
                        DeleteIndex(index);
                        select_nove_role = -1;
                    }
                    EditorGUILayout.EndHorizontal();
                    ++index;
                }
            }
            EditorGUILayout.EndScrollView();
        }

        EditorGUILayout.EndVertical();

        EditorGUILayout.BeginVertical(GUILayout.Width(600));

        if(select_nove_role > -1)
        {
            int indexColumnCount = 0;
            scroll_column = EditorGUILayout.BeginScrollView(scroll_column);
            for (indexColumnCount = 0; indexColumnCount < novel_id.id_names[select_nove_role].colmun_list.Count;)
            {
                EditorGUILayout.BeginHorizontal();
                novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].id = (NovelIDEditor.ID.ParameterID)EditorGUILayout.EnumPopup("ID", novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].id);
                novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].value_name = EditorGUILayout.TextField("", novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].value_name, GUILayout.Width(150));
                novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].explanation = EditorGUILayout.TextField("", novel_id.id_names[select_nove_role].colmun_list[indexColumnCount].explanation, GUILayout.Width(150));
                if (GUILayout.Button("×", GUILayout.Width(100)))
                {
                    DeleteColumnIndex(indexColumnCount);
                    return;
                }
                ++indexColumnCount;
                EditorGUILayout.EndVertical();
            }
            if (GUILayout.Button("Add", GUILayout.Width(100)))
            {
                AddColumn();
            }
            if (GUILayout.Button("Save", GUILayout.Width(100)))
            {
                Save();
            }
            if (GUILayout.Button("Generate", GUILayout.Width(100)))
            {
                GenerateClass();
                UnityEditor.AssetDatabase.Refresh();
            }
            EditorGUILayout.EndScrollView();
        }

        EditorGUILayout.EndVertical();
        EditorGUILayout.EndHorizontal();
    }
}
