using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NovelEditor : EditorWindow
{
    public static NovelEditor instance = null;
    public NovelExecGroupManager manager = new NovelExecGroupManager();

    Novel_Role_ID role_id;
    Vector2 scroll = Vector2.zero;

    [MenuItem("Custom/ノベルAAA作成")]
    public static void ShowWindow()
    {

        // ウィンドウを作成または表示します
        NovelEditor.instance = EditorWindow.GetWindow(typeof(NovelEditor)) as NovelEditor;


    }

    private void OnGUI()
    {
        manager.OnGUI(ref scroll,ref role_id,this);
    }
}
