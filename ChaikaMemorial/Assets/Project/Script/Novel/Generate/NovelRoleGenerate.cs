using System.Collections;

public class NovelRoleGenerate {
    public static BaseNovelRole Create(Novel_Role_ID id) {
        BaseNovelRole novelRole = null;
        switch(id) {
       case Novel_Role_ID.LoadSprite:
       novelRole = new LoadSpriteNovelRole();
        break;
       case Novel_Role_ID.ShowCharacter:
       novelRole = new ShowCharacterNovelRole();
        break;
       case Novel_Role_ID.TextOutput:
       novelRole = new TextOutputNovelRole();
        break;
       case Novel_Role_ID.FadeStartup:
       novelRole = new FadeStartupNovelRole();
        break;
       case Novel_Role_ID.FavorabilitCalc:
       novelRole = new FavorabilitCalcNovelRole();
        break;
       case Novel_Role_ID.JumpScript:
       novelRole = new JumpScriptNovelRole();
        break;
       case Novel_Role_ID.ChoicesSelect:
       novelRole = new ChoicesSelectNovelRole();
        break;
       case Novel_Role_ID.CutFadeStarUp:
       novelRole = new CutFadeStarUpNovelRole();
        break;
       case Novel_Role_ID.ShowStill:
       novelRole = new ShowStillNovelRole();
        break;
       case Novel_Role_ID.CharacterRootAnimation:
       novelRole = new CharacterRootAnimationNovelRole();
        break;
       case Novel_Role_ID.PlayAudioSE:
       novelRole = new PlayAudioSENovelRole();
        break;
       case Novel_Role_ID.PlayAudioBGM:
       novelRole = new PlayAudioBGMNovelRole();
        break;
       case Novel_Role_ID.StopAudioBGM:
       novelRole = new StopAudioBGMNovelRole();
        break;
       case Novel_Role_ID.CharacterFavoUI:
       novelRole = new CharacterFavoUINovelRole();
        break;
       case Novel_Role_ID.StillRootAnimation:
       novelRole = new StillRootAnimationNovelRole();
        break;
       case Novel_Role_ID.Delay:
       novelRole = new DelayNovelRole();
        break;
       case Novel_Role_ID.JumpEvent:
       novelRole = new JumpEventNovelRole();
        break;
       case Novel_Role_ID.ShowBackGround:
       novelRole = new ShowBackGroundNovelRole();
        break;
       case Novel_Role_ID.ShowCharacterCostume:
       novelRole = new ShowCharacterCostumeNovelRole();
        break;
       case Novel_Role_ID.CompulsoryGameFinish:
       novelRole = new CompulsoryGameFinishNovelRole();
        break;
       case Novel_Role_ID.ProbabilityJumpScript:
       novelRole = new ProbabilityJumpScriptNovelRole();
        break;
       case Novel_Role_ID.NickNameChoice:
       novelRole = new NickNameChoiceNovelRole();
        break;
       case Novel_Role_ID.CharacterFavoJumpScript:
       novelRole = new CharacterFavoJumpScriptNovelRole();
        break;
        default:
        break;
        }
        return novelRole;
    }
}
