using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public enum Character_Favo_Anim_ID
{
    NONE = -1,
    StartUP = 8251,
    LooPIdle,
    MAX
}

public class CharacterFavoSystemNovelObject : BaseSystemNovelObject
{
    /// <summary>
    /// セレクトキャラクター
    /// </summary>
    CharacterName[] character_name_array = new CharacterName[5];

    CharacterUpImage character_image = new CharacterUpImage();

    List<AddressableData<Sprite>> character_sprite = new List<AddressableData<Sprite>>();

    readonly int[] character_id_array = new int[(int)Character_ID.MAX]
    {
        2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001,2001
    };

    Animator animator;
    private bool inversion_flag;
    private bool change_flag;

    readonly int move_add = 150;
    readonly int max_pos = 300;
    readonly int min_pos = -300;
    public override void BaseInit()
    {

        var rect = gameObject.GetComponent<RectTransform>();
        if(rect != null)
        {
            rect.anchoredPosition = Vector2.zero;
        }
        var childs = gameObject.GetComponentsInChildren<TextMeshProUGUI>();

        foreach(var child in childs)
        {
            int count = -1;
            var parent = child.rectTransform.parent.gameObject;
            if (parent == null) continue;

            string pattern = @"\d+"; // 数字の正規表現パターン

            // 正規表現に一致する部分を検索
            Match match = Regex.Match(parent.gameObject.name, pattern);

            // 一致した部分があるか確認
            if (match.Success)
            {
                // 一致した部分を取得
                int extractedNumber = int.Parse(match.Value);
                count = extractedNumber;
                count -= 1;

            }
            if (count <= -1) continue;
            character_name_array[count] = new CharacterName();
            character_name_array[count].rect_transform = parent.gameObject.GetComponent<RectTransform>();
            character_name_array[count].text_mesh = child;
            
        }

        var image_childs = gameObject.GetComponentsInChildren<Image>();
        foreach(var child in image_childs)
        {
            if(child.gameObject.name == "StandingPictureBack")
            {
                character_image.back_image = child;
            }
            else if(child.gameObject.name == "StandingPicture")
            {
                character_image.front_image = child;
            }

            if(character_image.front_image != null && character_image.back_image != null)
            {
                break;
            }
        }

        foreach(var id in character_id_array)
        {
            var add = new AddressableData<Sprite>();
            add.LoadStart($"Assets/Project/image/Character/{id}.png");
            character_sprite.Add(add);
        }

        animator = gameObject.GetComponentInChildren<Animator>();
    }

    public bool IsLoad()
    {
        foreach(var data in character_sprite)
        {
            if(data.GetFlagSetUpLoading() == false)
            {
                return false;
            }
        }
        return true;
    }

    public void ChangeCharacterPicture(Character_ID id)
    {
        character_image.front_image.sprite = character_image.back_image.sprite = character_sprite[(int)id].GetAddressableData();
    }

    public void StartMove(bool is_up)
    {
        int add_num = move_add;
        if (is_up == true) add_num *= -1;

        foreach(var chara in character_name_array)
        {
            chara.move_pos.start_pos = chara.rect_transform.anchoredPosition;
            chara.move_pos.end_pos = chara.move_pos.start_pos;
            chara.move_pos.end_pos.y += move_add;
        }
    }

    public bool UpdateMove(float lerp)
    {
        if (lerp >= 1.0f)
        {
            for (int count = 0; count < character_name_array.Length;)
            {
                character_name_array[count].rect_transform.anchoredPosition = Vector2.Lerp(character_name_array[count].move_pos.start_pos, character_name_array[count].move_pos.end_pos, 1.0f);
                if (character_name_array[count].rect_transform.anchoredPosition.y < min_pos)
                {
                    character_name_array[count].rect_transform.anchoredPosition = new Vector2(character_name_array[count].rect_transform.anchoredPosition.x,max_pos);
                }
                else if (character_name_array[count].rect_transform.anchoredPosition.y > max_pos)
                {
                    character_name_array[count].rect_transform.anchoredPosition = new Vector2(character_name_array[count].rect_transform.anchoredPosition.x,min_pos);
                }
                ++count;
            }
            return true;
        }
        else
        {
            for (int count = 0; count < (int)character_name_array.Length;)
            {
                character_name_array[count].rect_transform.anchoredPosition = Vector2.Lerp(character_name_array[count].move_pos.start_pos, character_name_array[count].move_pos.end_pos, lerp);
                ++count;
            }
        }

        return false;
    }

    public void PlayAnim(Character_Favo_Anim_ID anim_id, bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Character_Favo_Anim_ID.StartUP || id_num >= (int)Character_Favo_Anim_ID.MAX) return;

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", -1.0f);
        else animator.SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", 1.0f);
        }
        animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }

    public override void Release()
    {
        foreach(var data in character_sprite)
        {
            data.OnAutoRelease();
        }
        character_sprite.Clear();
        character_sprite = null;
        animator = null;
        character_name_array = null;
    }
}

public class CharacterUpImage
{
    public Image front_image;
    public Image back_image;
}

public class CharacterName
{
    public RectTransform rect_transform;
    public TextMeshProUGUI text_mesh;

    public class MovePos
    {
        public Vector2 start_pos;
        public Vector2 end_pos;
    }

    public MovePos move_pos = new MovePos();
}
