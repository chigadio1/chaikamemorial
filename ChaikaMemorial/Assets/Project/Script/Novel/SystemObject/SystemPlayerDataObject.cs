using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public enum Player_Data_UI_Anim_ID
{
    NONE = -1,
    StartUP = 8161,
    MAX
}

public enum Player_Data_Select_UI_Anim_ID
{
    NONE = -1,
    StartUP = 8162,
    MAX
}

public class ContentText
{
    public TextMeshProUGUI name_text;
    public TextMeshProUGUI date_time_text;
}

public class SystemPlayerDataObject : BaseSystemNovelObject
{
    AnimatorUI<Player_Data_UI_Anim_ID> player_data_anim = new AnimatorUI<Player_Data_UI_Anim_ID>();
    public AnimatorUI<Player_Data_UI_Anim_ID> PlayerDataAnima { get { return player_data_anim; } }
    AnimatorUI<Player_Data_Select_UI_Anim_ID> player_data_select_anim = new AnimatorUI<Player_Data_Select_UI_Anim_ID>();
    public AnimatorUI<Player_Data_Select_UI_Anim_ID> PlayerDataSelectAnima { get { return player_data_select_anim; } }

    Image back_ground;
    Image select_bacl_ground;

    ContentText[] content_text_array = new ContentText[5];

    TextMeshProUGUI content_text;

    public void SaveDataViewFix()
    {
        for(int count = 0; count < content_text_array.Length;)
        {
            if(PlayerCore.Instance.IsSaveData(count + 1))
            {
                var data = PlayerCore.Instance.GetLoadData(count + 1);
                if(data != null)
                {
                    var name = data.GetPlayerStatusData.PlayerInputData.SurnName + " " + data.GetPlayerStatusData.PlayerInputData.Name;
                    var year_mont = PlayerCore.Instance.EventCalenderCoreData.GetNouTurnYearMonthID(data.GetPlayerStatusData.GetTurn);
                    var day = PlayerCore.Instance.EventCalenderCoreData.GetNowTurnToDayData(data.GetPlayerStatusData.GetTurn);
                    var date_text = $"{EventYearData.start_year + (int)year_mont.Item1}/{(int)(year_mont.Item2 + 1):00}/{day.GetDay:00}";
                    if(content_text_array[count] != null)
                    {
                        content_text_array[count].name_text.text = name;
                        content_text_array[count].date_time_text.text = date_text;
                    }
                }
            }
            ++count;
        }
    }
    public override void BaseInit()
    {
        Animator[] animators = gameObject.GetComponentsInChildren<Animator>();

        foreach(var anim in animators)
        {
            if(anim.gameObject.name == "SystemData")
            {
                player_data_anim.BeginLoad(anim);
            }
            else if(anim.gameObject.name == "Select")
            {
                player_data_select_anim.BeginLoad(anim);
            }
        }

        var images = gameObject.GetComponentsInChildren<Image>();
        foreach(var image in images)
        {
            if(image.gameObject.name == "BackGround")
            {
                back_ground = image;
            }
            else if(image.gameObject.name == "SelectBackGround")
            {
                select_bacl_ground = image;
            }
        }

        Button[] content_button_array = new Button[5];

        var buttons = gameObject.GetComponentsInChildren<Button>();

        foreach(var button in buttons)
        {
            if(button.gameObject.name == "YES")
            {
                button.onClick.AddListener(() =>
                {
                    if (PlayerCore.Instance.is_button_push || PlayerCore.Instance.is_button_cancel_push) return;
                    PlayerCore.Instance.is_button_push = true;
                });
            }
            else if(button.gameObject.name == "NO")
            {
                button.onClick.AddListener(() =>
                {
                    if (PlayerCore.Instance.is_button_push || PlayerCore.Instance.is_button_cancel_push) return;
                    PlayerCore.Instance.is_button_cancel_push = true;
                });
            }
            else if(button.gameObject.name == "Close Button")
            {
                button.onClick.AddListener(() =>
                {
                    if (PlayerCore.Instance.is_button_push || PlayerCore.Instance.is_button_cancel_push) return;
                    PlayerCore.Instance.is_button_cancel_push = true;
                    PlayerCore.Instance.player_save_id = 0;
                });
            }
            else if(button.gameObject.name.IndexOf("SaveData") >= 0)
            {
                // 正規表現パターンを定義
                string pattern = @"SaveData(\d+)";

                // 正規表現に一致する部分を取得
                Match match = Regex.Match(button.gameObject.name, pattern);

                // グループ1の値を取得してintに変換
                if (match.Success)
                {
                    string numberString = match.Groups[1].Value;
                    int number = int.Parse(numberString);

                    button.onClick.AddListener(() =>
                    {
                        if (PlayerCore.Instance.is_button_push || PlayerCore.Instance.is_button_cancel_push) return;
                        PlayerCore.Instance.player_save_id = number;
                        PlayerCore.Instance.is_button_push = true;
                    });


                    var texts = button.gameObject.GetComponentsInChildren<TextMeshProUGUI>();
                    ContentText contentText = new ContentText();
                    foreach (var text in texts)
                    {
                        if(text.gameObject.name == "Name")
                        {
                            contentText.name_text = text;
                        }
                        else if(text.gameObject.name == "DateTime")
                        {
                            contentText.date_time_text = text;
                        }

                    }
                    content_text_array[number - 1] = contentText;

                    content_button_array[number - 1] = button;

                }
            }
        }

        content_text =  gameObject.GetComponentsInChildren<TextMeshProUGUI>().Where(a => a.gameObject.name == "ConfigText").First();

        SaveDataViewFix();
    }

    public void SetTextConfig(string text)
    {
        if (content_text == null) return;
        content_text.text = text;
    }
}
