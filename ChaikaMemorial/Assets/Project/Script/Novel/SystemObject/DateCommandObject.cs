using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum Date_Commnad_Anim_ID
{
    NONE = -1,
    StartUP = 8572,
    MAX
}

public enum Date_Spot_Commnad_Anim_ID
{
    NONE = -1,
    StartUP = 8573,
    MAX
}

public class DateCommandObject : BaseSystemNovelObject
{
    /// <summary>
    /// アニメーター
    /// </summary>
    Animator animator;

    Animator date_spot_animator;
    /// <summary>
    /// コンテントオブジェクト
    /// </summary>
    GridLayoutGroup content;

    AddressableData<GameObject> button_obj;

    AddressableData<Sprite>[] character_icon ;
    private bool inversion_flag;
    private bool change_flag;

    /// <summary>
    /// スクロール
    /// </summary>
    ScrollRect scrollbar;

    public bool IsLoad()
    {
        bool res = true;
        res &= button_obj.GetFlagSetUpLoading();
        foreach(var icon in character_icon)
        {
            res &= icon.GetFlagSetUpLoading();
        }
        return res;
    }

    /// <summary>
    /// キャラアイコン
    /// </summary>
    private static readonly int[] character_icon_id_array = new int[(int)Character_ID.MAX]
    {
        1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251,1251
    };

    /// <summary>
    /// 初期ボタン
    /// </summary>
    private float[] start_scale_button_array = new float[(int)Character_ID.MAX];

    /// <summary>
    /// ボタン
    /// </summary>
    private RectTransform[] button_array = new RectTransform[(int)Character_ID.MAX];

    private bool is_button_animatio_end = false;
    private bool is_back_button_animatio = false;

    /// <summary>
    /// デートスポットタイトル
    /// </summary>
    TextMeshProUGUI date_spot_text;
    public override void BaseInit()
    {
        
        button_obj = new AddressableData<GameObject>();
        character_icon = new AddressableData<Sprite>[(int)Character_ID.MAX];

        var child_anim = gameObject.GetComponentsInChildren<Animator>();
        foreach(var child in child_anim)
        {
            if(child.gameObject.name == "DateSpot")
            {
                date_spot_animator = child;
            }
            else if (child.gameObject.name == "Mask")
            {
                animator = child;
            }
        }

        content = gameObject.GetComponentInChildren<GridLayoutGroup>();
        scrollbar = gameObject.GetComponentInChildren<ScrollRect>();
        button_obj.LoadStart("Assets/Project/Assets/Object/UI/CharaButton.prefab");

        for(int i = 0; i < (int)Character_ID.MAX;)
        {
            if(character_icon[i] != null)
            {
                character_icon[i].OnAutoRelease();
                character_icon[i] = null;
            }
            character_icon[i] = new AddressableData<Sprite>();
            character_icon[i].LoadStart($"Assets/Project/image/Icon/{character_icon_id_array[i]}.png");
            ++i;
        }

        date_spot_text = date_spot_animator.GetComponentsInChildren<TextMeshProUGUI>().Where(text => text.gameObject.name.Contains("TitletextDateSpot")).First();
    }

    public override void SubInstance()
    {
        if (content == null) return;
        if (IsLoad() == false) return;

        var content_rect = content.GetComponent<RectTransform>();
        for(int i = 0; i < (int)Character_ID.MAX;)
        {
            if(PlayerCore.Instance.GetPlayerCoreData.GetCharacterUnlockData.GetFlag((Character_ID)i) == false)
            {
                ++i;
                continue;
            }
            var child = GameObject.Instantiate(button_obj.GetAddressableData());
            child.transform.SetParent(content.transform,false);
            var sprite = child.GetComponent<Image>();
            if(sprite == null)
            {
                ++i;
                continue;
            }
            sprite.sprite = character_icon[i].GetAddressableData();
            var button = child.GetComponent<Button>();
            if (button == null)
            {
                ++i;
                continue;
            }
            int chara = i;
            button_array[i] = button.GetComponent<RectTransform>();
            button_array[i].localScale = Vector3.zero;
            button.onClick.AddListener(() =>
            {
                if (gameUiCore.Instance.is_button_push) return;
                gameUiCore.Instance.is_button_push = true;
                gameUiCore.Instance.SetSelectCharacterID((Character_ID)chara);
            });
            ++i;
        }
    }

    public void Update()
    {
        if (scrollbar != null && is_button_animatio_end == false)
        {
            scrollbar.verticalNormalizedPosition = 1.0f;
        }
    }

    public bool ButtonAnimation()
    {
        bool res = true;
        if (button_array == null) return true;

        for(int i = 0;i < button_array.Length;)
        {
            if(button_array[i] == null)
            {
                ++i;
                continue;
            }
            start_scale_button_array[i] += Time.deltaTime * 2.0f * (is_back_button_animatio == true ? -1.0f : 1.0f);
            if(start_scale_button_array[i] >= 1.0f)
            {
                start_scale_button_array[i] = 1.0f;
            }
            float value_param = start_scale_button_array[i];
            value_param = Mathf.Clamp(value_param, 0.0f, 1.0f);
            if(is_back_button_animatio == false)res &= start_scale_button_array[i] >= 1.0f ? true : false;
            else res &= start_scale_button_array[i] <= 0.0f ? true : false;
            button_array[i].localScale = new Vector3(value_param,value_param, 0.0f);
            ++i;
        }
        if(res == true)
        {
            is_button_animatio_end = true;
        }
        return res;
    }

    public void ButtonStartAnimation()
    {
        start_scale_button_array[0] = 0.0f;
        for(int i = 1; i < (int)Character_ID.MAX;)
        {
            start_scale_button_array[i] = start_scale_button_array[i - 1] - 0.05f;
            ++i;
        }
    }

    public void ButtonFinishAnimation()
    {
        start_scale_button_array[(int)Character_ID.MAX - 1] = 1.0f;
        for (int i = (int)Character_ID.MAX - 2; i > 0;)
        {
            start_scale_button_array[i] = start_scale_button_array[i + 1] + 0.05f;
            --i;
        }
        is_back_button_animatio = true;
    }

    public override void Release()
    {
        if(button_obj != null)
        {
            button_obj.OnAutoRelease();
            button_obj = null;
        }
        if (character_icon != null)
        {
            foreach (var data in character_icon)
            {
                data.OnAutoRelease();
            }
        }
        button_array = null;
        start_scale_button_array = null;
        character_icon = null;
    }

    public void OnDestroy()
    {
        Release();
    }

    public void PlayAnim(Date_Commnad_Anim_ID anim_id)
    {
        PlayAnim(anim_id, 1.0f, false);
    }

    public void PlayAnim(Date_Commnad_Anim_ID anim_id, bool inversion = false)
    {
        PlayAnim(anim_id, 1.0f, inversion);
    }

    public void PlayAnim(Date_Commnad_Anim_ID anim_id,float time = 1.0f, bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Date_Commnad_Anim_ID.StartUP || id_num >= (int)Date_Commnad_Anim_ID.MAX) return;

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", time * -1.0f);
        else animator.SetFloat("Speed", time);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", time * -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", time);
        }
        animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }

    public void PlayAnim(Date_Spot_Commnad_Anim_ID anim_id)
    {
        PlayAnim(anim_id, 1.0f, false);
    }

    public void PlayAnim(Date_Spot_Commnad_Anim_ID anim_id, bool inversion = false)
    {
        PlayAnim(anim_id, 1.0f, inversion);
    }

    public void PlayAnim(Date_Spot_Commnad_Anim_ID anim_id, float time = 1.0f,bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Date_Spot_Commnad_Anim_ID.StartUP || id_num >= (int)Date_Spot_Commnad_Anim_ID.MAX) return;

        if (date_spot_animator == null) return;
        change_flag = true;
        if (inversion) date_spot_animator.SetFloat("Speed", time * -1.0f);
        else date_spot_animator.SetFloat("Speed", time);

        float normalize_time = 0.0f;
        if (inversion)
        {
            date_spot_animator.SetFloat("Speed",time * -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            date_spot_animator.SetFloat("Speed", time);
        }
        date_spot_animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnimDateSpot()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = date_spot_animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }

    public void SetDateSpotTextTitle(string text)
    {
        if (date_spot_text == null) return;
        date_spot_text.text = text;
    }
}

