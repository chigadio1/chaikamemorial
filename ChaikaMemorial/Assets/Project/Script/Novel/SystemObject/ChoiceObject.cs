using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public enum Choice_Commnad_Anim_ID
{
    NONE = -1,
    IDLE = 8151,
    StartUP = 8152,
    MAX
}

public class ChoiceObject : BaseSystemNovelObject
{
    /// <summary>
    /// アニメーター
    /// </summary>
    Animator animator;

    private List<SelectButtonChoice> list_select_button = new List<SelectButtonChoice>();
    private bool change_flag;
    private bool inversion_flag;
    private Image fade_image;

    private int select_index = -1;
    public int GetSelectIndex() { return select_index; }
    public void SetSelectIndex(int value)
    {
        select_index = value;
        select_index = Mathf.Clamp(select_index, 0, list_select_button.Count - 1);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    public void SetFadeAlpha(float value_alpha)
    {
        if (fade_image == null) return;
        Color color = fade_image.color;
        color.a = value_alpha;
        fade_image.color = color;
    }

    public bool IsFinish()
    {
        return select_index != -1 && select_index < list_select_button.Count;
    }

    public bool ButtonStartAnimation(float lerp)
    {
        foreach(var data in list_select_button)
        {
            float set_scale_y = Mathf.Lerp(0.0f, 1.0f, lerp);
            data.SetScaleY(set_scale_y);
        }

        return lerp >= 1.0f;
    }

    public bool ButtonFinishAnimation(float lerp)
    {
        for(int i = 0; i < list_select_button.Count;)
        {
            if (i == select_index)
            {
                float set_pos_y = 0.0f;
                if (list_select_button.Count % 2 == 0)
                {
                    float startY = 0f;
                    float spacing = 50f;
                    set_pos_y = (startY + (i % 2 == 0 ? spacing : -spacing) * (i / 2));
                }
                else
                {
                    float startY = 400f;
                    float spacing = -200f;

                    set_pos_y = (startY + spacing * i);
                }
                list_select_button[i].SetPositionY(Mathf.Lerp(set_pos_y, 0.0f, lerp));
            }
            else
            {
                float set_scale_y = Mathf.Lerp(1.0f, 0.0f, lerp * 1.5f);
                list_select_button[i].SetScaleY(set_scale_y);
            }
            ++i;
        }

        return lerp >= 1.0f;
    }

    public bool ButtonSelectFinishAnimation(float lerp)
    {
        float add_x = -5000.0f;

        float value_add = Mathf.Lerp(0.0f, add_x, lerp);

        for(int i = 0;   i < list_select_button.Count;)
        {
            list_select_button[i].SetAddPositionX(value_add);
            ++i;
        }

        return lerp >= 1.0f;
    }

    public void SelectMove(bool up)
    {
        if(up)
        {
            select_index = select_index - 1;
            if(select_index < 0)
            {
                select_index = list_select_button.Count - 1;
            }
        }
        else
        {
            select_index = select_index + 1;
            select_index = select_index % list_select_button.Count;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //入力していたら、色を変える
        if (select_index != -1)
        {
            for (int i = 0; i < list_select_button.Count;)
            {
                if (i == select_index)
                {
                    list_select_button[i].SetImageColor(Color.yellow);
                }
                else
                {
                    list_select_button[i].SetImageColor(Color.white);
                }
                ++i;
            }
        }
    }

    public override void BaseInit()
    {
        animator = gameObject.GetComponentInChildren<Animator>();
        fade_image = gameObject.GetComponentsInChildren<Image>().Where(a => a.gameObject.name == "BackGround").First();

    }

    public void BeginInitButton(string[] text_strings)
    {
        var button = gameObject.GetComponentsInChildren<Image>().Where(x => x.gameObject.name == "SelectButton").First();

        var parent = gameObject.GetComponentsInChildren<RectTransform>().Where(x => x.gameObject.name == "ChoiceSelects").First();
        if (button != null && parent != null)
        {
            SelectButtonChoice add = new SelectButtonChoice();
            add.Load(button.gameObject);
            add.SetText(text_strings[0]);
            list_select_button.Add(add);

            for (int i = 0; i < text_strings.Length - 1;)
            {
                GameObject button_obj = GameObject.Instantiate(button.gameObject);
                button_obj.transform.SetParent(parent, false);

                SelectButtonChoice add_count = new SelectButtonChoice();
                add_count.Load(button_obj.gameObject);
                add_count.SetText(text_strings[i + 1]);
                list_select_button.Add(add_count);
                ++i;
            }
        }

        if(list_select_button.Count % 2 == 0)
        {
            float startY = 0f;
            float spacing = 50f;

            for (int i = 0; i < list_select_button.Count; i++)
            {
                float yPos = (startY + (i % 2 == 0 ? spacing : -spacing) * (i / 2));
                list_select_button[i].SetPositionY(yPos);
            }
        }
        else
        {
            float startY = 400f;
            float spacing = -200f;

            for (int i = 0; i < list_select_button.Count; i++)
            {
                float yPos = (startY + spacing * i);
                list_select_button[i].SetPositionY(yPos);
            }
        }

        for(int i = 0; i < list_select_button.Count;)
        {
            int index = i;
            list_select_button[i].button.onClick.AddListener(() =>
            {
                if (gameUiCore.Instance.is_button_push) return;
                gameUiCore.Instance.is_button_push = true;
                gameUiCore.Instance.SettingCanvas.ChoiceObject?.SetSelectIndex(index);
            });
            
            ++i;
        }
    }

    public void OnDestroy()
    {
        if(list_select_button != null)
        {
            for(int count = 0; count < list_select_button.Count;)
            {
                GameObject.Destroy(list_select_button[count].rectTransform.gameObject);
                list_select_button[count] = null;
                ++count;
            }
            list_select_button.Clear();
        }
    }

    public void PlayAnim(Choice_Commnad_Anim_ID anim_id, float time = 1.0f, bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Choice_Commnad_Anim_ID.IDLE || id_num >= (int)Choice_Commnad_Anim_ID.MAX) return;

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", time * -1.0f);
        else animator.SetFloat("Speed", time);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", time * -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", time);
        }
        animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }

    public bool IsPlayEndAnim()
    {
        if (animator == null) return true;

        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        return stateInfo.normalizedTime >= 1.0f;
    }
}

public class SelectButtonChoice
{
    public TextMeshProUGUI text;
    public Image image;
    public RectTransform rectTransform;
    public Button button;
    public static readonly float start_x = -331.0f;

    public bool Load(GameObject game_object)
    {
        text = game_object.GetComponentInChildren<TextMeshProUGUI>();
        image = game_object.GetComponentsInChildren<Image>().Where(x => x.gameObject.name == "ChpiceButton").First();
        if(image != null)
        {
            button = image.GetComponent<Button>();
        }
        else
        {
            button = game_object.GetComponentsInChildren<Button>().Where(x => x.gameObject.name == "ChpiceButton").First();
        }
        rectTransform = game_object.GetComponent<RectTransform>();

        return text != null && rectTransform != null && image != null && button != null;
    }

    public void SetPositionY(float y)
    {
        if (rectTransform == null) return;
        Vector2 pos = rectTransform.anchoredPosition;
        pos.y = y;
        pos.x = start_x;
        rectTransform.anchoredPosition = pos;
    }

    public void SetAddPositionX(float add_x)
    {
        if (rectTransform == null) return;
        Vector2 pos = rectTransform.anchoredPosition;
        pos.x = start_x + add_x;
        rectTransform.anchoredPosition = pos;
    }

    public void SetScaleY(float y)
    {
        if(rectTransform == null) return;
        Vector2 scale = rectTransform.localScale;
        scale.y = y;
        rectTransform.localScale = scale;
    }

    public void SetImageColor(Color color)
    {
        if(image == null) return;
        image.color = color;
    }

    public void SetText(string value_text)
    {
        if(value_text == null) return;
        text.text = value_text;
    }

}

