using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSystemNovelObject : MonoBehaviour
{
    public virtual void BaseInit()
    {

    }

    public virtual void Release()
    {

    }

    public virtual void SubInstance()
    {

    }
}
