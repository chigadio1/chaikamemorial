using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;

public enum Nick_Name_Choice_Anim_ID
{
    NONE = -1,
    StartUP = 8561,
    MAX
}
public class NickNameChoiceObject : BaseSystemNovelObject
{
    /// <summary>
    /// アニメーション
    /// </summary>
    Animator animator;

    /// <summary>
    /// チョイステキスト
    /// </summary>
    TextMeshProUGUI[] choice_array = new TextMeshProUGUI[(int)Character_NickName_ID.MAX];
    private bool change_flag;
    private bool inversion_flag;

    public override void BaseInit()
    {
        animator = gameObject.GetComponent<Animator>();

        var childs = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        foreach(var child in childs)
        {
            int count = -1;
            var parent = child.rectTransform.parent.gameObject;
            if (parent == null) continue;

            string pattern = @"\d+"; // 数字の正規表現パターン

            // 正規表現に一致する部分を検索
            Match match = Regex.Match(parent.gameObject.name, pattern);

            // 一致した部分があるか確認
            if (match.Success)
            {
                // 一致した部分を取得
                int extractedNumber = int.Parse(match.Value);
                count = extractedNumber;
                count -= 1;

            }
            if (count <= -1) continue;
            choice_array[count] = child;
        }
    }

    public void SetChoiceText(CharacterNickName name_data)
    {
        if (name_data == null) return;

        for(int i = 0; i < (int)Character_NickName_ID.MAX;)
        {
            Character_NickName_ID id = (Character_NickName_ID)i;

            var value_name = name_data.GetNickName(id);
            if (choice_array[i] == null) continue;
            choice_array[i].text = value_name;

            ++i;
        }
    }

    public void PlayAnim(Nick_Name_Choice_Anim_ID anim_id, bool inversion = false)
    {
        int id_num = (int)anim_id;
        if (id_num < (int)Nick_Name_Choice_Anim_ID.StartUP || id_num >= (int)Nick_Name_Choice_Anim_ID.MAX) return;

        if (animator == null) return;
        change_flag = true;
        if (inversion) animator.SetFloat("Speed", -1.0f);
        else animator.SetFloat("Speed", 1.0f);

        float normalize_time = 0.0f;
        if (inversion)
        {
            animator.SetFloat("Speed", -1.0f);
            normalize_time = 1.0f;
            inversion_flag = true;

        }

        else
        {
            inversion_flag = false;
            animator.SetFloat("Speed", 1.0f);
        }
        animator.Play($"{(int)anim_id:0000}", 0, normalize_time);
    }

    public bool IsPlayAnim()
    {

        if (animator == null) return true;
        if (change_flag == true)
        {
            change_flag = false;
            return false;
        }
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        if (!inversion_flag) return stateInfo.normalizedTime >= 1.0f;
        else return stateInfo.normalizedTime <= 0.0f;
    }
}
